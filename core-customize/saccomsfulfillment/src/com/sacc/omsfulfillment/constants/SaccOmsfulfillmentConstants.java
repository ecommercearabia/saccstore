/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.omsfulfillment.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class SaccOmsfulfillmentConstants extends GeneratedSaccOmsfulfillmentConstants
{
	public static final String EXTENSIONNAME = "saccomsfulfillment";
	
	private SaccOmsfulfillmentConstants()
	{
		//empty
	}
	
	
}
