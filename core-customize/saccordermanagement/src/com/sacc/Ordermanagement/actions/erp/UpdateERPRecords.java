package com.sacc.Ordermanagement.actions.erp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sacc.Ordermanagement.actions.erp.AbstractERPConsignmentAction.Transition;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

public class UpdateERPRecords extends AbstractAction<ERPConsignmentProcessModel>
{

	private static final Logger LOG = Logger.getLogger(UpdateERPRecords.class);
	
	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;
	
	@Override
	public String execute(ERPConsignmentProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Updating ERP records ...");
		if (process == null || process.getErpConsignment() == null || process.getErpConsignment().getOrder() == null)
		{
			LOG.error("Cannot proceed with the ERP records update");
			return Transition.ERROR.toString();
		}
		final ConsignmentModel consignment = process.getErpConsignment();
		final OrderModel order = (OrderModel) consignment.getOrder();
		
		final List<SaccERPOrderOperationRecordModel> records = saccERPOrderOperationService
				.getAllOrderRecords(Arrays.asList(ERPWebServiceType.PICK_SALES_ORDER,
						ERPWebServiceType.PACK_SALES_ORDER,
						ERPWebServiceType.SHIP_SALES_ORDER), order.getCode(), null, null, Boolean.FALSE);
		
		if (CollectionUtils.isNotEmpty(records))
		{
			records.forEach(record -> updateRecord(record));
		}
		LOG.info("Updating the ERP records for order " + order.getCode() + " has been successfully done"); 
		return Transition.OK.toString();
	}

	private void updateRecord(final SaccERPOrderOperationRecordModel record)
	{
		record.setDone(true);
		modelService.save(record);
	}
	
	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}
}
