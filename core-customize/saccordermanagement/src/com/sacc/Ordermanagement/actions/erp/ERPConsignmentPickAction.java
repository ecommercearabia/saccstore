/**
 *
 */
package com.sacc.Ordermanagement.actions.erp;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.Ordermanagement.actions.erp.AbstractERPConsignmentAction.Transition;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;

import schemas.dynamics.microsoft.page.sales_order_web_services.OnlineStatus;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;


/**
 * @author monzer
 *
 */
public class ERPConsignmentPickAction extends AbstractERPConsignmentAction
{
	private static final Logger LOG = Logger.getLogger(ERPConsignmentPickAction.class);

	@Override
	public String execute(final ERPConsignmentProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Picking ERP Consignment");
		if (process == null || process.getErpConsignment() == null || process.getErpConsignment().getOrder() == null)
		{
			LOG.error("Cannot proceed with the ERP Pick Consignment");
			return Transition.ERROR.toString();
		}
		final ConsignmentModel consignment = process.getErpConsignment();
		final OrderModel order = (OrderModel) consignment.getOrder();

		if (StringUtils.isBlank(order.getSaccERPKey()))
		{
			LOG.info("creating the sales order for order " + order.getCode());
			getSaccERPService().createSalesOrderAction(order);
		}

		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = getSalesOrderWebServices().read(order.getSaccERPKey());
		}
		catch (final SaccERPWebServiceException e)
		{
			LOG.error("Error during reading the sales order from the ERP", e);
		}
		if (salesOrder == null)
		{
			LOG.error("Failure to reading the sales order from the ERP, the sales order might not be created successfully!");
			return Transition.NOK.toString();
		}

		if (OnlineStatus.SHIPPED.equals(salesOrder.getOnlineStatus())
				|| OnlineStatus.DELIVERED.equals(salesOrder.getOnlineStatus()))
		{
			LOG.info("Sales Order " + order.getCode() + " is already " + salesOrder.getOnlineStatus() + " on the ERP system");
			return Transition.COMPLETE.toString();
		}

		if (process.isPicked())
		{
			LOG.info("Sales Order " + order.getCode() + " is already " + salesOrder.getOnlineStatus() + " on the ERP system");
			return Transition.OK.toString();
		}

		if(consignment.isPicked()) {
   		LOG.info("Updating the ERP consignment PICK");
   		boolean updated = getSaccERPService().pickSaleOrder(order);
   		process.setPicked(updated);
   		updateProcess(process);
		}
		
		return Transition.OK.toString();
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
