/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.Ordermanagement.actions.erp;

import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import com.sacc.Ordermanagement.constants.SaccOrdermanagementConstants;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Update the consignment process to done and notify the corresponding order process that it is completed.
 */
public class ERPConsignmentProcessEndAction extends AbstractProceduralAction<ERPConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ERPConsignmentProcessEndAction.class);

	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	@Override
	public void executeAction(final ERPConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		process.setDone(true);
		save(process);
		LOG.debug("Process: {} wrote DONE marker", process.getCode() );

		final String eventId =
				process.getErpConsignment().getCode() + "_" + SaccOrdermanagementConstants.ERP_CONSIGNMENT;

		final BusinessProcessEvent event = BusinessProcessEvent.builder(eventId).withChoice("consignmentProcessEnded").build();
		getBusinessProcessService().triggerEvent(event);

		LOG.debug("Process: {} fired event {}", process.getCode(), SaccOrdermanagementConstants.ERP_CONSIGNMENT);
	}
}
