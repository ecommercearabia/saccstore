/**
 *
 */
package com.sacc.Ordermanagement.actions.erp;

import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import com.sacc.saccerpclientservices.client.service.SaccERPSalesOrderWebService;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;


/**
 * @author monzer
 *
 */
public abstract class AbstractERPConsignmentAction extends AbstractAction<ERPConsignmentProcessModel>
{

	public enum Transition
	{
		OK, COMPLETE, NOK, ERROR;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	@Resource(name = "saccERPSalesOrderWebService")
	private SaccERPSalesOrderWebService salesOrderWebServices;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;
	
	@Resource(name="modelService")
	private ModelService modelService;

	protected void updateProcess(ERPConsignmentProcessModel process) {
		modelService.save(process);
	}
	
	/**
	 * @return the salesOrderWebServices
	 */
	public SaccERPSalesOrderWebService getSalesOrderWebServices()
	{
		return salesOrderWebServices;
	}

	/**
	 * @return the saccERPService
	 */
	public SaccERPService getSaccERPService()
	{
		return saccERPService;
	}
	
	public ModelService getModelService()
	{
		return modelService;
	}

}
