/**
 *
 */
package com.sacc.Ordermanagement.actions.erp;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.Ordermanagement.actions.erp.AbstractERPConsignmentAction.Transition;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;
import schemas.dynamics.microsoft.page.sales_order_web_services.OnlineStatus;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;


/**
 * @author monzer
 *
 */
public class ERPConsignmentPackAction extends AbstractERPConsignmentAction
{
	private static final Logger LOG = Logger.getLogger(ERPConsignmentPackAction.class);

	@Override
	public String execute(final ERPConsignmentProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Packing ERP Consignment");
		if (process == null || process.getErpConsignment() == null || process.getErpConsignment().getOrder() == null)
		{
			LOG.error("Cannot proceed with the ERP Pack Consignment");
			return Transition.ERROR.toString();
		}
		final ConsignmentModel consignment = process.getErpConsignment();
		final OrderModel order = (OrderModel) consignment.getOrder();

		//		if(ConsignmentStatus.PICKPACK.equals(consignment.getStatus())) {
		//			LOG.info("Consignment not Packed yet on Hybris");
		//			return Transition.COMPLETE.toString();
		//		}

		if (StringUtils.isBlank(order.getSaccERPKey()))
		{
			LOG.error("Order ERP key is empty which indicated that the sales order has not been created successfully!");
			return Transition.NOK.toString();
		}

		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = getSalesOrderWebServices().read(order.getSaccERPKey());
		}
		catch (final SaccERPWebServiceException e)
		{
			LOG.error("Error during reading the sales order from the ERP", e);
			return Transition.NOK.toString();
		}
		if (salesOrder == null)
		{
			LOG.error("Failure to reading the sales order from the ERP");
			return Transition.NOK.toString();
		}

		if (OnlineStatus.SHIPPED.equals(salesOrder.getOnlineStatus())
				|| OnlineStatus.DELIVERED.equals(salesOrder.getOnlineStatus()))
		{
			LOG.info("Sales Order " + order.getCode() + " is already " + salesOrder.getOnlineStatus() + " on the ERP system");
			return Transition.COMPLETE.toString();
		}

		if (process.isPacked())
		{
			LOG.info("Sales Order " + order.getCode() + " is already " + salesOrder.getOnlineStatus() + " on the ERP system");
			return Transition.OK.toString();
		}

      if(consignment.isPacked()) {
   		LOG.info("Updating the ERP consignment PACK");
   		boolean updated = getSaccERPService().packSaleOrder(order);
   		process.setPacked(updated);
   		updateProcess(process);
      }
      return Transition.OK.toString();
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
