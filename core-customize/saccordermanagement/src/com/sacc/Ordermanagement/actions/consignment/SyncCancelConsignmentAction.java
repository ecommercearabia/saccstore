package com.sacc.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;

import com.sacc.Ordermanagement.events.SendOrderCancelEmailEvent;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SyncCancelConsignmentAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SyncCancelConsignmentAction.class);

	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());		
		
	}
	

}
