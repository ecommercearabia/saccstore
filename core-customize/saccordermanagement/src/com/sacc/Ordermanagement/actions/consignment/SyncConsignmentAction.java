/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.service.SaccERPService;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;


/**
 * Synchronizes a consignment status to SACC ERP System.
 * @author mohammad-abumuhasien
 */
public class SyncConsignmentAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;
	private static final Logger LOG = LoggerFactory.getLogger(SyncConsignmentAction.class);

	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		OrderModel order = (OrderModel) consignment.getOrder();

		switch(consignment.getStatus()) {
			case PICKPACK :
				saccERPService.pickSaleOrder(order);
				break;
			case READY_FOR_SHIPPING:
				saccERPService.packSaleOrder(order);
				break;

		}
	}
}
