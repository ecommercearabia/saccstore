/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.Ordermanagement.actions.returns;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.warehousing.allocation.AllocationService;
import de.hybris.platform.warehousing.constants.WarehousingConstants;
import de.hybris.platform.warehousing.data.sourcing.SourcingResult;
import de.hybris.platform.warehousing.data.sourcing.SourcingResults;
import de.hybris.platform.warehousing.sourcing.SourcingService;
import com.sacc.Ordermanagement.constants.SaccOrdermanagementConstants;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.service.SaccERPService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Locale;

import javax.annotation.Resource;


/**
 * Action node responsible for sourcing the order and allocating the consignments. After the consignments are created,
 * the consignment sub-process is started for every consignment.
 */
public class SyncSalesOrderReturnAction extends AbstractProceduralAction<ReturnProcessModel>
{

	private static final Logger LOGGER = LoggerFactory.getLogger(SyncSalesOrderReturnAction.class);

	@Resource(name="saccERPService")
	private SaccERPService saccERPService;
	
	@Override
	public void executeAction(final ReturnProcessModel process) throws RetryLaterException, Exception
	{
		LOGGER.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		ReturnRequestModel returnRequest = process.getReturnRequest();
		LOGGER.info("Order: {}", process.getReturnRequest().getOrder());
		LOGGER.info("Currency: {}", process.getReturnRequest().getCurrency());
		LOGGER.info("Replacement Order: {}", process.getReturnRequest().getReplacementOrder());
		LOGGER.info("Return Entries: {}", process.getReturnRequest().getReturnEntries().size());
		try{
			saccERPService.createReturnRequest(returnRequest);
		}catch (SaccERPWebServiceException e) {
			LOGGER.error("Could not sync return request due to : {}", e.getStackTrace());
		}
	}
}
