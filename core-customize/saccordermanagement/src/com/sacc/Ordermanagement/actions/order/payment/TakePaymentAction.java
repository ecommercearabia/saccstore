/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.Ordermanagement.actions.order.payment;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(TakePaymentAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final OrderModel order = process.getOrder();
		boolean paymentFailed = false;
		
		if (order.getPaymentMode() == null || "cod".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{
			LOG.info("PaymentMode is {} , Action status is {}",order.getPaymentMode() ,Transition.OK);
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(PaymentStatus.NOTPAID);
			modelService.save(order);
			return Transition.OK;
		}
		LOG.info("The number of PaymentTransactions is :  {}",order.getPaymentTransactions().size());
		if (CollectionUtils.isEmpty(order.getPaymentTransactions()))
		{
			LOG.error("PaymentTransactions are empty,order status is : {}", OrderStatus.PAYMENT_NOT_CAPTURED);
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.refresh(order);
			order.setPaymentStatus(PaymentStatus.NOTPAID);
			modelService.save(order);
			return Transition.NOK;
		}
		PaymentTransactionEntryModel entry =null;
		for (final PaymentTransactionModel txn : order.getPaymentTransactions())
		{
			modelService.refresh(txn);
			entry = getPaymentTransactionEntry(txn,PaymentTransactionType.CAPTURE);
			if (entry == null)
			{
				LOG.error("CAPTURE entry not found! ,paymentFailed is set to true.");
				paymentFailed = true;
				continue;
			}
			else {
				LOG.info("CAPTURE entry found! ,paymentFailed is set to false.");
				paymentFailed = false;
				break;
			}
		}

		if (paymentFailed)
		{
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			return Transition.NOK;
		}
		else
		{
			setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
			return Transition.OK;
		}
	}
	
	private PaymentTransactionEntryModel getPaymentTransactionEntry(final PaymentTransactionModel ptModel , PaymentTransactionType paymentTransactionType )
	{
		if (CollectionUtils.isEmpty(ptModel.getEntries())||paymentTransactionType==null)
		{
			return null;
		}
		for (PaymentTransactionEntryModel pteModel : ptModel.getEntries())
		{
			if(paymentTransactionType.equals(pteModel.getType())	&& TransactionStatus.ACCEPTED.name().equalsIgnoreCase(pteModel.getTransactionStatus()))
			{
				return pteModel;
			}
		} 

		return null;
	}
}
