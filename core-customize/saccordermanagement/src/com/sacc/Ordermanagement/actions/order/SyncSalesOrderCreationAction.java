/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.Ordermanagement.actions.order;

import java.util.List;

import javax.annotation.Resource;
import javax.annotation.Resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.jalo.SaccERPClientSalesOrderWebService;
import com.sacc.saccerpclientservices.model.SaccERPClientCustomerWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientPaymentWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientSalesOrderWebServiceModel;
import com.sacc.saccerpclientservices.service.SaccERPService;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;
import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebService;
import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebService;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;


/**
 * Action node responsible for sourcing the order and allocating the consignments. After the consignments are created,
 * the consignment sub-process is started for every consignment.
 */
public class SyncSalesOrderCreationAction extends AbstractProceduralAction<OrderProcessModel>
{
	@Resource(name="saccERPService")
	private SaccERPService saccERPService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SyncSalesOrderCreationAction.class);

	@Override
	public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		LOGGER.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final OrderModel order = process.getOrder();
		getSaccERPService().createSalesOrderAction(order);

	}
	
	protected SaccERPService getSaccERPService()
	{
		return saccERPService;
	}

	protected void setSaccERPService(SaccERPService saccERPService)
	{
		this.saccERPService = saccERPService;
	}
}
