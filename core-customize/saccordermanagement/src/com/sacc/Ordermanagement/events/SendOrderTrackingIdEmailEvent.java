package com.sacc.Ordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;



/**
 *@author abu-muhasien
 */
public class SendOrderTrackingIdEmailEvent extends ConsignmentProcessingEvent
{

	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process the process
	 */
	public SendOrderTrackingIdEmailEvent(ConsignmentProcessModel process)
	{
		
		super(process);
	}
	

	
}