/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwebhooksnotificationservice.saee.service;

import com.sacc.saccfulfillment.saee.exceptions.SaeeException;
import com.sacc.saccwebhooksnotificationservice.saee.entry.NotificationRequest;


/**
 *
 */
public interface SaeeNotificationService
{
	void notifyConsignment(final String baseStoreId, final String secretKey, final NotificationRequest notificationRequest)
			throws SaeeException;

}
