/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwebhooksnotificationservice.saee.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.exception.enums.FulfillmentExceptionType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SaeeFulfillmentProviderModel;
import com.sacc.saccfulfillment.saee.exceptions.SaeeException;
import com.sacc.saccfulfillment.service.CustomConsignmentService;
import com.sacc.saccfulfillment.service.SaeeUpdateNotificationRecordService;
import com.sacc.saccwebhooksnotificationservice.saee.entry.NotificationRequest;
import com.sacc.saccwebhooksnotificationservice.saee.service.SaeeNotificationService;


/**
 * The Class DefaultSaeeNotificationService.
 *
 * @author monzer
 */
public class DefaultSaeeNotificationService implements SaeeNotificationService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSaeeNotificationService.class);

	/** The fulfillment provider context. */
	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/** The custom consignment service. */
	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	/** The saee webhook status map. */
	@Resource(name = "saeeWebhookStatusMap")
	private Map<String, ConsignmentStatus> saeeWebhookStatusMap;

	/** The saee update notification record service. */
	@Resource(name = "saeeUpdateNotificationRecordService")
	private SaeeUpdateNotificationRecordService saeeUpdateNotificationRecordService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;


	/**
	 * Notify consignment.
	 *
	 * @param trackingId
	 *           the tracking id
	 * @param secretKey
	 *           the secret key
	 * @param notificationStatus
	 *           the notification status
	 * @throws SaeeException
	 *            the saee exception
	 */
	@Override
	public void notifyConsignment(final String baseStoreId, final String secretKey, final NotificationRequest notificationRequest)
			throws SaeeException
	{
		if (notificationRequest == null)
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"Notification Request body was null", null, null, false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Notification Request body was null", null);
		}
		if (notificationRequest.getMessage() == null)
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"Mesage Object in Notification Request body was null", null, null, false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Mesage Object in Notification Request body was null",
					null);
		}
		if (StringUtils.isBlank(baseStoreId))
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"baseStoreUid is null", notificationRequest.getMessage().getWaybill(),
					notificationRequest.getStatus(), false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "baseStoreUid is null", null);
		}
		if (StringUtils.isBlank(secretKey))
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"secret key is null", notificationRequest.getMessage().getWaybill(), notificationRequest.getStatus(), false,
					Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "secret key is null", null);
		}
		if (isWaybillsContainsNull(notificationRequest.getMessage().getWaybills()))
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"trackingId is null", notificationRequest.getMessage().getWaybill(), notificationRequest.getStatus(), false,
					Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "trackingId is null", null);
		}
		final String notificationStatus = notificationRequest.getStatus();
		if (StringUtils.isBlank(notificationStatus))
		{
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"notificationStatus is null", notificationRequest.getMessage().getWaybill(), notificationRequest.getStatus(),
					false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "notificationStatus is null", null);
		}

		final String trackingIds = getAllWaybillsAsString(notificationRequest.getMessage().getWaybills());

		// BaseStore by uid
		final BaseStoreModel baseStoreForUid = baseStoreService.getBaseStoreForUid(baseStoreId);
		if (baseStoreForUid == null)
		{
			LOG.error("DefaultSaeeNotificationService : BaseStore does not exist for uid of : {}" + baseStoreId);
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"BaseStore does not exist for uid of : {}" + baseStoreId, trackingIds, notificationStatus, false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST,
					"Base store with uid '" + baseStoreId + "' not found!", null);
		}

		// Saee fulfillment provider
		final Optional<FulfillmentProviderModel> provider = fulfillmentProviderContext.getProvider(baseStoreForUid,
				SaeeFulfillmentProviderModel.class);
		if (provider.isEmpty())
		{
			LOG.error("DefaultSaeeNotificationService : Saee Fulfillment Provider does not exists");
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"Saee Fulfillment Provider is not avaialbe",
					trackingIds, notificationStatus, false, null);
			throw new SaeeException(FulfillmentExceptionType.PROVIDER_NOT_SUPPORTED, "Saee Fulfillment Provider is not avaialbe",
					null);
		}

		// authorizing the webhook request
		final SaeeFulfillmentProviderModel saeeProvider = (SaeeFulfillmentProviderModel) provider.get();
		if (StringUtils.isBlank(saeeProvider.getSecretKey()))
		{
			LOG.error("DefaultSaeeNotificationService : Saee provider secret key is null, add it to the provider");
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"Saee provider secret key is null, add it to the provider", trackingIds, notificationStatus, false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.PROVIDER_NOT_SUPPORTED,
					"Saee provider secret key is null, add it to the provider", null);
		}
		if (!validateKey(secretKey, saeeProvider.getSecretKey()))
		{
			LOG.error("DefaultSaeeNotificationService : Bad credentials : {}", secretKey);
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
					"Bad credentials : {}" + secretKey, trackingIds,
					notificationStatus, false, Boolean.FALSE);
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Bad credentials for : " + secretKey, null);
		}
		final List<String> allWaybills = notificationRequest.getMessage().getWaybills();
		for (final String trackingId : allWaybills)
		{
			// Consignment by tracking id
			final Optional<ConsignmentModel> byTackingId = customConsignmentService.findByTackingId(trackingId, baseStoreId);
			if (byTackingId.isEmpty())
			{
				LOG.error("DefaultSaeeNotificationService : No Consignment was found for Saee Tracking Id of : {}", trackingId);
				saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
						"No Consignment was found for Saee Tracking Id of :" + trackingId, trackingId, notificationStatus, false,
						Boolean.TRUE);
				continue;
			}
			final BaseStoreModel baseStoreModel = byTackingId.get().getOrder().getStore();

			if (!baseStoreModel.getUid().equals(baseStoreForUid.getUid()))
			{
				LOG.error("DefaultSaeeNotificationService : Conflect between two base stores " + baseStoreModel.getUid() + " and "
						+ baseStoreForUid.getUid());
				saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey,
						"Conflect between two base stores " + baseStoreModel.getUid() + " and " + baseStoreForUid.getUid(), trackingId,
						notificationStatus, false, Boolean.FALSE);
				throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST,
						"Conflect between two base stores " + baseStoreModel.getUid() + " and " + baseStoreForUid.getUid(), null);
			}

			updateConsignment(byTackingId.get(), notificationStatus);
			saeeUpdateNotificationRecordService.createNotificationRecord(notificationRequest.toString(), secretKey, "200",
					trackingId, notificationStatus, true, Boolean.TRUE);
		}
	}

	private boolean validateKey(String requestKey, final String providerKey)
	{
		requestKey = requestKey.toLowerCase();
		String token = "";
		if (requestKey.startsWith("bearer "))
		{
			token = StringUtils.isBlank(requestKey.split("bearer ")[1]) ? "" : requestKey.split("bearer ")[1];
		}
		else if (requestKey.startsWith("bearer"))
		{
			token = StringUtils.isBlank(requestKey.split("bearer")[1]) ? "" : requestKey.split("bearer")[1];
		}
		return StringUtils.isNotBlank(token) && providerKey.equalsIgnoreCase(token);
	}

	private boolean isWaybillsContainsNull(final List<String> waybills)
	{
		if (CollectionUtils.isEmpty(waybills))
		{
			return true;
		}
		for (final String waybill : waybills)
		{
			if (waybill == null || StringUtils.isBlank(waybill) || StringUtils.isEmpty(waybill))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the status mapping.
	 *
	 * @param notificationStatus
	 *           the notification status
	 * @return the status mapping
	 */
	private ConsignmentStatus getStatusMapping(final String notificationStatus)
	{
		final ConsignmentStatus status = saeeWebhookStatusMap.get(notificationStatus);
		return status;
	}

	/**
	 * Update consignment.
	 *
	 * @param consignment
	 *           the consignment
	 * @param notificationStatus
	 *           the notification status
	 */
	private void updateConsignment(final ConsignmentModel consignment, final String notificationStatus)
	{
		final ConsignmentStatus statusMapping = getStatusMapping(notificationStatus);
		if (statusMapping != null)
		{
			consignment.setFulfillmentStatus(statusMapping);
		}
		consignment.setFulfillmentStatusText(notificationStatus);
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(getStatusMapping(notificationStatus)))
		{
			consignment.setStatus(ConsignmentStatus.DELIVERY_COMPLETED);
			saccERPService.updateSaleOrderDeliveredStatus(consignment);
		}
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	private String getAllWaybillsAsString(final List<String> waybills)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("[");
		waybills.stream().forEach(waybill -> builder.append(waybill).append(","));
		builder.deleteCharAt(builder.length() - 1).append("]");
		return builder.toString();
	}

}
