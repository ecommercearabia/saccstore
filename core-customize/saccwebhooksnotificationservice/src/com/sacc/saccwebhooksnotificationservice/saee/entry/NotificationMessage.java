package com.sacc.saccwebhooksnotificationservice.saee.entry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class NotificationMessage implements Serializable
{
	@JsonProperty(value = "event_status")
	private String event_status;
	@JsonProperty(value = "waybill")
	private String waybill;
	@JsonProperty(value = "waybills")
	private List<String> waybills;
	@JsonProperty(value = "status_code")
	private String status_code;

	/**
	 * @param waybills
	 *           the waybills to set
	 */
	public void setWaybills(final List<String> waybills)
	{
		this.waybills = waybills;
	}

	/**
	 *
	 */
	public NotificationMessage()
	{
		super();
		waybills = new ArrayList<>();
	}

	/**
	 * @return the event_status
	 */
	public String getEvent_status()
	{
		return event_status;
	}

	/**
	 * @param event_status
	 *           the event_status to set
	 */
	public void setEvent_status(final String event_status)
	{
		this.event_status = event_status;
	}

	/**
	 * @return the waybill
	 */
	public String getWaybill()
	{
		return waybill;
	}

	/**
	 * @param waybill
	 *           the waybill to set
	 */
	public void setWaybill(final String waybill)
	{
		this.waybill = waybill;
	}

	/**
	 * @return the status_code
	 */
	public String getStatus_code()
	{
		return status_code;
	}

	/**
	 * @param status_code
	 *           the status_code to set
	 */
	public void setStatus_code(final String status_code)
	{
		this.status_code = status_code;
	}

	/**
	 * @return the waybills
	 */
	public List<String> getWaybills()
	{
		return waybills;
	}

	@Override
	public String toString()
	{
		return "NotificationMessage [event_status=" + event_status + ", waybill=" + waybill + ", waybills="
				+ getArrayAsString(waybills)
				+ ", status_code=" + status_code + "]";
	}

	/**
	 *
	 */
	private String getArrayAsString(final List<String> list)
	{
		final StringBuilder builder = new StringBuilder();
		if (list == null)
		{
			return null;
		}
		builder.append("[");
		list.stream().map(item -> builder.append(item).append(", "));
		builder.append("]");
		return builder.toString();
	}



}
