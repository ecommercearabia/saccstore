/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.jobs;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.core.GenericSearchConstants.LOG;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccproductsservices.facade.CustomProductFacade;
import com.sacc.saccproductsservices.model.UpdateProductCronJobModel;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;


/**
 *
 */
public class UpdateProductJob extends AbstractJobPerformable<UpdateProductCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(UpdateProductJob.class);

	@Resource(name = "customProductsFacade")
	private CustomProductFacade customProductFacade;


	private static final String STAGED = "Staged";
	private static final String ONLINE = "Online";

	/** The sales order web services. */
	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService saccERPItemWebService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;
	@Resource(name = "stockService")
	private StockService stockService;
	@Resource(name = "unitService")
	private UnitService unitService;
	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;
	@Resource(name = "indexerService")
	private IndexerService indexerService;

	@Resource(name = "facetSearchConfigService")
	private FacetSearchConfigService facetSearchConfigService;

	@Override
	public PerformResult perform(final UpdateProductCronJobModel cronJobModel)
	{

		LOG.info("UpdateProductJob is Starting ...");
		final List<SaccERPOrderOperationRecordModel> allFailedOrderRecords = saccERPOrderOperationService
				.getAllOrderRecords(Arrays.asList(ERPWebServiceType.CREATE_SALES_ORDER), null, null, null, false);
		if (!CollectionUtils.isEmpty(allFailedOrderRecords) && cronJobModel.isUpdateStock())
		{
			LOG.warn("Some failed Order Records found!");
			LOG.info("UpdateProductJob is Finished ...");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}
		try
		{

			final List<ItemWebService> items = saccERPItemWebService.readMultiple(0, null, "");
			if (CollectionUtils.isEmpty(items))
			{
				LOG.info("No items retrieved from response to update.");
				LOG.info("UpdateProductJob is Finished ...");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}
			updateProducts(cronJobModel, items);
		}
		catch (final Exception e)
		{
			LOG.error("UpdateProductJob :" + e);
			if (e.getCause() != null)
			{
				LOG.error("UpdateProductJob :" + e.getCause().getMessage());
			}

			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		finally
		{
			if (Boolean.TRUE.equals(cronJobModel.getIsSync()))
			{
				synchronizeCatalog(cronJobModel);
			}
			indexSolrFacetSearchIndesies(cronJobModel);
		}

		LOG.info("UpdateProductJob is Finished ...");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 *
	 */
	private void indexSolrFacetSearchIndesies(final UpdateProductCronJobModel cronjob)
	{
		if (!Boolean.TRUE.equals(cronjob.getIsIndexing()))
		{
			LOG.info("Cronjob is not indexable, enable isIndexing property to index the SOLR Facets");
			return;
		}

		if (cronjob.getSolrFacetSearchConfig() == null)
		{
			LOG.error("Cronjob SolrFacetSearchConfig is empty!");
			return;
		}

		final FacetSearchConfig facetSearchConfig = getFacetSearchConfig(cronjob.getSolrFacetSearchConfig());
		if (facetSearchConfig != null)
		{
			try
			{
				getIndexerService().performFullIndex(facetSearchConfig);
			}
			catch (final IndexerException e)
			{
				LOG.error(e.getMessage());
				return;
			}
		}

	}

	private FacetSearchConfig getFacetSearchConfig(final SolrFacetSearchConfigModel facetSearchConfigModel)
	{
		try
		{
			return getFacetSearchConfigService().getConfiguration(facetSearchConfigModel.getName());
		}
		catch (final FacetConfigServiceException var3)
		{
			LOG.error("Configuration service could not load configuration with name: " + facetSearchConfigModel.getName(), var3);
			return null;
		}
	}

	/**
	 *
	 */
	private void updateProducts(final UpdateProductCronJobModel cronJobModel, final List<ItemWebService> items)
	{
		for (final ItemWebService item : items)
		{
			boolean productNotFound = false;

			ProductModel product = null;
			try
			{
				product = productService.getProductForCode(getActiveCatalogVersion(cronJobModel), item.getBarCodeNo());
			}
			catch (final Exception e)
			{
				LOG.info("PRODUCT NO: " + item.getBarCodeNo() + " ,NOT FOUND!");
				productNotFound = true;
			}
			if (product == null)
			{
				LOG.info("PRODUCT NO: " + item.getBarCodeNo() + " ,NOT FOUND!");
				productNotFound = true;
			}
			ProductModel productModel = null;
			if (productNotFound)
			{
				productModel = createProduct(cronJobModel, item);
			}
			else
			{
				productModel = updateProduct(cronJobModel, product, item, false);
			}
			if (productModel != null)
			{
				if (cronJobModel.isUpdatePrice())
				{
					try
					{
						updatePriceRow(cronJobModel, productModel, item);
					}
					catch (final Exception e)
					{
						LOG.info("PRODUCT NO: " + item.getBarCodeNo() + " ,Coudn't Update Price!");
						continue;
					}
				}

				if (cronJobModel.isUpdateStock())
				{
					try
					{
						updateStockLevel(cronJobModel, productModel, item);
					}
					catch (final Exception e)
					{
						LOG.info("PRODUCT NO: " + item.getBarCodeNo() + " ,Coudn't Update Stocklevel!");
						continue;
					}
				}

			}

		}

	}

	/**
	 *
	 */
	private void updateStockLevel(final UpdateProductCronJobModel cronJobModel, final ProductModel productModel,
			final ItemWebService item)
	{

		if (cronJobModel.getWarehouses() == null || cronJobModel.getWarehouses().isEmpty())
		{
			return;
		}
		Integer quantity = getQuantity(item);
		if (quantity == null)
		{
			return;
		}

		quantity = updateQuantity(cronJobModel, quantity);

		for (final WarehouseModel warehouse : cronJobModel.getWarehouses())
		{
			final StockLevelModel stockLevel = stockService.getStockLevel(productModel, warehouse);

			if (stockLevel == null && cronJobModel.isAutoCreate())
			{
				createStockLevel(productModel, quantity, warehouse, item);
			}
			else if (stockLevel != null)
			{
				updateStockLevel(productModel, stockLevel, quantity, item, cronJobModel);
				updateInventoryEvents(cronJobModel, stockLevel);
			}
		}


	}

	protected void updateInventoryEvents(final UpdateProductCronJobModel cronJob, final StockLevelModel stockLevel)
	{
		if (cronJob.getRemoveInventoryEvents() == null || Boolean.TRUE.equals(cronJob.getRemoveInventoryEvents()))
		{
			LOG.info("Delete inventory events where product code=" + stockLevel.getProductCode());
			stockLevel.setInventoryEvents(null);
			modelService.save(stockLevel);
		}
	}

	protected int updateQuantity(final UpdateProductCronJobModel cronJob, final int quantity)
	{
		LOG.info("isUpdateFixedStock=" + cronJob.isUpdateFixedStock());
		if (cronJob.isUpdateFixedStock())
		{
			LOG.info("updateQuantity=" + cronJob.getFixedStock());

			return (int) cronJob.getFixedStock();
		}
		else
		{
			LOG.info("updateQuantity=" + quantity);
			return quantity;
		}
	}

	private void createStockLevel(final ProductModel product, final Integer quantity, final WarehouseModel warehouse,
			final ItemWebService item)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setProductCode(product.getCode());
		newStock.setCreateResponse(item.toString());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE: " + product.getCode());
	}

	private void updateStockLevel(final ProductModel product, final StockLevelModel stockLevel, final Integer quantity,
			final ItemWebService item, final UpdateProductCronJobModel cronJobModel)
	{

		if (cronJobModel.isResetStockReserved())
		{
			stockLevel.setReserved(0);
		}
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		saveUpdateStockLevelResponse(stockLevel, item.toString());
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: " + product.getCode());
	}

	private Integer getQuantity(final ItemWebService item)
	{
		int quantity = 0;
		try
		{
			quantity = Integer.parseInt(item.getInventory());
		}
		catch (final Exception e)
		{
			LOG.error("EXCEPTION PARSING THE QUANTITY FOR PRODUCT Inventory: " + item.getInventory());
			return null;
		}
		return quantity >= 0 ? Integer.valueOf(quantity) : Integer.valueOf(0);
	}

	private void updatePriceRow(final UpdateProductCronJobModel cronJobModel, final ProductModel product,
			final ItemWebService item)
	{
		final Collection<PriceRowModel> europe1Prices = product.getEurope1Prices();
		if (CollectionUtils.isEmpty(europe1Prices))
		{
			if (cronJobModel.isAutoCreate())
			{

				final PriceRowModel createPriceRow = createPriceRow(cronJobModel, product, item);
				if (createPriceRow != null)
				{

					final Collection<PriceRowModel> prices = new ArrayList<>();
					prices.add(createPriceRow(cronJobModel, product, item));
					product.setEurope1Prices(prices);
					modelService.save(product);
					LOG.info("Price created for product: " + product.getCode());

				}

			}
		}
		else
		{
			final List<PriceRowModel> collect = europe1Prices.stream().filter(Objects::nonNull)
					.filter(row -> cronJobModel.getSessionCurrency().equals(row.getCurrency())
							&& Boolean.valueOf(cronJobModel.isNet()).equals(row.getNet()))
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(collect) && cronJobModel.isAutoCreate())
			{

				final PriceRowModel createPriceRow = createPriceRow(cronJobModel, product, item);
				if (createPriceRow != null)
				{
					europe1Prices.add(createPriceRow);
					product.setEurope1Prices(europe1Prices);
					modelService.save(product);
					LOG.info("Price created for product: " + product.getCode());
				}



			}
			else if (!CollectionUtils.isEmpty(collect))
			{

				for (final PriceRowModel row : collect)
				{
					try
					{
						updatePriceRow(cronJobModel, row, item);
					}
					catch (final Exception e)
					{
						continue;
					}
				}

			}
		}
	}

	private PriceRowModel createPriceRow(final UpdateProductCronJobModel cronJobModel, final ProductModel product,
			final ItemWebService item)
	{

		Double price = null;
		try
		{
			if (StringUtils.isBlank(item.getUnitPrice()))
			{
				LOG.error("item: [" + item.getBarCodeNo() + "] is Blank Price!");
				return null;
			}
			price = Double.valueOf(item.getUnitPrice());

		}
		catch (final NumberFormatException e)
		{
			LOG.error("item: [" + item.getBarCodeNo() + "] is Wrong Price Format![" + item.getUnitPrice() + "]");
			return null;
		}
		UnitModel unitModel = null;
		try
		{
			unitModel = cronJobModel.getDefaultUnit() == null ? unitService.getUnitForCode("pieces") : cronJobModel.getDefaultUnit();
		}
		catch (final Exception e)
		{
			LOG.error("Unit: [pieces] is not found![" + item.getUnitPrice() + "]");
			return null;
		}

		if (unitModel == null)
		{
			return null;
		}

		final PriceRowModel priceRow = modelService.create(PriceRowModel.class);
		priceRow.setCurrency(cronJobModel.getSessionCurrency());
		priceRow.setCatalogVersion(getActiveCatalogVersion(cronJobModel));
		priceRow.setPrice(price);
		priceRow.setProduct(product);
		priceRow.setUnit(unitModel);
		priceRow.setNet(cronJobModel.isNet());
		modelService.save(priceRow);
		modelService.refresh(priceRow);
		return priceRow;
	}

	private void updatePriceRow(final UpdateProductCronJobModel cronJobModel, final PriceRowModel row, final ItemWebService item)
	{
		Double price = null;
		try
		{
			if (StringUtils.isBlank(item.getUnitPrice()))
			{
				LOG.error("item: [" + item.getBarCodeNo() + "] is Blank Price!");
				return;
			}
			price = Double.valueOf(item.getUnitPrice());

		}
		catch (final NumberFormatException e)
		{
			LOG.error("item: [" + item.getBarCodeNo() + "] is Wrong Price Format![" + item.getUnitPrice() + "]");
			return;
		}
		UnitModel unitModel = null;
		try
		{
			unitModel = cronJobModel.getDefaultUnit() == null ? unitService.getUnitForCode("pieces") : cronJobModel.getDefaultUnit();
		}
		catch (final Exception e)
		{
			LOG.error("Unit: [pieces] is not found![" + item.getUnitPrice() + "]");
		}

		if (unitModel != null)
		{
			row.setUnit(unitModel);
		}
		row.setPrice(price);
		modelService.save(row);
		LOG.info("Price updated for product: " + row.getProduct().getCode());
	}

	/**
	 *
	 */
	private ProductModel updateProduct(final UpdateProductCronJobModel cronJobModel, final ProductModel productModel,
			final ItemWebService item, final boolean isNew)
	{
		if (StringUtils.isNotBlank(item.getDescription()))
		{
			productModel.setName(item.getDescription(), Locale.ENGLISH);
		}
		if (StringUtils.isNotBlank(item.getDescription2()))
		{
			productModel.setName(item.getDescription2(), new Locale("ar"));
		}
		if (StringUtils.isNotBlank(item.getSummary()))
		{
			productModel.setSummary(item.getSummary(), Locale.ENGLISH);
		}
		if (StringUtils.isNotBlank(item.getArabicSummary()))
		{
			productModel.setSummary(item.getArabicSummary(), new Locale("ar"));
		}
		productModel.setEan(item.getNo());
		double unitCost = 0;
		try
		{
			if (StringUtils.isBlank(item.getUnitCost()))
			{
				LOG.error("item: [" + item.getBarCodeNo() + "] is Blank UnitCost!");
			}
			else
			{
				unitCost = Double.valueOf(item.getUnitCost());
			}

		}
		catch (final NumberFormatException e)
		{
			LOG.error("item: [" + item.getBarCodeNo() + "] is Wrong UnitCost Format![" + item.getUnitCost() + "]");
		}
		productModel.setUnitCost(unitCost);

		final ArticleApprovalStatus approved = isNew ? ArticleApprovalStatus.CHECK : productModel.getApprovalStatus();
		productModel.setApprovalStatus(approved);
		UnitModel unitModel = null;
		try
		{
			unitModel = cronJobModel.getDefaultUnit() == null ? unitService.getUnitForCode("pieces") : cronJobModel.getDefaultUnit();
		}
		catch (final Exception e)
		{
			LOG.error("Unit: [pieces] is not found![" + item.getUnitPrice() + "]");
		}
		productModel.setUnit(unitModel);

		if (item.getOnlineVatPercent() == null || item.getOnlineVatPercent().intValue() != 15)
		{
			productModel.setEurope1PriceFactory_PTG(cronJobModel.getVatFree());
		}
		else
		{
			productModel.setEurope1PriceFactory_PTG(cronJobModel.getVat15());
		}

		double weight;
		try
		{
			if (StringUtils.isBlank(item.getNetWeight()))
			{
				LOG.error("item: [" + item.getBarCodeNo() + "] is Blank Weight!");
				weight = 1;
			}
			weight = Double.valueOf(item.getNetWeight()) <= 0 ? 1 : Double.valueOf(item.getNetWeight());

		}
		catch (final NumberFormatException e)
		{
			LOG.error("item: [" + item.getBarCodeNo() + "] is Wrong Weight Format![" + item.getNetWeight() + "]");
			weight = 1;
		}
		productModel.setWeight(weight);

		if (!isNew)
		{
			saveUpdateProductResponse(productModel, item.toString());
		}

		modelService.saveAll(productModel);
		modelService.refresh(productModel);

		return productModel;
	}

	/**
	 *
	 */
	private ProductModel createProduct(final UpdateProductCronJobModel cronJobModel, final ItemWebService item)
	{
		try
		{

			final ProductModel productModel = modelService.create(ProductModel.class);
			productModel.setCode(item.getBarCodeNo());
			productModel.setCatalogVersion(getActiveCatalogVersion(cronJobModel));
			productModel.setCreateResponse(item.toString());
			LOG.info("Product created for code: " + item.getBarCodeNo());
			return updateProduct(cronJobModel, productModel, item, true);

		}
		catch (final Exception e)
		{
			LOG.error("Couldn't Create Product for code: " + item.getBarCodeNo() + " , " + e.getMessage());

		}
		return null;

	}

	private CatalogVersionSyncJobModel createCatalogVersionSyncJobModel(final CatalogVersionModel sourceVersion,
			final CatalogVersionModel targetVersion)
	{
		final String jobCode = "sync " + sourceVersion.getCatalog().getId() + ":Staged->Online";
		final JobModel job = cronJobService.getJob(jobCode);
		CatalogVersionSyncJobModel syncJob = null;
		if (job == null)
		{
			syncJob = modelService.create(CatalogVersionSyncJobModel.class);
			syncJob.setCode(jobCode);
			syncJob.setSourceVersion(sourceVersion);
			syncJob.setTargetVersion(targetVersion);
			modelService.save(syncJob);

			modelService.refresh(syncJob);

			return syncJob;
		}
		syncJob = (CatalogVersionSyncJobModel) job;
		return syncJob;
	}

	private void synchronizeCatalog(final CatalogVersionModel sourceVersion, final CatalogVersionModel targetVersion)
	{
		cronJobService.performCronJob(createSyncCronJob(createCatalogVersionSyncJobModel(sourceVersion, targetVersion)), true);
	}

	private void synchronizeCatalog(final UpdateProductCronJobModel cronJobModel)
	{
		final CatalogVersionModel sourceVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
		final CatalogVersionModel targetVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), ONLINE);
		catalogVersionService.setSessionCatalogVersion(targetVersion.getCatalog().getId(), ONLINE);
		synchronizeCatalog(sourceVersion, targetVersion);
		LOG.info("Finished synchronization of product catalog: " + targetVersion.getCatalog().getId());
	}

	private CatalogVersionSyncCronJobModel createSyncCronJob(final CatalogVersionSyncJobModel catalogVersionSyncJobModel)
	{
		final CatalogVersionSyncCronJobModel cronJobModel = modelService.create(CatalogVersionSyncCronJobModel.class);
		cronJobModel.setJob(catalogVersionSyncJobModel);
		cronJobModel.setCode("update_product_Job_" + UUID.randomUUID().toString());

		modelService.save(cronJobModel);
		return cronJobModel;
	}

	private CatalogVersionModel getActiveCatalogVersion(final UpdateProductCronJobModel cronJobModel)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), "Staged");
		return catalogVersion;
	}


	private void saveUpdateProductResponse(final ProductModel productModel, final String updateRecord)
	{

		final List<String> history = new ArrayList<>();
		if (productModel.getUpdateRecords() != null)
		{
			history.addAll(productModel.getUpdateRecords());
		}

		history.add(updateRecord);
		productModel.setUpdateRecords(history);
	}

	private void saveUpdateStockLevelResponse(final StockLevelModel stockLevel, final String updateRecord)
	{

		final List<String> history = new ArrayList<>();
		history.add(updateRecord);
		stockLevel.setUpdateRecords(history);
	}

	/**
	 * @return the indexerService
	 */
	protected IndexerService getIndexerService()
	{
		return indexerService;
	}

	/**
	 * @return the facetSearchConfigService
	 */
	protected FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}

}
