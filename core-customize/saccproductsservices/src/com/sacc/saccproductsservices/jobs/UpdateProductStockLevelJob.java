package com.sacc.saccproductsservices.jobs;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccproductsservices.model.UpdateProductStockLevelCronJobModel;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;


/**
 * @author mnasro
 */
public class UpdateProductStockLevelJob extends AbstractJobPerformable<UpdateProductStockLevelCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(UpdateProductStockLevelJob.class);

	private static final String ERROR_UPDATING_STOCK_FOR_NO = "ERROR UPDATING STOCK FOR ITEM NO: ";

	@Resource(name = "stockService")
	private StockService stockService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService saccERPItemWebService;
	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;

	@Override
	public PerformResult perform(final UpdateProductStockLevelCronJobModel cronJobModel)
	{
		LOG.info("UpdateProductStockLevelJob is Starting ...");
		final List<SaccERPOrderOperationRecordModel> allFailedOrderRecords = saccERPOrderOperationService
				.getAllOrderRecords(Arrays.asList(ERPWebServiceType.CREATE_SALES_ORDER), null, null, null, false);
		if (!CollectionUtils.isEmpty(allFailedOrderRecords))
		{
			LOG.warn("Some failed Order Records found!");
			LOG.info("UpdateProductStockLevelJob is Finished ...");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}
		try
		{
			final List<ItemWebService> items = saccERPItemWebService.readMultiple(0, null, "");
			if (CollectionUtils.isEmpty(items))
			{
				LOG.info("No items retrieved from response to update.");
				LOG.info("UpdateProductStockLevelCronJob is Finished ...");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}
			updateProductQuantities(cronJobModel, items);
		}
		catch (final Exception e)
		{
			LOG.error("UpdateProductStockLevelCronJob :" + e.getMessage());
			if (e.getCause() != null)
			{
				LOG.error("UpdateProductJob :" + e.getCause().getMessage());
			}
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		LOG.info("UpdateProductStockLevelCronJob is Finished ...");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void updateProductQuantities(final UpdateProductStockLevelCronJobModel cronJobModel, final List<ItemWebService> items)
	{
		if (!CollectionUtils.isEmpty(cronJobModel.getWarehouses()))
		{
			cronJobModel.getWarehouses().forEach(w -> updateProductQuantitiesByWarehouse(cronJobModel, w, items));
		}
	}

	private void updateProductQuantitiesByWarehouse(final UpdateProductStockLevelCronJobModel cronJobModel,
			final WarehouseModel warehouse, final List<ItemWebService> items)
	{
		items.forEach(p -> updateProductQuantity(cronJobModel, warehouse, p));
	}

	protected void updateInventoryEvents(final UpdateProductStockLevelCronJobModel cronJob, final StockLevelModel stockLevel)
	{
		if (cronJob.getRemoveInventoryEvents() == null || Boolean.TRUE.equals(cronJob.getRemoveInventoryEvents()))
		{
			LOG.info("Delete inventory events where product code=" + stockLevel.getProductCode());
			stockLevel.setInventoryEvents(null);
			modelService.save(stockLevel);
		}
	}

	private void updateProductQuantity(final UpdateProductStockLevelCronJobModel cronJobModel, final WarehouseModel warehouse,
			final ItemWebService item)
	{
		if (StringUtils.isEmpty(item.getBarCodeNo()))
		{
			LOG.error("ERROR UPDATING STOCK FOR ITEM No: null or empty");
			return;
		}
		final CatalogVersionModel catalogVersion = getActiveCatalogVersion(cronJobModel);
		ProductModel product = null;
		try
		{
			product = productService.getProductForCode(catalogVersion, item.getBarCodeNo());
		}
		catch (final Exception e)
		{
			LOG.error(ERROR_UPDATING_STOCK_FOR_NO + item.getBarCodeNo() + ", " + e.getMessage());
			return;
		}
		if (product == null)
		{
			LOG.error(ERROR_UPDATING_STOCK_FOR_NO + item.getBarCodeNo());
			return;
		}

		Integer quantity = getQuantity(item);
		if (quantity == null)
		{
			return;
		}

		quantity = updateQuantity(cronJobModel, quantity);

		final StockLevelModel stockLevel = stockService.getStockLevel(product, warehouse);

		if (stockLevel == null && cronJobModel.isAutoCreate())
		{
			createStockLevel(cronJobModel, product, quantity, warehouse, item);
		}
		else if (stockLevel != null)
		{
			updateStockLevel(cronJobModel, product, stockLevel, quantity, item);
			updateInventoryEvents(cronJobModel, stockLevel);
		}
	}

	protected int updateQuantity(final UpdateProductStockLevelCronJobModel cronJob, final int quantity)
	{
		LOG.info("isUpdateFixedStock=" + cronJob.isUpdateFixedStock());
		if (cronJob.isUpdateFixedStock())
		{
			LOG.info("updateQuantity=" + cronJob.getFixedStock());

			return (int) cronJob.getFixedStock();
		}
		else
		{
			LOG.info("updateQuantity=" + quantity);
			return quantity;
		}
	}

	private void createStockLevel(final UpdateProductStockLevelCronJobModel cronjobModel, final ProductModel product,
			final Integer quantity, final WarehouseModel warehouse, final ItemWebService item)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(0);
		newStock.setProductCode(product.getCode());
		newStock.setCreateResponse(item.toString());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(newStock);
		updateStockLevel(cronjobModel, product, newStock, quantity, item);
		LOG.info("STOCK CREATED FOR PRODUCT CODE: " + product.getCode());
	}

	private void updateStockLevel(final UpdateProductStockLevelCronJobModel cronjobModel, final ProductModel product,
			final StockLevelModel stockLevel, final Integer quantity, final ItemWebService item)
	{
		if (cronjobModel.isResetStockReserved())
		{
			stockLevel.setReserved(0);
		}
		final String stockResponse = buildStockRecord(stockLevel, quantity, item);
		saveUpdateStockLevelResponse(stockLevel, stockResponse, cronjobModel.isSaveUpdateStockRecords());
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: " + product.getCode());
	}

	private String buildStockRecord(final StockLevelModel stockLevel, final Integer quantity, final ItemWebService item)
	{
		final Map<String, String> stockResponseMap = new HashMap<>();
		if (stockLevel != null)
		{
			stockResponseMap.put("stockLevelBeforeUpdate", String.valueOf(stockLevel.getAvailable()));
			stockResponseMap.put("stockReservedBeforeUpdate", String.valueOf(stockLevel.getReserved()));
		}
		else
		{
			stockResponseMap.put("stockLevelBeforeUpdate", "New stock level with available quantity of " + quantity);
			stockResponseMap.put("stockReservedBeforeUpdate", "New stock level with  0 reserved quantiity");
		}
		stockResponseMap.put("ERPStockLevel", String.valueOf(quantity));
		stockResponseMap.put("ERPStockResponse", item.toString());
		return stockResponseMap.toString();
	}


	private Integer getQuantity(final ItemWebService item)
	{
		int quantity = 0;
		try
		{
			quantity = Integer.parseInt(item.getInventory());
		}
		catch (final Exception e)
		{
			LOG.error("EXCEPTION PARSING THE QUANTITY FOR PRODUCT Inventory: " + item.getInventory());
			return null;
		}
		return quantity >= 0 ? Integer.valueOf(quantity) : Integer.valueOf(0);
	}

	private CatalogVersionModel getActiveCatalogVersion(final UpdateProductStockLevelCronJobModel cronJobModel)
	{
		return catalogVersionService.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), "Staged");
	}

	private void saveUpdateStockLevelResponse(final StockLevelModel stockLevel, final String updateRecord,
			final boolean saveUpdateStockRecords)
	{
		final List<String> records = new ArrayList<>();
		if (saveUpdateStockRecords)
		{
			records.add(updateRecord);
		}
		records.addAll(stockLevel.getUpdateRecords());
		stockLevel.setUpdateRecords(records);
	}

}
