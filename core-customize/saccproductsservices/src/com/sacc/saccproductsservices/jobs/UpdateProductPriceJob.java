package com.sacc.saccproductsservices.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccproductsservices.model.UpdateProductPriceCronJobModel;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;


/**
 * @author mnasro
 */
public class UpdateProductPriceJob extends AbstractJobPerformable<UpdateProductPriceCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(UpdateProductPriceJob.class);

	private static final String STAGED = "Staged";
	private static final String ONLINE = "Online";

	/** The sales order web services. */
	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService saccERPItemWebService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;


	@Override
	public PerformResult perform(final UpdateProductPriceCronJobModel cronJobModel)
	{
		//		LOG.info("UpdateProductPriceJob is Starting ...");
		//
		//		try
		//		{
		//			final List<ItemWebService> items = saccERPItemWebService.readMultiple(0, null, "");
		//			if (CollectionUtils.isEmpty(items))
		//			{
		//				LOG.info("No items retrieved from response to update.");
		//				LOG.info("UpdateProductPriceJob is Finished ...");
		//				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		//			}
		//			final CatalogVersionModel catalogVersion = catalogVersionService
		//					.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
		//			catalogVersionService.setSessionCatalogVersion(catalogVersion.getCatalog().getId(), STAGED);
		//
		//			updatePriceForProducts(cronJobModel, items);
		//		}
		//		catch (final Exception e)
		//		{
		//			LOG.error("UpdateProductPriceJob :" + e);
		//			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		//		}
		//		finally
		//		{
		//			if (Boolean.TRUE.equals(cronJobModel.getIsSync()))
		//			{
		//				synchronizeCatalog(cronJobModel);
		//			}
		//		}
		//
		//		LOG.info("UpdateProductPriceJob is Finished ...");
		LOG.error("UpdateProductPriceJob : not supported for update price!");
		return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
	}

	private void updatePriceForProducts(final UpdateProductPriceCronJobModel cronJobModel, final List<ItemWebService> items)
	{
		for (final ItemWebService item : items)
		{

			final Double originalPrice = null;
			final Double price = Double.valueOf(item.getUnitPrice());
			ProductModel product = null;
			try
			{
				product = productService.getProductForCode(item.getBarCodeNo());
			}
			catch (final Exception e)
			{
				LOG.error("ERROR UPDATING PRICE FOR PRODUCT NO: " + item.getBarCodeNo() + " ,NOT FOUND!");
				continue;
			}
			if (product == null)
			{
				LOG.error("ERROR UPDATING PRICE FOR PRODUCT NO: " + item.getBarCodeNo() + " ,NOT FOUND!");
				continue;
			}
			final Collection<PriceRowModel> europe1Prices = product.getEurope1Prices();
			if (CollectionUtils.isEmpty(europe1Prices))
			{
				if (cronJobModel.isAutoCreate())
				{
					final Collection<PriceRowModel> prices = new ArrayList<>();
					prices.add(createPriceRow(cronJobModel, product, price, originalPrice));
					product.setEurope1Prices(prices);
					modelService.save(product);
					LOG.info("Price created for product: " + product.getCode());
				}
			}
			else
			{
				final List<PriceRowModel> collect = europe1Prices.stream().filter(Objects::nonNull)
						.filter(row -> cronJobModel.getSessionCurrency().equals(row.getCurrency())
								&& Boolean.valueOf(cronJobModel.isNet()).equals(row.getNet()))
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(collect) && cronJobModel.isAutoCreate())
				{
					europe1Prices.add(createPriceRow(cronJobModel, product, price, originalPrice));
					product.setEurope1Prices(europe1Prices);
					modelService.save(product);
					LOG.info("Price created for product: " + product.getCode());
				}
				else if (!CollectionUtils.isEmpty(collect))
				{
					collect.forEach(row -> updatePriceRow(row, price, originalPrice));
				}
			}
		}
	}

	private PriceRowModel createPriceRow(final UpdateProductPriceCronJobModel cronJobModel, final ProductModel product,
			final Double price, final Double originalPrice)
	{
		final PriceRowModel priceRow = modelService.create(PriceRowModel.class);
		priceRow.setCurrency(cronJobModel.getSessionCurrency());
		priceRow.setCatalogVersion(cronJobModel.getProductCatalogVersion());
		priceRow.setPrice(price);
		priceRow.setOriginalPrice(originalPrice);
		priceRow.setProduct(product);
		priceRow.setNet(cronJobModel.isNet());
		modelService.save(priceRow);
		modelService.refresh(priceRow);
		return priceRow;
	}

	private void updatePriceRow(final PriceRowModel row, final Double price, final Double originalPrice)
	{
		row.setPrice(price);
		row.setOriginalPrice(originalPrice);
		modelService.save(row);
		LOG.info("Price updated for product: " + row.getProduct().getCode());
	}

	private CatalogVersionSyncJobModel createCatalogVersionSyncJobModel(final CatalogVersionModel sourceVersion,
			final CatalogVersionModel targetVersion)
	{
		final String jobCode = "sync " + sourceVersion.getCatalog().getId() + ":Staged->Online";
		final JobModel job = cronJobService.getJob(jobCode);
		CatalogVersionSyncJobModel syncJob = null;
		if (job == null)
		{
			syncJob = modelService.create(CatalogVersionSyncJobModel.class);
			syncJob.setCode(jobCode);
			syncJob.setSourceVersion(sourceVersion);
			syncJob.setTargetVersion(targetVersion);
			modelService.save(syncJob);

			modelService.refresh(syncJob);

			return syncJob;
		}
		syncJob = (CatalogVersionSyncJobModel) job;
		return syncJob;
	}

	private void synchronizeCatalog(final CatalogVersionModel sourceVersion, final CatalogVersionModel targetVersion)
	{
		cronJobService.performCronJob(createSyncCronJob(createCatalogVersionSyncJobModel(sourceVersion, targetVersion)), true);
	}

	private void synchronizeCatalog(final UpdateProductPriceCronJobModel cronJobModel)
	{
		final CatalogVersionModel sourceVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
		final CatalogVersionModel targetVersion = catalogVersionService
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), ONLINE);
		catalogVersionService.setSessionCatalogVersion(targetVersion.getCatalog().getId(), ONLINE);
		synchronizeCatalog(sourceVersion, targetVersion);
		LOG.info("Finished synchronization of product catalog: " + targetVersion.getCatalog().getId());
	}

	private CatalogVersionSyncCronJobModel createSyncCronJob(final CatalogVersionSyncJobModel catalogVersionSyncJobModel)
	{
		final CatalogVersionSyncCronJobModel cronJobModel = modelService.create(CatalogVersionSyncCronJobModel.class);
		cronJobModel.setJob(catalogVersionSyncJobModel);
		cronJobModel.setCode(UUID.randomUUID().toString());

		modelService.save(cronJobModel);
		return cronJobModel;
	}
}
