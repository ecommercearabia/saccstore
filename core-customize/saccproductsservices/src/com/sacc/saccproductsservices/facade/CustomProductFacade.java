/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.facade;

import java.util.List;

import com.sacc.saccproductsservices.beans.Result;
import com.sacc.saccproductsservices.productsData.ProductData;
import com.sacc.saccproductsservices.productsData.ProductPriceData;
import com.sacc.saccproductsservices.productsData.ProductsStockLevelData;


/**
 * @author Husam
 * @author mnasro
 * @version 0.1
 */
public interface CustomProductFacade
{


	Result insertUpdateProduct(String catalogVersion, String catalogVersionId, List<ProductData> data);

	Result insertUpdatePrice(String catalogVersion, String catalogVersionId, List<ProductPriceData> data, final boolean checkNet,
			final boolean checkCurrency);

	Result insertUpdateStockLevel(String catalogVersion, String catalogVersionId, List<ProductsStockLevelData> data);

	//	Result InsertUpdateProductReference(String catalogVersion, String catalogVersionId, List<ProductReferenceData> data);
}
