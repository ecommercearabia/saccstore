/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.sacc.fulfilmentprocess.constants.SaccFulfilmentProcessConstants;

public class SaccFulfilmentProcessManager extends GeneratedSaccFulfilmentProcessManager
{
	public static final SaccFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SaccFulfilmentProcessManager) em.getExtension(SaccFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
