package com.sacc.saccstorecreditfacades.facade;
import java.util.List;
import java.util.Optional;

import com.sacc.saccstorecreditfacades.data.StoreCreditModeData;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeFacade
{
	public Optional<StoreCreditModeData> getStoreCreditMode(String storeCreditModeTypeCode);

	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModesCurrentBaseStore();

	public boolean isStoreCreditModeSupportedByCurrentBaseStore(final String StoreCreditTypeCode);

}
