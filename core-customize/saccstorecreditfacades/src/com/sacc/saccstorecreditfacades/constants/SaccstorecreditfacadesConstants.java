package com.sacc.saccstorecreditfacades.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class SaccstorecreditfacadesConstants extends GeneratedSaccstorecreditfacadesConstants
{
	public static final String EXTENSIONNAME = "saccstorecreditfacades";
	
	private SaccstorecreditfacadesConstants()
	{
		//empty
	}
	
	
}
