/**
 * DeliveredShipmentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class DeliveredShipmentResponse  implements java.io.Serializable {
    private java.lang.String requestStatus;

    private com.smsaexpress.track.secom.DeliveredShipmentDetails[] trackingDetailsList;

    public DeliveredShipmentResponse() {
    }

    public DeliveredShipmentResponse(
           java.lang.String requestStatus,
           com.smsaexpress.track.secom.DeliveredShipmentDetails[] trackingDetailsList) {
           this.requestStatus = requestStatus;
           this.trackingDetailsList = trackingDetailsList;
    }


    /**
     * Gets the requestStatus value for this DeliveredShipmentResponse.
     * 
     * @return requestStatus
     */
    public java.lang.String getRequestStatus() {
        return requestStatus;
    }


    /**
     * Sets the requestStatus value for this DeliveredShipmentResponse.
     * 
     * @param requestStatus
     */
    public void setRequestStatus(java.lang.String requestStatus) {
        this.requestStatus = requestStatus;
    }


    /**
     * Gets the trackingDetailsList value for this DeliveredShipmentResponse.
     * 
     * @return trackingDetailsList
     */
    public com.smsaexpress.track.secom.DeliveredShipmentDetails[] getTrackingDetailsList() {
        return trackingDetailsList;
    }


    /**
     * Sets the trackingDetailsList value for this DeliveredShipmentResponse.
     * 
     * @param trackingDetailsList
     */
    public void setTrackingDetailsList(com.smsaexpress.track.secom.DeliveredShipmentDetails[] trackingDetailsList) {
        this.trackingDetailsList = trackingDetailsList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeliveredShipmentResponse)) return false;
        DeliveredShipmentResponse other = (DeliveredShipmentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestStatus==null && other.getRequestStatus()==null) || 
             (this.requestStatus!=null &&
              this.requestStatus.equals(other.getRequestStatus()))) &&
            ((this.trackingDetailsList==null && other.getTrackingDetailsList()==null) || 
             (this.trackingDetailsList!=null &&
              java.util.Arrays.equals(this.trackingDetailsList, other.getTrackingDetailsList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestStatus() != null) {
            _hashCode += getRequestStatus().hashCode();
        }
        if (getTrackingDetailsList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTrackingDetailsList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTrackingDetailsList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeliveredShipmentResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "DeliveredShipmentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "RequestStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingDetailsList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "TrackingDetailsList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "DeliveredShipmentDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "DeliveredShipmentDetails"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
