/**
 * GetPDFBr.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetPDFBr  implements java.io.Serializable {
    private java.lang.String awbNo;

    private java.lang.String passKey;

    private java.lang.String forwrdr;

    public GetPDFBr() {
    }

    public GetPDFBr(
           java.lang.String awbNo,
           java.lang.String passKey,
           java.lang.String forwrdr) {
           this.awbNo = awbNo;
           this.passKey = passKey;
           this.forwrdr = forwrdr;
    }


    /**
     * Gets the awbNo value for this GetPDFBr.
     * 
     * @return awbNo
     */
    public java.lang.String getAwbNo() {
        return awbNo;
    }


    /**
     * Sets the awbNo value for this GetPDFBr.
     * 
     * @param awbNo
     */
    public void setAwbNo(java.lang.String awbNo) {
        this.awbNo = awbNo;
    }


    /**
     * Gets the passKey value for this GetPDFBr.
     * 
     * @return passKey
     */
    public java.lang.String getPassKey() {
        return passKey;
    }


    /**
     * Sets the passKey value for this GetPDFBr.
     * 
     * @param passKey
     */
    public void setPassKey(java.lang.String passKey) {
        this.passKey = passKey;
    }


    /**
     * Gets the forwrdr value for this GetPDFBr.
     * 
     * @return forwrdr
     */
    public java.lang.String getForwrdr() {
        return forwrdr;
    }


    /**
     * Sets the forwrdr value for this GetPDFBr.
     * 
     * @param forwrdr
     */
    public void setForwrdr(java.lang.String forwrdr) {
        this.forwrdr = forwrdr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPDFBr)) return false;
        GetPDFBr other = (GetPDFBr) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.awbNo==null && other.getAwbNo()==null) || 
             (this.awbNo!=null &&
              this.awbNo.equals(other.getAwbNo()))) &&
            ((this.passKey==null && other.getPassKey()==null) || 
             (this.passKey!=null &&
              this.passKey.equals(other.getPassKey()))) &&
            ((this.forwrdr==null && other.getForwrdr()==null) || 
             (this.forwrdr!=null &&
              this.forwrdr.equals(other.getForwrdr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAwbNo() != null) {
            _hashCode += getAwbNo().hashCode();
        }
        if (getPassKey() != null) {
            _hashCode += getPassKey().hashCode();
        }
        if (getForwrdr() != null) {
            _hashCode += getForwrdr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPDFBr.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getPDFBr"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("awbNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "awbNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "passKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forwrdr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "forwrdr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
