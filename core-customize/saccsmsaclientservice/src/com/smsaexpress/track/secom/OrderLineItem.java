/**
 * OrderLineItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class OrderLineItem  implements java.io.Serializable {
    private java.lang.String iSKU;

    private java.lang.String iQty;

    private java.lang.String iSerial;

    public OrderLineItem() {
    }

    public OrderLineItem(
           java.lang.String iSKU,
           java.lang.String iQty,
           java.lang.String iSerial) {
           this.iSKU = iSKU;
           this.iQty = iQty;
           this.iSerial = iSerial;
    }


    /**
     * Gets the iSKU value for this OrderLineItem.
     * 
     * @return iSKU
     */
    public java.lang.String getISKU() {
        return iSKU;
    }


    /**
     * Sets the iSKU value for this OrderLineItem.
     * 
     * @param iSKU
     */
    public void setISKU(java.lang.String iSKU) {
        this.iSKU = iSKU;
    }


    /**
     * Gets the iQty value for this OrderLineItem.
     * 
     * @return iQty
     */
    public java.lang.String getIQty() {
        return iQty;
    }


    /**
     * Sets the iQty value for this OrderLineItem.
     * 
     * @param iQty
     */
    public void setIQty(java.lang.String iQty) {
        this.iQty = iQty;
    }


    /**
     * Gets the iSerial value for this OrderLineItem.
     * 
     * @return iSerial
     */
    public java.lang.String getISerial() {
        return iSerial;
    }


    /**
     * Sets the iSerial value for this OrderLineItem.
     * 
     * @param iSerial
     */
    public void setISerial(java.lang.String iSerial) {
        this.iSerial = iSerial;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderLineItem)) return false;
        OrderLineItem other = (OrderLineItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.iSKU==null && other.getISKU()==null) || 
             (this.iSKU!=null &&
              this.iSKU.equals(other.getISKU()))) &&
            ((this.iQty==null && other.getIQty()==null) || 
             (this.iQty!=null &&
              this.iQty.equals(other.getIQty()))) &&
            ((this.iSerial==null && other.getISerial()==null) || 
             (this.iSerial!=null &&
              this.iSerial.equals(other.getISerial())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getISKU() != null) {
            _hashCode += getISKU().hashCode();
        }
        if (getIQty() != null) {
            _hashCode += getIQty().hashCode();
        }
        if (getISerial() != null) {
            _hashCode += getISerial().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderLineItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "OrderLineItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISKU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "iSKU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IQty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "iQty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISerial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "iSerial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
