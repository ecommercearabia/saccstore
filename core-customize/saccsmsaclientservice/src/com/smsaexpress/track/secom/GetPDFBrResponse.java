/**
 * GetPDFBrResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetPDFBrResponse  implements java.io.Serializable {
    private byte[] getPDFBrResult;

    public GetPDFBrResponse() {
    }

    public GetPDFBrResponse(
           byte[] getPDFBrResult) {
           this.getPDFBrResult = getPDFBrResult;
    }


    /**
     * Gets the getPDFBrResult value for this GetPDFBrResponse.
     * 
     * @return getPDFBrResult
     */
    public byte[] getGetPDFBrResult() {
        return getPDFBrResult;
    }


    /**
     * Sets the getPDFBrResult value for this GetPDFBrResponse.
     * 
     * @param getPDFBrResult
     */
    public void setGetPDFBrResult(byte[] getPDFBrResult) {
        this.getPDFBrResult = getPDFBrResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPDFBrResponse)) return false;
        GetPDFBrResponse other = (GetPDFBrResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPDFBrResult==null && other.getGetPDFBrResult()==null) || 
             (this.getPDFBrResult!=null &&
              java.util.Arrays.equals(this.getPDFBrResult, other.getGetPDFBrResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPDFBrResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPDFBrResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPDFBrResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPDFBrResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getPDFBrResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPDFBrResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getPDFBrResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
