/**
 * GetRTLCitiesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetRTLCitiesResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getRTLCitiesResult;

    public GetRTLCitiesResponse() {
    }

    public GetRTLCitiesResponse(
           com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getRTLCitiesResult) {
           this.getRTLCitiesResult = getRTLCitiesResult;
    }


    /**
     * Gets the getRTLCitiesResult value for this GetRTLCitiesResponse.
     * 
     * @return getRTLCitiesResult
     */
    public com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getGetRTLCitiesResult() {
        return getRTLCitiesResult;
    }


    /**
     * Sets the getRTLCitiesResult value for this GetRTLCitiesResponse.
     * 
     * @param getRTLCitiesResult
     */
    public void setGetRTLCitiesResult(com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getRTLCitiesResult) {
        this.getRTLCitiesResult = getRTLCitiesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRTLCitiesResponse)) return false;
        GetRTLCitiesResponse other = (GetRTLCitiesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getRTLCitiesResult==null && other.getGetRTLCitiesResult()==null) || 
             (this.getRTLCitiesResult!=null &&
              this.getRTLCitiesResult.equals(other.getGetRTLCitiesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetRTLCitiesResult() != null) {
            _hashCode += getGetRTLCitiesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRTLCitiesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getRTLCitiesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getRTLCitiesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getRTLCitiesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getRTLCitiesResponse>getRTLCitiesResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
