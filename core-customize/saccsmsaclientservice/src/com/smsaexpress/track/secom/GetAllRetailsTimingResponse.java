/**
 * GetAllRetailsTimingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetAllRetailsTimingResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getAllRetailsTimingResult;

    public GetAllRetailsTimingResponse() {
    }

    public GetAllRetailsTimingResponse(
           com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getAllRetailsTimingResult) {
           this.getAllRetailsTimingResult = getAllRetailsTimingResult;
    }


    /**
     * Gets the getAllRetailsTimingResult value for this GetAllRetailsTimingResponse.
     * 
     * @return getAllRetailsTimingResult
     */
    public com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getGetAllRetailsTimingResult() {
        return getAllRetailsTimingResult;
    }


    /**
     * Sets the getAllRetailsTimingResult value for this GetAllRetailsTimingResponse.
     * 
     * @param getAllRetailsTimingResult
     */
    public void setGetAllRetailsTimingResult(com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getAllRetailsTimingResult) {
        this.getAllRetailsTimingResult = getAllRetailsTimingResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAllRetailsTimingResponse)) return false;
        GetAllRetailsTimingResponse other = (GetAllRetailsTimingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getAllRetailsTimingResult==null && other.getGetAllRetailsTimingResult()==null) || 
             (this.getAllRetailsTimingResult!=null &&
              this.getAllRetailsTimingResult.equals(other.getGetAllRetailsTimingResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetAllRetailsTimingResult() != null) {
            _hashCode += getGetAllRetailsTimingResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAllRetailsTimingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getAllRetailsTimingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getAllRetailsTimingResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getAllRetailsTimingResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getAllRetailsTimingResponse>getAllRetailsTimingResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
