/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccsmsaclientservice.setup;

import static com.sacc.saccsmsaclientservice.constants.SaccsmsaclientserviceConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.sacc.saccsmsaclientservice.constants.SaccsmsaclientserviceConstants;
import com.sacc.saccsmsaclientservice.service.SaccsmsaclientserviceService;


@SystemSetup(extension = SaccsmsaclientserviceConstants.EXTENSIONNAME)
public class SaccsmsaclientserviceSystemSetup
{
	private final SaccsmsaclientserviceService saccsmsaclientserviceService;

	public SaccsmsaclientserviceSystemSetup(final SaccsmsaclientserviceService saccsmsaclientserviceService)
	{
		this.saccsmsaclientserviceService = saccsmsaclientserviceService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		saccsmsaclientserviceService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SaccsmsaclientserviceSystemSetup.class.getResourceAsStream("/saccsmsaclientservice/sap-hybris-platform.png");
	}
}
