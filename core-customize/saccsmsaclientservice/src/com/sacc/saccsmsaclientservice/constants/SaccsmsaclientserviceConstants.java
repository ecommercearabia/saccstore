/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccsmsaclientservice.constants;

/**
 * Global class for all Saccsmsaclientservice constants. You can add global constants for your extension into this class.
 */
public final class SaccsmsaclientserviceConstants extends GeneratedSaccsmsaclientserviceConstants
{
	public static final String EXTENSIONNAME = "saccsmsaclientservice";

	private SaccsmsaclientserviceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccsmsaclientservicePlatformLogo";
}
