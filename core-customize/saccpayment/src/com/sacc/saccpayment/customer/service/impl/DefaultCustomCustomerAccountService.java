package com.sacc.saccpayment.customer.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.event.ForgottenPwdEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.BillingInfo;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.sacc.saccpayment.ccavenue.exception.MaximumNumberOfPasswordRestCustomerException;
import com.sacc.saccpayment.customer.service.CustomCustomerAccountService;
import com.sacc.saccpayment.customer.service.dao.CustomCustomerAccountDao;
import com.sacc.saccpayment.enums.NoCardType;


/**
 * @author mnasro
 * <p>
 * The Class DefaultCustomCustomerAccountService.
 */
public class DefaultCustomCustomerAccountService extends DefaultCustomerAccountService
        implements CustomCustomerAccountService {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = Logger.getLogger(DefaultCustomerAccountService.class);

    private static final String CUSTOMER_VALIDATION_NONULL = "Customer model cannot be null";

    /**
     * The sefam customer account dao.
     */
    @Resource(name = "defaultCustomCustomerAccountDao")
    private CustomCustomerAccountDao customerAccountDao;

    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;


    /**
     * @return the cmsSiteService
     */
    protected CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    /**
     * Creates the payment subscription.
     *
     * @param customerModel   the customer model
     * @param billingInfo     the billing info
     * @param titleCode       the title code
     * @param paymentProvider the payment provider
     * @param saveInAccount   the save in account
     * @param noCardTypeCode  the no card type code
     * @return the no card payment info model
     */
    @Override
    public Optional<NoCardPaymentInfoModel> createPaymentSubscription(final CustomerModel customerModel,
                                                                      final BillingInfo billingInfo,
                                                                      final String titleCode, final String paymentProvider, final boolean saveInAccount, final String noCardTypeCode) {
        validateParameterNotNull(customerModel, "Customer cannot be null");
        validateParameterNotNull(titleCode, "titleCode cannot be null");
        validateParameterNotNull(billingInfo, "billingInfo cannot be null");
        validateParameterNotNull(paymentProvider, "PaymentProvider cannot be null");
        validateParameterNotNull(noCardTypeCode, "noCardTypeCode cannot be null");
        final CurrencyModel currencyModel = getCurrency(customerModel);
        validateParameterNotNull(currencyModel, "Customer session currency cannot be null");

        final AddressModel billingAddress = getModelService().create(AddressModel.class);
        if (StringUtils.isNotBlank(titleCode)) {
            final TitleModel title = new TitleModel();
            title.setCode(titleCode);
            billingAddress.setTitle(getFlexibleSearchService().getModelByExample(title));
        }
        billingAddress.setFirstname(billingInfo.getFirstName());
        billingAddress.setLastname(billingInfo.getLastName());
        billingAddress.setLine1(billingInfo.getStreet1());
        billingAddress.setLine2(billingInfo.getStreet2());
        billingAddress.setTown(billingInfo.getCity());
        billingAddress.setPostalcode(billingInfo.getPostalCode());
        billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
        if (billingInfo.getRegion() != null) {
            billingAddress.setRegion(getCommonI18NService().getRegion(billingAddress.getCountry(), billingInfo.getRegion()));
        }
        billingAddress.setPhone1(billingInfo.getPhoneNumber());
        final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
        billingAddress.setEmail(email);
        billingAddress.setOwner(customerModel);
        getModelService().save(billingAddress);

        try {
            final NoCardPaymentInfoModel noCardPaymentInfoModel = getModelService().create(NoCardPaymentInfoModel.class);
            noCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
            noCardPaymentInfoModel.setUser(customerModel);
            noCardPaymentInfoModel.setBillingAddress(billingAddress);
            noCardPaymentInfoModel.setType(NoCardType.valueOf(noCardTypeCode));

            noCardPaymentInfoModel.setSaved(saveInAccount);

            billingAddress.setOwner(noCardPaymentInfoModel);
            getModelService().save(noCardPaymentInfoModel);
            getModelService().refresh(customerModel);

            addPaymentInfo(customerModel, noCardPaymentInfoModel);

            return Optional.ofNullable(noCardPaymentInfoModel);
        } catch (final AdapterException ae) //NOSONAR
        {
            LOG.error("Failed to create subscription for customer. Customer PK: " + customerModel.getPk() + " Exception: "
                    + ae.getClass().getName());

            return Optional.empty();
        }

    }

    /**
     * Gets the no card payment infos.
     *
     * @param customerModel the customer model
     * @param saved         the saved
     * @return the no card payment infos
     */
    @Override
    public Optional<List<NoCardPaymentInfoModel>> getNoCardPaymentInfos(final CustomerModel customerModel, final boolean saved) {
        validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
        return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfosByCustomer(customerModel, saved));
    }

    @Override
    protected CustomCustomerAccountDao getCustomerAccountDao() {
        return customerAccountDao;
    }


    /**
     * Gets the no card payment info for code.
     *
     * @param customerModel the customer model
     * @param code          the code
     * @return the no card payment info for code
     */
    @Override
    public Optional<NoCardPaymentInfoModel> getNoCardPaymentInfoForCode(final CustomerModel customerModel, final String code) {
        validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
        validateParameterNotNull(code, "code cannot be null");
        return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfoByCustomer(customerModel, code));
    }

    /**
     * Gets the payment info for code.
     *
     * @param customerModel the customer model
     * @param code          the code
     * @return the payment info for code
     */
    @Override
    public Optional<PaymentInfoModel> getPaymentInfoForCode(final CustomerModel customerModel, final String code) {
        validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
        validateParameterNotNull(code, "code cannot be null");
        return Optional.ofNullable(getCustomerAccountDao().findPaymentInfoByCustomer(customerModel, code));
    }

    @Override
    public void forgottenPassword(final CustomerModel customerModel) {
        validateParameterNotNullStandardMessage("customerModel", customerModel);
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();


		  if (currentSite != null && currentSite.isPasswordResetRequestLimitationFeatureEnabled())
		  {
            final Date lastDateOfForgetPasswordRequest=customerModel.getLastDateOfForgetPasswordRequest();

            if (lastDateOfForgetPasswordRequest==null|| !DateUtils.isSameDay(new Date(),lastDateOfForgetPasswordRequest)){
                customerModel.setLastDateOfForgetPasswordRequest(new Date());
                customerModel.setNumberOfForgetPasswordRequests(0);
            }

            final int numberOfForgetPasswordRequests=customerModel.getNumberOfForgetPasswordRequests();
				if (numberOfForgetPasswordRequests >= currentSite.getMaximumNumberOfForgetPasswordRequests())
	           {
					  throw new MaximumNumberOfPasswordRestCustomerException(
							  "Maximum number of password reset requests has been reached");
	           }
            customerModel.setNumberOfForgetPasswordRequests(numberOfForgetPasswordRequests+1);
        }

        final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
        final SecureToken data = new SecureToken(customerModel.getUid(), timeStamp);
        final String token = getSecureTokenService().encryptData(data);
        customerModel.setToken(token);
        getModelService().save(customerModel);
        getEventService().publishEvent(initializeEvent(new ForgottenPwdEvent(token), customerModel));

    }


}
