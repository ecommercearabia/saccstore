/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.dao.PaymentProviderDao;
import com.sacc.saccpayment.model.HyperpayApplePayPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayMadaPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultHyperpayPaymentProviderDao.
 */
public class DefaultHyperpayPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultHyperpayPaymentProviderDao()
	{
		super(HyperpayPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return HyperpayPaymentProviderModel._TYPECODE;
	}

	public boolean filterApplepayAndMadaProviders(final PaymentProviderModel model)
	{
		return !((model instanceof HyperpayMadaPaymentProviderModel) || (model instanceof HyperpayApplePayPaymentProviderModel));
	}

	@Override
	public Optional<PaymentProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<PaymentProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE, 3);

		if (find.isEmpty() || find.get().isEmpty())
		{
			return Optional.ofNullable(null);
		}

		final List<PaymentProviderModel> paymentProviders = find.get().stream().filter(this::filterApplepayAndMadaProviders)
				.collect(Collectors.toList());
		return CollectionUtils.isEmpty(paymentProviders) ? Optional.ofNullable(null) : Optional.ofNullable(paymentProviders.get(0));
	}

}
