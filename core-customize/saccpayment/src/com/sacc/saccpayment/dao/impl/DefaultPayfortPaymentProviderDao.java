/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.dao.PaymentProviderDao;
import com.sacc.saccpayment.model.ApplePayPayfortPaymentProviderModel;
import com.sacc.saccpayment.model.PayfortPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultHyperpayPaymentProviderDao.
 */
public class DefaultPayfortPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultPayfortPaymentProviderDao()
	{
		super(PayfortPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return PayfortPaymentProviderModel._TYPECODE;
	}

	@Override
	public Optional<PaymentProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<PaymentProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE, 2);

		if (find.isEmpty() || find.get().isEmpty())
		{
			return Optional.ofNullable(null);
		}

		final List<PaymentProviderModel> paymentProviders = find.get().stream()
				.filter(p -> !(p instanceof ApplePayPayfortPaymentProviderModel)).collect(Collectors.toList());
		return CollectionUtils.isEmpty(paymentProviders) ? Optional.ofNullable(null) : Optional.ofNullable(paymentProviders.get(0));
	}

}
