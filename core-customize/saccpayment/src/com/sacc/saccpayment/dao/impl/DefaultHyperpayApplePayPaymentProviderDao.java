/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.impl;

import com.sacc.saccpayment.dao.PaymentProviderDao;
import com.sacc.saccpayment.model.HyperpayApplePayPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultMadaPaymentProviderDao.
 */
public class DefaultHyperpayApplePayPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default ApplePay provider dao.
	 */
	public DefaultHyperpayApplePayPaymentProviderDao()
	{
		super(HyperpayApplePayPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return HyperpayApplePayPaymentProviderModel._TYPECODE;
	}

}
