/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.impl;

import com.sacc.saccpayment.dao.PaymentProviderDao;
import com.sacc.saccpayment.model.HyperpayMadaPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultMadaPaymentProviderDao.
 */
public class DefaultHyperpayMadaPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default Mada provider dao.
	 */
	public DefaultHyperpayMadaPaymentProviderDao()
	{
		super(HyperpayMadaPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return HyperpayMadaPaymentProviderModel._TYPECODE;
	}

}
