/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.hyperpay.enums;

/**
 * @author monzer
 */
public enum TransactionStatus
{
	SUCCESS, FAILED;
}