/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.hyperpay.enums;

/**
 * @author monzer
 */
public enum ResultPatterns
{
	FAILED("000000000"), SUCCESSFUL_TRANSACTION("^(000\\.000\\.|000\\.100\\.1|000\\.[36])"), SUCCESSFUL_TRANSACTION_MANUALY(
			"^(000\\.400\\.0[^3]|000\\.400\\.100)");

	private String pattern;

	private ResultPatterns(final String pattern)
	{
		this.pattern = pattern;
	}

	public String getPattern()
	{
		return pattern;
	}
}
