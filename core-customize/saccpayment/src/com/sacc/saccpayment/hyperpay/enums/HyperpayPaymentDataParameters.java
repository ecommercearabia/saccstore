package com.sacc.saccpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum HyperpayPaymentDataParameters {

	ENTITY_ID("entityId", "[a-f0-9]{32}", "entityId"),
	TEST_MODE("testMode", "EXTERNAL|INTERNAL", "testMode"),
	//Basic
	AMOUNT("amount", "[0-9]{1,10}(\\.[0-9]{2})?", "amount"),
	TAX_AMOUNT("taxAmount", "[0-9]{1,10}(\\.[0-9]{2})?", "taxAmount"),
	CURRENCY("currency", "[A-Z]{3}", "currency"),
	PAYMENT_BRAND("paymentBrand", "[a-zA-Z0-9_] {1,32}", "paymentBrand"),
	PAYMENT_TYPE("paymentType", "[A-Z]{2}", "paymentType"),
	OVERRIDE_PAYMENT_TYPE("overridePaymentType", "[a-zA-Z0-9_]{1,32}", "overridePaymentType"),
	DESCRIPTOR("descriptor", "[\\s\\S]{1,127}", "descriptor"), 
	MERCHANT_TRANSACTION_ID("merchantTransactionId", "[\\s\\S]{1,127}", "merchantTransactionId"),
	MERCHANT_INVOICE_ID("merchantInvoiceId", "[\\s\\S]{8,255}", "merchantInvoiceId"),
	MERCHANT_MEMO("merchantMemo", "[\\s\\S]{8,255}", "merchantMemo"),
	TRANSACTION_CATEGORY("transactionCategory", "[a-zA-Z0-9]{0,32}", "transactionCategory"),
	CREATE_REGISTRATION("createRegistration", "true|false", "createRegistration"),
	//Customer
	CUSTOMER_MERCHANT_CUSTOMER_ID("customer.merchantCustomerId", "[\\s\\S]{1,255}", "customerMerchantCustomerId"),
	CUSTOMER_GIVEN_NAME("customer.givenName", "[\\s\\S]", "customerGivenName"),
	CUSTOMER_MIDDLE_NAME("customer.middleName", "[\\s\\S]{2,50}", "customerMiddleName"),
	CUSTOMER_SURNAME("customer.surname", "[\\s\\S]", "customerSurname"),
	CUSTOMER_SEX("customer.sex", "M|F", "customerSex"),
	CUSTOMER_BIRTH_DATE("customer.birthDate", "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$", "customerBirthDate"), // the birth date regex on hyperpay api refernce is wrong. 
	CUSTOMER_PHONE("customer.phone", "[+0-9][0-9 \\.()/-]{7,25}", "customerPhone"),
	CUSTOMER_MOBILE("customer.mobile", "[+0-9][0-9 \\.()/-]{5,25}", "customerMobile"),
	CUSTOMER_WORK_PHONE("customer.workPhone", "[\\s\\S]{1,25}", "customerWorkPhone"),
	CUSTOMER_EMAIL("customer.email", "[\\s\\S]{6,128}", "customerEmail"),
	//Billing
	BILLING_STREET_1("billing.street1", "[\\s\\S]{1,100}", "billingStreet1"),
	BILLING_STREET_2("billing.street2", "[\\s\\S]{1,100}", "billingStreet2"),
	BILLING_HOUSE_NUMBER_1("billing.houseNumber1", "[\\s\\S]{1,100}", "billingHouseNumber1"),
	BILLING_HOUSE_NUMBER_2("billing.houseNumber2", "[\\s\\S]{1,100}", "billingHouseNumber2"),
	BILLING_CITY("billing.city", "[\\s\\S]{1,80}", "billingCity"),
	BILLING_STATE("billing.state", "[a-zA-Z0-9\\.]{1,50}", "billingState"),
	BILLING_POST_CODE("billing.postcode", "[A-Za-z0-9]{1,30}", "billingPostCode"),
	BILLING_COUNTRY("billing.country", "[A-Z]{2}", "billingCountry"),
	//Shipping
	SHIPPING_STREET_1("shipping.street1", "[\\s\\S]{1,100}", "shippingStreet1"),
	SHIPPING_STREET_2("shipping.street2", "[\\s\\S]{1,100}", "shippingStreet2"),
	SHIPPING_HOUSE_NUMBER_1("shipping.houseNumber1", "[\\s\\S]{1,100}", "shippingHouseNumber1"),
	SHIPPING_HOUSE_NUMBER_2("shipping.houseNumber2", "[\\s\\S]{1,100}", "shippingHouseNumber2"),
	SHIIPING_CITY("shipping.city", "[a-zA-Z]{1,80}", "shippingCity"),
	SHIIPING_STATE("shipping.state", "[a-zA-Z0-9\\.]{1,50}", "shippingState"),
	SHIPPING_POST_CODE("shipping.postcode", "[A-Za-z0-9]{1,30}", "shippingPostCode"),
	SHIPPING_COUNTRY("shipping.country", "[A-Za-z]{2}", "shippingCountry"),
	SHIPPING_METHOD("shipping.method", "[A-Z_]{5,30}", "shippingMethod"),
	SHIPPING_COST("shipping.cost", "[0-9]{1,10}\\.[0-9]{2}", "shippingCost"),
	SHIPPING_COMMENT("shipping.comment", "[\\s\\S]{1,160}", "shippingComment"),
	//Merchant
	MERCHANT_NAME("merchant.name", "[\\s\\S]{1,100}", "merchantName"),
	MERCHANT_CITY("merchant.city", "[\\s\\S]{1,100}", "merchantCity"),
	MERCHANT_STREET("merchant.street", "[\\s\\S]{1,100}", "merchantStreet"),
	MERCHANT_POST_CODE("merchant.postcode", "[A-Za-z0-9\\-]{1,10}", "merchantPostCode"),
	MERCHANT_STATE("merchant.state", "[a-zA-Z0-9]{1,50}", "merchantState"),
	MERCHANT_COUNTRY("merchant.country", "[A-Za-z]{2}", "merchantCountry"),
	MERCHANT_PHONE("merchant.phone", "[a-zA-Z0-9\\+-.]{0,25}", "merchantPhone"),
	MERCHANT_MCC("merchant.mcc", "[a-zA-Z0-9]{0,4}", "merchantMCC"),
	MERCHANT_SUB_MERCHANT_ID("merchant.submerchantId", "[\\s\\S]{1,100}", "merchantSubMerchantId"),
	MERCHANT_DATA("merchant.data", "[\\s\\S]{0,4096}", "merchantData"),
	//Redirect
	REDIRECT_URL("redirect.url", "[\\s\\S]{6,2048}", "redirectUrl");
	
	private String paramName;
	private String paramPattern;
	private String modelParam;
	
	
	HyperpayPaymentDataParameters(String paramName, String paramPattern, String modelParam) {
		this.paramName = paramName;
		this.paramPattern = paramPattern;
		this.modelParam = modelParam;
	}
	
	
	public String getParamName() {
		return paramName;
	}
	public String getParamPattern() {
		return paramPattern;
	}
	public String getModelParam() {
		return modelParam;
	}
	
	public static HyperpayPaymentDataParameters getDataParameterByName(String name) {
		for(HyperpayPaymentDataParameters data : values()) {
			if(name.equals(data.getModelParam())) {
				return data;
			}
		}
		return null;
	}
	
}
