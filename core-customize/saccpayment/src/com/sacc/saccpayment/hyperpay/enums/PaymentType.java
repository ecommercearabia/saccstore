package com.sacc.saccpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum PaymentType {

	PA("PA"), 
	DB("DB"), 
	CD("CD"), 
	CP("PA.CP"), 
	RV("RV"), 
	RF("RF");
	
	private String paymentCode;

	private PaymentType(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	
	public String getPaymentCode() {
		return paymentCode;
	}
	
}
