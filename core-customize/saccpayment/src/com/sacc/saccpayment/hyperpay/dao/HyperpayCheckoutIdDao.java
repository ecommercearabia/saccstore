/**
 *
 */
package com.sacc.saccpayment.hyperpay.dao;

import com.sacc.saccpayment.model.HyperpayCheckoutIdModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface HyperpayCheckoutIdDao
{
	HyperpayCheckoutIdModel getCheckoutIdById(final String id);
}
