/**
 *
 */
package com.sacc.saccpayment.hyperpay.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.sacc.saccpayment.hyperpay.dao.HyperpayCheckoutIdDao;
import com.sacc.saccpayment.hyperpay.service.HyperpayCheckoutIdService;
import com.sacc.saccpayment.model.HyperpayCheckoutIdModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public class DefaultHyperpayCheckoutIdService implements HyperpayCheckoutIdService
{
	@Resource(name = "hyperpayCheckoutIdDao")
	private HyperpayCheckoutIdDao hyperpayCheckoutIdDao;

	@Resource(name = "modelService")
	private ModelService modelService;


	@Override
	public HyperpayCheckoutIdModel getCheckoutIdById(final String id)
	{
		return getHyperpayCheckoutIdDao().getCheckoutIdById(id);
	}

	@Override
	public HyperpayCheckoutIdModel createHyperpayCheckoutId(final String id, final String orderCode, final String cartCode)
	{
		final HyperpayCheckoutIdModel create = getModelService().create(HyperpayCheckoutIdModel.class);
		if (Strings.isBlank(id))
		{
			throw new IllegalArgumentException("checkout id cannot be null");
		}

		if (Strings.isBlank(orderCode))
		{
			throw new IllegalArgumentException("orderCode cannot be null");
		}

		create.setCheckoutId(id);
		create.setOrderCode(orderCode);
		create.setCartCode(cartCode);

		getModelService().save(create);
		getModelService().refresh(create);

		return create;
	}

	/**
	 * @return the hyperpayCheckoutIdDao
	 */
	public HyperpayCheckoutIdDao getHyperpayCheckoutIdDao()
	{
		return hyperpayCheckoutIdDao;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}





}
