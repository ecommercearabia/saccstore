/**
 *
 */
package com.sacc.saccpayment.hyperpay.service;

import com.sacc.saccpayment.model.HyperpayCheckoutIdModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public interface HyperpayCheckoutIdService
{
	HyperpayCheckoutIdModel getCheckoutIdById(final String id);


	HyperpayCheckoutIdModel createHyperpayCheckoutId(final String id, final String orderCode, final String cartCode);
}
