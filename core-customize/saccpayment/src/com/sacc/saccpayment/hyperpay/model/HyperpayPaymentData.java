package com.sacc.saccpayment.hyperpay.model;

import com.sacc.saccpayment.hyperpay.enums.CustomerSex;
import com.sacc.saccpayment.hyperpay.enums.ShippingMethod;
import com.sacc.saccpayment.hyperpay.enums.TestMode;
import com.sacc.saccpayment.hyperpay.enums.TransactionCategory;

/**
 * 
 * @author monzer
 *
 */
public class HyperpayPaymentData {
	
	@Override
	public String toString() {
		return "HyperpayPaymentData [entityId=" + entityId + ", amount=" + amount + ", taxAmount=" + taxAmount
				+ ", currency=" + currency + ", paymentBrand=" + paymentBrand + ", paymentType=" + paymentType
				+ ", overridePaymentType=" + overridePaymentType + ", createRegistration=" + createRegistration
				+ ", testMode=" + testMode + ", descriptor=" + descriptor + ", merchantTransactionId="
				+ merchantTransactionId + ", merchantInvoiceId=" + merchantInvoiceId + ", merchantMemo=" + merchantMemo
				+ ", transactionCategory=" + transactionCategory + ", customerMerchantCustomerId="
				+ customerMerchantCustomerId + ", customerGivenName=" + customerGivenName + ", customerMiddleName="
				+ customerMiddleName + ", customerSurname=" + customerSurname + ", customerSex=" + customerSex
				+ ", customerBirthDate=" + customerBirthDate + ", customerPhone=" + customerPhone + ", customerMobile="
				+ customerMobile + ", customerWorkPhone=" + customerWorkPhone + ", customerEmail=" + customerEmail
				+ ", billingStreet1=" + billingStreet1 + ", billingStreet2=" + billingStreet2 + ", billingHouseNumber1="
				+ billingHouseNumber1 + ", billingHouseNumber2=" + billingHouseNumber2 + ", billingCity=" + billingCity
				+ ", billingState=" + billingState + ", billingPostCode=" + billingPostCode + ", billingCountry="
				+ billingCountry + ", shippingStreet1=" + shippingStreet1 + ", shippingStreet2=" + shippingStreet2
				+ ", shippingHouseNumber1=" + shippingHouseNumber1 + ", shippingHouseNumber2=" + shippingHouseNumber2
				+ ", shippingCity=" + shippingCity + ", shippingState=" + shippingState + ", shippingPostCode="
				+ shippingPostCode + ", shippingCountry=" + shippingCountry + ", shippingMethod=" + shippingMethod
				+ ", shippingCost=" + shippingCost + ", shippingComment=" + shippingComment + ", merchantName="
				+ merchantName + ", merchantCity=" + merchantCity + ", merchantStreet=" + merchantStreet
				+ ", merchantPostCode=" + merchantPostCode + ", merchantState=" + merchantState + ", merchantCountry="
				+ merchantCountry + ", merchantPhone=" + merchantPhone + ", merchantMCC=" + merchantMCC
				+ ", merchantSubMerchantId=" + merchantSubMerchantId + ", merchantData=" + merchantData + "]";
	}

	private String entityId;//
	private String amount;//
	private String taxAmount;//
	private String currency; //
	private String paymentBrand;//
	private String paymentType;//
	private String overridePaymentType;//
	private String createRegistration;//
	private TestMode testMode;
	private String descriptor;//
	private String merchantTransactionId;
	private String merchantInvoiceId;
	private String merchantMemo;
	private TransactionCategory transactionCategory;
	
	// Customer 
	private String customerMerchantCustomerId;
	private String customerGivenName;
	private String customerMiddleName;
	private String customerSurname;
	private CustomerSex customerSex;
	private String customerBirthDate;
	private String customerPhone;
	private String customerMobile;
	private String customerWorkPhone;
	private String customerEmail;
	
	//Billing
	private String billingStreet1;
	private String billingStreet2;
	private String billingHouseNumber1;
	private String billingHouseNumber2;
	private String billingCity;
	private String billingState;
	private String billingPostCode;
	private String billingCountry;
	
	//Shipping
	private String shippingStreet1;
	private String shippingStreet2;
	private String shippingHouseNumber1;
	private String shippingHouseNumber2;
	private String shippingCity;
	private String shippingState;
	private String shippingPostCode;
	private String shippingCountry;
	private ShippingMethod shippingMethod;
	private String shippingCost;
	private String shippingComment;
	
	//Merchant
	private String merchantName;
	private String merchantCity;
	private String merchantStreet;
	private String merchantPostCode;
	private String merchantState;
	private String merchantCountry;
	private String merchantPhone;
	private String merchantMCC;
	private String merchantSubMerchantId;
	private String merchantData;
	
	//Redirect
//	private String redirectUrl;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentBrand() {
		return paymentBrand;
	}

	public void setPaymentBrand(String paymentBrand) {
		this.paymentBrand = paymentBrand;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getOverridePaymentType() {
		return overridePaymentType;
	}

	public void setOverridePaymentType(String overridePaymentType) {
		this.overridePaymentType = overridePaymentType;
	}

	public String getCreateRegistration() {
		return createRegistration;
	}

	public void setCreateRegistration(String createRegistration) {
		this.createRegistration = createRegistration;
	}

	public TestMode getTestMode() {
		return testMode;
	}

	public void setTestMode(TestMode testMode) {
		this.testMode = testMode;
	}

	public String getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public String getMerchantInvoiceId() {
		return merchantInvoiceId;
	}

	public void setMerchantInvoiceId(String merchantInvoiceId) {
		this.merchantInvoiceId = merchantInvoiceId;
	}

	public String getMerchantMemo() {
		return merchantMemo;
	}

	public void setMerchantMemo(String merchantMemo) {
		this.merchantMemo = merchantMemo;
	}

	public TransactionCategory getTransactionCategory() {
		return transactionCategory;
	}

	public void setTransactionCategory(TransactionCategory transactionCategory) {
		this.transactionCategory = transactionCategory;
	}

	public String getCustomerMerchantCustomerId() {
		return customerMerchantCustomerId;
	}

	public void setCustomerMerchantCustomerId(String customerMerchantCustomerId) {
		this.customerMerchantCustomerId = customerMerchantCustomerId;
	}

	public String getCustomerGivenName() {
		return customerGivenName;
	}

	public void setCustomerGivenName(String customerGivenName) {
		this.customerGivenName = customerGivenName;
	}

	public String getCustomerMiddleName() {
		return customerMiddleName;
	}

	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}

	public CustomerSex getCustomerSex() {
		return customerSex;
	}

	public void setCustomerSex(CustomerSex customerSex) {
		this.customerSex = customerSex;
	}

	public String getCustomerBirthDate() {
		return customerBirthDate;
	}

	public void setCustomerBirthDate(String customerBirthDate) {
		this.customerBirthDate = customerBirthDate;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerWorkPhone() {
		return customerWorkPhone;
	}

	public void setCustomerWorkPhone(String customerWorkPhone) {
		this.customerWorkPhone = customerWorkPhone;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getBillingStreet1() {
		return billingStreet1;
	}

	public void setBillingStreet1(String billingStreet1) {
		this.billingStreet1 = billingStreet1;
	}

	public String getBillingStreet2() {
		return billingStreet2;
	}

	public void setBillingStreet2(String billingStreet2) {
		this.billingStreet2 = billingStreet2;
	}

	public String getBillingHouseNumber1() {
		return billingHouseNumber1;
	}

	public void setBillingHouseNumber1(String billingHouseNumber1) {
		this.billingHouseNumber1 = billingHouseNumber1;
	}

	public String getBillingHouseNumber2() {
		return billingHouseNumber2;
	}

	public void setBillingHouseNumber2(String billingHouseNumber2) {
		this.billingHouseNumber2 = billingHouseNumber2;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingPostCode() {
		return billingPostCode;
	}

	public void setBillingPostCode(String billingPostCode) {
		this.billingPostCode = billingPostCode;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getShippingStreet1() {
		return shippingStreet1;
	}

	public void setShippingStreet1(String shippingStreet1) {
		this.shippingStreet1 = shippingStreet1;
	}

	public String getShippingStreet2() {
		return shippingStreet2;
	}

	public void setShippingStreet2(String shippingStreet2) {
		this.shippingStreet2 = shippingStreet2;
	}

	public String getShippingHouseNumber1() {
		return shippingHouseNumber1;
	}

	public void setShippingHouseNumber1(String shippingHouseNumber1) {
		this.shippingHouseNumber1 = shippingHouseNumber1;
	}

	public String getShippingHouseNumber2() {
		return shippingHouseNumber2;
	}

	public void setShippingHouseNumber2(String shippingHouseNumber2) {
		this.shippingHouseNumber2 = shippingHouseNumber2;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingState() {
		return shippingState;
	}

	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}

	public String getShippingPostCode() {
		return shippingPostCode;
	}

	public void setShippingPostCode(String shippingPostCode) {
		this.shippingPostCode = shippingPostCode;
	}

	public String getShippingCountry() {
		return shippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}

	public ShippingMethod getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(ShippingMethod shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getShippingComment() {
		return shippingComment;
	}

	public void setShippingComment(String shippingComment) {
		this.shippingComment = shippingComment;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantCity() {
		return merchantCity;
	}

	public void setMerchantCity(String merchantCity) {
		this.merchantCity = merchantCity;
	}

	public String getMerchantStreet() {
		return merchantStreet;
	}

	public void setMerchantStreet(String merchantStreet) {
		this.merchantStreet = merchantStreet;
	}

	public String getMerchantPostCode() {
		return merchantPostCode;
	}

	public void setMerchantPostCode(String merchantPostCode) {
		this.merchantPostCode = merchantPostCode;
	}

	public String getMerchantState() {
		return merchantState;
	}

	public void setMerchantState(String merchantState) {
		this.merchantState = merchantState;
	}

	public String getMerchantCountry() {
		return merchantCountry;
	}

	public void setMerchantCountry(String merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	public String getMerchantPhone() {
		return merchantPhone;
	}

	public void setMerchantPhone(String merchantPhone) {
		this.merchantPhone = merchantPhone;
	}

	public String getMerchantMCC() {
		return merchantMCC;
	}

	public void setMerchantMCC(String merchantMCC) {
		this.merchantMCC = merchantMCC;
	}

	public String getMerchantSubMerchantId() {
		return merchantSubMerchantId;
	}

	public void setMerchantSubMerchantId(String merchantSubMerchantId) {
		this.merchantSubMerchantId = merchantSubMerchantId;
	}

	public String getMerchantData() {
		return merchantData;
	}

	public void setMerchantData(String merchantData) {
		this.merchantData = merchantData;
	}

	
//	public String getRedirectUrl() {
//		return redirectUrl;
//	}
//
//	public void setRedirectUrl(String redirectUrl) {
//		this.redirectUrl = redirectUrl;
//	}
	
}
