/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.hyperpay;

/**
 *
 */
public enum HyperpayStatusReason
{
	ID("id"), PAYMENT_TYPE("paymentType"), PAYMENT_BRAND("paymentBrand"), AMOUNT("amount"), CURRENCY("currency"), DESCRIPTOR(
			"descriptor"), RESULT("result"), RESULT_DETAILS("resultDetails"), CARD("card"), CUSTOMER("customer"), BILLING(
					"billing"), SHIPPING("shipping"), RISK("risk"), BUILD_NUMBER("buildNumber"), TIMESTAMP("timestamp"), NDC(
							"ndc"), CARD_BIN("bin"), CARD_BIN_COUNTRY("binCountry"), CARD_LAST4DIGITS("last4Digits"), CARD_HOLDER(
									"holder"), CARD_EXPIRY_MONTH("expiryMonth"), CARD_EXPIRY_YEAR(
											"expiryYear"), RESULT_CODE("code"), RESULT_DESCRIPTION("description");
	private String key;

	private HyperpayStatusReason(final String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}

}
