package com.sacc.saccpayment.entry;

import java.io.Serializable;


/**
 * The Class PaymentRequestData.
 *
 * @author abu-muhasien
 *
 */
public class PaymentRequestData implements Serializable
{

	/** The script src. */
	private String scriptSrc;

	/** The payment provider. */
	private String paymentProvider;
	/** The payment provider Data. */
	private Object paymentProviderData;

	private String checkoutId;

	/**
	 *
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider, final Object paymentProviderData)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
		this.paymentProviderData = paymentProviderData;
	}

	/**
	 *
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider, final Object paymentProviderData,
			final String checkoutId)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
		this.paymentProviderData = paymentProviderData;
		this.checkoutId = checkoutId;
	}



	/**
	 * @return the paymentProviderData
	 */
	public Object getPaymentProviderData()
	{
		return paymentProviderData;
	}

	/**
	 * @return the checkoutId
	 */
	public String getCheckoutId()
	{
		return checkoutId;
	}

	/**
	 * @param checkoutId
	 *           the checkoutId to set
	 */
	public void setCheckoutId(final String checkoutId)
	{
		this.checkoutId = checkoutId;
	}

	/**
	 * @param paymentProviderData
	 *           the paymentProviderData to set
	 */
	public void setPaymentProviderData(final Object paymentProviderData)
	{
		this.paymentProviderData = paymentProviderData;
	}

	/**
	 * Instantiates a new payment request data.
	 *
	 * @param scriptSrc
	 *           the script src
	 * @param paymentProvider
	 *           the payment provider
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
	}

	/**
	 * Sets the payment provider.
	 *
	 * @param paymentProvider
	 *           the new payment provider
	 */
	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	/**
	 * Gets the payment provider.
	 *
	 * @return the payment provider
	 */
	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	/**
	 * Gets the script src.
	 *
	 * @return the scriptSrc
	 */
	public String getScriptSrc()
	{
		return scriptSrc;
	}

	/**
	 * Sets the script src.
	 *
	 * @param scriptSrc
	 *           the scriptSrc to set
	 */
	public void setScriptSrc(final String scriptSrc)
	{
		this.scriptSrc = scriptSrc;
	}

}
