/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.enums;

/**
 *
 */
public enum PaymentResponseStatus
{
	SUCCESS, FAILURE
}
