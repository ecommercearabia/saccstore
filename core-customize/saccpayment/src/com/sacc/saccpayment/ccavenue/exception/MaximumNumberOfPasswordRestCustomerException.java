package com.sacc.saccpayment.ccavenue.exception;

public class MaximumNumberOfPasswordRestCustomerException extends RuntimeException{

	public MaximumNumberOfPasswordRestCustomerException()
	{
		super();
    }

	public MaximumNumberOfPasswordRestCustomerException(final String message) {
        super(message);
    }
}
