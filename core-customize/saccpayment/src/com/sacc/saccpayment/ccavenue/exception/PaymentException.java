/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.ccavenue.exception;

/**
 *
 */
public class PaymentException extends Exception
{

	public PaymentException(final String message)
	{
		super(message);
	}
}
