/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.ccavenue.exception;


import com.sacc.saccpayment.ccavenue.enums.PaymentExceptionType;


/**
 *
 */
public class CCAvenueException extends PaymentException
{
	private PaymentExceptionType type;

	public CCAvenueException(final String message)
	{
		super(message);

	}

	public CCAvenueException(final String message, final PaymentExceptionType type)
	{
		super(message);
		this.type = type;
	}

	public PaymentExceptionType getType()
	{
		return type;
	}


}
