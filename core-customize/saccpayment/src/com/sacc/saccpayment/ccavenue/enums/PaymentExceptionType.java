/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.ccavenue.enums;

public enum PaymentExceptionType
{
	BAD_REQUEST("Bad Request"), INVALID_RESPONSE_SYNTAX_EXPECTED_JSON(
			"Invalid response syntax,bad json syntax"), THE_ORDER_IS_ALLREADY_CANCELED(
					"The order is allready canceled"), ORDER_IS_AUTHORIZED(
							"The order is allready authorized"), TRACKING_NUMBER_DOES_NOT_EXIST(
									"Tracking number does not exist"), THE_ORDER_IS_ALLREADY_CAPTUARED(
											"The order is allready captuared"), CAN_NOT_CANCEL_CAPTUARED_ORDER(
													"Can not cancel captuared order please refund first"), THE_ORDER_IS_ALLREADY_REFUNDED(
															"The order is allready refunded or refund limit exceeds"), THE_ORDER_IS_NOT_AUTHORIZED(
																	"The order is not authorized"), THE_ORDER_IS_NOT_CAPTUARED(
																			"The order is not captuared yet."), UNKNOWN_ORDER_STATUS(
																					"The order status  not unknown"), INVALID_ORDER_STATUS(
																							"Invalid order status ");

	String message;


	private PaymentExceptionType(final String message)
	{
		this.message = message;

	}


	public String getMessage()
	{
		return message;
	}


}
