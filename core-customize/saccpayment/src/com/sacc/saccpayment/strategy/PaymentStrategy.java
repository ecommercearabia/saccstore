/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.strategy;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;
import java.util.Optional;

import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.hyperpay.exception.HyperpayPaymentException;
import com.sacc.saccpayment.model.PaymentProviderModel;


/**
 * The Interface PaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Interface PaymentStrategy.
 */
public interface PaymentStrategy
{

	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 * @throws PaymentException
	 */
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrder) throws PaymentException;

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the optional
	 */
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, final Object data);

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			PaymentProviderModel paymentProviderModel);



	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final Map<String, Object> responseParams, PaymentProviderModel paymentProviderModel);

	/**
	 * Interpret response.
	 *
	 * @param order
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			PaymentProviderModel paymentProviderModel);


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(PaymentProviderModel paymentProviderModel, AbstractOrderModel abstractOrderModel,
			Object data);

	public boolean isSuccessfulPaidOrder(PaymentProviderModel paymentProviderModel, AbstractOrderModel abstractOrderModel);

	public boolean isWebHookNotificationPending(final Map<String, Object> payload) throws HyperpayPaymentException;

	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException;

	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response);

}
