package com.sacc.saccpayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.hyperpay.enums.ShippingMethod;
import com.sacc.saccpayment.hyperpay.enums.TestMode;
import com.sacc.saccpayment.hyperpay.enums.TransactionStatus;
import com.sacc.saccpayment.hyperpay.exception.ExceptionType;
import com.sacc.saccpayment.hyperpay.exception.HyperpayPaymentException;
import com.sacc.saccpayment.hyperpay.model.HyperpayPaymentData;
import com.sacc.saccpayment.hyperpay.response.ResultCode;
import com.sacc.saccpayment.hyperpay.service.HyperpayCheckoutIdService;
import com.sacc.saccpayment.hyperpay.service.HyperpayService;
import com.sacc.saccpayment.model.HyperpayPaymentModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.service.records.PaymentTransactionRecordService;
import com.sacc.saccpayment.strategy.CustomPaymentTransactionStrategy;
import com.sacc.saccpayment.strategy.PaymentStrategy;


/**
 * The Class DefaultHyperpayPaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultHyperpayPaymentStrategy.
 */
public class DefaultHyperpayPaymentStrategy implements PaymentStrategy
{

	private static final String DESCRIPTION = "description";
	private static final String CODE = "code";
	private static final String RESULT = "result";
	private static final String GET_PAYMENT_STATUS = "GET_PAYMENT_STATUS";
	private static final String FAILED = "FAILED";
	private static final String ID = "id";
	private static final String AMOUNT = "amount";

	/** The Constant CITY_STATE. */
	private static final String CITY_STATE = "Riyadh";

	/** The Constant STREET_1. */
	private static final String STREET_1 = "Prince Sultan Road - Al Muhammadiyah District 5";

	private static final String COUNTRY_AE_ISOCODE = "AE";

	/** The default Post code. */
	private static final String POSTCODE = "11564";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHyperpayPaymentStrategy.class);

	/** The Constant THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL. */
	private static final String THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL = "the provider moder is not a HyperpayPaymentProviderModel ";


	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	/** The payment provider service. */
	@Resource(name = "hyperpayService")
	private HyperpayService hyperpayService;


	/** The cart service. */
	@Resource(name = "cartService")
	private CartService cartService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "hyperpayCheckoutIdService")
	private HyperpayCheckoutIdService hyperpayCheckoutIdService;

	/** The create subscription result converter. */
	@Resource(name = "hyperpayCreateSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;

	@Resource(name = "hyperpayCreateSubscriptionResultForAbstractOrderConverter")
	private Converter<AbstractOrderModel, CreateSubscriptionResult> createSubscriptionResultAbstractOrderConverter;

	/**
	 * @return the createSubscriptionResultAbstractOrderConverter
	 */
	protected Converter<AbstractOrderModel, CreateSubscriptionResult> getCreateSubscriptionResultAbstractOrderConverter()
	{
		return createSubscriptionResultAbstractOrderConverter;
	}



	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/** The payment transaction record service. */
	@Resource(name = "paymentTransactionRecordService")
	private PaymentTransactionRecordService paymentTransactionRecordService;

	/**
	 * Gets the creates the subscription result converter.
	 *
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}



	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";


	/** The Constant ABSTRACTORDER_MUSTN_T_BE_NULL. */
	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";
	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	private static final String ORDER_MUSTN_T_BE_NULL = "order mustn't be null";


	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data mustn't be null";

	/** The Constant ID_CAN_NOT_BE_NULL. */
	private static final String ID_CAN_NOT_BE_NULL = "id mustn't be null";

	/** The key generator. */
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;


	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 * @throws PaymentException
	 */
	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel, ABSTRACTORDER_MUSTN_T_BE_NULL);

		LOG.info("DefaultHyperpayPaymentStrategy : Building the Payment Request Data to prepare the checkout by Hyperpay");
		final HyperpayPaymentData requestData = getRequestData(paymentProviderModel, abstractOrder);

		try
		{

			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
			validateCheckout(hyperpayPaymentProviderModel, abstractOrder);
			final Map<String, Object> prepareCheckout = hyperpayService.prepareCheckout(
					hyperpayPaymentProviderModel.getAccessToken(), hyperpayPaymentProviderModel.getEntityId(),
					hyperpayPaymentProviderModel.getCheckoutsURL(), requestData);

			abstractOrder.setRequestPaymentBody(requestData.toString());
			abstractOrder.setResponsePaymentBody(String.valueOf(prepareCheckout));
			modelService.save(abstractOrder);

			if (prepareCheckout != null && prepareCheckout.get(ID) != null)
			{
				paymentTransactionRecordService.savePaymentRecords("PREPARE_CHECKOUT", extractResultDescription(prepareCheckout),
						requestData.toString(), prepareCheckout.toString(), extractResponseCode(prepareCheckout), abstractOrder);
				final String id = (String) prepareCheckout.get(ID);
				getHyperpayCheckoutIdService().createHyperpayCheckoutId(id, abstractOrder.getOrderCode(), abstractOrder.getCode());
				LOG.info("DefaultHyperpayPaymentStrategy : Checkout prepared successfully with checkoutId : {}", id);
				modelService.refresh(abstractOrder);
				final Set<String> hyperpayCheckoutIds = new HashSet<>(abstractOrder.getHyperpayCheckoutIds());
				hyperpayCheckoutIds.add(id);
				abstractOrder.setHyperpayCheckoutIds(hyperpayCheckoutIds);
				LOG.info("DefaultHyperpayPaymentStrategy : Checkout response : {}", prepareCheckout);
				modelService.save(abstractOrder);
				final PaymentRequestData paymentRequestData = new PaymentRequestData(hyperpayPaymentProviderModel.getSrcForm() + id,
						HyperpayPaymentProviderModel._TYPECODE, hyperpayPaymentProviderModel, id);
				return Optional.ofNullable(paymentRequestData);

			}
		}
		catch (final PaymentException e)
		{
			LOG.error(this.getClass() + " : " + e.getMessage(), e);
			paymentTransactionRecordService.savePaymentRecords("PREPARE_CHECKOUT", "NOT_PREPARED", requestData.toString(),
					e.toString(), FAILED, abstractOrder);

			if (e instanceof HyperpayPaymentException
					&& ExceptionType.ORDER_ALREADY_PAID.equals(((HyperpayPaymentException) e).getType()))
			{
				throw e;
			}

			return Optional.empty();
		}


		return Optional.empty();
	}

	/**
	 * @throws HyperpayPaymentException
	 *
	 */
	private Map<String, Object> validateCheckout(final HyperpayPaymentProviderModel hyperpayPaymentProviderModel,
			final AbstractOrderModel abstractOrder) throws HyperpayPaymentException
	{

		Map<String, Object> lastResult = MapUtils.EMPTY_MAP;
		if (!CollectionUtils.isEmpty(abstractOrder.getHyperpayCheckoutIds()))
		{
			lastResult = getSuccessPaymentFromCheckoutId(abstractOrder.getHyperpayCheckoutIds(), hyperpayPaymentProviderModel,
					abstractOrder);
		}

		if (CollectionUtils.isEmpty(abstractOrder.getHyperpayPayments()))
		{
			return lastResult;
		}
		abstractOrder.setPaymentStatus(PaymentStatus.NOTPAID);
		for (final HyperpayPaymentModel hyperpayPaymentModel : abstractOrder.getHyperpayPayments())
		{
			checkSuccessPayment(hyperpayPaymentModel, abstractOrder, hyperpayPaymentProviderModel);
		}

		modelService.refresh(abstractOrder);
		if (PaymentStatus.PAID.equals(abstractOrder.getPaymentStatus()))
		{
			final String message = "order[" + abstractOrder.getCode() + "] is already paid";
			throw new HyperpayPaymentException(ExceptionType.ORDER_ALREADY_PAID, message);
		}
		abstractOrder.setPaymentReferenceId(Strings.EMPTY);
		abstractOrder.setResponsePaymentBody(Strings.EMPTY);
		abstractOrder.setHyperpayCheckoutMap(null);
		return lastResult;
	}

	/**
	 * @throws HyperpayPaymentException
	 *
	 */
	private void checkSuccessPayment(final HyperpayPaymentModel hyperpayPaymentModel, final AbstractOrderModel order,
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel) throws HyperpayPaymentException
	{


		if (Strings.isBlank(hyperpayPaymentModel.getPaymentRef()) || hyperpayPaymentModel.isRefunded())
		{
			return;
		}


		final Map<String, Object> checkoutStatus = hyperpayService.searchForTransactionById(
				hyperpayPaymentProviderModel.getAccessToken(), hyperpayPaymentProviderModel.getEntityId(),
				hyperpayPaymentProviderModel.getQueryURL(), hyperpayPaymentModel.getPaymentRef());
		if (!MapUtils.isEmpty(checkoutStatus) && PaymentResponseStatus.SUCCESS.equals(isSuccessfullPaymnetOrder(checkoutStatus))
				&& checkoutStatus.get(AMOUNT) != null
				&& Double.valueOf((String) checkoutStatus.get(AMOUNT)).equals(getTotalPrice(order))
				&& !PaymentStatus.PAID.equals(order.getPaymentStatus()))

		{
			order.setPaymentStatus(PaymentStatus.PAID);
			order.setPaymentReferenceId(hyperpayPaymentModel.getPaymentRef());
			order.setResponsePaymentBody(checkoutStatus.toString());
			order.setHyperpayCheckoutMap(checkoutStatus);
		}
		else if (!MapUtils.isEmpty(checkoutStatus)
				&& PaymentResponseStatus.SUCCESS.equals(isSuccessfullPaymnetOrder(checkoutStatus)))
		{

			hyperpayPaymentModel.setRefunded(refundTransaction(order, hyperpayPaymentProviderModel,
					String.valueOf(checkoutStatus.get(ID)), String.valueOf(checkoutStatus.get(AMOUNT))));
		}

		modelService.saveAll(hyperpayPaymentModel, order);
		modelService.refresh(order);
	}

	/**
	 * @param hyperpayPaymentProviderModel
	 * @return
	 * @throws HyperpayPaymentException
	 *
	 */
	private Map<String, Object> getSuccessPaymentFromCheckoutId(final Collection<String> hyperpayCheckoutIds,
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel, final AbstractOrderModel order)
	{
		final Set<String> checkoutIds = new HashSet<>(hyperpayCheckoutIds);
		final List<HyperpayPaymentModel> successTransactions = new LinkedList<>(order.getHyperpayPayments());
		Map<String, Object> checkoutMap = null;
		for (final Iterator<String> iterator = checkoutIds.iterator(); iterator.hasNext();)
		{
			final String id = iterator.next();
			try
			{

				final Optional<PaymentResponseData> paymentResponseData = getPaymentResponseData(order, id,
						hyperpayPaymentProviderModel);

				checkoutMap = paymentResponseData.isPresent() ? paymentResponseData.get().getResponseData() : MapUtils.EMPTY_MAP;

				if (paymentResponseData.isPresent() && paymentResponseData.get().getResponseData() != null
						&& PaymentResponseStatus.SUCCESS.equals(isSuccessfullPaymnetOrder(paymentResponseData.get().getResponseData())))
				{
					successTransactions.add(getHyperpayPayment(paymentResponseData.get().getResponseData(), id));
				}

				iterator.remove();
			}
			catch (final Exception e)
			{
				LOG.error(e.getMessage());
			}
		}
		order.setHyperpayCheckoutIds(checkoutIds);
		final List<HyperpayPaymentModel> hyperpayPaymentModels = new LinkedList<>(successTransactions);
		order.setHyperpayPayments(hyperpayPaymentModels);
		modelService.save(order);
		modelService.refresh(order);

		return checkoutMap;

	}





	/**
	 *
	 */
	private HyperpayPaymentModel getHyperpayPayment(final Map<String, Object> checkoutStatus, final String checkoutId)
	{
		final HyperpayPaymentModel hyperpayPaymentModel = modelService.create(HyperpayPaymentModel.class);
		hyperpayPaymentModel.setAmount(Double.valueOf(String.valueOf(checkoutStatus.get(AMOUNT))));
		hyperpayPaymentModel.setCheckoutId(checkoutId);
		hyperpayPaymentModel.setRefunded(false);
		hyperpayPaymentModel.setPaymentRef(String.valueOf(checkoutStatus.get(ID)));
		hyperpayPaymentModel.setMerchantTransactionId(String.valueOf(checkoutStatus.get("merchantTransactionId")));
		modelService.save(hyperpayPaymentModel);
		return hyperpayPaymentModel;
	}

	/**
	 * @param hyperpayPaymentProviderModel
	 * @throws HyperpayPaymentException
	 *
	 */
	private boolean refundTransaction(final AbstractOrderModel order,
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel, final String paymentId) throws HyperpayPaymentException
	{
		if (Strings.isBlank(paymentId))
		{
			return false;
		}

		final Map<String, Object> refundPayment = hyperpayService.refundPayment(hyperpayPaymentProviderModel.getAccessToken(),
				hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getPaymentsURL(),
				getRequestData(hyperpayPaymentProviderModel, order), paymentId);


		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(refundPayment));
	}

	private boolean refundTransaction(final AbstractOrderModel order,
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel, final String paymentId, final String amount)
			throws HyperpayPaymentException
	{
		if (Strings.isBlank(paymentId) || Strings.isBlank(amount))
		{
			return false;
		}
		double amountDouble = 0;
		try
		{
			amountDouble = Double.valueOf(amount);
		}
		catch (final NumberFormatException ex)
		{
			LOG.error("amount is not valid");
			return false;
		}

		final HyperpayPaymentData requestData = getRequestData(hyperpayPaymentProviderModel, order);

		if (hyperpayPaymentProviderModel.isRound())
		{
			requestData.setAmount(round(amountDouble, hyperpayPaymentProviderModel.getCurrency()));
		}
		else
		{
			requestData.setAmount((toDigits(amountDouble, hyperpayPaymentProviderModel.getCurrency())));
		}
		final Map<String, Object> refundPayment = hyperpayService.refundPayment(hyperpayPaymentProviderModel.getAccessToken(),
				hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getPaymentsURL(), requestData, paymentId);


		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(refundPayment));
	}

	/**
	 * Generate order code.
	 *
	 * @return the string
	 */
	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the optional
	 */
	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		LOG.info("DefaultHyperpayPaymentStrategy : Building Payment Response Data via PaymentCheckoutStatus");

		PaymentResponseData paymentResponseData = null;

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		try
		{
			final Map<String, Object> validateCheckout = validateCheckout(hyperpayPaymentProviderModel, abstractOrder);
			paymentResponseData = new PaymentResponseData(validateCheckout, HyperpayPaymentProviderModel._TYPECODE, null);


		}
		catch (final HyperpayPaymentException e)
		{
			LOG.warn(e.getMessage());
			paymentResponseData = new PaymentResponseData(abstractOrder.getHyperpayCheckoutMap(),
					HyperpayPaymentProviderModel._TYPECODE, null);
		}

		return Optional.ofNullable(paymentResponseData);
	}

	private Optional<PaymentResponseData> getPaymentResponseData(final AbstractOrderModel abstractOrder, final String id,
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel)
	{
		Map<String, Object> checkoutStatus = null;
		LOG.info("CheckoutId: {}", id);
		try
		{
			checkoutStatus = hyperpayService.getCheckoutStatus(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getCheckoutsURL(), id);

			LOG.info("DefaultHyperpayPaymentStrategy : PaymentCheckoutStatus response is : {}", checkoutStatus);
			paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, extractResultDescription(checkoutStatus),
					"[PaymentId : ]" + id, checkoutStatus.toString(), extractResponseCode(checkoutStatus), abstractOrder);
			if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get(ID) != null)
			{
				abstractOrder.setPaymentReferenceId((String) checkoutStatus.get(ID));
				modelService.save(abstractOrder);

				final PaymentResponseStatus paymentResponseStatus = isSuccessfullPaymnetOrder(checkoutStatus);
				LOG.info("DefaultHyperpayPaymentStrategy : Payment Response Status is : " + paymentResponseStatus);

				final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
								? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
								: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status is : " + transactionStatus);

				final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
								: TransactionStatusDetails.REVIEW_NEEDED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status Details is : " + transactionStatusDetails);

				final String requestPaymentBody = "Payment Id= [" + (String) checkoutStatus.get(ID) + "]";
				final String responsePaymentBody = checkoutStatus.toString();
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, (String) checkoutStatus.get(ID),
						PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
						responsePaymentBody, null, hyperpayPaymentProviderModel.getCode());

				final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
						HyperpayPaymentProviderModel._TYPECODE, null);
				return Optional.ofNullable(paymentResponseData);
			}
			else
			{
				LOG.error("DefaultHyperpayPaymentStrategy : checkout response is empty or does nothave an id");
				final String requestPaymentBody = "Payment Id= [" + id + "]";
				final String responsePaymentBody = checkoutStatus.toString();

				paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, extractResultDescription(checkoutStatus),
						"[PaymentId : ]" + id, checkoutStatus.toString(), extractResponseCode(checkoutStatus), abstractOrder);

				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
						"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null,
						hyperpayPaymentProviderModel.getCode());

				return Optional.empty();
			}
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = "Payment Id= [" + id + "]";
			final String responsePaymentBody = checkoutStatus.toString();

			paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, extractResultDescription(checkoutStatus),
					"[PaymentId : " + id + "]", ExceptionUtils.getStackTrace(e), extractResponseCode(checkoutStatus), abstractOrder);

			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null, hyperpayPaymentProviderModel.getCode());
			LOG.error("DefaultHyperpayPaymentStrategy : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	/**
	 * To digits.
	 *
	 * @param d
	 *           the d
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String toDigits(final double d, final CurrencyModel currency)
	{
		final int digits = currency.getDigits() == null ? 0 : currency.getDigits().intValue();
		final String value = String.format("%." + digits + "f", d);
		return value;
	}

	/**
	 * Round.
	 *
	 * @param num
	 *           the num
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String round(final double num, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return "" + num;
		}
		final int digits = currency.getDigits() == null ? 0 : currency.getDigits().intValue();
		BigDecimal decimal = BigDecimal.valueOf(num);
		decimal = decimal.setScale(digits, RoundingMode.HALF_UP);
		return decimal.toString();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the order
	 * @return the response data
	 */
	protected HyperpayPaymentData getRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel,
				THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL);
		Preconditions.checkArgument(commonI18NService.getCurrentLanguage() != null, "CurrentLanguage is null");

		if (StringUtils.isBlank(abstractOrderModel.getOrderCode()))
		{
			abstractOrderModel.setOrderCode(generateOrderCode());
			modelService.save(abstractOrderModel);
		}

		final HyperpayPaymentProviderModel providerModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final AddressModel paymentAddress = abstractOrderModel.getPaymentAddress();
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		final HyperpayPaymentData data = new HyperpayPaymentData();

		final Double totalPrice = getTotalPrice(abstractOrderModel);
		final Double taxAmount = getTaxAmountPrice(abstractOrderModel);
		final boolean round = providerModel.isRound();
		if (round)
		{
			data.setAmount(round(totalPrice.doubleValue(), providerModel.getCurrency()));
			data.setTaxAmount(round(taxAmount.doubleValue(), providerModel.getCurrency()));
		}
		else
		{
			data.setAmount((totalPrice == null ? null : toDigits(totalPrice.doubleValue(), providerModel.getCurrency())));
			data.setTaxAmount((taxAmount == null ? null : toDigits(taxAmount.doubleValue(), providerModel.getCurrency())));
		}
		final Collection<String> merchantTransactions = abstractOrderModel.getMerchantTransactions();
		String merchantTransaction = abstractOrderModel.getOrderCode() + "_";
		if (CollectionUtils.isEmpty(merchantTransactions))
		{
			merchantTransaction += String.valueOf(0);
		}
		else
		{
			merchantTransaction += String.valueOf(merchantTransactions.size());
		}
		addMerchantTranscation(abstractOrderModel, merchantTransaction);

		data.setMerchantTransactionId(merchantTransaction);
		final CustomerModel customerModel = (de.hybris.platform.core.model.user.CustomerModel) abstractOrderModel.getUser();
		data.setEntityId(providerModel.getEntityId());

		data.setCurrency(providerModel.getCurrency().getIsocode());
		data.setPaymentType(providerModel.getPaymentType());

		if (round)
		{
			data.setTestMode(TestMode.INTERNAL.toString().equalsIgnoreCase(providerModel.getTestMode()) ? TestMode.INTERNAL
					: TestMode.EXTERNAL);
		}
		data.setCustomerMerchantCustomerId(abstractOrderModel.getCode());

		data.setCustomerPhone(customerModel.getMobileNumber());
		data.setCustomerMobile(customerModel.getMobileNumber());
		data.setCustomerWorkPhone(customerModel.getMobileNumber());
		data.setCustomerEmail(customerModel.getContactEmail());

		if (paymentAddress != null)
		{
			data.setBillingPostCode(POSTCODE);
			data.setBillingCountry(COUNTRY_AE_ISOCODE);
			data.setBillingStreet1(STREET_1);
			data.setBillingStreet2(STREET_1);
			data.setBillingCity(CITY_STATE);
			data.setBillingState(CITY_STATE);
		}
		if (deliveryAddress != null)
		{
			data.setShippingStreet1(STREET_1);
			data.setShippingStreet2(STREET_1);
			data.setShippingPostCode("00000");
			data.setShippingCountry(COUNTRY_AE_ISOCODE);
			data.setShippingMethod(ShippingMethod.OTHER);

			data.setCustomerGivenName("A");
			data.setCustomerSurname("A");
		}

		if (round)
		{
			data.setShippingCost(round(abstractOrderModel.getDeliveryCost(), providerModel.getCurrency()));
		}
		else
		{
			data.setShippingCost((abstractOrderModel.getDeliveryCost() == null ? null
					: toDigits(abstractOrderModel.getDeliveryCost().doubleValue(), providerModel.getCurrency())));
		}

		return data;
	}

	/**
	 *
	 */
	private void addMerchantTranscation(final AbstractOrderModel abstractOrderModel, final String merchantTransaction)
	{

		final Set<String> merchantTransactions = new HashSet<>(abstractOrderModel.getMerchantTransactions());
		merchantTransactions.add(merchantTransaction);
		abstractOrderModel.setMerchantTransactions(merchantTransactions);
		modelService.save(abstractOrderModel);
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = abstractOrderModel.getTotalPrice();
			return amount;
		}

		amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(), abstractOrderModel.getTotalPrice());

		return amount;
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTaxAmountPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = abstractOrderModel.getTotalTax();
			return amount;
		}

		amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(), abstractOrderModel.getTotalTax());


		return amount;
	}



	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final String id = (String) data;
		final PaymentResponseStatus responseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
				hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);

		return PaymentResponseStatus.SUCCESS.equals(responseStatus);
	}

	/**
	 * Gets the payment order status response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final String id = data == null ? abstractOrder.getPaymentReferenceId() : (String) data;
		Preconditions.checkArgument(id != null, ID_CAN_NOT_BE_NULL);

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		Map<String, Object> checkoutStatus = null;
		try
		{
			checkoutStatus = hyperpayService.searchForTransactionById(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);

			if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get(ID) != null)
			{
				abstractOrder.setPaymentReferenceId((String) checkoutStatus.get(ID));
				modelService.save(abstractOrder);

				paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY",
						extractResultDescription(checkoutStatus), "[PaymentId : " + id + "]", checkoutStatus.toString(),
						extractResponseCode(checkoutStatus), abstractOrder);

				final PaymentResponseStatus paymentResponseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
						hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(),
						abstractOrder.getPaymentReferenceId());
				LOG.info("DefaultHyperpayPaymentStrategy : Payment Response Status By Payment Id "
						+ abstractOrder.getPaymentReferenceId() + " is : " + paymentResponseStatus);

				final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
								? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
								: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status By Payment Id " + abstractOrder.getPaymentReferenceId()
						+ " is : " + transactionStatus);

				final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
								: TransactionStatusDetails.REVIEW_NEEDED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status Details By Payment Id "
						+ abstractOrder.getPaymentReferenceId() + " is : " + transactionStatusDetails);

				final String requestPaymentBody = "Payment Id= [" + abstractOrder.getPaymentReferenceId() + "]";
				final String responsePaymentBody = checkoutStatus.toString();

				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
						responsePaymentBody, null, paymentProviderModel.getCode());

				modelService.refresh(abstractOrder);
				abstractOrder.setResponsePaymentBody(checkoutStatus.toString());
				modelService.save(abstractOrder);

				final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
						HyperpayPaymentProviderModel._TYPECODE, null);
				LOG.info("DefaultHyperpayPaymentStrategy : transaction Response Data is : " + paymentResponseData);
				return Optional.ofNullable(paymentResponseData);
			}
			else
			{
				LOG.error(
						"DefaultHyperpayPaymentStrategy : Transaction Response Data By Payment Id is failed due to empty response ormissing Id");
				paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY",
						extractResultDescription(checkoutStatus), "[PaymentId : " + id + "]", checkoutStatus.toString(),
						extractResponseCode(checkoutStatus), abstractOrder);
				final String requestPaymentBody = "Payment Id= [" + id + "]";
				final String responsePaymentBody = checkoutStatus.toString();
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
						"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null,
						hyperpayPaymentProviderModel.getCode());

				return Optional.empty();
			}
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = "Payment Id= [" + id + "]";
			final String responsePaymentBody = checkoutStatus.toString();

			paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY", extractResultDescription(checkoutStatus),
					"[PaymentId : " + id + "]", ExceptionUtils.getStackTrace(e), extractResponseCode(checkoutStatus), abstractOrder);

			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null, hyperpayPaymentProviderModel.getCode());
			LOG.error("DefaultHyperpayPaymentStrategy : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	/**
	 * Capture order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Cancel order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Gets the response status.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param queryURL
	 *           the query URL
	 * @param paymentId
	 *           the payment id
	 * @return the response status
	 */
	private PaymentResponseStatus getResponseStatus(final String accessToken, final String entityId, final String queryURL,
			final String paymentId)
	{
		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(accessToken, entityId, queryURL, paymentId))
				? PaymentResponseStatus.SUCCESS
				: PaymentResponseStatus.FAILURE;
	}

	private PaymentResponseStatus isSuccessfullPaymnetOrder(final Map<String, Object> response)
	{
		final TransactionStatus transactionStatus = hyperpayService.getTransactionStatus(response);

		return TransactionStatus.SUCCESS.equals(transactionStatus) ? PaymentResponseStatus.SUCCESS : PaymentResponseStatus.FAILURE;
	}


	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response)
	{
		return isSuccessfullPaymnetOrder(response);
	}

	/**
	 * Refund order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Extract response code.
	 *
	 * @param response
	 *           the response
	 * @return the string
	 */
	private String extractResponseCode(final Map<String, Object> response)
	{
		return response.get(RESULT) != null && ((Map<String, Object>) response.get(RESULT)).get(CODE) != null
				? String.valueOf(((Map<String, Object>) response.get(RESULT)).get(CODE))
				: FAILED;
	}

	/**
	 * Extract result description.
	 *
	 * @param response
	 *           the response
	 * @return the string
	 */
	private String extractResultDescription(final Map<String, Object> response)
	{
		return response.get(RESULT) != null && ((Map<String, Object>) response.get(RESULT)).get(DESCRIPTION) != null
				? String.valueOf(((Map<String, Object>) response.get(RESULT)).get(DESCRIPTION))
				: FAILED;
	}


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null)
		{
			return false;
		}
		if (StringUtils.isBlank(abstractOrderModel.getPaymentReferenceId()))
		{
			return false;
		}
		if (CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions()))
		{
			return false;
		}
		for (final PaymentTransactionModel transaction : abstractOrderModel.getPaymentTransactions())
		{
			LOG.info("THE SAME TRANSACTION? : " + transaction.getRequestId().equals(abstractOrderModel.getPaymentReferenceId()));
			if (transaction.getRequestId().equals(abstractOrderModel.getPaymentReferenceId()))
			{
				final Optional<PaymentTransactionEntryModel> successTransaction = transaction.getEntries().stream()
						.filter(entry -> de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED.name()
								.equals(entry.getTransactionStatus()) && PaymentTransactionType.REVIEW_DECISION.equals(entry.getType()))
						.findAny();
				return successTransaction.isPresent();
			}
		}
		return false;
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(order != null, ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultAbstractOrderConverter()
				.convert(order);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	@Override
	public boolean isWebHookNotificationPending(final Map<String, Object> payload) throws HyperpayPaymentException
	{
		Preconditions.checkArgument(payload != null, "Payload Must not be null");
		final String resultString = RESULT;
		final String codeString = CODE;


		final Map<String, Object> result = (Map<String, Object>) payload.getOrDefault(resultString, null);
		if (result == null)
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, "Pay Load does not contain 'result' object");
		}

		final String code = (String) result.getOrDefault(codeString, Strings.EMPTY);

		return ResultCode.TRANSACTION_PENDING.equals(ResultCode.getResultByCode(code));
	}

	/**
	 * @return the hyperpayCheckoutIdService
	 */
	public HyperpayCheckoutIdService getHyperpayCheckoutIdService()
	{
		return hyperpayCheckoutIdService;
	}


	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final Map<String, Object> responseParams, final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(order != null, ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);

		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

}
