/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotSupportedException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.model.PayfortPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.model.RefundedPaymentModel;
import com.sacc.saccpayment.payfort.bean.PayfortConnectionBean;
import com.sacc.saccpayment.payfort.entry.form.TokenizationForm;
import com.sacc.saccpayment.payfort.entry.request.AuthorizePurchaseRequestBean;
import com.sacc.saccpayment.payfort.entry.request.CheckStatusRequestBean;
import com.sacc.saccpayment.payfort.entry.request.RefundRequestBean;
import com.sacc.saccpayment.payfort.entry.request.TokenizationRequestBean;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;
import com.sacc.saccpayment.payfort.entry.response.CheckStatusResponse;
import com.sacc.saccpayment.payfort.entry.response.RefundResponseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;
import com.sacc.saccpayment.payfort.enums.PayfortExceptionType;
import com.sacc.saccpayment.payfort.enums.PayfortHashingType;
import com.sacc.saccpayment.payfort.enums.PayfortResponseMsg;
import com.sacc.saccpayment.payfort.enums.PayfortResponseStatus;
import com.sacc.saccpayment.payfort.exception.PayfortException;
import com.sacc.saccpayment.payfort.service.PayfortService;
import com.sacc.saccpayment.payfort.util.PayfortHashingUtil;
import com.sacc.saccpayment.service.records.PaymentTransactionRecordService;
import com.sacc.saccpayment.strategy.CustomPaymentTransactionStrategy;
import com.sacc.saccpayment.strategy.PaymentStrategy;



/**
 *
 */
public class DefaultPayfortPaymentStrategy implements PaymentStrategy
{
	private static final String RESPONSE_CODE = "response_code";
	private static final String SIGNATURE = "signature";
	protected static final String THREE_D_SECURE_URL = "3dsecureUrl";
	protected static final String RESPONSE_CODE_MAP_KEY = "responseCode";
	protected static final String RESPONSE_CODE_KEY = RESPONSE_CODE;
	protected static final String STATUS_KEY = "status";
	protected static final String COMMAND_KEY = "command";
	protected static final String SERVICE_COMMAND_KEY = "service_command";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPayfortPaymentStrategy.class);

	protected static final String INVALID_PROVIDER_MSG = "Invalid provider";
	protected static final String ABSTRACT_ORDER_NULL_MSG = "Abstract Order is null";

	protected static final String BASE_URL_PROPERTY = "website.skysales-sa.https";

	@Resource(name = "payfortService")
	private PayfortService payfortService;

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The key generator. */
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/** The payment transaction record service. */
	@Resource(name = "paymentTransactionRecordService")
	private PaymentTransactionRecordService paymentTransactionRecordService;

	/** The create subscription result converter. */
	@Resource(name = "payfortCreateSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;


	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder) throws PaymentException
	{
		Preconditions.checkArgument(Objects.nonNull(abstractOrder), ABSTRACT_ORDER_NULL_MSG);
		final PayfortPaymentProviderModel payfortProvider = getPayfortProvider(paymentProviderModel);
		final PayfortConnectionBean connectionBean = getConnectionBean(payfortProvider);
		connectionBean.setReturnUrl(getBaseURL().concat(payfortProvider.getPaymentRedirectUrl()));

		final TokenizationForm tokenizationForm = getTokenizationForm(abstractOrder, payfortProvider);
		try
		{
			final Optional<TokenizationRequestBean> prepareTokenizationForm = getPayfortService()
					.prepareTokenizationForm(connectionBean, tokenizationForm);
			if (prepareTokenizationForm.isPresent())
			{
				return getPaymentRequestData(prepareTokenizationForm.get());
			}
			return Optional.empty();
		}
		catch (final PayfortException e)
		{
			LOG.error(e.getMessage());
			throw new PaymentException(e.getMessage());
		}

	}

	/**
	 *
	 */
	protected Optional<PaymentRequestData> getPaymentRequestData(final TokenizationRequestBean tokenizationRequestBean)
	{
		try
		{
			final Map<String, Object> requestMap = getRequestMap(tokenizationRequestBean);
			final PaymentRequestData paymentRequestData = new PaymentRequestData("", PayfortPaymentProviderModel._TYPECODE);
			requestMap.values().remove("null");
			paymentRequestData.setPaymentProviderData(requestMap);

			return Optional.ofNullable(paymentRequestData);
		}
		catch (IllegalArgumentException | IllegalAccessException e)
		{
			LOG.error(e.getMessage());
		}
		return Optional.empty();
	}

	/**
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 *
	 */
	protected Map<String, Object> getRequestMap(final TokenizationRequestBean tokenizationRequestBean)
			throws IllegalArgumentException, IllegalAccessException
	{
		final Map<String, Object> map = new LinkedHashMap<>();
		final List<Field> fields = getObjectFields(tokenizationRequestBean);

		for (final Field field : fields)
		{
			field.setAccessible(true);
			final String key = field.getAnnotation(SerializedName.class).value();
			final String value = String.valueOf(field.get(tokenizationRequestBean));
			map.put(key, value);
			field.setAccessible(false);
		}
		return map;
	}

	protected List<Field> getObjectFields(final Object ob)
	{
		final Class<?> clazz = ob.getClass();
		final List<Field> currentFields = Arrays.asList(clazz.getDeclaredFields());
		final List<Field> superFields = Arrays.asList(clazz.getSuperclass().getDeclaredFields());
		final List<Field> fields = new ArrayList<>();
		fields.addAll(currentFields);
		fields.addAll(superFields);
		return fields;
	}

	protected PayfortPaymentProviderModel getPayfortProvider(final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(
				Objects.nonNull(paymentProviderModel) && paymentProviderModel instanceof PayfortPaymentProviderModel,
				INVALID_PROVIDER_MSG);
		return (PayfortPaymentProviderModel) paymentProviderModel;
	}

	/**
	 * @throws PayfortException
	 *
	 */
	protected TokenizationForm getTokenizationForm(final AbstractOrderModel abstractOrder,
			final PayfortPaymentProviderModel payfortPaymentProviderModel) throws PayfortException
	{
		final TokenizationForm tokenizationForm = new TokenizationForm();
		tokenizationForm.setLanguage(getLang());
		tokenizationForm.setOrderNumber(generatePaymentCode(abstractOrder, payfortPaymentProviderModel));
		return tokenizationForm;
	}

	/**
	 * @throws PayfortException
	 *
	 */
	protected String generatePaymentCode(final AbstractOrderModel abstractOrder,
			final PayfortPaymentProviderModel payfortPaymentProviderModel) throws PayfortException
	{
		if (StringUtils.isBlank(abstractOrder.getOrderCode()))
		{
			abstractOrder.setOrderCode(generateOrderCode());
			getModelService().save(abstractOrder);
			appendToOrderCode(abstractOrder);
		}

		if (isSuccessfulPaidOrder(payfortPaymentProviderModel, abstractOrder))
		{
			throw new PayfortException(PayfortExceptionType.ORDER_ALREADY_PAID.getMsg(), PayfortExceptionType.ORDER_ALREADY_PAID);
		}
		if (isTransactionExist(abstractOrder, payfortPaymentProviderModel))
		{
			appendToOrderCode(abstractOrder);
		}
		return getLastPaymentCode(abstractOrder);
	}

	protected String getLastPaymentCode(final AbstractOrderModel abstractOrder)
	{
		final Collection<String> paymentTransactionCodes = abstractOrder.getPaymentTransactionCodes();
		final List<String> paymentList = new LinkedList<>(paymentTransactionCodes);
		return paymentList.get(paymentList.size() - 1);
	}

	protected void appendToOrderCode(final AbstractOrderModel abstractOrder)
	{
		final Collection<String> paymentTransactionCodes = abstractOrder.getPaymentTransactionCodes();
		final String orderCode = abstractOrder.getOrderCode().concat("_" + paymentTransactionCodes.size());
		final List<String> paymentTransactionCodesList = new LinkedList<>(paymentTransactionCodes);
		paymentTransactionCodesList.add(orderCode);
		abstractOrder.setPaymentTransactionCodes(paymentTransactionCodesList);
		getModelService().save(abstractOrder);
	}

	/**
	 * Generate order code.
	 *
	 * @return the string
	 */
	protected String generateOrderCode()
	{
		final Object generatedValue = getKeyGenerator().generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}

	/**
	 *
	 */
	protected String getBaseURL()
	{
		final String lang = getLang();
		final String site = getCmsSiteService().getCurrentSite() == null ? "" : getCmsSiteService().getCurrentSite().getUid();
		final String siteFormat = Strings.isBlank(site) ? "" : site + "/";

		return getConfigurationService().getConfiguration().getString(BASE_URL_PROPERTY) + "/" + siteFormat + lang + "/";
	}

	protected String getLang()
	{
		return getCommonI18NService().getCurrentLanguage() == null ? "en"
				: getCommonI18NService().getCurrentLanguage().getIsocode();
	}

	/**
	 *
	 */
	protected PayfortConnectionBean getConnectionBean(final PayfortPaymentProviderModel payfortProvider)
	{
		final PayfortConnectionBean connectionBean = new PayfortConnectionBean();
		connectionBean.setAccessCode(payfortProvider.getAccessCode());
		connectionBean.setMerchantIdentifier(payfortProvider.getMerchantIdentifier());
		connectionBean.setHashingType(PayfortHashingType.HASH_256);
		connectionBean.setLanguage(getLang());
		connectionBean.setUrl(payfortProvider.getBaseURL());
		connectionBean.setHashingKey(payfortProvider.getShaRequestPhrase());
		return connectionBean;
	}

	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)

	{
		Preconditions.checkArgument(Objects.nonNull(data), "data is null");
		final HttpServletRequest request = (HttpServletRequest) data;
		final PayfortPaymentProviderModel payfortProvider = getPayfortProvider(paymentProviderModel);
		final Map<String, String[]> responseData = request.getParameterMap();
		abstractOrderModel.setPaymentResponseMap(getPayfortReturnMap(request));


		if (!validate(responseData, payfortProvider))
		{
			LOG.error(PayfortExceptionType.SIGNATURE_MISMATCH.getMsg());
			return Optional.empty();
		}

		if (responseData.containsKey(SERVICE_COMMAND_KEY)
				&& PayfortCommandType.TOKENIZATION.toString().equalsIgnoreCase(responseData.get(SERVICE_COMMAND_KEY)[0]))
		{
			try
			{

				return Optional
						.ofNullable(completeTokenization(abstractOrderModel, responseData, payfortProvider, request.getRemoteAddr()));
			}
			catch (final PayfortException e)
			{
				LOG.error(e.getExceptionMessage());
			}
		}
		else if (responseData.containsKey(COMMAND_KEY)
				&& PayfortCommandType.PURCHASE.toString().equalsIgnoreCase(responseData.get(COMMAND_KEY)[0]))
		{

			return completePayment(abstractOrderModel, responseData, payfortProvider);
		}
		return Optional.empty();
	}

	/**
	 * @param httpServletRequest
	 * @return
	 */
	protected Map<String, Object> getPayfortReturnMap(final HttpServletRequest httpServletRequest)
	{
		final Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
		if (MapUtils.isEmpty(parameterMap))
		{
			return MapUtils.EMPTY_MAP;
		}
		final Map<String, Object> newMap = new LinkedHashMap<>();
		for (final Entry<String, String[]> entry : parameterMap.entrySet())
		{
			newMap.put(entry.getKey(), entry.getValue()[0]);

		}
		return newMap;
	}

	/**
	 *
	 */
	protected Optional<PaymentResponseData> completePayment(final AbstractOrderModel abstractOrderModel,
			final Map<String, String[]> responseData, final PayfortPaymentProviderModel payfortProvider)
	{

		final String statusCode = responseData.get(STATUS_KEY)[0];
		final Optional<PayfortResponseStatus> statusFromCode = PayfortResponseStatus.getStatusFromCode(statusCode);
		if (statusFromCode.isPresent() && PayfortResponseStatus.PURCHASE_SUCCESS.equals(statusFromCode.get())
				&& isSuccessfulPaidOrder(payfortProvider, abstractOrderModel))
		{
			return Optional
					.ofNullable(new PaymentResponseData(null, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.SUCCESS));
		}

		appendToOrderCode(abstractOrderModel);
		final Map<String, Object> response = new LinkedHashMap<>();
		if (responseData.containsKey(RESPONSE_CODE_KEY))
		{
			response.put(RESPONSE_CODE_MAP_KEY, (responseData.get(RESPONSE_CODE_KEY)[0]).substring(2));
		}
		return Optional
				.ofNullable(new PaymentResponseData(response, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.FAILURE));

	}

	/**
	 * @param string
	 * @throws PayfortException
	 *
	 */
	protected PaymentResponseData completeTokenization(final AbstractOrderModel abstractOrderModel,
			final Map<String, String[]> responseData, final PayfortPaymentProviderModel paymentProviderModel,
			final String customerIpAddress) throws PayfortException
	{
		if (!responseData.containsKey(STATUS_KEY))
		{
			return new PaymentResponseData(MapUtils.EMPTY_MAP, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.FAILURE);
		}

		final String statusCode = responseData.get(STATUS_KEY)[0];
		final String respnseMsg = responseData.get(RESPONSE_CODE)[0].substring(2);
		final Optional<PayfortResponseMsg> messageFromCode = PayfortResponseMsg.getMessageFromCode(respnseMsg);
		final Optional<PayfortResponseStatus> statusFromCode = PayfortResponseStatus.getStatusFromCode(statusCode);
		if (statusFromCode.isPresent() && PayfortResponseStatus.TOKENIZATION_SUCCESS.equals(statusFromCode.get()))
		{
			return createOrder(abstractOrderModel, paymentProviderModel, responseData.get("token_name")[0], customerIpAddress);
		}
		else if (messageFromCode.isPresent() && PayfortResponseMsg.MERCHANT_REFERENCE_ALREADY_EXISTS.equals(messageFromCode.get())
				&& Strings.isNotBlank(abstractOrderModel.getPayfortPurchaseResponse())
				&& Strings.isNotBlank(abstractOrderModel.getPayfortPurchaseRequest()))
		{
			final AuthorizePurchaseResponseBean responseBean = new Gson().fromJson(abstractOrderModel.getPayfortPurchaseResponse(),
					AuthorizePurchaseResponseBean.class);
			final AuthorizePurchaseRequestBean requestBean = new Gson().fromJson(abstractOrderModel.getPayfortPurchaseRequest(),
					AuthorizePurchaseRequestBean.class);
			return get3dSecureUrl(abstractOrderModel, requestBean, responseBean);


		}
		else if (messageFromCode.isPresent() && PayfortResponseMsg.MERCHANT_REFERENCE_ALREADY_EXISTS.equals(messageFromCode.get()))
		{
			appendToOrderCode(abstractOrderModel);
		}
		final Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put(RESPONSE_CODE_MAP_KEY, respnseMsg);

		return new PaymentResponseData(responseMap, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.FAILURE);

	}

	/**
	 * @param responseData
	 *
	 */
	private boolean validate(final Map<String, String[]> responseData, final PayfortPaymentProviderModel paymentProviderModel)
	{

		final Map<String, Object> response = getResponseMap(responseData);

		try
		{
			final String hashRequest = PayfortHashingUtil.hashRequest(response, PayfortHashingType.HASH_256,
					paymentProviderModel.getShaResponsePhrase());

			return hashRequest.equalsIgnoreCase(responseData.get(SIGNATURE)[0]);
		}
		catch (final PayfortException e)
		{
			LOG.error(e.getExceptionMessage());
		}

		return false;
	}

	/**
	 * @return
	 *
	 */
	private Map<String, Object> getResponseMap(final Map<String, String[]> responseData)
	{

		final Map<String, Object> map = new LinkedHashMap<>();
		for (final Entry<String, String[]> e : responseData.entrySet())
		{
			map.put(e.getKey(), e.getValue()[0]);
		}
		map.remove(SIGNATURE);
		return map;
	}

	/**
	 * @param customerIpAddress
	 * @throws PayfortException
	 *
	 */
	protected PaymentResponseData createOrder(final AbstractOrderModel abstractOrderModel,
			final PayfortPaymentProviderModel paymentProviderModel, final String tokenName, final String customerIpAddress)
			throws PayfortException
	{

		final PayfortConnectionBean connectionBean = getConnectionBean(paymentProviderModel);
		connectionBean.setReturnUrl(getBaseURL().concat(paymentProviderModel.getReturnCallbackURL()));
		final AuthorizePurchaseRequestBean purchaseRequest = getPurchaseRequest(abstractOrderModel, paymentProviderModel,
				customerIpAddress, tokenName);
		final Optional<AuthorizePurchaseResponseBean> purchase = getPayfortService().purchase(connectionBean, purchaseRequest);

		final String requestJson = new Gson().toJson(purchaseRequest);
		abstractOrderModel.setRequestPaymentBody(requestJson);
		abstractOrderModel.setPayfortPurchaseRequest(requestJson);

		getModelService().save(abstractOrderModel);
		if (purchase.isEmpty())
		{

			return new PaymentResponseData(null, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.FAILURE);
		}
		final String responseJson = new Gson().toJson(purchase.get());
		abstractOrderModel.setResponsePaymentBody(responseJson);
		getModelService().save(abstractOrderModel);
		return get3dSecureUrl(abstractOrderModel, purchaseRequest, purchase.get());
	}

	protected PaymentResponseData get3dSecureUrl(final AbstractOrderModel abstractOrderModel,
			final AuthorizePurchaseRequestBean purchaseRequest, final AuthorizePurchaseResponseBean authorizePurchaseResponseBean)
	{
		final Map<String, Object> responseMap = new LinkedHashMap<>();
		final AuthorizePurchaseResponseBean response = authorizePurchaseResponseBean;
		final Optional<PayfortResponseStatus> statusFromCode = PayfortResponseStatus.getStatusFromCode(response.getStatus());
		final Optional<PayfortResponseMsg> messageFromCode = PayfortResponseMsg
				.getMessageFromCode(response.getResponseCode().substring(2));
		if (statusFromCode.isPresent() && PayfortResponseStatus.ON_HOLD.equals(statusFromCode.get()) && messageFromCode.isPresent()
				&& PayfortResponseMsg.THREE_D_SECURE_CHECK_REQUESTED.equals(messageFromCode.get()))
		{
			getPaymentTransactionRecordService().savePaymentRecords(PayfortCommandType.PURCHASE.toString(),
					PaymentResponseStatus.SUCCESS.toString(), purchaseRequest.toString(), authorizePurchaseResponseBean.toString(),
					authorizePurchaseResponseBean.getResponseCode(), abstractOrderModel);
			responseMap.put(THREE_D_SECURE_URL, response.getThreeDSUrl());
			final String responseJson = new Gson().toJson(response);
			abstractOrderModel.setPayfortPurchaseResponse(responseJson);
			getModelService().save(abstractOrderModel);
			return new PaymentResponseData(responseMap, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.SUCCESS);
		}
		else if (statusFromCode.isPresent() && PayfortResponseStatus.PURCHASE_SUCCESS.equals(statusFromCode.get())
				&& messageFromCode.isPresent() && PayfortResponseMsg.SUCCESS.equals(messageFromCode.get()))
		{

			getPaymentTransactionRecordService().savePaymentRecords(PayfortCommandType.PURCHASE.toString(),
					PaymentResponseStatus.SUCCESS.toString(), purchaseRequest.toString(), authorizePurchaseResponseBean.toString(),
					authorizePurchaseResponseBean.getResponseCode(), abstractOrderModel);
			final String responseJson = new Gson().toJson(response);
			abstractOrderModel.setPayfortPurchaseResponse(responseJson);
			getModelService().save(abstractOrderModel);
			return new PaymentResponseData(responseMap, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.SUCCESS);
		}

		getPaymentTransactionRecordService().savePaymentRecords(PayfortCommandType.PURCHASE.toString(),
				PaymentResponseStatus.FAILURE.toString(), purchaseRequest.toString(), authorizePurchaseResponseBean.toString(),
				authorizePurchaseResponseBean.getResponseCode(), abstractOrderModel);
		responseMap.put(RESPONSE_CODE_MAP_KEY, response.getResponseCode().substring(2));

		return new PaymentResponseData(responseMap, PayfortPaymentProviderModel._TYPECODE, PaymentResponseStatus.FAILURE);
	}

	/**
	 *
	 */
	protected boolean isTransactionExist(final AbstractOrderModel order, final PayfortPaymentProviderModel paymentProviderModel)
	{
		final String lastPaymentCode = getLastPaymentCode(order);
		final CheckStatusRequestBean checkStatusBean = getCheckStatusBean(paymentProviderModel, lastPaymentCode);
		final PayfortConnectionBean connectionBean = getConnectionBean(paymentProviderModel);

		Optional<CheckStatusResponse> status = Optional.empty();

		try
		{
			status = getPayfortService().getStatus(connectionBean, checkStatusBean);

		}
		catch (final PayfortException e)
		{
			return false;
		}

		return status.isPresent() && Objects.nonNull(status.get().getCapturedAmount());

	}

	/**
	 * @param customerIpAddress
	 * @param tokenName
	 *
	 */
	protected AuthorizePurchaseRequestBean getPurchaseRequest(final AbstractOrderModel abstractOrderModel,
			final PayfortPaymentProviderModel payfortPaymentProviderModel, final String customerIpAddress, final String tokenName)
	{
		Preconditions.checkArgument(Objects.nonNull(abstractOrderModel.getUser()), "Customer is NULL");
		final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
		final AuthorizePurchaseRequestBean requestBean = new AuthorizePurchaseRequestBean();
		requestBean.setAccessCode(payfortPaymentProviderModel.getAccessCode());
		requestBean.setCurrency(getCurrency(abstractOrderModel));
		requestBean.setCustomerEmail(customer.getContactEmail());
		requestBean.setCustomerName(customer.getDisplayName());
		requestBean.setCustomerIp(customerIpAddress);
		requestBean.setLanguage(getLang());
		requestBean.setMerchantIdentifier(payfortPaymentProviderModel.getMerchantIdentifier());
		final List<String> paymentTransactionCodesList = new LinkedList<>(abstractOrderModel.getPaymentTransactionCodes());

		requestBean.setMerchantReference(paymentTransactionCodesList.get(paymentTransactionCodesList.size() - 1));
		requestBean.setTokenName(tokenName);
		final String amount = String.valueOf(getTotalPrice(abstractOrderModel)).substring(0);
		requestBean.setAmount(amount.indexOf('.') == -1 ? amount : amount.substring(0, amount.indexOf('.')));
		requestBean.setReturnUrl(getBaseURL().concat(payfortPaymentProviderModel.getReturnCallbackURL()));
		requestBean.setPhoneNumber(customer.getMobileNumber());
		return requestBean;
	}


	protected String getCurrency(final AbstractOrderModel abstractOrderModel)
	{

		return Objects.nonNull(abstractOrderModel.getCurrency()) ? abstractOrderModel.getCurrency().getIsocode() : "SA";
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected String getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = getCommonI18NService().getBaseCurrency();
		CurrencyModel currency = null;
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			currency = baseCurrency;
			amount = getCommonI18NService().convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		final double currencyDigits = getCurrencyDigits(currency.getIsocode());
		amount = (amount * Math.pow(10, currencyDigits));
		final String amountStr = String.valueOf(amount);


		return amountStr.indexOf('.') == -1 ? amountStr : amountStr.substring(0, amountStr.indexOf('.'));

	}


	/**
	 * @param isocode
	 * @return number of digits
	 */
	protected double getCurrencyDigits(final String isocode)
	{
		switch (isocode)
		{
			case "JOD":
				return 3;
			case "SAR":
				return 2;
			case "AED":
				return 2;
			case "KWD":
				return 3;
			case "OMR":
				return 3;
			default:
				return 2;
		}
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(MapUtils.isNotEmpty(responseParams), "responseParams is null");
		final String code = String.valueOf(responseParams.get("merchant_reference"));
		final PayfortPaymentProviderModel payfortProvider = getPayfortProvider(paymentProviderModel);
		final PayfortConnectionBean connectionBean = getConnectionBean(payfortProvider);
		Optional<CheckStatusResponse> status;
		try
		{
			status = getPayfortService().getStatus(connectionBean, getCheckStatusBean(payfortProvider, code));
		}
		catch (final PayfortException e)
		{
			LOG.error(e.getExceptionMessage());

			return Optional.empty();
		}

		if (status.isEmpty())
		{
			LOG.error("empty status for code {}", code);
			return Optional.empty();
		}

		final Map<String, Object> orderInfoMap = getMapFromStatus(status.get());
		orderInfoMap.putAll(responseParams);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(orderInfoMap);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);

	}

	/**
	 *
	 */
	protected Map<String, Object> getMapFromStatus(final Object checkStatusResponse)
	{

		final Map<String, Object> map = new LinkedHashMap<>();
		final List<Field> fields = getObjectFields(checkStatusResponse);
		for (final Field field : fields)
		{
			field.setAccessible(true);
			final String key = field.getAnnotation(SerializedName.class) != null ? field.getAnnotation(SerializedName.class).value()
					: null;
			String value;
			try
			{
				value = String.valueOf(field.get(checkStatusResponse));
				map.put(key, value);
			}
			catch (IllegalArgumentException | IllegalAccessException e)
			{
				LOG.error(e.getMessage());
			}

			field.setAccessible(false);

		}

		return map;
	}

	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		return isSuccessfulPaidOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{

		Preconditions.checkArgument(Objects.nonNull(abstractOrderModel), ABSTRACT_ORDER_NULL_MSG);
		final PayfortPaymentProviderModel payfortProvider = getPayfortProvider(paymentProviderModel);

		if (CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactionCodes()))
		{
			return false;
		}
		final Set<String> refundedPayments = abstractOrderModel.getRefundedPayments().stream().map(e -> e.getPaymentRef())
				.collect(Collectors.toSet());
		boolean isPaid = false;
		for (final String code : abstractOrderModel.getPaymentTransactionCodes())
		{

			if (refundedPayments.contains(code))
			{
				continue;
			}
			try
			{
				if (checkPaid(payfortProvider, code, abstractOrderModel))
				{
					isPaid = true;
				}
			}
			catch (final PayfortException e)
			{
				LOG.error(e.getMessage());
			}

		}

		return isPaid;
	}

	/**
	 * @throws PaymentException
	 *
	 */
	protected boolean checkPaid(final PayfortPaymentProviderModel payfortProvider, final String code,
			final AbstractOrderModel order) throws PayfortException
	{
		final PayfortConnectionBean connectionBean = getConnectionBean(payfortProvider);
		final Optional<CheckStatusResponse> status = getPayfortService().getStatus(connectionBean,
				getCheckStatusBean(payfortProvider, code));

		if (status.isEmpty())
		{
			LOG.error("empty status for code {}", code);
			return false;
		}


		final CheckStatusResponse checkStatusResponse = status.get();

		if (Strings.isBlank(checkStatusResponse.getResponseCode()) || Strings.isBlank(checkStatusResponse.getStatus()))
		{
			return false;
		}

		final Optional<PayfortResponseStatus> checkStatus = PayfortResponseStatus
				.getStatusFromCode(checkStatusResponse.getStatus());

		if (checkStatus.isEmpty() || !PayfortResponseStatus.CHECK_STATUS_SUCCESS.equals(checkStatus.get()))
		{
			return false;
		}


		final String transactionStatus = checkStatusResponse.getTransactionStatus();
		final Optional<PayfortResponseStatus> transacrionStatusResult = PayfortResponseStatus.getStatusFromCode(transactionStatus);
		if (transacrionStatusResult.isEmpty() || !PayfortResponseStatus.PURCHASE_SUCCESS.equals(transacrionStatusResult.get()))
		{
			return false;
		}

		if (Strings.isBlank(checkStatusResponse.getCapturedAmount()))
		{
			LOG.error("empty captured status for code {}", code);
			return false;
		}


		final Integer capturedAmount = Integer.valueOf(checkStatusResponse.getCapturedAmount());
		final Integer refundedAmount = Integer.valueOf(checkStatusResponse.getRefundedAmount());


		if (String.valueOf(capturedAmount - refundedAmount).equals(getTotalPrice(order)))
		{
			completeOrder(order, checkStatusResponse);
			return true;
		}

		refundTransaction(payfortProvider, checkStatusResponse, order);


		return true;

	}




	/**
	 * @throws PaymentException
	 *
	 */
	protected void refundTransaction(final PayfortPaymentProviderModel payfortProvider,
			final CheckStatusResponse checkStatusResponse, final AbstractOrderModel order) throws PayfortException
	{

		final RefundedPaymentModel createPaymentRecord = createRefundPayment(checkStatusResponse, order);
		if (!payfortProvider.isRefundEnabled())
		{
			createPaymentRecord.setRefunded(false);
			getModelService().save(createPaymentRecord);
		}

		final Optional<PaymentResponseData> refundOrder = refundOrder(payfortProvider, order);
		if (refundOrder.isEmpty())
		{
			return;
		}

		final PaymentResponseStatus status = refundOrder.get().getStatus();
		if (PaymentResponseStatus.SUCCESS.equals(status))
		{
			createPaymentRecord.setRefunded(true);
			getModelService().save(createPaymentRecord);
		}
	}



	/**
	 *
	 */
	protected RefundedPaymentModel createRefundPayment(final CheckStatusResponse checkStatusResponse,
			final AbstractOrderModel order)
	{
		final RefundedPaymentModel payfortPayment = getModelService().create(RefundedPaymentModel.class);
		payfortPayment.setPaymentRef(checkStatusResponse.getMerchantReference());
		payfortPayment.setAmount(checkStatusResponse.getCapturedAmount());
		payfortPayment.setOrder(order);
		getModelService().save(payfortPayment);

		return payfortPayment;

	}

	/**
	 *
	 */
	protected void completeOrder(final AbstractOrderModel order, final CheckStatusResponse status)
	{
		order.setPaymentStatus(PaymentStatus.PAID);
		order.setPaymentReferenceId(status.getMerchantReference());
		order.setResponsePaymentBody(status.toString());
		getModelService().save(order);
	}

	/**
	 *
	 */
	protected CheckStatusRequestBean getCheckStatusBean(final PayfortPaymentProviderModel payfortProvider,
			final String paymentCode)
	{
		final CheckStatusRequestBean checkStatusRequestBean = new CheckStatusRequestBean();

		checkStatusRequestBean.setAccessCode(payfortProvider.getAccessCode());
		checkStatusRequestBean.setLanguage(getLang());
		checkStatusRequestBean.setMerchantIdentifier(payfortProvider.getMerchantIdentifier());
		checkStatusRequestBean.setMerchantReference(paymentCode);

		return checkStatusRequestBean;
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		throw new NotSupportedException();
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		throw new NotSupportedException();
	}

	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		throw new NotSupportedException();
	}

	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PayfortException
	{
		final PayfortPaymentProviderModel payfortProvider = getPayfortProvider(paymentProviderModel);
		final RefundRequestBean refundRequestBean = getRefundRequestBean(payfortProvider, abstractOrderModel);

		return refund(refundRequestBean, payfortProvider);
	}

	protected Optional<PaymentResponseData> refund(final RefundRequestBean refundRequestBean,
			final PayfortPaymentProviderModel payfortProvider)
	{

		final PayfortConnectionBean connectionBean = getConnectionBean(payfortProvider);
		Optional<RefundResponseBean> refund = Optional.empty();
		try
		{
			refund = getPayfortService().refund(connectionBean, refundRequestBean);
		}
		catch (final PayfortException ex)
		{
			return Optional.ofNullable(new PaymentResponseData(MapUtils.EMPTY_MAP, Strings.EMPTY, PaymentResponseStatus.FAILURE));
		}

		if (refund.isEmpty())
		{
			return Optional.ofNullable(new PaymentResponseData(MapUtils.EMPTY_MAP, Strings.EMPTY, PaymentResponseStatus.FAILURE));

		}
		final Optional<PayfortResponseStatus> statusFromCode = PayfortResponseStatus
				.getStatusFromCode(refund.get().getResponseCode());

		if (statusFromCode.isPresent() && PayfortResponseStatus.REFUND_SUCCESS.equals(statusFromCode.get()))
		{
			return Optional.ofNullable(new PaymentResponseData(MapUtils.EMPTY_MAP, Strings.EMPTY, PaymentResponseStatus.SUCCESS));

		}
		return Optional.ofNullable(new PaymentResponseData(MapUtils.EMPTY_MAP, Strings.EMPTY, PaymentResponseStatus.FAILURE));
	}

	/**
	 *
	 */
	protected RefundRequestBean getRefundRequestBean(final PayfortPaymentProviderModel payfortProvider,
			final AbstractOrderModel abstractOrderModel)
	{
		final RefundRequestBean bean = new RefundRequestBean();
		bean.setAccessCode(payfortProvider.getAccessCode());
		bean.setAmount(getTotalPrice(abstractOrderModel));
		bean.setCurrency(getCurrency(abstractOrderModel));
		bean.setLanguage(getLang());
		bean.setMerchantIdentifier(payfortProvider.getMerchantIdentifier());
		bean.setMerchantReference(getLastPaymentCode(abstractOrderModel));
		return bean;
	}

	/**
	 * @return the payfortService
	 */
	protected PayfortService getPayfortService()
	{
		return payfortService;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the configurationService
	 */
	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @return the keyGenerator
	 */
	protected KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * @return the paymentTransactionStrategy
	 */
	protected CustomPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	/**
	 * @return the paymentTransactionRecordService
	 */
	protected PaymentTransactionRecordService getPaymentTransactionRecordService()
	{
		return paymentTransactionRecordService;
	}

	/**
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final PaymentProviderModel paymentProviderModel)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isWebHookNotificationPending(final Map<String, Object> payload)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final Map<String, Object> responseParams, final PaymentProviderModel paymentProviderModel)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response)
	{
		// XXX Auto-generated method stub
		return null;
	}

}
