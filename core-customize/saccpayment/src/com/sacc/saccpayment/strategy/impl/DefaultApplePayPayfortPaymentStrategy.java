/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Objects;
import java.util.Optional;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.model.ApplePayPayfortPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.payfort.entry.base.ApplePayRequestBean;


/**
 *
 */
public class DefaultApplePayPayfortPaymentStrategy extends DefaultPayfortPaymentStrategy
{

	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder) throws PaymentException
	{
		Preconditions.checkArgument(Objects.nonNull(abstractOrder), ABSTRACT_ORDER_NULL_MSG);
		final ApplePayPayfortPaymentProviderModel applePayfortProvider = getApplePayProvider(paymentProviderModel);

		final Object applePaySession = getPayfortService().getApplePaySession(applePayfortProvider.getApplePayUrl(),
				getApplePayRequest(applePayfortProvider));

		return null;
	}

	/**
	 *
	 */
	protected ApplePayRequestBean getApplePayRequest(final ApplePayPayfortPaymentProviderModel applePayfortProvider)
	{
		final ApplePayRequestBean applePayRequestBean = new ApplePayRequestBean();

		applePayRequestBean.setDisplayName(applePayfortProvider.getDisplayName());
		applePayRequestBean.setInitiative(applePayfortProvider.getInitiative());
		applePayRequestBean.setInitiativeContext(applePayfortProvider.getInitiativeContext());
		applePayRequestBean.setMerchantIdentifier(applePayfortProvider.getApplePayMerchantIdentifier());
		return applePayRequestBean;
	}

	/**
	 *
	 */
	protected ApplePayPayfortPaymentProviderModel getApplePayProvider(final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(
				Objects.nonNull(paymentProviderModel) && paymentProviderModel instanceof ApplePayPayfortPaymentProviderModel,
				INVALID_PROVIDER_MSG);
		return (ApplePayPayfortPaymentProviderModel) paymentProviderModel;
	}


}
