package com.sacc.saccpayment.context;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.sacc.saccpayment.model.PaymentProviderModel;



/**
 * The Interface PaymentProviderContext.
 *
 * @author mnasro
 *
 *         The Interface PaymentProviderContext.
 */
public interface PaymentProviderContext
{

	/**
	 * Gets the provider.
	 *
	 * the provider class
	 *
	 * @return the provider
	 */
	public Optional<PaymentProviderModel> getProvider(Class<?> providerClass, BaseStoreModel baseStoreModel);


	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the provider
	 */
	public Optional<PaymentProviderModel> getProvider(BaseStoreModel baseStoreModel);


	/**
	 * Gets the provider current store.
	 *
	 * @return the provider current store
	 */
	public Optional<PaymentProviderModel> getProviderByCurrentStore();

	public Optional<PaymentProviderModel> getProvider(AbstractOrderModel order);

	public Optional<PaymentProviderModel> getCardProvider(BaseStoreModel baseStoreModel);

	public Optional<PaymentProviderModel> getMadaProvider(final BaseStoreModel baseStoreModel);

	public Optional<PaymentProviderModel> getProviderByPaymentMode(final PaymentModeModel paymentModeModel,
			BaseStoreModel baseStore);

	public Optional<PaymentProviderModel> getProviderByPaymentModeAndCurrentStore(final PaymentModeModel paymentModeModel);


}
