package com.sacc.saccpayment.context.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.sacc.saccpayment.context.PaymentProviderContext;
import com.sacc.saccpayment.model.ApplePayPayfortPaymentProviderModel;
import com.sacc.saccpayment.model.CCAvenuePaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayApplePayPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayMadaPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PayfortPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.strategy.PaymentProviderStrategy;


/**
 * @author mnasro
 *
 *         The Class DefaultPaymentProviderContext.
 */
public class DefaultPaymentProviderContext implements PaymentProviderContext
{

	/** The payment provider map. */
	@Resource(name = "paymentProviderMap")
	private Map<Class<?>, PaymentProviderStrategy> paymentProviderMap;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;



	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String BASESTORE__MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER__MUSTN_T_BE_NULL = "paymentProvider mustn't be null";



	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentProviderModel> getProvider(final Class<?> providerClass, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);

		final Optional<PaymentProviderStrategy> strategy = getStrategy(providerClass);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentProviderStrategy strategy = getPaymentProviderMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PROVIDER_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the payment provider map.
	 *
	 * @return the payment provider map
	 */
	protected Map<Class<?>, PaymentProviderStrategy> getPaymentProviderMap()
	{
		return paymentProviderMap;
	}

	@Override
	public Optional<PaymentProviderModel> getProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE__MUSTN_T_BE_NULL);

		if (StringUtils.isBlank(baseStoreModel.getPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getPaymentProvider().toUpperCase())
		{
			case "CCAVENUEPAYMENTPROVIDER":
				return getProvider(CCAvenuePaymentProviderModel.class, baseStoreModel);
			case "HYPERPAYPAYMENTPROVIDER":
				return getProvider(HyperpayPaymentProviderModel.class, baseStoreModel);
			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<PaymentProviderModel> getProviderByCurrentStore()
	{
		return getProvider(getBaseStoreService().getCurrentBaseStore());
	}

	@Override
	public Optional<PaymentProviderModel> getProvider(final AbstractOrderModel order)
	{
		Preconditions.checkArgument(order != null, "order is null");
		Preconditions.checkArgument(order.getStore() != null, BASESTORE__MUSTN_T_BE_NULL);

		return getProviderByPaymentMode(order.getPaymentMode(), order.getStore());

	}


	@Override
	public Optional<PaymentProviderModel> getCardProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE__MUSTN_T_BE_NULL);



		if (Objects.isNull(baseStoreModel.getDefaultCardPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getDefaultCardPaymentProvider())
		{
			case CCAVENUE:
				return getProvider(CCAvenuePaymentProviderModel.class, baseStoreModel);
			case HYPERPAY:
				return getProvider(HyperpayPaymentProviderModel.class, baseStoreModel);
			case PAYFORT:
				return getProvider(PayfortPaymentProviderModel.class, baseStoreModel);
			default:
				return Optional.empty();
		}

	}


	@Override
	public Optional<PaymentProviderModel> getMadaProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE__MUSTN_T_BE_NULL);

		if (Objects.isNull(baseStoreModel.getDefaultMadaPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getDefaultMadaPaymentProvider())
		{
			case HYPERPAY:
				return getProvider(HyperpayMadaPaymentProviderModel.class, baseStoreModel);

			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<PaymentProviderModel> getProviderByPaymentMode(final PaymentModeModel paymentModeModel,
			final BaseStoreModel baseStore)

	{
		Preconditions.checkArgument(paymentModeModel != null, "Payment mode is null");

		switch (Strings.nullToEmpty(paymentModeModel.getCode()).toUpperCase())
		{
			case "MADA":
				return getMadaProvider(baseStore);
			case "CARD":
				return getCardProvider(baseStore);
			case "APPLEPAY":
				return getApplePayProvider(baseStore);

			default:
				return Optional.empty();
		}
	}

	/**
	 *
	 */
	private Optional<PaymentProviderModel> getApplePayProvider(final BaseStoreModel baseStore)
	{
		Preconditions.checkArgument(baseStore != null, BASESTORE__MUSTN_T_BE_NULL);

		if (Objects.isNull(baseStore.getDefaultApplePayPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStore.getDefaultApplePayPaymentProvider())
		{
			case PAYFORT:
				return getProvider(ApplePayPayfortPaymentProviderModel.class, baseStore);
			case HYPERPAY:
				return getProvider(HyperpayApplePayPaymentProviderModel.class, baseStore);
			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<PaymentProviderModel> getProviderByPaymentModeAndCurrentStore(final PaymentModeModel paymentModeModel)
	{
		return getProviderByPaymentMode(paymentModeModel, baseStoreService.getCurrentBaseStore());
	}
}
