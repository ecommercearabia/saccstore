package com.sacc.saccpayment.context.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.context.PaymentContext;
import com.sacc.saccpayment.context.PaymentProviderContext;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.strategy.PaymentStrategy;


/**
 * The Class DefaultPaymentContext.
 *
 * @author mnasro
 * @author abu-muhasien
 */
public class DefaultPaymentContext implements PaymentContext
{

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data can not be null";

	/** The Constant PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL = "paymentProviderModel must not be null";
	private static final String ABSTRACT_ORDER_MODEL_MUST_NOT_BE_NULL = "order must not be null";


	/** The Constant PAYMENT_STRATEGY_NOT_FOUND. */
	private static final String PAYMENT_STRATEGY_NOT_FOUND = "strategy not found";

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment strategy map. */
	@Resource(name = "paymentStrategyMap")
	private Map<Class<?>, PaymentStrategy> paymentStrategyMap;

	/**
	 * Gets the payment strategy map.
	 *
	 * @return the payment strategy map
	 */
	protected Map<Class<?>, PaymentStrategy> getPaymentStrategyMap()
	{
		return paymentStrategyMap;
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentStrategy strategy = getPaymentStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PAYMENT_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the payment data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 * @throws PaymentException
	 */
	@Override
	public Optional<PaymentRequestData> getPaymentData(final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = getProvider(abstractOrderModel);
		Preconditions.checkArgument(paymentProviderModel.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> paymentStrategy = getStrategy(paymentProviderModel.get().getClass());
		if (paymentStrategy.isPresent())
		{
			return paymentStrategy.get().buildPaymentRequestData(paymentProviderModel.get(), abstractOrderModel);
		}
		return Optional.empty();

	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final AbstractOrderModel order)
	{

		final Optional<PaymentProviderModel> provider = getProvider(order);
		return interpretResponse(responseParams, provider.orElse(null));


	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order)
	{
		Preconditions.checkArgument(order != null, ABSTRACT_ORDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentProviderModel> provider = getProvider(order);
		return interpretResponse(order, provider.orElse(null));

	}

	protected Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().interpretResponse(order, paymentProviderModel);


	}

	protected Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().interpretResponse(responseParams, paymentProviderModel);


	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final BaseStoreModel baseStoreModel)
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (paymentProvider.isEmpty())
		{
			return Optional.empty();

		}

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProvider.get().getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}



		return Optional.empty();
	}

	/**
	 * Interpret response by current store.
	 *
	 * @param responseParams
	 *           the response params
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponseByCurrentStore(final Map<String, Object> responseParams)
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (paymentProvider.isPresent())
		{
			return interpretResponse(responseParams, paymentProvider.get());
		}
		return Optional.empty();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	@Override
	public Optional<PaymentResponseData> getResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().buildPaymentResponseData(paymentProviderModel, abstractOrderModel, data);
	}

	/**
	 * Gets the response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	@Override
	public Optional<PaymentResponseData> getResponseData(final AbstractOrderModel abstractOrderModel, final Object data)
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (paymentProvider.isEmpty())
		{
			return Optional.empty();
		}
		return getResponseData(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Gets the response data by current store.
	 *
	 * @param data
	 *           the data
	 * @return the response data by current store
	 */
	@Override
	public Optional<PaymentResponseData> getResponseDataByCurrentStore(final AbstractOrderModel abstractOrderModel,
			final Object data)
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getResponseData(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return false;
		}
		return strategy.get().isSuccessfulPaidOrder(paymentProviderModel, abstractOrderModel, data);
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final AbstractOrderModel abstractOrderModel, final Object data)
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (!paymentProvider.isPresent())
		{
			return false;
		}
		return isSuccessfulPaidOrder(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Checks if is successful paid order by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order by current store
	 */
	@Override
	public boolean isSuccessfulPaidOrderByCurrentStore(final AbstractOrderModel abstractOrderModel, final Object data)
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProvider.isPresent())
		{
			return false;
		}
		return isSuccessfulPaidOrder(paymentProvider.get(), abstractOrderModel, data);

	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseDataByCurrentStore(final Object data,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProviderModel.isPresent())
		{
			return Optional.empty();
		}

		return getPaymentOrderStatusResponseData(paymentProviderModel.get(), abstractOrderModel, data);
	}


	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final AbstractOrderModel abstractOrderModel,
			final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getPaymentOrderStatusResponseData(paymentProvider.get(), abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getPaymentOrderStatusResponseData(paymentProviderModel, abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (!paymentProvider.isPresent())
		{

			return Optional.empty();
		}
		return getPaymentOrderConfirmedResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().captureOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getPaymentOrderCancelResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().cancelOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(abstractOrderModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getPaymentOrderRefundResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().refundOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public boolean isSuccessfulPaidOrderByOrderTransactions(final AbstractOrderModel abstractOrder)
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(abstractOrder);
		if (!paymentProvider.isPresent())
		{
			return false;
		}
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProvider.get().getClass());
		if (!strategy.isPresent())
		{
			return false;
		}
		return strategy.get().isSuccessfulPaidOrder(paymentProvider.get(), abstractOrder);
	}


	private Optional<PaymentProviderModel> getProvider(final AbstractOrderModel order)
	{
		if (Objects.isNull(order) || Objects.isNull(order.getStore()))
		{
			return Optional.empty();
		}

		return paymentProviderContext.getProvider(order);
	}

	@Override
	public Optional<PaymentRequestData> getPaymentDataByCurrentPaymentMode(final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> provider = getProvider(abstractOrderModel);

		if (provider.isEmpty())
		{
			return Optional.empty();
		}
		return getPaymentDataByCurrentPaymentMode(abstractOrderModel);
	}

	@Override
	public Map<String, Object> getCheckoutMap(final AbstractOrderModel abstractOrderModel)
	{
		final Optional<PaymentProviderModel> supportedPaymentProvider = paymentProviderContext.getProvider(abstractOrderModel);

		if (supportedPaymentProvider.isEmpty() || Objects.isNull(abstractOrderModel))
		{
			return MapUtils.EMPTY_MAP;
		}

		final PaymentProviderModel paymentProviderModel = supportedPaymentProvider.get();

		if (paymentProviderModel instanceof HyperpayPaymentProviderModel)
		{
			return abstractOrderModel.getHyperpayCheckoutMap();
		}
		else if (paymentProviderModel instanceof PaymentProviderModel)
		{
			return abstractOrderModel.getPaymentResponseMap();
		}
		return MapUtils.EMPTY_MAP;

	}

	@Override
	public boolean isWebHookNotificationPending(final PaymentProviderModel paymentProviderModel, final Map<String, Object> payload)
			throws PaymentException
	{
		final PaymentStrategy strategy = getStrategy(paymentProviderModel.getClass()).orElse(null);
		if (strategy == null)
		{
			throw new PaymentException(String.format("Cannot find strategy for PaymentProvider: Pk[%s], Code[%s]",
					paymentProviderModel.getPk(), paymentProviderModel.getCode()));
		}


		return strategy.isWebHookNotificationPending(payload);
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponseUsingWebhook(final Map<String, Object> orderInfoMap,
			final AbstractOrderModel order)
	{
		final Optional<PaymentProviderModel> provider = getProvider(order);
		return interpretResponse(orderInfoMap, provider.orElse(null));
	}

	@Override
	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> orderInfoMap, final AbstractOrderModel order)
	{
		final Optional<PaymentProviderModel> provider = getProvider(order);
		Preconditions.checkArgument(provider.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(provider.get().getClass());
		if (!strategy.isPresent())
		{
			return null;
		}

		return strategy.get().getTransactionStatus(orderInfoMap);
	}


}
