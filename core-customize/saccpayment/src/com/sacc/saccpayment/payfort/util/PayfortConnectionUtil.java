package com.sacc.saccpayment.payfort.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;

import com.sacc.saccpayment.payfort.bean.PayfortConnectionBean;
import com.sacc.saccpayment.payfort.enums.PayfortExceptionType;
import com.sacc.saccpayment.payfort.exception.PayfortException;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;


public final class PayfortConnectionUtil
{

	private PayfortConnectionUtil()
	{

	}

	public static <T> T httpGet(final String url, final Map<String, Object> params, final Class<T> clazz) throws PayfortException
	{
		Unirest.config().reset();
		Unirest.config().verifySsl(false).connectTimeout(0);
		HttpResponse<T> httpResponse = null;

		try
		{
			httpResponse = Unirest.get(url).queryString(params).asObject(clazz);
		}
		catch (final UnirestException ex)
		{
			throw new PayfortException(ex.getMessage(), PayfortExceptionType.SERVER_ERROR, ex);
		}

		if (Objects.isNull(httpResponse.getBody()) && httpResponse.getParsingError().isPresent())
		{
			throw new PayfortException(PayfortExceptionType.PARSING_ERROR.getMsg(), PayfortExceptionType.PARSING_ERROR,
					httpResponse.getParsingError().orElseGet(null));
		}
		if (httpResponse.isSuccess())
		{
			return httpResponse.getBody();
		}
		return null;
	}

	public static String httpPost(final String url, final Map<String, Object> params, final String body,
			Map<String, String> headers) throws PayfortException
	{
		Unirest.config().reset();
		Unirest.config().verifySsl(false).connectTimeout(0);
		HttpResponse<String> httpResponseString = null;

		if (MapUtils.isEmpty(headers))
		{
			headers = new LinkedHashMap<>();
			headers.put("Accept", "application/json");
		}

		try
		{
			httpResponseString = Unirest.post(url).body(body).headers(headers).contentType("application/json").asString();
		}
		catch (final UnirestException ex)
		{
			throw new PayfortException(ex.getMessage(), PayfortExceptionType.SERVER_ERROR, ex);
		}

		if (httpResponseString.isSuccess())
		{
			return httpResponseString.getBody();
		}

		return Strings.EMPTY;
	}

	public static PayfortConnectionBean getConnectionBean()
	{
		final PayfortConnectionBean connectionBean = new PayfortConnectionBean();
		//		connectionBean.setAccessCode(PayfortCredentialsConstants.ACCESS_CODE);
		//		connectionBean.setMerchantIdentifier(PayfortCredentialsConstants.MERCHANT_IDENTIFIER);
		//		connectionBean.setKey(PayfortCredentialsConstants.SHA_REQUEST_PHRASE);
		//		connectionBean.setLanguage(PayfortCredentialsConstants.LANGUAGE);
		//		connectionBean.setHashingType(PayfortHashingType.HASH_256);
		//		connectionBean.setReturnUrl(PayfortCredentialsConstants.RETURN_URL);
		//		connectionBean.setUrl(PayfortCredentialsConstants.URL);
		return connectionBean;
	}
}
