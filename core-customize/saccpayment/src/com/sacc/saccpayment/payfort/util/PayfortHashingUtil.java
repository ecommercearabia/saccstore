package com.sacc.saccpayment.payfort.util;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.enums.PayfortHashingType;
import com.sacc.saccpayment.payfort.exception.PayfortException;


public class PayfortHashingUtil
{

	private PayfortHashingUtil()
	{

	}

	public static String hashRequest(final Object request, final PayfortHashingType hashingType, final String hashingKey)
			throws PayfortException
	{
		final StringBuilder signature = new StringBuilder();
		final Class<?> clazz = request.getClass();
		final Field[] currentFields = clazz.getDeclaredFields();
		final Field[] superFields = clazz.getSuperclass().getDeclaredFields();
		final Field[] fields = new Field[currentFields.length + superFields.length];

		Arrays.setAll(fields, i -> (i < currentFields.length ? currentFields[i] : superFields[i - currentFields.length]));

		final List<Field> collect = Arrays.stream(fields).filter(field -> field.isAnnotationPresent(IncludeInSignature.class))
				.sorted(Comparator.comparing(f -> f.getAnnotation(SerializedName.class).value())).collect(Collectors.toList());

		signature.append(hashingKey);
		for (final Field field : collect)
		{
			field.setAccessible(true);
			String value;
			try
			{
				value = String.valueOf(field.get(request));
			}
			catch (final IllegalAccessException e)
			{
				throw new PayfortException("error", null);
			}
			if (field.isAnnotationPresent(IncludeInSignature.class) && !Strings.isNullOrEmpty(value) && !"null".equals(value))
			{
				signature.append(field.getAnnotation(SerializedName.class).value()).append("=").append(value);
			}
		}
		signature.append(hashingKey);
		System.err.println(signature);
		return hash(signature.toString(), hashingType);
	}

	public static String hashRequest(final Map<String, Object> request, final PayfortHashingType hashingType,
			final String hashingKey) throws PayfortException
	{
		final Map<String, Object> requestMap = request.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(
				Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

		String requestString = hashingKey;
		for (final Entry<String, Object> entry : requestMap.entrySet())
		{
			requestString += entry.getKey() + "=" + entry.getValue();
		}
		requestString += hashingKey;
		System.err.println(requestString);
		return hash(requestString, hashingType);
	}

	public static String hash(final String data, final PayfortHashingType hashingType) throws PayfortException
	{
		final MessageDigest md;
		try
		{
			md = PayfortHashingType.HASH_512.equals(hashingType) ? MessageDigest.getInstance("SHA-512")
					: MessageDigest.getInstance("SHA-256");
		}
		catch (final NoSuchAlgorithmException e)
		{
			throw new PayfortException("error", null);
		}
		final byte[] hashed = md.digest(data.getBytes(StandardCharsets.UTF_8));
		return javax.xml.bind.DatatypeConverter.printHexBinary(hashed);

	}

}
