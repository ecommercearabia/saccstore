package com.sacc.saccpayment.payfort.service;

import java.util.Optional;

import com.sacc.saccpayment.payfort.bean.PayfortConnectionBean;
import com.sacc.saccpayment.payfort.entry.base.ApplePayRequestBean;
import com.sacc.saccpayment.payfort.entry.form.TokenizationForm;
import com.sacc.saccpayment.payfort.entry.request.AuthorizePurchaseRequestBean;
import com.sacc.saccpayment.payfort.entry.request.CheckStatusRequestBean;
import com.sacc.saccpayment.payfort.entry.request.RefundRequestBean;
import com.sacc.saccpayment.payfort.entry.request.TokenizationRequestBean;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;
import com.sacc.saccpayment.payfort.entry.response.CheckStatusResponse;
import com.sacc.saccpayment.payfort.entry.response.RefundResponseBean;
import com.sacc.saccpayment.payfort.exception.PayfortException;


public interface PayfortService
{

	public Optional<AuthorizePurchaseResponseBean> authorize(PayfortConnectionBean connectionBean,
			AuthorizePurchaseRequestBean request) throws PayfortException;

	public Optional<AuthorizePurchaseResponseBean> purchase(PayfortConnectionBean connectionBean,
			AuthorizePurchaseRequestBean request) throws PayfortException;

	public Optional<RefundResponseBean> refund(PayfortConnectionBean connectionBean, RefundRequestBean request)
			throws PayfortException;

	public Optional<TokenizationRequestBean> prepareTokenizationForm(PayfortConnectionBean connectionBean,
			TokenizationForm tokenizationForm) throws PayfortException;

	public Optional<CheckStatusResponse> getStatus(PayfortConnectionBean connectionBean, CheckStatusRequestBean status)
			throws PayfortException;

	public Object getApplePaySession(String baseUrl, ApplePayRequestBean applePayRequestBean) throws PayfortException;
}
