package com.sacc.saccpayment.payfort.service;

public interface PayfortCredentialsConstants {

	public static final String ACCESS_CODE = "BwNLYmQjZFDTHmmbqig7";
	public static final String MERCHANT_IDENTIFIER = "XjeXEmnB";
	public static final String SHA_REQUEST_PHRASE= "pass_PHRASE1";
	public static final String SHA_RESPONSE_PHRASE= "pass_PHRASE2";
	public static final String LANGUAGE = "en";
	public static final String RETURN_URL = "https://payfort.erabia.io/callback";
	public static final String URL ="https://sbcheckout.payfort.com/FortAPI/";
}
