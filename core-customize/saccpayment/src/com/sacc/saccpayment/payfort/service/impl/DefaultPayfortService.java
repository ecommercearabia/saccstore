package com.sacc.saccpayment.payfort.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.bean.PayfortConnectionBean;
import com.sacc.saccpayment.payfort.entry.base.ApplePayRequestBean;
import com.sacc.saccpayment.payfort.entry.form.TokenizationForm;
import com.sacc.saccpayment.payfort.entry.request.AuthorizePurchaseRequestBean;
import com.sacc.saccpayment.payfort.entry.request.CheckStatusRequestBean;
import com.sacc.saccpayment.payfort.entry.request.RefundRequestBean;
import com.sacc.saccpayment.payfort.entry.request.TokenizationRequestBean;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;
import com.sacc.saccpayment.payfort.entry.response.CheckStatusResponse;
import com.sacc.saccpayment.payfort.entry.response.RefundResponseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;
import com.sacc.saccpayment.payfort.exception.PayfortException;
import com.sacc.saccpayment.payfort.service.PayfortService;
import com.sacc.saccpayment.payfort.util.PayfortConnectionUtil;
import com.sacc.saccpayment.payfort.util.PayfortHashingUtil;


@Service(value = "payfortService")
public class DefaultPayfortService implements PayfortService
{

	private static final String PAYMENT_API_PATH = "paymentApi";
	private static final String APPLE_API_PATH = "paymentSession";
	private static final String CONNECTION_BEAN_IS_NULL = "connectionBean is null";
	private static final String PAYMENT_PAGE_PATH = "paymentPage";

	private static Gson gson;
	static
	{
		gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
	}

	private void validatePayfortRequests(final Object request) throws PayfortException
	{
		final List<Field> fields = getObjectFields(request);

		final List<Field> collect = fields.stream().filter(field -> field.isAnnotationPresent(RequiredParam.class))
				.collect(Collectors.toList());
		for (final Field field : collect)
		{
			field.setAccessible(true);
			try
			{
				if (field.get(request) == null)
				{
					throw new PayfortException(field.getAnnotation(RequiredParam.class).message(), null);
				}
			}
			catch (final IllegalAccessException e)
			{
				throw new PayfortException(e.getMessage(), null);
			}

		}
	}

	private Gson getGsonInstance()
	{
		return gson;
	}

	private List<Field> getObjectFields(final Object ob)
	{
		final Class<?> clazz = ob.getClass();
		final List<Field> currentFields = Arrays.asList(clazz.getDeclaredFields());
		final List<Field> superFields = Arrays.asList(clazz.getSuperclass().getDeclaredFields());
		final List<Field> fields = new ArrayList<>();
		fields.addAll(currentFields);
		fields.addAll(superFields);
		return fields;
	}

	@Override
	public Optional<AuthorizePurchaseResponseBean> authorize(final PayfortConnectionBean connectionBean,
			final AuthorizePurchaseRequestBean request) throws PayfortException
	{
		return purchaseAndAuthorize(connectionBean, request, PayfortCommandType.AUTHORIZATION.getCommand());
	}

	@Override
	public Optional<AuthorizePurchaseResponseBean> purchase(final PayfortConnectionBean connectionBean,
			final AuthorizePurchaseRequestBean request) throws PayfortException
	{
		return purchaseAndAuthorize(connectionBean, request, PayfortCommandType.PURCHASE.getCommand());

	}

	@Override
	public Optional<RefundResponseBean> refund(final PayfortConnectionBean connectionBean, final RefundRequestBean request)
			throws PayfortException
	{
		validatePayfortRequests(request);
		validateConnectionBean(connectionBean);
		Preconditions.checkArgument(Objects.nonNull(connectionBean), CONNECTION_BEAN_IS_NULL);
		request.setCommand(PayfortCommandType.REFUND.getCommand());
		final String signature = PayfortHashingUtil.hashRequest(request, connectionBean.getHashingType(),
				connectionBean.getHashingKey());
		request.setSignature(signature);
		final String jsonRequest = getGsonInstance().toJson(request);
		final String httpResponse = PayfortConnectionUtil.httpPost(connectionBean.getUrl(), null, jsonRequest, null);
		final RefundResponseBean result = getGsonInstance().fromJson(httpResponse, RefundResponseBean.class);
		return Optional.ofNullable(result);
	}

	private Optional<AuthorizePurchaseResponseBean> purchaseAndAuthorize(final PayfortConnectionBean connectionBean,
			final AuthorizePurchaseRequestBean request, final String command) throws PayfortException
	{

		validatePayfortRequests(request);
		validateConnectionBean(connectionBean);
		Preconditions.checkArgument(Objects.nonNull(connectionBean), CONNECTION_BEAN_IS_NULL);
		request.setCommand(command);
		final String signature = PayfortHashingUtil.hashRequest(request, connectionBean.getHashingType(),
				connectionBean.getHashingKey());
		request.setSignature(signature);
		final String jsonRequest = getGsonInstance().toJson(request);
		final String httpResponse = PayfortConnectionUtil.httpPost(connectionBean.getUrl().concat(PAYMENT_API_PATH), null,
				jsonRequest, null);
		final AuthorizePurchaseResponseBean result = getGsonInstance().fromJson(httpResponse, AuthorizePurchaseResponseBean.class);
		return Optional.ofNullable(result);
	}

	@Override
	public Optional<TokenizationRequestBean> prepareTokenizationForm(final PayfortConnectionBean connectionBean,
			final TokenizationForm tokenizationForm) throws PayfortException
	{
		validateConnectionBean(connectionBean);
		return Optional.ofNullable(getTokenizationBean(connectionBean, tokenizationForm));
	}

	private void validateTokenizationForm(final TokenizationForm tokenizationForm)
	{
		Preconditions.checkArgument(Objects.nonNull(tokenizationForm), "tokenizationForm is null");
	}

	private void validateConnectionBean(final PayfortConnectionBean connectionBean)
	{
		Preconditions.checkArgument(Objects.nonNull(connectionBean), CONNECTION_BEAN_IS_NULL);
		Preconditions.checkArgument(Strings.isNotBlank(connectionBean.getAccessCode()), "Access Code is empty");
		Preconditions.checkArgument(Strings.isNotBlank(connectionBean.getMerchantIdentifier()), "Merchant Identifier is empty");
		Preconditions.checkArgument(Strings.isNotBlank(connectionBean.getUrl()), "base url is empty");
		Preconditions.checkArgument(Strings.isNotBlank(connectionBean.getHashingKey()), "hashingkey is empty");


	}

	private TokenizationRequestBean getTokenizationBean(final PayfortConnectionBean connectionBean,
			final TokenizationForm tokenizationForm) throws PayfortException
	{
		validateTokenizationForm(tokenizationForm);
		final TokenizationRequestBean requestBean = new TokenizationRequestBean();
		requestBean.setAccessCode(connectionBean.getAccessCode());
		requestBean.setMerchantIdentifier(connectionBean.getMerchantIdentifier());
		requestBean.setLanguage(tokenizationForm.getLanguage());
		requestBean.setMerchantReference(tokenizationForm.getOrderNumber());
		requestBean.setReturnUrl(connectionBean.getReturnUrl());
		requestBean.setUrl(connectionBean.getUrl().concat(PAYMENT_PAGE_PATH));
		final String signature = PayfortHashingUtil.hashRequest(requestBean, connectionBean.getHashingType(),
				connectionBean.getHashingKey());
		requestBean.setSignature(signature);
		return requestBean;
	}

	@Override
	public Optional<CheckStatusResponse> getStatus(final PayfortConnectionBean connectionBean,
			final CheckStatusRequestBean request) throws PayfortException
	{
		validateConnectionBean(connectionBean);
		validatePayfortRequests(request);
		request.setCommand(PayfortCommandType.CHECK_STATUS.getCommand());
		final String signature = PayfortHashingUtil.hashRequest(request, connectionBean.getHashingType(),
				connectionBean.getHashingKey());
		request.setSignature(signature);
		final String jsonRequest = getGsonInstance().toJson(request);
		final String httpResponse = PayfortConnectionUtil.httpPost(connectionBean.getUrl().concat(PAYMENT_API_PATH), null,
				jsonRequest, null);
		final CheckStatusResponse result = getGsonInstance().fromJson(httpResponse, CheckStatusResponse.class);
		return Optional.ofNullable(result);
	}

	@Override
	public Object getApplePaySession(final String baseUrl, final ApplePayRequestBean applePayRequestBean) throws PayfortException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseUrl), "base url is empty");
		Preconditions.checkArgument(Objects.nonNull(applePayRequestBean), "applePayRequestBean is null");
		validatePayfortRequests(applePayRequestBean);
		final String jsonRequest = getGsonInstance().toJson(applePayRequestBean);
		final String httpPost = PayfortConnectionUtil.httpPost(baseUrl.concat(APPLE_API_PATH), null, jsonRequest, null);

		final Object result = getGsonInstance().fromJson(httpPost, Object.class);

		return result;
	}

}
