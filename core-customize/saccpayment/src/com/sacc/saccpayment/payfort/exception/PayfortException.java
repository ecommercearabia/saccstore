package com.sacc.saccpayment.payfort.exception;

import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.payfort.enums.PayfortExceptionType;


public class PayfortException extends PaymentException
{

	private final String exceptionMessage;

	private final PayfortExceptionType type;

	private final Object exception;

	public PayfortExceptionType getType() {
		return this.type;
	}

	public String getExceptionMessage() {
		return this.exceptionMessage;
	}

	public PayfortException(final String exceptionMessage, final PayfortExceptionType type, final Object exception) {
		super(exceptionMessage);
		this.exceptionMessage = exceptionMessage;
		this.type = type;
		this.exception = exception;
	}

	public PayfortException(final String exceptionMessage, final PayfortExceptionType type) {
		this(exceptionMessage, type, null);
	}

	public Object getException() {
		return exception;
	}

}
