package com.sacc.saccpayment.payfort.rest;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sacc.saccpayment.payfort.entry.request.AuthorizePurchaseRequestBean;
import com.sacc.saccpayment.payfort.entry.request.CheckStatusRequestBean;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;
import com.sacc.saccpayment.payfort.entry.response.CheckStatusResponse;
import com.sacc.saccpayment.payfort.exception.PayfortException;
import com.sacc.saccpayment.payfort.service.PayfortService;
import com.sacc.saccpayment.payfort.util.PayfortConnectionUtil;

@RestController
@RequestMapping("/payfort")
public class PayfortRestController {

	@Resource(name = "payfortService")
	private PayfortService payfortService;

	@PostMapping("/purchase")
	public AuthorizePurchaseResponseBean purchase(@RequestBody final AuthorizePurchaseRequestBean request)
			throws PayfortException {
		return getPayfortService().purchase(PayfortConnectionUtil.getConnectionBean(), request).orElse(null);
	}

	@PostMapping("/checkstatus")
	public CheckStatusResponse checkStatus(@RequestBody(required = true) final CheckStatusRequestBean request)
			throws PayfortException {
		return getPayfortService().getStatus(PayfortConnectionUtil.getConnectionBean(), request).orElse(null);
	}

	public PayfortService getPayfortService() {
		return payfortService;
	}

}
