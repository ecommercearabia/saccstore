package com.sacc.saccpayment.payfort.enums;

public enum PayfortCommandType {

	AUTHORIZATION("AUTHORIZATION"),
	PURCHASE("PURCHASE"),
	TOKENIZATION("TOKENIZATION"),
	REFUND("REFUND"),
	CHECK_STATUS("CHECK_STATUS");

	private String command;

	public String getCommand() {
		return command;
	}

	private PayfortCommandType(final String command) {
		this.command=command;
	}
}
