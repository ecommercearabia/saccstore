package com.sacc.saccpayment.payfort.enums;

public enum PayfortHashingType {

	HASH_256("SHA-256"), HASH_512("SHA-512");

	private final String type;

	public String getType() {
		return type;
	}

	private PayfortHashingType(final String type) {
		this.type = type;
	}
}
