package com.sacc.saccpayment.payfort.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum PayfortResponseMsg {

	SUCCESS("000", "Success."), MISSING_PARAMETER("001", "Missing parameter."),
	INVALID_PARAMETER_FORMAT("002", "Invalid parameter format."),
	PAYMENT_OPTION_IS_NOT_AVAILABLE_FOR_THIS_MERCHANTS_ACCOUNT("003",
			"Payment option is not available for this merchant’s account."),
	INVALID_COMMAND("004", "Invalid command."), INVALID_AMOUNT("005", "Invalid amount."),
	TECHNICAL_PROBLEM("006", "Technical problem."), DUPLICATE_ORDER_NUMBER("007", "Duplicate order number."),
	SIGNATURE_MISMATCH("008", "Signature mismatch."),
	INVALID_MERCHANT_IDENTIFIER("009", "Invalid merchant identifier."),
	INVALID_ACCESS_CODE("010", "Invalid access code."), ORDER_NOT_SAVED("011", "Order not saved."),
	CARD_EXPIRED("012", "Card expired."), INVALID_CURRENCY("013", "Invalid currency."),
	INACTIVE_PAYMENT_OPTION("014", "Inactive payment option."),
	INACTIVE_MERCHANT_ACCOUNT("015", "Inactive merchant account."), INVALID_CARD_NUMBER("016", "Invalid card number."),
	OPERATION_NOT_ALLOWED_BY_THE_ACQUIRER("017", "Operation not allowed by the acquirer."),
	OPERATION_NOT_ALLOWED_BY_PROCESSOR("018", "Operation not allowed by processor."),
	INACTIVE_ACQUIRER("019", "Inactive acquirer."), PROCESSOR_IS_INACTIVE("020", "Processor is inactive."),
	PAYMENT_OPTION_DEACTIVATED_BY_ACQUIRER("021", "Payment option deactivated by acquirer."),
	CURRENCY_NOT_ACCEPTED_BY_ACQUIRER("023", "Currency not accepted by acquirer."),
	CURRENCY_NOT_ACCEPTED_BY_PROCESSOR("024", "Currency not accepted by processor."),
	PROCESSOR_INTEGRATION_SETTINGS_ARE_MISSING("025", "Processor integration settings are missing."),
	ACQUIRER_INTEGRATION_SETTINGS_ARE_MISSING("026", "Acquirer integration settings are missing."),
	INVALID_EXTRA_PARAMETERS("027", "Invalid extra parameters."), INSUFFICIENT_FUNDS("029", "Insufficient funds."),
	AUTHENTICATION_FAILED("030", "Authentication failed."), INVALID_ISSUER("031", "Invalid issuer."),
	INVALID_PARAMETER_LENGTH("032", "Invalid parameter length."),
	PARAMETER_VALUE_NOT_ALLOWED("033", "Parameter value not allowed."),
	OPERATION_NOT_ALLOWED("034", "Operation not allowed."),
	ORDER_CREATED_SUCCESSFULLY("035", "Order created successfully."), ORDER_NOT_FOUND("036", "Order not found."),
	MISSING_RETURN_URL("037", "Missing return URL."), TOKEN_SERVICE_INACTIVE("038", "Token service inactive."),
	NO_ACTIVE_PAYMENT_OPTION_FOUND("039", "No active payment option found."),
	INVALID_TRANSACTION_SOURCE("040", "Invalid transaction source."),
	OPERATION_AMOUNT_EXCEEDS_THE_AUTHORIZED_AMOUNT("042", "Operation amount exceeds the authorized amount."),
	INACTIVE_OPERATION("043", "Inactive Operation."), TOKEN_NAME_DOES_NOT_EXIST("044", "Token name does not exist."),
	CHANNEL_IS_NOT_CONFIGURED_FOR_THE_SELECTED_PAYMENT_OPTION("046",
			"Channel is not configured for the selected payment option."),
	ORDER_ALREADY_PROCESSED("047", "Order already processed."),
	OPERATION_AMOUNT_EXCEEDS_CAPTURED_AMOUNT("048", "Operation amount exceeds captured amount."),
	OPERATION_NOT_VALID_FOR_THIS_PAYMENT_OPTION("049", "Operation not valid for this payment option."),
	MERCHANT_PER_TRANSACTION_LIMIT_EXCEEDED("050", "Merchant per transaction limit exceeded."),
	TECHNICAL_ERROR("051", "Technical error."),
	CONSUMER_IS_NOT_IN_OLP_DATABASE("052", "Consumer is not in OLP database."),
	MERCHANT_IS_NOT_FOUND_IN_OLP_ENGINE_DB("053", "Merchant is not found in OLP Engine DB."),
	TRANSACTION_CANNOT_BE_PROCESSED_AT_THIS_MOMENT("054", "Transaction cannot be processed at this moment."),
	OLP_ID_ALIAS_IS_NOT_VALID_PLEASE_CONTACT_YOUR_BANK("055", "OLP ID Alias is not valid. Please contact your bank."),
	OLP_ID_ALIAS_DOES_NOT_EXIST_PLEASE_ENTER_A_VALID_OLP_ID_ALIAS("056",
			"OLP ID Alias does not exist. Please enter a valid OLP ID Alias."),
	TRANSACTION_AMOUNT_EXCEEDS_THE_DAILY_TRANSACTION_LIMIT("057",
			"Transaction amount exceeds the daily transaction limit."),
	TRANSACTION_AMOUNT_EXCEEDS_THE_PER_TRANSACTION_LIMIT("058",
			"Transaction amount exceeds the per transaction limit."),
	MERCHANT_NAME_AND_SADAD_MERCHANT_ID_DO_NOT_MATCH("059", "Merchant Name and SADAD Merchant ID do not match."),
	THE_ENTERED_OLP_PASSWORD_IS_INCORRECT_PLEASE_PROVIDE_A_VALID_PASSWORD("060",
			"The entered OLP password is incorrect. Please provide a valid password."),
	TOKEN_HAS_BEEN_CREATED("062", "Token has been created."), TOKEN_HAS_BEEN_UPDATED("063", "Token has been updated."),
	THREE_D_SECURE_CHECK_REQUESTED("064", "3D Secure check requested."),
	TRANSACTION_WAITING_FOR_CUSTOMERS_ACTION("065", "Transaction waiting for customer’s action."),
	MERCHANT_REFERENCE_ALREADY_EXISTS("066", "Merchant reference already exists."),
	DYNAMIC_DESCRIPTOR_NOT_CONFIGURED_FOR_SELECTED_PAYMENT_OPTION("067",
			"Dynamic Descriptor not configured for selected payment option."),
	SDK_SERVICE_IS_INACTIVE("068", "SDK service is inactive."),
	MAPPING_NOT_FOUND_FOR_THE_GIVEN_ERROR_CODE("069", "Mapping not found for the given error code."),
	DEVICE_ID_MISMATCH("070", "device_id mismatch."),
	FAILED_TO_INITIATE_CONNECTION("071", "Failed to initiate connection."),
	TRANSACTION_HAS_BEEN_CANCELLED_BY_THE_CONSUMER("072", "Transaction has been cancelled by the consumer."),
	INVALID_REQUEST_FORMAT("073", "Invalid request format."), TRANSACTION_FAILED("074", "Transaction failed."),
	_TRANSACTION_FAILED("075", "Transaction failed."),
	TRANSACTION_NOT_FOUND_IN_OLP("076", "Transaction not found in OLP."),
	ERROR_TRANSACTION_CODE_NOT_FOUND("077", "Error transaction code not found."),
	FAILED_TO_CHECK_FRAUD_SCREEN("078", "Failed to check fraud screen."),
	TRANSACTION_CHALLENGED_BY_FRAUD_RULES("079", "Transaction challenged by fraud rules."),
	INVALID_PAYMENT_OPTION("080", "Invalid payment option."), INACTIVE_FRAUD_SERVICE("082", "Inactive fraud service."),
	UNEXPECTED_USER_BEHAVIOR("083", "Unexpected user behavior."),
	TRANSACTION_AMOUNT_INVALID("084",
			"Transaction amount is either bigger than maximum or less than minimum amount accepted for the selected plan."),
	INSTALLMENT_PLAN_IS_NOT_CONFIGURED_FOR_MERCHANT_ACCOUNT("086",
			"Installment plan is not configured for Merchant account."),
	CARD_BIN_DOES_NOT_MATCH_ACCEPTED_ISSUER_BANK("087", "Card BIN does not match accepted issuer bank."),
	TOKEN_NAME_WAS_NOT_CREATED_FOR_THIS_TRANSACTION("088", "Token name was not created for this transaction."),
	FAILED_TO_RETRIEVE_DIGITAL_WALLET_DETAILS("089", "Failed to retrieve digital wallet details."),
	TRANSACTION_IN_REVIEW("090", "Transaction in review."), INVALID_ISSUER_CODE("092", "Invalid issuer code."),
	SERVICE_INACTIVE("093", "service inactive."), INVALID_PLAN_CODE("094", "Invalid Plan Code."),
	INACTIVE_ISSUER("095", "Inactive Issuer."), INACTIVE_PLAN("096", "Inactive Plan."),
	OPERATION_NOT_ALLOWED_FOR_SERVICE("097", "Operation not allowed for service."),
	INVALID_OR_EXPIRED_CALL_ID("098", "Invalid or expired call_id."),
	FAILED_TO_EXECUTE_SERVICE("099", "Failed to execute service."), INVALID_EXPIRY_DATE("100", "Invalid expiry date."),
	BILL_NUMBER_NOT_FOUND("101", "Bill number not found."),
	APPLE_PAY_ORDER_HAS_BEEN_EXPIRED("102", "Apple Pay order has been expired."),
	DUPLICATE_SUBSCRIPTION_ID("103", "Duplicate subscription ID."),
	NO_PLANS_VALID_FOR_REQUEST("104", "No plans valid for request."), INVALID_BANK_CODE("105", "Invalid bank code."),
	INACTIVE_BANK("106", "Inactive bank."), INVALID_TRANSFER_DATE("107", "Invalid transfer_date."),
	CONTRADICTING_PARAMETERS("110", "Contradicting parameters, please refer to the integration guide."),
	SERVICE_NOT_APPLICABLE_FOR_PAYMENT_OPTION("111", "Service not applicable for payment option."),
	SERVICE_NOT_APPLICABLE_FOR_PAYMENT_OPERATION("112", "Service not applicable for payment operation."),
	SERVICE_NOT_APPLICABLE("113", "Service not applicable for e-commerce indicator."),
	TOKEN_ALREADY_EXIST("114", "Token already exist."),
	EXPIRED_INVOICE_PAYMENT_LINK("115", "Expired invoice payment link."),
	INACTIVE_NOTIFICATION_TYPE("116", "Inactive notification type."),
	INVOICE_PAYMENT_LINK_ALREADY_PROCESSED("117", "Invoice payment link already processed."),
	ORDER_BOUNCED("118", "Order bounced."), REQUEST_DROPPED("119", "Request dropped."),
	PAYMENT_LINK_TERMS_AND_CONDITIONS_NOT_FOUND("120", "Payment link terms and conditions not found."),
	CARD_NUMBER_IS_NOT_VERIFIED("121", "Card number is not verified."),
	INVALID_DATE_INTERVAL("122", "Invalid date interval."),
	YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_ATTEMPTS("123", "You have exceeded the maximum number of attempts."),
	ACCOUNT_SUCCESSFULLY_CREATED("124", "Account successfully created."),
	INVOICE_ALREADY_PAID("125", "Invoice already paid."), DUPLICATE_INVOICE_ID("126", "Duplicate invoice ID."),
	MERCHANT_REFERENCE_IS_NOT_GENERATED_YET("127", "Merchant reference is not generated yet."),
	GENERATED_REPORT_PENDING("128", "The generated report is still pending, you can’t download it now."),
	QUEUE_IS_FULL("129", "“Downloaded report” queue is full. Wait till its empty again."),
	YOUR_SEARCH_RESULTS_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_RECORDS("134",
			"Your search results have exceeded the maximum number of records."),
	THE_BATCH_FILE_VALIDATION_IS_FAILED("136", "The Batch file validation is failed."),
	INVALID_BATCH_FILE_EXECUTION_DATE("137", "Invalid Batch file execution date."),
	THE_BATCH_FILE_STILL_UNDER_VALIDATION("138", "The Batch file still under validation."),
	THE_BATCH_FILE_STILL_UNDER_PROCESSING("140", "The Batch file still under processing."),
	THE_BATCH_REFERENCE_DOES_NOT_EXIST("141", "The Batch reference does not exist."),
	THE_BATCH_FILE_HEADER_IS_INVALID("142", "The Batch file header is invalid."),
	INVALID_BATCH_FILE("144", "Invalid Batch file."),
	THE_BATCH_REFERENCE_IS_ALREADY_EXIST("146", "The Batch reference is already exist."),
	THE_BATCH_PROCESS_REQUEST_HAS_BEEN_RECEIVED("147", "The Batch process request has been received."),
	BATCH_FILE_WILL_BE_PROCESSED("148", "Batch file will be processed."),
	PAYMENT_LINK_REQUEST_ID_NOT_FOUND("149", "Payment link request id not found."),
	PAYMENT_LINK_IS_ALREADY_OPEN("150", "Payment link is already open."),
	THREE_DS_ID_DOES_NOT_EXIST("151", "3ds_id does not exist."),
	THREE_DS_VERIFICATION_DOESNT_MATCH_THE_REQUEST_DETAILS("152",
			"3Ds verification doesn’t match the request details."),
	YOU_HAVE_REACHED_THE_MAXIMUM_NUMBER_OF_UPLOAD_RETRIES("154",
			"You have reached the maximum number of upload retries."),
	THE_UPLOAD_RETRIES_IS_NOT_CONFIGURED("155", "The upload retries is not configured."),
	OPERATION_NOT_ALLOWED_THE_SPECIFIED_ORDER_IS_NOT_CONFIRMED_YET("662",
			"Operation not allowed. The specified order is not confirmed yet."),
	TRANSACTION_DECLINED("666", "Transaction declined."), TRANSACTION_CLOSED("773", "Transaction closed."),
	THE_TRANSACTION_PROCESSED("777", "The transaction has been processed, but failed to receive confirmation."),
	SESSION_TIMED_OUT("778", "Session timed-out."), TRANSFORMATION_ERROR("779", "Transformation error."),
	TRANSACTION_NUMBER_TRANSFORMATION_ERROR("780", "Transaction number transformation error."),
	MESSAGE_OR_RESPONSE_CODE_TRANSFORMATION_ERROR("781", "Message or response code transformation error."),
	INSTALLMENTS_SERVICE_INACTIVE("783", "Installments service inactive."),
	TRANSACTION_STILL_PROCESSING("784", "Transaction still processing you can’t make another transaction."),
	TRANSACTION_BLOCKED_BY_FRAUD_CHECK("785", "Transaction blocked by fraud check."),
	FAILED_TO_AUTHENTICATE_THE_USER("787", "Failed to authenticate the user."),
	INVALID_BILL_NUMBER("788", "Invalid bill number."), EXPIRED_BILL_NUMBER("789", "Expired bill number."),
	INVALID_BILL_TYPE_CODE("790", "Invalid bill type code.");

	private final String code;
	private final String msg;

	private static Map<String, PayfortResponseMsg> map = new HashMap<>();

	static {
		for (final PayfortResponseMsg payfortResponseMsg : PayfortResponseMsg.values()) {
			map.put(payfortResponseMsg.code, payfortResponseMsg);
		}
	}

	private PayfortResponseMsg(final String code, final String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public static Optional<PayfortResponseMsg> getMessageFromCode(final String code) {
		return Optional.ofNullable(map.get(code));
	}

}
