package com.sacc.saccpayment.payfort.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum PayfortResponseStatus {

	INVALID_REQUEST("00", "Invalid Request."), ORDER_STORED("01", "Order Stored."),
	AUTHORIZATION_SUCCESS("02", "Authorization Success."), AUTHORIZATION_FAILED("03", "Authorization Failed."),
	CAPTURE_SUCCESS("04", "Capture Success."), CAPTURE_FAILED("05", "Capture failed."),
	REFUND_SUCCESS("06", "Refund Success."), REFUND_FAILED("07", "Refund Failed."),
	AUTHORIZATION_VOIDED_SUCCESSFULLY("08", "Authorization Voided Successfully."),
	AUTHORIZATION_VOID_FAILED("09", "Authorization Void Failed."), INCOMPLETE("10", "Incomplete."),
	CHECK_STATUS_FAILED("11", "Check status Failed."), CHECK_STATUS_SUCCESS("12", "Check status success."),
	PURCHASE_FAILURE("13", "Purchase Failure."), PURCHASE_SUCCESS("14", "Purchase Success."),
	UNCERTAIN_TRANSACTION("15", "Uncertain Transaction."), TOKENIZATION_FAILED("17", "Tokenization failed."),
	TOKENIZATION_SUCCESS("18", "Tokenization success."), TRANSACTION_PENDING("19", "Transaction pending."),
	ON_HOLD("20", "On hold."), SDK_TOKEN_CREATION_FAILURE("21", "SDK Token creation failure."),
	SDK_TOKEN_CREATION_SUCCESS("22", "SDK Token creation success."),
	FAILED_TO_PROCESS_DIGITAL_WALLET_SERVICE("23", "Failed to process Digital Wallet service."),
	DIGITAL_WALLET_ORDER_PROCESSED_SUCCESSFULLY("24", "Digital wallet order processed successfully."),
	CHECK_CARD_BALANCE_FAILED("27", "Check card balance failed."),
	CHECK_CARD_BALANCE_SUCCESS("28", "Check card balance success."), REDEMPTION_FAILED("29", "Redemption failed."),
	REDEMPTION_SUCCESS("30", "Redemption success."),
	REVERSE_REDEMPTION_TRANSACTION_FAILED("31", "Reverse Redemption transaction failed."),
	REVERSE_REDEMPTION_TRANSACTION_SUCCESS("32", "Reverse Redemption transaction success."),
	TRANSACTION_IN_REVIEW("40", "Transaction In review."),
	CURRENCY_CONVERSION_SUCCESS("42", "Currency conversion success."),
	CURRENCY_CONVERSION_FAILED("43", "Currency conversion failed."), THREE_DS_SUCCESS("44", "3ds success."),
	THREE_DS_FAILED("45", "3ds failed."), BILL_CREATION_SUCCESS("46", "Bill creation success."),
	BILL_CREATION_FAILED("47", "Bill creation failed."),
	GENERATING_INVOICE_PAYMENT_LINK_SUCCESS("48", "Generating invoice payment link success."),
	GENERATING_INVOICE_PAYMENT_LINK_FAILED("49", "Generating invoice payment link failed."),
	BATCH_FILE_UPLOAD_SUCCESSFULLY("50", "Batch file upload successfully."),
	UPLOAD_BATCH_FILE_FAILED("51", "Upload batch file failed."),
	TOKEN_CREATED_SUCCESSFULLY("52", "Token created successfully."),
	TOKEN_CREATION_FAILED("53", "Token creation failed."), GET_TOKENS_SUCCESS("54", "Get Tokens Success."),
	GET_TOKENS_FAILED("55", "Get Tokens Failed."), REPORTING_REQUEST_SUCCESS("56", "Reporting Request Success."),
	REPORTING_REQUEST_FAILED("57", "Reporting Request Failed."),
	TOKEN_UPDATED_SUCCESSFULLY("58", "Token updated successfully."),
	TOKEN_UPDATED_FAILED("59", "Token updated failed."),
	GET_INSTALLMENT_PLANS_SUCCESSFULLY("62", "Get Installment Plans Successfully."),
	GET_INSTALLMENT_PLANS_FAILED("63", "Get Installment plans Failed."),
	DELETE_TOKEN_SUCCESS("66", "Delete Token Success."),
	GET_BATCH_RESULTS_SUCCESSFULLY("70", "Get batch results successfully."),
	GET_BATCH_RESULTS_FAILED("71", "Get batch results failed."),
	BATCH_PROCESSING_SUCCESS("72", "Batch processing success."),
	BATCH_PROCESSING_FAILED("73", "Batch processing failed."),
	BANK_TRANSFER_SUCCESSFULLY("74", "Bank transfer successfully."),
	BANK_TRANSFER_FAILED("75", "Bank transfer failed."),
	BATCH_VALIDATION_SUCCESSFULLY("76", "Batch validation successfully."),
	BATCH_VALIDATION_FAILED("77", "Batch validation failed."),
	CREDIT_CARD_VERIFIED_SUCCESSFULLY("80", "Credit card verified successfully."),
	FAILED_TO_VERIFY_CREDIT_CARD("81", "Failed to verify credit card.");

	private final String code;
	private final String msg;
	private static Map<String, PayfortResponseStatus> map = new HashMap<>();

	static {
		for (final PayfortResponseStatus payfortResponseStatus : PayfortResponseStatus.values()) {
			map.put(payfortResponseStatus.code, payfortResponseStatus);
		}
	}

	private PayfortResponseStatus(final String code, final String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public static Optional<PayfortResponseStatus> getStatusFromCode(final String code) {
		return Optional.ofNullable(map.get(code));
	}

}
