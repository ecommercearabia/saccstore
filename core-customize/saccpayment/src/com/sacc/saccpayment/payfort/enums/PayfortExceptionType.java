package com.sacc.saccpayment.payfort.enums;

public enum PayfortExceptionType {

	SERVER_ERROR("PAYFORT ERVER ERROR"), PARSING_ERROR("PARSING ERROR"), HASHING_ERROR("HASHING ERROR"), ORDER_ALREADY_PAID(
			"ORDER ALREADY PAID"), SIGNATURE_MISMATCH("SIGNATURE MISMATCH");

	private final String msg;

	public String getMsg() {
		return msg;
	}

	private PayfortExceptionType(final String msg) {
		this.msg = msg;
	}

}
