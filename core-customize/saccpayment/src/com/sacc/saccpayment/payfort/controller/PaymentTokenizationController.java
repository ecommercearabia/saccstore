package com.sacc.saccpayment.payfort.controller;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sacc.saccpayment.payfort.bean.PayfortConnectionBean;
import com.sacc.saccpayment.payfort.entry.form.PurchaseForm;
import com.sacc.saccpayment.payfort.entry.form.TokenizationForm;
import com.sacc.saccpayment.payfort.entry.request.AuthorizePurchaseRequestBean;
import com.sacc.saccpayment.payfort.entry.request.TokenizationRequestBean;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;
import com.sacc.saccpayment.payfort.exception.PayfortException;
import com.sacc.saccpayment.payfort.service.PayfortService;
import com.sacc.saccpayment.payfort.util.PayfortConnectionUtil;

@Controller
@RequestMapping("/")
public class PaymentTokenizationController {

	@Resource(name = "payfortService")
	private PayfortService payfortService;

	@PostMapping(value = "/standard")
	public String getPayfortSourceForm(final Model model,
			@ModelAttribute("tokenizationForm") final TokenizationForm tokenizationForm) throws PayfortException {
		final PayfortConnectionBean connectionBean = PayfortConnectionUtil.getConnectionBean();
		final Optional<TokenizationRequestBean> requestBean = getPayfortService()
				.prepareTokenizationForm(PayfortConnectionUtil.getConnectionBean(), tokenizationForm);
		model.addAttribute("tokenizationRequest", requestBean.orElse(null));

		return "tokenizationForm";
	}

	@GetMapping(value = "/callback")
	public String callback(final HttpServletRequest request, final Model model) throws PayfortException {
		final String tokenName = request.getParameter("token_name");
		final String orderNumber = request.getParameter("merchant_reference");

		final PurchaseForm purchaseForm = new PurchaseForm();
		purchaseForm.setTokenName(tokenName);
		purchaseForm.setOrderNumber(orderNumber);
		purchaseForm.setCustomerIp(request.getLocalAddr());

		model.addAttribute("purchaseForm", purchaseForm);

		return "purchaseForm";
	}

	@PostMapping(value = "/purchase")
	public String purchase(final HttpServletRequest request, final Model model,
			@ModelAttribute("purchaseForm") final PurchaseForm purchaseForm) throws PayfortException {
		final AuthorizePurchaseRequestBean pRequest = new AuthorizePurchaseRequestBean();
		final PayfortConnectionBean connectionBean = PayfortConnectionUtil.getConnectionBean();
		pRequest.setAccessCode(connectionBean.getAccessCode());
		pRequest.setMerchantIdentifier(connectionBean.getMerchantIdentifier());
		pRequest.setAmount(String.valueOf(purchaseForm.getAmount()));
		pRequest.setCurrency(purchaseForm.getCurrency());
		pRequest.setCustomerIp(purchaseForm.getCustomerIp());
		pRequest.setCustomerEmail(purchaseForm.getCustomerEmail());
		pRequest.setTokenName(purchaseForm.getTokenName());
		pRequest.setMerchantReference(purchaseForm.getOrderNumber());
		pRequest.setCustomerName(purchaseForm.getCustomerName());
		pRequest.setLanguage(purchaseForm.getLanguage());
		pRequest.setReturnUrl(connectionBean.getReturnUrl().concat("/finish"));
		final Optional<AuthorizePurchaseResponseBean> purchase = getPayfortService().purchase(connectionBean, pRequest);

		return purchase.isPresent() ? "redirect:".concat(purchase.get().getThreeDSUrl()) : "purchaseForm";
	}

	private AuthorizePurchaseRequestBean getPurchaseRequestBean() {

//		String tokenName = String.valueOf(request.getAttribute("token_name"));
//		PayfortConnectionBean connectionBean = PayfortConnectionUtil.getConnectionBean();
//		String returnUrl = connectionBean.getReturnUrl().concat("/finish");
//		connectionBean.setReturnUrl(returnUrl);
//		getPayfortService().purchase(PayfortConnectionUtil.getConnectionBean(), getPurchaseRequestBean());
//
//		AuthorizePurchaseRequestBean requestBean = new AuthorizePurchaseRequestBean();
//		requestBean.setAccessCode(PayfortCredentialsConstants.ACCESS_CODE);
//		requestBean.setMerchantIdentifier();

		return null;
	}

	@RequestMapping("/")
	public String getIndex(final Model model) {
		final TokenizationForm tokenizationForm = new TokenizationForm();
		tokenizationForm.setLanguage("en");
		model.addAttribute("tokenizationForm", tokenizationForm);
		return "index";
	}

	public PayfortService getPayfortService() {
		return payfortService;
	}

}
