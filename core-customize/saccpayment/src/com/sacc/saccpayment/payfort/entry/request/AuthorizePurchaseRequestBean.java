package com.sacc.saccpayment.payfort.entry.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.RequestBaseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;

public class AuthorizePurchaseRequestBean extends RequestBaseBean {

	@SerializedName("command")
	@Expose
	@IncludeInSignature
	private String command;

	public AuthorizePurchaseRequestBean(final PayfortCommandType command) {
		setCommand(command.getCommand());
	}

	public AuthorizePurchaseRequestBean() {
		super();
	}

	@SerializedName("amount")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Amount cannot be empty on Authorization/Purchase")
	private String amount;

	@SerializedName("currency")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Currency cannot be empty on Authorization/Purchase")
	private String currency;

	@SerializedName("customer_email")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Customer Email cannot be empty on Authorization/Purchase")
	private String customerEmail;

	@SerializedName("customer_ip")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Customer IP cannot be empty on Authorization/Purchase!")
	private String customerIp;

	@SerializedName("token_name")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Token Name cannot be empty on Authorization/Purchase!")
	private String tokenName;

	@SerializedName("payment_option")
	@Expose
	@IncludeInSignature
	private String paymentOption;

	@SerializedName("eci")
	@Expose
	@IncludeInSignature
	private String eci;

	@SerializedName("order_description")
	@Expose
	@IncludeInSignature
	private String orderDescription;

	@SerializedName("card_security_code")
	@Expose
	@IncludeInSignature
	private String cardSecurityCode;

	@SerializedName("customer_name")
	@Expose
	@IncludeInSignature
	private String customerName;

	@SerializedName("merchant_extra")
	@Expose
	@IncludeInSignature
	private String merchantExtra;

	@SerializedName("merchant_extra1")
	@Expose
	@IncludeInSignature
	private String merchantExtra1;

	@SerializedName("merchant_extra2")
	@Expose
	@IncludeInSignature
	private String merchantExtra2;

	@SerializedName("merchant_extra3")
	@Expose
	@IncludeInSignature
	private String merchantExtra3;

	@SerializedName("merchant_extra4")
	@Expose
	@IncludeInSignature
	private String merchantExtra4;

	@SerializedName("merchant_extra5")
	@Expose
	@IncludeInSignature
	private String merchantExtra5;

	@SerializedName("remember_me")
	@Expose
	@IncludeInSignature
	private Boolean rememberMe;

	@SerializedName("phone_number")
	@Expose
	@IncludeInSignature
	private String phoneNumber;

	@SerializedName("settlement_reference")
	@Expose
	@IncludeInSignature
	private String settlementReference;

	@SerializedName("return_url")
	@Expose
	@IncludeInSignature
	private String returnUrl;

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(final String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerIp() {
		return customerIp;
	}

	public void setCustomerIp(final String customerIp) {
		this.customerIp = customerIp;
	}

	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(final String tokenName) {
		this.tokenName = tokenName;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(final String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(final String eci) {
		this.eci = eci;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(final String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public String getCardSecurityCode() {
		return cardSecurityCode;
	}

	public void setCardSecurityCode(final String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	public String getMerchantExtra() {
		return merchantExtra;
	}

	public void setMerchantExtra(final String merchantExtra) {
		this.merchantExtra = merchantExtra;
	}

	public String getMerchantExtra1() {
		return merchantExtra1;
	}

	public void setMerchantExtra1(final String merchantExtra1) {
		this.merchantExtra1 = merchantExtra1;
	}

	public String getMerchantExtra2() {
		return merchantExtra2;
	}

	public void setMerchantExtra2(final String merchantExtra2) {
		this.merchantExtra2 = merchantExtra2;
	}

	public String getMerchantExtra3() {
		return merchantExtra3;
	}

	public void setMerchantExtra3(final String merchantExtra3) {
		this.merchantExtra3 = merchantExtra3;
	}

	public String getMerchantExtra4() {
		return merchantExtra4;
	}

	public void setMerchantExtra4(final String merchantExtra4) {
		this.merchantExtra4 = merchantExtra4;
	}

	public String getMerchantExtra5() {
		return merchantExtra5;
	}

	public void setMerchantExtra5(final String merchantExtra5) {
		this.merchantExtra5 = merchantExtra5;
	}

	public Boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(final Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSettlementReference() {
		return settlementReference;
	}

	public void setSettlementReference(final String settlementReference) {
		this.settlementReference = settlementReference;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(final String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "AuthorizePurchaseRequestBean [command=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", amount=" + amount + ", currency=" + currency + ", language=" + getLanguage() + ", customerEmail="
				+ customerEmail + ", customerIp=" + customerIp + ", tokenName=" + tokenName + ", signature="
				+ getSignature() + ", paymentOption=" + paymentOption + ", eci=" + eci + ", orderDescription="
				+ orderDescription + ", cardSecurityCode=" + cardSecurityCode + ", customerName=" + customerName
				+ ", merchantExtra=" + merchantExtra + ", merchantExtra1=" + merchantExtra1 + ", merchantExtra2="
				+ merchantExtra2 + ", merchantExtra3=" + merchantExtra3 + ", merchantExtra4=" + merchantExtra4
				+ ", merchantExtra5=" + merchantExtra5 + ", rememberMe=" + rememberMe + ", phoneNumber=" + phoneNumber
				+ ", settlementReference=" + settlementReference + ", returnUrl=" + returnUrl + "]";
	}

}
