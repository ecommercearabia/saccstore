package com.sacc.saccpayment.payfort.entry.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;

public class RequestBaseBean {


	@SerializedName("access_code")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Access Code cannot be empty!")
	private String accessCode;

	@SerializedName("merchant_identifier")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Merchant Identifier cannot be empty!")
	private String merchantIdentifier;

	@SerializedName("merchant_reference")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Merchant Reference cannot be empty!")
	private String merchantReference;

	@SerializedName("language")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Language cannot be empty!")
	private String language;

	@SerializedName("signature")
	@Expose
	private String signature;

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(final String accessCode) {
		this.accessCode = accessCode;
	}

	public String getMerchantIdentifier() {
		return merchantIdentifier;
	}

	public void setMerchantIdentifier(final String merchantIdentifier) {
		this.merchantIdentifier = merchantIdentifier;
	}

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(final String merchantReference) {
		this.merchantReference = merchantReference;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(final String signature) {
		this.signature = signature;
	}

}
