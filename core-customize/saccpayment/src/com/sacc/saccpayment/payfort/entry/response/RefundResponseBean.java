package com.sacc.saccpayment.payfort.entry.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.ResponseBaseBean;

public class RefundResponseBean extends ResponseBaseBean {

	@SerializedName("command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Command cannot be empty!")
	private String command;

	@SerializedName("amount")
	@Expose
	private String amount;

	@SerializedName("currency")
	@Expose
	private String currency;

	@SerializedName("fort_id")
	@Expose
	private String fortId;

	@SerializedName("order_description")
	@Expose
	private String orderDescription;

	@SerializedName("maintenance_reference")
	@Expose
	private String maintenanceReference;

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getFortId() {
		return fortId;
	}

	public void setFortId(final String fortId) {
		this.fortId = fortId;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(final String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public String getMaintenanceReference() {
		return maintenanceReference;
	}

	public void setMaintenanceReference(final String maintenanceReference) {
		this.maintenanceReference = maintenanceReference;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "RefundResponseBean [command=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMaintenanceReference()
				+ ", amount=" + amount + ", currency=" + currency + ", language=" + getLanguage() + ", signature="
				+ getSignature() + ", fortId=" + fortId + ", orderDescription=" + orderDescription
				+ ", responseMessage=" + getResponseMessage() + ", responseCode=" + getAccessCode() + ", status="
				+ getStatus() + ", maintenanceReference=" + maintenanceReference + ", getCommand()=" + getCommand()
				+ ", getAccessCode()=" + getAccessCode() + ", getMerchantIdentifier()=" + getMerchantIdentifier()
				+ ", getMerchantReference()=" + getMerchantReference() + ", getAmount()=" + getAmount()
				+ ", getCurrency()=" + getCurrency() + ", getLanguage()=" + getLanguage() + ", getSignature()="
				+ getSignature() + ", getFortId()=" + getFortId() + ", getOrderDescription()=" + getOrderDescription()
				+ ", getResponseMessage()=" + getResponseMessage() + ", getResponseCode()=" + getResponseCode()
				+ ", getStatus()=" + getStatus() + ", getMaintenanceReference()=" + getMaintenanceReference()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
