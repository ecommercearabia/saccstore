package com.sacc.saccpayment.payfort.entry.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.RequestBaseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;

public class TokenizationRequestBean extends RequestBaseBean {

	public TokenizationRequestBean() {
		setCommand(PayfortCommandType.TOKENIZATION.getCommand());
	}

	@SerializedName("service_command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "service_command cannot be empty!")
	private String command;

	@SerializedName("token_name")
	@Expose
	@IncludeInSignature
	private String tokenName;

	@SerializedName("url")
	private String url;

	@SerializedName("return_url")
	@Expose
	@IncludeInSignature
	private String returnUrl;

	public String getCommand() {
		return this.command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(final String tokenName) {
		this.tokenName = tokenName;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(final String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "TokenizationRequestBean [serviceCommand=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchanntIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", language=" + getLanguage() + ", signature=" + getSignature() + ", tokenName=" + tokenName
				+ ", returnUrl=" + returnUrl + "]";
	}

}
