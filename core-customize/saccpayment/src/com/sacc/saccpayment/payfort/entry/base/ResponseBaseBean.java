package com.sacc.saccpayment.payfort.entry.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;

public class ResponseBaseBean extends RequestBaseBean {

	@SerializedName("response_message")
	@Expose
	@IncludeInSignature
	private String responseMessage;

	@SerializedName("response_code")
	@Expose
	@IncludeInSignature
	private String responseCode;

	@SerializedName("status")
	@Expose
	@IncludeInSignature
	private String status;

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(final String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(final String responseCode) {
		this.responseCode = responseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ResponseBaseBean [responseMessage=" + responseMessage + ", responseCode=" + responseCode + ", status="
				+ status + "]";
	}

}
