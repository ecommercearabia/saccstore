package com.sacc.saccpayment.payfort.entry.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.ResponseBaseBean;

public class AuthorizePurchaseResponseBean extends ResponseBaseBean{

	@SerializedName("command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Command cannot be empty!")
	private String command;

	@SerializedName("amount")
	@Expose
	private String amount;

	@SerializedName("currency")
	@Expose
	private String currency;

	@SerializedName("customer_email")
	@Expose
	private String customerEmail;

	@SerializedName("customer_ip")
	@Expose
	private String customerIp;

	@SerializedName("token_name")
	@Expose
	private String tokenName;

	@SerializedName("payment_option")
	@Expose
	private String paymentOption;

	@SerializedName("eci")
	@Expose
	private String eci;

	@SerializedName("order_description")
	@Expose
	private String orderDescription;

	@SerializedName("customer_name")
	@Expose
	private String customerName;

	@SerializedName("merchant_extra")
	@Expose
	private String merchantExtra;

	@SerializedName("merchant_extra1")
	@Expose
	private String merchantExtra1;

	@SerializedName("merchant_extra2")
	@Expose
	private String merchantExtra2;

	@SerializedName("merchant_extra3")
	@Expose
	private String merchantExtra3;

	@SerializedName("merchant_extra4")
	@Expose
	private String merchantExtra4;

	@SerializedName("merchant_extra5")
	@Expose
	private String merchantExtra5;

	@SerializedName("expiry_date")
	@Expose
	private String expiryDate;

	@SerializedName("card_number")
	@Expose
	private String cardNumber;

	@SerializedName("remember_me")
	@Expose
	private boolean rememberMe;

	@SerializedName("phone_number")
	@Expose
	private String phoneNumber;

	@SerializedName("settlement_reference")
	@Expose
	private String settlementReference;

	@SerializedName("fort_id")
	@Expose
	private String fortId;

	@SerializedName("authorization_code")
	@Expose
	private String authorizationCode;

	@SerializedName("card_holder_name")
	@Expose
	private String cardHolderName;

	@SerializedName("3ds_url")
	@Expose
	private String threeDSUrl;

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(final String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerIp() {
		return customerIp;
	}

	public void setCustomerIp(final String customerIp) {
		this.customerIp = customerIp;
	}

	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(final String tokenName) {
		this.tokenName = tokenName;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(final String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(final String eci) {
		this.eci = eci;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(final String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	public String getMerchantExtra() {
		return merchantExtra;
	}

	public void setMerchantExtra(final String merchantExtra) {
		this.merchantExtra = merchantExtra;
	}

	public String getMerchantExtra1() {
		return merchantExtra1;
	}

	public void setMerchantExtra1(final String merchantExtra1) {
		this.merchantExtra1 = merchantExtra1;
	}

	public String getMerchantExtra2() {
		return merchantExtra2;
	}

	public void setMerchantExtra2(final String merchantExtra2) {
		this.merchantExtra2 = merchantExtra2;
	}

	public String getMerchantExtra3() {
		return merchantExtra3;
	}

	public void setMerchantExtra3(final String merchantExtra3) {
		this.merchantExtra3 = merchantExtra3;
	}

	public String getMerchantExtra4() {
		return merchantExtra4;
	}

	public void setMerchantExtra4(final String merchantExtra4) {
		this.merchantExtra4 = merchantExtra4;
	}

	public String getMerchantExtra5() {
		return merchantExtra5;
	}

	public void setMerchantExtra5(final String merchantExtra5) {
		this.merchantExtra5 = merchantExtra5;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(final String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(final boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSettlementReference() {
		return settlementReference;
	}

	public void setSettlementReference(final String settlementReference) {
		this.settlementReference = settlementReference;
	}

	public String getFortId() {
		return fortId;
	}

	public void setFortId(final String fortId) {
		this.fortId = fortId;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(final String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(final String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getThreeDSUrl() {
		return threeDSUrl;
	}

	public void setThreeDSUrl(final String threeDSUrl) {
		this.threeDSUrl = threeDSUrl;
	}


	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "AuthorizePurchaseResponseBean [command=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", language=" + getLanguage() + ", signature=" + getSignature() + ", amount=" + amount + ", currency="
				+ currency + ", customerEmail=" + customerEmail + ", customerIp=" + customerIp + ", tokenName="
				+ tokenName + ", paymentOption=" + paymentOption + ", eci=" + eci + ", orderDescription="
				+ orderDescription + ", customerName=" + customerName + ", merchantExtra=" + merchantExtra
				+ ", merchantExtra1=" + merchantExtra1 + ", merchantExtra2=" + merchantExtra2 + ", merchantExtra3="
				+ merchantExtra3 + ", merchantExtra4=" + merchantExtra4 + ", merchantExtra5=" + merchantExtra5
				+ ", expiryDate=" + expiryDate + ", cardNumber=" + cardNumber + ", status=" + getStatus() + ", rememberMe="
				+ rememberMe + ", phoneNumber=" + phoneNumber + ", settlementReference=" + settlementReference
				+ ", fortId=" + fortId + ", authorizationCode=" + authorizationCode + ", responseMessage="
				+ getResponseMessage() + ", responseCode=" + getResponseCode() + ", cardHolderName=" + cardHolderName
				+ ", threeDSUrl=" + threeDSUrl + "]";
	}

}