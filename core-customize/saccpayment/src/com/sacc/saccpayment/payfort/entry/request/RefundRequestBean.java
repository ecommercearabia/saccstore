package com.sacc.saccpayment.payfort.entry.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.RequestBaseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;

public class RefundRequestBean extends RequestBaseBean {

	@SerializedName("command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "service_command cannot be empty!")
	private String command;

	public RefundRequestBean() {
		setCommand(PayfortCommandType.REFUND.getCommand());
	}

	@SerializedName("amount")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Amount cannot be empty on Refund")
	private String amount;

	@SerializedName("currency")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "currency cannot be empty on Refund")
	private String currency;

	@SerializedName("fort_id")
	@Expose
	@IncludeInSignature
	private String fortId;

	@SerializedName("order_description")
	@Expose
	@IncludeInSignature
	private String orderDescription;

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getFortId() {
		return fortId;
	}

	public void setFortId(final String fortId) {
		this.fortId = fortId;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(final String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "RefundRequestBean [command=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", amount=" + amount + ", currency=" + currency + ", language=" + getLanguage() + ", signature="
				+ getSignature() + ", fortId=" + fortId + ", orderDescription=" + orderDescription + "]";
	}

}
