package com.sacc.saccpayment.payfort.entry.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.ResponseBaseBean;

public class TokenizationResponseBean extends ResponseBaseBean {

	@SerializedName("service_command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "service_command cannot be empty!")
	private String command;

	@SerializedName("token_name")
	@Expose
	@IncludeInSignature
	private String tokenName;

	@SerializedName("expiry_date")
	@Expose
	private String expiryDate;

	@SerializedName("card_number")
	@Expose
	private String cardNumber;

	@SerializedName("card_bin")
	@Expose
	private String cardBin;

	@SerializedName("return_url")
	@Expose
	@IncludeInSignature
	private String returnUrl;

	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(final String tokenName) {
		this.tokenName = tokenName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(final String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardBin() {
		return cardBin;
	}

	public void setCardBin(final String cardBin) {
		this.cardBin = cardBin;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(final String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String toString() {
		return "TokenizationResponseBean [serviceCommand=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchanntIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", language=" + getLanguage() + ", signature=" + getSignature() + ", tokenName=" + tokenName
				+ ", expiryDate=" + expiryDate + ", cardNumber=" + cardNumber + ", responseMessage="
				+ getResponseMessage() + ", responseCode=" + getResponseCode() + ", status=" + getStatus()
				+ ", cardBin=" + cardBin + ", returnUrl=" + returnUrl + "]";
	}

}
