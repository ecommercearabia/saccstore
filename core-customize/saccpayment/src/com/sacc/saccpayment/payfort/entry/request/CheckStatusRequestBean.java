package com.sacc.saccpayment.payfort.entry.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.RequestBaseBean;
import com.sacc.saccpayment.payfort.enums.PayfortCommandType;

public class CheckStatusRequestBean extends RequestBaseBean {

	public CheckStatusRequestBean() {
		setCommand(PayfortCommandType.CHECK_STATUS.getCommand());
	}

	@SerializedName("query_command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "service_command cannot be empty!")
	private String command;

	@SerializedName("fort_id")
	@Expose
	@IncludeInSignature
	private String fortId;

	@SerializedName("return_third_party_response_codes")
	@Expose
	@IncludeInSignature
	private String returnThirdPartyResponseCodes;


	public String getCommand() {
		return this.command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	public String getFortId() {
		return fortId;
	}

	public void setFortId(final String fortId) {
		this.fortId = fortId;
	}

	public String getReturnThirdPartyResponseCodes() {
		return returnThirdPartyResponseCodes;
	}

	public void setReturnThirdPartyResponseCodes(final String returnThirdPartyResponseCodes) {
		this.returnThirdPartyResponseCodes = returnThirdPartyResponseCodes;
	}

	@Override
	public String toString() {
		return "CheckStatusRequestBean [queryCommand=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", language=" + getLanguage() + ", signature=" + getSignature() + ", fortId=" + fortId
				+ ", returnThirdPartyResponseCodes=" + returnThirdPartyResponseCodes + "]";
	}

}
