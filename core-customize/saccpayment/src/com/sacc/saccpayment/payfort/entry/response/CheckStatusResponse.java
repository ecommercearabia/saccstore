package com.sacc.saccpayment.payfort.entry.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;
import com.sacc.saccpayment.payfort.entry.base.ResponseBaseBean;

public class CheckStatusResponse extends ResponseBaseBean {

	@SerializedName("query_command")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Command cannot be empty!")
	private String command;

	@SerializedName("fort_id")
	@Expose
	private String fortId;

	@SerializedName("transaction_status")
	@Expose
	private String transactionStatus;

	@SerializedName("transaction_code")
	@Expose
	private String transactionCode;

	@SerializedName("transaction_message")
	@Expose
	private String transactionMessage;

	@SerializedName("refunded_amount")
	@Expose
	private String refundedAmount;

	@SerializedName("captured_amount")
	@Expose
	private String capturedAmount;

	@SerializedName("authorized_amount")
	@Expose
	private String authorizedAmount;

	@SerializedName("authorization_code")
	@Expose
	private String authorizationCode;

	@Expose
	private String processorResponseCode;

	@SerializedName("acquirer_response_code")
	@Expose
	private String acquirerResponseCode;

	public String getFortId() {
		return fortId;
	}

	public void setFortId(final String fortId) {
		this.fortId = fortId;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(final String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(final String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getTransactionMessage() {
		return transactionMessage;
	}

	public void setTransactionMessage(final String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}

	public String getRefundedAmount() {
		return refundedAmount;
	}

	public void setRefundedAmount(final String refundedAmount) {
		this.refundedAmount = refundedAmount;
	}

	public String getCapturedAmount() {
		return capturedAmount;
	}

	public void setCapturedAmount(final String capturedAmount) {
		this.capturedAmount = capturedAmount;
	}

	public String getAuthorizedAmount() {
		return authorizedAmount;
	}

	public void setAuthorizedAmount(final String authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(final String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getProcessorResponseCode() {
		return processorResponseCode;
	}

	public void setProcessorResponseCode(final String processorResponseCode) {
		this.processorResponseCode = processorResponseCode;
	}

	public String getAcquirerResponseCode() {
		return acquirerResponseCode;
	}

	public void setAcquirerResponseCode(final String acquirerResponseCode) {
		this.acquirerResponseCode = acquirerResponseCode;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(final String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "CheckStatusResponse [queryCommand=" + getCommand() + ", accessCode=" + getAccessCode()
				+ ", merchantIdentifier=" + getMerchantIdentifier() + ", merchantReference=" + getMerchantReference()
				+ ", language=" + getLanguage() + ", signature=" + getSignature() + ", fortId=" + fortId
				+ ", responseMessage=" + getResponseMessage() + ", responseCode=" + getResponseCode() + ", status="
				+ getStatus() + ", transactionStatus=" + transactionStatus + ", transactionCode=" + transactionCode
				+ ", transactionMessage=" + transactionMessage + ", refundedAmount=" + refundedAmount
				+ ", capturedAmount=" + capturedAmount + ", authorizedAmount=" + authorizedAmount
				+ ", authorizationCode=" + authorizationCode + ", processorResponseCode=" + processorResponseCode
				+ ", acquirerResponseCode=" + acquirerResponseCode + "]";
	}

}
