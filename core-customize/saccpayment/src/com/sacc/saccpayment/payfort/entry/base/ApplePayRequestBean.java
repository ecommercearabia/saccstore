package com.sacc.saccpayment.payfort.entry.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.payfort.annotations.IncludeInSignature;
import com.sacc.saccpayment.payfort.annotations.RequiredParam;


public class ApplePayRequestBean
{

	@SerializedName("merchantIdentifier")
	@Expose
	@RequiredParam(message = "Merchant Identifier cannot be empty!")
	private String merchantIdentifier;

	@SerializedName("displayName")
	@Expose
	@RequiredParam(message = "Display Name cannot be empty!")
	private String displayName;

	@SerializedName("initiative")
	@Expose
	@IncludeInSignature
	@RequiredParam(message = "Initiative cannot be empty!")
	private String initiative;

	@SerializedName("initiativeContext")
	@RequiredParam(message = "Initiative Context cannot be empty!")
	@Expose
	private String initiativeContext;

	/**
	 * @return the merchantIdentifier
	 */
	public String getMerchantIdentifier()
	{
		return merchantIdentifier;
	}

	/**
	 * @param merchantIdentifier
	 *           the merchantIdentifier to set
	 */
	public void setMerchantIdentifier(final String merchantIdentifier)
	{
		this.merchantIdentifier = merchantIdentifier;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the initiative
	 */
	public String getInitiative()
	{
		return initiative;
	}

	/**
	 * @param initiative
	 *           the initiative to set
	 */
	public void setInitiative(final String initiative)
	{
		this.initiative = initiative;
	}

	/**
	 * @return the initiativeContext
	 */
	public String getInitiativeContext()
	{
		return initiativeContext;
	}

	/**
	 * @param initiativeContext
	 *           the initiativeContext to set
	 */
	public void setInitiativeContext(final String initiativeContext)
	{
		this.initiativeContext = initiativeContext;
	}


}
