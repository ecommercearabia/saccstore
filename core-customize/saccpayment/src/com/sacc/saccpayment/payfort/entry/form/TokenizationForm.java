package com.sacc.saccpayment.payfort.entry.form;

public class TokenizationForm {

	private String orderNumber;

	private String language;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(final String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

}
