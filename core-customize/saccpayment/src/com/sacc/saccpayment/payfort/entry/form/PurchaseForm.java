package com.sacc.saccpayment.payfort.entry.form;

public class PurchaseForm {

	private String orderNumber;

	private String amount;

	private String language;

	private String currency;

	private String customerEmail;

	private String customerIp;

	private String customerName;

	private String tokenName;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(final String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(final String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerIp() {
		return customerIp;
	}

	public void setCustomerIp(final String customerIp) {
		this.customerIp = customerIp;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	public String getTokenName() {
		return tokenName;
	}

	public void setTokenName(final String tokenName) {
		this.tokenName = tokenName;
	}



}
