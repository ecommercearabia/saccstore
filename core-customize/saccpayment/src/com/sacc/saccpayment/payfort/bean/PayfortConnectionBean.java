package com.sacc.saccpayment.payfort.bean;

import com.sacc.saccpayment.payfort.enums.PayfortHashingType;

public class PayfortConnectionBean {

	private String url;

	private String accessCode;

	private PayfortHashingType hashingType;

	private String merchantIdentifier;

	private String language;

	private String returnUrl;

	private String hashingKey;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public PayfortHashingType getHashingType() {
		return hashingType;
	}

	public void setHashingType(final PayfortHashingType hashingType) {
		this.hashingType = hashingType;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(final String accessCode) {
		this.accessCode = accessCode;
	}

	public String getMerchantIdentifier() {
		return merchantIdentifier;
	}

	public void setMerchantIdentifier(final String merchantIdentifier) {
		this.merchantIdentifier = merchantIdentifier;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(final String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * @return the hashingKey
	 */
	public String getHashingKey()
	{
		return hashingKey;
	}

	/**
	 * @param hashingKey
	 *           the hashingKey to set
	 */
	public void setHashingKey(final String hashingKey)
	{
		this.hashingKey = hashingKey;
	}



}
