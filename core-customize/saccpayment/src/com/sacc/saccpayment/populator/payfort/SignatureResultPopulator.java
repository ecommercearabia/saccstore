/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 */
public class SignatureResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Resource(name = "cartService")
	private CartService cartService;

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		final BigDecimal amount = getAmount(source);
		final String currency = getCurrency();
		final String merchantID = "";
		final String orderPageSerialNumber = null;
		final String sharedSecret = null;
		final String orderPageVersion = "1.1";
		final String amountPublicSignature = null;
		final String currencyPublicSignature = getCurrency();
		final String transactionSignature = String.valueOf(source.get("merchant_reference"));
		final String signedFields = null;

		final SignatureData data = new SignatureData();
		data.setAmount(amount);
		data.setCurrency(currency);
		data.setMerchantID(merchantID);
		data.setOrderPageSerialNumber(orderPageSerialNumber);
		data.setSharedSecret(sharedSecret);
		data.setOrderPageVersion(orderPageVersion);
		data.setAmountPublicSignature(amountPublicSignature);
		data.setCurrencyPublicSignature(currencyPublicSignature);
		data.setTransactionSignature(transactionSignature);
		data.setSignedFields(signedFields);
		target.setSignatureData(data);
	}

	/**
	 *
	 */
	private String getCurrency()
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		final CurrencyModel cartCurrency = sessionCart.getCurrency();
		return null;
	}

	private BigDecimal getAmount(final Map<String, Object> source)
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		final CurrencyModel cartCurrency = sessionCart.getCurrency();
		final CurrencyModel baseCurrency = getCommonI18NService().getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = sessionCart.getTotalPrice();
		}
		else
		{
			amount = getCommonI18NService().convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(), sessionCart.getTotalPrice());
		}

		return BigDecimal.valueOf(amount);

	}

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


}
