/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;

import java.util.Map;


/**
 * @author amjad.shati@erabia.com
 */
public class SubscriptionInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		final SubscriptionInfoData data = new SubscriptionInfoData();
		final String subscriptionID = String.valueOf(source.get("merchant_reference"));
		final String subscriptionIDPublicSignature = null;
		final String subscriptionSignedValue = null;
		data.setSubscriptionID(subscriptionID);
		data.setSubscriptionIDPublicSignature(subscriptionIDPublicSignature);
		data.setSubscriptionSignedValue(subscriptionSignedValue);
		target.setSubscriptionInfoData(data);
	}
}
