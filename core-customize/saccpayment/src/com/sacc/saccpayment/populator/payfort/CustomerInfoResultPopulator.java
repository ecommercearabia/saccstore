/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;

import java.util.Map;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 */
public class CustomerInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		AddressModel billingAddress = getCartService().getSessionCart().getPaymentAddress();
		if (billingAddress == null)
		{
			billingAddress = getCartService().getSessionCart().getDeliveryAddress();
		}
		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) getCartService().getSessionCart().getUser())
				.getContactEmail();

		final CustomerInfoData data = new CustomerInfoData();
		if (billingAddress != null)
		{
			saveBillingAddress(billingAddress, contactEmail, data);
		}
		final AddressModel deliveryAddress = getCartService().getSessionCart().getDeliveryAddress();
		if (deliveryAddress != null)
		{
			saveDeliveryAddress(data, deliveryAddress);
		}

		target.setCustomerInfoData(data);
	}

	private void saveDeliveryAddress(final CustomerInfoData data, final AddressModel deliveryAddress)
	{
		data.setShipToCity(deliveryAddress.getCity() == null ? deliveryAddress.getTown() : deliveryAddress.getCity().getName());
		data.setShipToCompany(deliveryAddress.getCompany());
		data.setShipToCountry(deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getIsocode());
		data.setShipToFirstName(deliveryAddress.getFirstname());
		data.setShipToLastName(deliveryAddress.getLastname());
		data.setShipToPhoneNumber(deliveryAddress.getMobile());
		data.setShipToPostalCode(deliveryAddress.getPostalcode());
		data.setShipToShippingMethod(getCartService().getSessionCart().getDeliveryMode() == null ? ""
				: getCartService().getSessionCart().getDeliveryMode().getName());
		data.setShipToState(deliveryAddress.getRegion() == null ? "" : deliveryAddress.getRegion().getIsocode());
		data.setShipToStreet1(deliveryAddress.getLine1());
		data.setShipToStreet2(deliveryAddress.getLine2());
	}

	private void saveBillingAddress(final AddressModel billingAddress, final String contactEmail, final CustomerInfoData data)
	{
		data.setBillToCity(billingAddress.getCity() == null ? billingAddress.getTown() : billingAddress.getCity().getName());
		data.setBillToCompany(billingAddress.getCompany());
		data.setBillToCountry(billingAddress.getCountry() == null ? "" : billingAddress.getCountry().getIsocode());
		data.setBillToCustomerIdRef(getCartService().getSessionCart().getUser().getUid());
		data.setBillToEmail(contactEmail);
		data.setBillToTitleCode(billingAddress.getTitle() == null ? "" : billingAddress.getTitle().getCode());
		data.setBillToFirstName(billingAddress.getFirstname());
		data.setBillToLastName(billingAddress.getLastname());
		data.setBillToPhoneNumber(billingAddress.getMobile());
		data.setBillToPostalCode(billingAddress.getPostalcode());
		data.setBillToState(billingAddress.getRegion() == null ? "" : billingAddress.getRegion().getIsocode());
		data.setBillToStreet1(billingAddress.getLine1());
		data.setBillToStreet2(billingAddress.getLine2());
	}

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}


}
