/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.sacc.saccpayment.payfort.enums.PayfortResponseStatus;
import com.sacc.saccpayment.payfort.service.PayfortService;


/**
 * @author amjad.shati@erabia.com
 */
public class CreateSubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Resource(name = "payfortService")
	private PayfortService payfortService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter source (Map<String, String>) cannot be null");
		validateParameterNotNull(target, "Parameter target (CreateSubscriptionResult) cannot be null");


		final String statusCode = String.valueOf(source.get("transaction_status"));
		final Optional<PayfortResponseStatus> statusFromCode = PayfortResponseStatus.getStatusFromCode(statusCode);
		if (statusFromCode.isPresent() && PayfortResponseStatus.PURCHASE_SUCCESS.equals(statusFromCode.get()))
		{
			target.setDecision(DecisionsEnum.ACCEPT.toString());
		}
		else
		{
			target.setDecision(DecisionsEnum.ERROR.toString());
		}
		target.setDecisionPublicSignature(getResultCode(source));
		target.setTransactionSignatureVerified(null);
		target.setReasonCode(1);
		target.setRequestId(String.valueOf(source.get("merchant_reference")));

	}

	private String getResultCode(final Map<String, Object> source)
	{
		final Object object = source.get("response_code");
		if (Objects.nonNull(object) && object instanceof String)
		{
			return String.valueOf(object);
		}


		return Strings.EMPTY;

	}

	/**
	 * @return the payfortService
	 */
	protected PayfortService getPayfortService()
	{
		return payfortService;
	}


}

