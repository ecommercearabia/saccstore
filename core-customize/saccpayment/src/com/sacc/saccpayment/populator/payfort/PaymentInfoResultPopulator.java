/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.order.CartService;

import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.google.gson.Gson;
import com.sacc.saccpayment.payfort.entry.response.AuthorizePurchaseResponseBean;


/**
 * @author amjad.shati@erabia.com
 */
public class PaymentInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Resource(name = "cartService")
	private CartService cartService;


	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final Map<String, Object> paymentResponseMap = getCartService().getSessionCart().getPaymentResponseMap();

		final String cardAccountNumber = getCardNum(paymentResponseMap);


		final String cardCardType = getCardType();// CreditCardType.valueOf(cardAccountNumber)
		final String cardCvNumber = null;
		final Integer cardExpirationMonth = getExpiryMonth(source);
		final Integer cardExpirationYear = getExpiryYear(source);
		final String cardIssueNumber = "0000";
		final String cardStartMonth = "01";
		final String cardStartYear = "30";
		final String paymentOption = "CARD";
		final String cardAccountHolderName = getCardHolder(source);

		final PaymentInfoData data = new PaymentInfoData();
		data.setCardAccountNumber(cardAccountNumber);
		data.setCardCardType(cardCardType);
		data.setCardCvNumber(cardCvNumber);
		data.setCardExpirationMonth(cardExpirationMonth);
		data.setCardExpirationYear(cardExpirationYear);
		data.setCardIssueNumber(cardIssueNumber);
		data.setCardStartMonth(cardStartMonth);
		data.setCardStartYear(cardStartYear);
		data.setPaymentOption(paymentOption);
		data.setCardAccountHolderName(cardAccountHolderName);

		target.setPaymentInfoData(data);
	}

	/**
	 *
	 */
	private String getCardType()
	{

		if (Objects.isNull(getCartService().getSessionCart())
				|| Strings.isBlank(getCartService().getSessionCart().getPayfortPurchaseResponse()))
		{
			return Strings.EMPTY;
		}
		final AuthorizePurchaseResponseBean responseBean = new Gson()
				.fromJson(getCartService().getSessionCart().getPayfortPurchaseResponse(), AuthorizePurchaseResponseBean.class);
		final CreditCardType creditCardType = CreditCardType.valueOf(responseBean.getPaymentOption().toUpperCase());
		return creditCardType.toString();
	}

	/**
	 *
	 */
	private Integer getExpiryYear(final Map<String, Object> source)
	{
		final Object object = source.get("expiry_date");

		if (Objects.isNull(object))
		{
			return 1;
		}
		final String date = String.valueOf(object);
		try
		{
			if (date.length() < 4)
			{
				return 1;
			}
			return Integer.valueOf(date.substring(2));
		}
		catch (final java.lang.NumberFormatException e)
		{
			return 1;
		}
	}

	/**
	 *
	 */
	private Integer getExpiryMonth(final Map<String, Object> source)
	{
		final Object object = source.get("expiry_date");

		if (Objects.isNull(object))
		{
			return 1;
		}
		final String date = String.valueOf(object);
		try
		{
			if (date.length() < 4)
			{
				return 1;
			}
			return Integer.valueOf(date.substring(0, 2));
		}
		catch (final java.lang.NumberFormatException e)
		{
			return 1;
		}

	}

	/**
	 *
	 */
	private String getCardHolder(final Map<String, Object> source)
	{
		final Object object = source.get("card_holder_name");

		return String.valueOf(object);

	}

	/**
	 *
	 */
	private String getCardNum(final Map<String, Object> source)
	{
		final Object object = source.get("card_number");


		return String.valueOf(object);

	}



	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

}
