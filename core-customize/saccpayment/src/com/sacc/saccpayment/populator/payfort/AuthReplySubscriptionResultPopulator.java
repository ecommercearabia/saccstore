/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.payfort;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;


/**
 * @author amjad.shati@erabia.com
 */
public class AuthReplySubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "cartService")
	private CartService cartService;


	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "source [Map<String, Object>] source cannot be null");

		final AuthReplyData data = new AuthReplyData();

		final Integer ccAuthReplyReasonCode = 1;
		final String ccAuthReplyAuthorizationCode = getResultCode(source);
		final String ccAuthReplyCvCode = null;
		final Boolean cvnDecision = Boolean.TRUE;
		final String ccAuthReplyAvsCodeRaw = "YES";
		final String ccAuthReplyAvsCode = "YES";
		final BigDecimal ccAuthReplyAmount = getAmount(source);
		//		new BigDecimal((Double) source.get(HyperpayStatusReason.AMOUNT.getKey()));
		final String ccAuthReplyProcessorResponse = null;
		final String ccAuthReplyAuthorizedDateTime = String.valueOf(new Date());


		data.setCcAuthReplyReasonCode(ccAuthReplyReasonCode);
		data.setCcAuthReplyAuthorizationCode(ccAuthReplyAuthorizationCode);
		data.setCcAuthReplyCvCode(ccAuthReplyCvCode);
		data.setCvnDecision(cvnDecision);
		data.setCcAuthReplyAvsCodeRaw(ccAuthReplyAvsCodeRaw);
		data.setCcAuthReplyAvsCode(ccAuthReplyAvsCode);
		data.setCcAuthReplyAmount(ccAuthReplyAmount);
		data.setCcAuthReplyProcessorResponse(ccAuthReplyProcessorResponse);
		data.setCcAuthReplyAuthorizedDateTime(ccAuthReplyAuthorizedDateTime);

		target.setAuthReplyData(data);
	}

	private BigDecimal getAmount(final Map<String, Object> source)
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		final CurrencyModel cartCurrency = sessionCart.getCurrency();
		final CurrencyModel baseCurrency = getCommonI18NService().getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = sessionCart.getTotalPrice();
		}
		else
		{
			amount = getCommonI18NService().convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(), sessionCart.getTotalPrice());
		}

		return BigDecimal.valueOf(amount);

	}

	private String getResultCode(final Map<String, Object> source)
	{
		final Object object = source.get("response_code");
		if (Objects.nonNull(object) && object instanceof String)
		{
			return String.valueOf(object);
		}
		return Strings.EMPTY;

	}

	/**
	 * @param isocode
	 * @return number of digits
	 */
	private double getCurrencyDigits(final String isocode)
	{
		switch (isocode)
		{
			case "JOD":
				return 3;
			case "SAR":
				return 2;
			case "AED":
				return 2;
			case "KWD":
				return 3;
			case "OMR":
				return 3;
			default:
				return 2;
		}
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}




}
