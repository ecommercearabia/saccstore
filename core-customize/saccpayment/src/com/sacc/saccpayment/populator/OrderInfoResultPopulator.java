/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.order.CartService;

import java.util.Map;

import javax.annotation.Resource;

import com.sacc.saccpayment.ccavenue.CcavenueStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class OrderInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final OrderInfoData data = new OrderInfoData();

		final String comments = null;
		final String orderNumber = String.valueOf(source.get(CcavenueStatusReason.ORDER_BANK_REF_NO.getKey()));
		final Boolean orderPageIgnoreAVS = null;
		final Boolean orderPageIgnoreCVN = null;
		final String orderPageRequestToken = null;
		final String orderPageTransactionType = "capture";
		final String recurringSubscriptionInfoPublicSignature = null;
		final String subscriptionTitle = null;
		final String taxAmount = String.valueOf(source.get(CcavenueStatusReason.ORDER_TAX.getKey()));

		data.setComments(comments);
		data.setOrderNumber(orderNumber);
		data.setOrderPageIgnoreAVS(orderPageIgnoreAVS);
		data.setOrderPageIgnoreCVN(orderPageIgnoreCVN);
		data.setOrderPageRequestToken(orderPageRequestToken);
		data.setOrderPageTransactionType(orderPageTransactionType);
		data.setRecurringSubscriptionInfoPublicSignature(recurringSubscriptionInfoPublicSignature);
		data.setSubscriptionTitle(subscriptionTitle);
		data.setTaxAmount(taxAmount);

		target.setOrderInfoData(data);
	}
}
