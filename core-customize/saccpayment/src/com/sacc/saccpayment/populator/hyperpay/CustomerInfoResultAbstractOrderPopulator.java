/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Map;

import javax.annotation.Resource;

import com.sacc.saccpayment.context.PaymentContext;


/**
 * @author mnasro
 */
public class CustomerInfoResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "hyperpayCustomerInfoResultPopulator")
	private Populator<Map<String, Object>, CreateSubscriptionResult> hyperpayCustomerInfoResultPopulator;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public void populate(final AbstractOrderModel source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		AddressModel billingAddress = source.getPaymentAddress();
		if (billingAddress == null)
		{
			billingAddress = source.getDeliveryAddress();
		}
		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) source.getUser()).getContactEmail();

		final CustomerInfoData data = new CustomerInfoData();
		if (billingAddress != null)
		{
			data.setBillToCity(billingAddress.getCity() == null ? billingAddress.getTown() : billingAddress.getCity().getName());
			data.setBillToCompany(billingAddress.getCompany());
			data.setBillToCountry(billingAddress.getCountry() == null ? "" : billingAddress.getCountry().getIsocode());
			data.setBillToCustomerIdRef(source.getUser().getUid());
			data.setBillToEmail(contactEmail);
			data.setBillToTitleCode(billingAddress.getTitle() == null ? "" : billingAddress.getTitle().getCode());
			data.setBillToFirstName(billingAddress.getFirstname());
			data.setBillToLastName(billingAddress.getLastname());
			data.setBillToPhoneNumber(billingAddress.getMobile());
			data.setBillToPostalCode(billingAddress.getPostalcode());
			data.setBillToState(billingAddress.getRegion() == null ? "" : billingAddress.getRegion().getIsocode());
			data.setBillToStreet1(billingAddress.getLine1());
			data.setBillToStreet2(billingAddress.getLine2());
		}
		final AddressModel deliveryAddress = source.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			data.setShipToCity(deliveryAddress.getCity() == null ? deliveryAddress.getTown() : deliveryAddress.getCity().getName());
			data.setShipToCompany(deliveryAddress.getCompany());
			data.setShipToCountry(deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getIsocode());
			data.setShipToFirstName(deliveryAddress.getFirstname());
			data.setShipToLastName(deliveryAddress.getLastname());
			data.setShipToPhoneNumber(deliveryAddress.getMobile());
			data.setShipToPostalCode(deliveryAddress.getPostalcode());
			data.setShipToShippingMethod(source.getDeliveryMode() == null ? "" : source.getDeliveryMode().getName());
			data.setShipToState(deliveryAddress.getRegion() == null ? "" : deliveryAddress.getRegion().getIsocode());
			data.setShipToStreet1(deliveryAddress.getLine1());
			data.setShipToStreet2(deliveryAddress.getLine2());
		}

		target.setCustomerInfoData(data);
	}


	/**
	 * @return the hyperpayCustomerInfoResultAbstractOrderPopulator
	 */
	protected Populator<Map<String, Object>, CreateSubscriptionResult> getHyperpayCustomerInfoResultPopulator()
	{
		return hyperpayCustomerInfoResultPopulator;
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}
}