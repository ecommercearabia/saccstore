/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;

import java.util.Map;

import com.sacc.saccpayment.hyperpay.HyperpayStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class PaymentInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final String cardAccountNumber = getCardNum(source);
		final String cardCardType = getCardType(String.valueOf(source.get(HyperpayStatusReason.PAYMENT_BRAND.getKey())));
		final String cardCvNumber = null;
		final Integer cardExpirationMonth = getExpiryMonth(source);
		final Integer cardExpirationYear = getExpiryYear(source);
		final String cardIssueNumber = "0000";
		final String cardStartMonth = "01";
		final String cardStartYear = "30";
		final String paymentOption = "CARD";
		final String cardAccountHolderName = getCardHolder(source);

		final PaymentInfoData data = new PaymentInfoData();
		data.setCardAccountNumber(cardAccountNumber);
		data.setCardCardType(cardCardType);
		data.setCardCvNumber(cardCvNumber);
		data.setCardExpirationMonth(cardExpirationMonth);
		data.setCardExpirationYear(cardExpirationYear);
		data.setCardIssueNumber(cardIssueNumber);
		data.setCardStartMonth(cardStartMonth);
		data.setCardStartYear(cardStartYear);
		data.setPaymentOption(paymentOption);
		data.setCardAccountHolderName(cardAccountHolderName);

		target.setPaymentInfoData(data);
	}

	/**
	 *
	 */
	private Integer getExpiryYear(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.CARD.getKey());
		if (!(object instanceof Map) || ((Map<String, Object>) object).get(HyperpayStatusReason.CARD_EXPIRY_YEAR.getKey()) == null)
		{
			return 1;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		final String cardExpiryYear = String.valueOf(cardMap.get(HyperpayStatusReason.CARD_EXPIRY_YEAR.getKey()));
		try
		{
			return Integer.valueOf(cardExpiryYear);
		}
		catch (final java.lang.NumberFormatException e)
		{
			return 1;
		}
	}

	/**
	 *
	 */
	private Integer getExpiryMonth(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.CARD.getKey());
		if (!(object instanceof Map) || ((Map<String, Object>) object).get(HyperpayStatusReason.CARD_EXPIRY_MONTH.getKey()) == null)
		{
			return 1;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		final String cardExpiryMonth = String.valueOf(cardMap.get(HyperpayStatusReason.CARD_EXPIRY_MONTH.getKey()));
		try
		{
			return Integer.valueOf(cardExpiryMonth);
		}
		catch (final java.lang.NumberFormatException e)
		{
			return 1;
		}

	}

	/**
	 *
	 */
	private String getCardHolder(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.CARD.getKey());
		if (!(object instanceof Map) || ((Map<String, Object>) object).get(HyperpayStatusReason.CARD_HOLDER.getKey()) == null)
		{
			return null;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		return String.valueOf(cardMap.get(HyperpayStatusReason.CARD_HOLDER.getKey()));

	}

	/**
	 *
	 */
	private String getCardNum(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.CARD.getKey());
		if (!(object instanceof Map) || ((Map<String, Object>) object).get(HyperpayStatusReason.CARD_BIN.getKey()) == null
				|| ((Map<String, Object>) object).get(HyperpayStatusReason.CARD_LAST4DIGITS.getKey()) == null)
		{
			return null;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		final String bin = String.valueOf(cardMap.get(HyperpayStatusReason.CARD_BIN.getKey()));
		final String last4digits = String.valueOf(cardMap.get(HyperpayStatusReason.CARD_LAST4DIGITS.getKey()));

		return bin + "***" + last4digits;

	}

	private String getCardType(final String brandType)
	{
		if (brandType.toLowerCase().contains("mastercard"))
		{
			return String.valueOf(CardTypeEnum.master.getStringValue());
		}

		if (brandType.toLowerCase().contains("visa"))
		{
			return String.valueOf(CardTypeEnum.visa.getStringValue());
		}

		if (brandType.toLowerCase().contains("mada"))
		{
			return brandType.toLowerCase();
		}

		return String.valueOf(CardTypeEnum.master.getStringValue());


	}

}
