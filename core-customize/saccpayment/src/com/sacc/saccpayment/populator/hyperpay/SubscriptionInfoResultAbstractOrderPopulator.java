/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

import javax.annotation.Resource;

import com.sacc.saccpayment.context.PaymentContext;


/**
 * @author mnasro
 */
public class SubscriptionInfoResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "hyperpaySubscriptionInfoResultPopulator")
	private Populator<Map<String, Object>, CreateSubscriptionResult> hyperpaySubscriptionInfoResultPopulator;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;


	@Override
	public void populate(final AbstractOrderModel source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		getHyperpaySubscriptionInfoResultPopulator().populate(getPaymentContext().getCheckoutMap(source), target);
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	/**
	 * @return the hyperpaySubscriptionInfoResultPopulator
	 */
	protected Populator<Map<String, Object>, CreateSubscriptionResult> getHyperpaySubscriptionInfoResultPopulator()
	{
		return hyperpaySubscriptionInfoResultPopulator;
	}

}
