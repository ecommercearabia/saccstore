/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

import javax.annotation.Resource;

import com.sacc.saccpayment.context.PaymentContext;


/**
 * @author mnasro
 */
public class PaymentInfoResultAbstractOrderPopulator extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "hyperpayPaymentInfoResultPopulator")
	private Populator<Map<String, Object>, CreateSubscriptionResult> hyperpayPaymentInfoResultPopulator;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public void populate(final AbstractOrderModel source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		getHyperpayPaymentInfoResultPopulator().populate(getPaymentContext().getCheckoutMap(source), target);
	}

	/**
	 * @return the hyperpayPaymentInfoResultAbstractOrderPopulator
	 */
	protected Populator<Map<String, Object>, CreateSubscriptionResult> getHyperpayPaymentInfoResultPopulator()
	{
		return hyperpayPaymentInfoResultPopulator;
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

}