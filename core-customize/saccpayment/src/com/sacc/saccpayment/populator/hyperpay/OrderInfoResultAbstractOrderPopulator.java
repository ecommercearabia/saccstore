/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

import javax.annotation.Resource;

import com.sacc.saccpayment.context.PaymentContext;


/**
 * @author mnasro
 */
public class OrderInfoResultAbstractOrderPopulator extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "hyperpayOrderInfoResultPopulator")
	private Populator<Map<String, Object>, CreateSubscriptionResult> hyperpayOrderInfoResultPopulator;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public void populate(final AbstractOrderModel source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		final OrderInfoData data = new OrderInfoData();

		final String comments = null;
		final String orderNumber = source.getOrderCode();
		final Boolean orderPageIgnoreAVS = null;
		final Boolean orderPageIgnoreCVN = null;
		final String orderPageRequestToken = null;
		final String orderPageTransactionType = "capture";
		final String recurringSubscriptionInfoPublicSignature = null;
		final String subscriptionTitle = null;
		final String taxAmount = null;

		data.setComments(comments);
		data.setOrderNumber(orderNumber);
		data.setOrderPageIgnoreAVS(orderPageIgnoreAVS);
		data.setOrderPageIgnoreCVN(orderPageIgnoreCVN);
		data.setOrderPageRequestToken(orderPageRequestToken);
		data.setOrderPageTransactionType(orderPageTransactionType);
		data.setRecurringSubscriptionInfoPublicSignature(recurringSubscriptionInfoPublicSignature);
		data.setSubscriptionTitle(subscriptionTitle);
		data.setTaxAmount(taxAmount);

		target.setOrderInfoData(data);
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	/**
	 * @return the hyperpayOrderInfoResultAbstractOrderPopulator
	 */
	protected Populator<Map<String, Object>, CreateSubscriptionResult> getHyperpayOrderInfoResultPopulator()
	{
		return hyperpayOrderInfoResultPopulator;
	}
}