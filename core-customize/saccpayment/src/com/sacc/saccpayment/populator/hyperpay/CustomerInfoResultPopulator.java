/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.core.service.CustomCartService;


/**
 * @author amjad.shati@erabia.com
 */
public class CustomerInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomerInfoResultPopulator.class);

	private static final String MERCHANT_TRANSACTION_ID = "merchantTransactionId";

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "defaultCartServiceForAccelerator")
	private CustomCartService customCartService;

	/**
	 *
	 */
	private String getOrderNumber(final Map<String, Object> source)
	{
		if (source == null || !source.containsKey(MERCHANT_TRANSACTION_ID)
				|| !(source.get(MERCHANT_TRANSACTION_ID) instanceof String)
				|| StringUtils.isBlank(((String) source.get(MERCHANT_TRANSACTION_ID))))
		{
			return "";
		}
		return ((String) source.get(MERCHANT_TRANSACTION_ID));
	}

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		final String orderNumber = getOrderNumber(source);
		if (StringUtils.isBlank(orderNumber))
		{
			LOG.error("Cannot get ordernumber from {}", source);
			return;
		}

		final List<CartModel> cartList = customCartService.getCartByMerchantTransactionId(orderNumber);
		if (CollectionUtils.isEmpty(cartList))
		{
			LOG.error("Cannot get cart from order number {}", orderNumber);
			return;
		}

		final CartModel cartModel = cartList.get(0);
		AddressModel billingAddress = cartModel.getPaymentAddress();
		if (billingAddress == null)
		{
			billingAddress = cartModel.getDeliveryAddress();
		}
		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) cartModel.getUser()).getContactEmail();

		final CustomerInfoData data = new CustomerInfoData();
		if (billingAddress != null)
		{
			data.setBillToCity(billingAddress.getCity() == null ? billingAddress.getTown() : billingAddress.getCity().getName());
			data.setBillToCompany(billingAddress.getCompany());
			data.setBillToCountry(billingAddress.getCountry() == null ? "" : billingAddress.getCountry().getIsocode());
			data.setBillToCustomerIdRef(cartModel.getUser().getUid());
			data.setBillToEmail(contactEmail);
			data.setBillToTitleCode(billingAddress.getTitle() == null ? "" : billingAddress.getTitle().getCode());
			data.setBillToFirstName(billingAddress.getFirstname());
			data.setBillToLastName(billingAddress.getLastname());
			data.setBillToPhoneNumber(billingAddress.getMobile());
			data.setBillToPostalCode(billingAddress.getPostalcode());
			data.setBillToState(billingAddress.getRegion() == null ? "" : billingAddress.getRegion().getIsocode());
			data.setBillToStreet1(billingAddress.getLine1());
			data.setBillToStreet2(billingAddress.getLine2());
		}
		final AddressModel deliveryAddress = cartModel.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			data.setShipToCity(deliveryAddress.getCity() == null ? deliveryAddress.getTown() : deliveryAddress.getCity().getName());
			data.setShipToCompany(deliveryAddress.getCompany());
			data.setShipToCountry(deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getIsocode());
			data.setShipToFirstName(deliveryAddress.getFirstname());
			data.setShipToLastName(deliveryAddress.getLastname());
			data.setShipToPhoneNumber(deliveryAddress.getMobile());
			data.setShipToPostalCode(deliveryAddress.getPostalcode());
			data.setShipToShippingMethod(cartModel.getDeliveryMode() == null ? "" : cartModel.getDeliveryMode().getName());
			data.setShipToState(deliveryAddress.getRegion() == null ? "" : deliveryAddress.getRegion().getIsocode());
			data.setShipToStreet1(deliveryAddress.getLine1());
			data.setShipToStreet2(deliveryAddress.getLine2());
		}

		target.setCustomerInfoData(data);
	}
}
