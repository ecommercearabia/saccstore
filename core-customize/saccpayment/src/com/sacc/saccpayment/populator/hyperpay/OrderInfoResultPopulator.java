/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.order.CartService;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;


/**
 * @author amjad.shati@erabia.com
 */
public class OrderInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	/**
	 * 
	 */
	private static final String MERCHANT_TRANSACTION_ID = "merchantTransactionId";
	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final OrderInfoData data = new OrderInfoData();

		final String comments = null;
		final String orderNumber = getOrderNumber(source);
		final Boolean orderPageIgnoreAVS = null;
		final Boolean orderPageIgnoreCVN = null;
		final String orderPageRequestToken = null;
		final String orderPageTransactionType = "capture";
		final String recurringSubscriptionInfoPublicSignature = null;
		final String subscriptionTitle = null;
		final String taxAmount = null;

		data.setComments(comments);
		data.setOrderNumber(orderNumber);
		data.setOrderPageIgnoreAVS(orderPageIgnoreAVS);
		data.setOrderPageIgnoreCVN(orderPageIgnoreCVN);
		data.setOrderPageRequestToken(orderPageRequestToken);
		data.setOrderPageTransactionType(orderPageTransactionType);
		data.setRecurringSubscriptionInfoPublicSignature(recurringSubscriptionInfoPublicSignature);
		data.setSubscriptionTitle(subscriptionTitle);
		data.setTaxAmount(taxAmount);

		target.setOrderInfoData(data);
	}

	/**
	 *
	 */
	private String getOrderNumber(final Map<String, Object> source)
	{
		if (source == null || !source.containsKey(MERCHANT_TRANSACTION_ID)
				|| !(source.get(MERCHANT_TRANSACTION_ID) instanceof String)
				|| StringUtils.isBlank(((String) source.get(MERCHANT_TRANSACTION_ID))))
		{
			return "";
		}
		return ((String) source.get(MERCHANT_TRANSACTION_ID)).split("_")[0];
	}
}
