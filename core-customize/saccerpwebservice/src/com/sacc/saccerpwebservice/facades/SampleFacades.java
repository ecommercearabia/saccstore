/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpwebservice.facades;

import de.hybris.platform.core.servicelayer.data.SearchPageData;

import java.util.List;

import com.sacc.saccerpwebservice.data.UserData;
import com.sacc.saccerpwebservice.dto.SampleWsDTO;
import com.sacc.saccerpwebservice.dto.TestMapWsDTO;


public interface SampleFacades
{
	SampleWsDTO getSampleWsDTO(final String value);

	UserData getUser(String id);

	List<UserData> getUsers();

	SearchPageData<UserData> getUsers(SearchPageData<?> params);

	TestMapWsDTO getMap();
}
