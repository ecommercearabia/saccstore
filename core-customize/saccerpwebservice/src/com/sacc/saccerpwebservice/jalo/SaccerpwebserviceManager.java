/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpwebservice.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.sacc.saccerpwebservice.constants.SaccerpwebserviceConstants;

public class SaccerpwebserviceManager extends GeneratedSaccerpwebserviceManager
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( SaccerpwebserviceManager.class.getName() );
	
	public static final SaccerpwebserviceManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SaccerpwebserviceManager) em.getExtension(SaccerpwebserviceConstants.EXTENSIONNAME);
	}
	
}
