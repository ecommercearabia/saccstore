package com.sacc.saccproductfacades.price.enums;

/**
 * @author amjad.shati@erabia.com
 *
 */
public enum RuleActionDefinitionType
{
	PERCENTAGE_DISCOUNT("y_order_entry_percentage_discount"), PRODUCT_FREE_GIFT("y_free_gift"), FIXED_AMOUNT_DISCOUNT(
			"y_order_entry_fixed_discount");

	private String name;

	private RuleActionDefinitionType(final String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}
