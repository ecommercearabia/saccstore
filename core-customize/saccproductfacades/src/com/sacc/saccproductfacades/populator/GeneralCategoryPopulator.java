package com.sacc.saccproductfacades.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Populator;



public class GeneralCategoryPopulator implements Populator<CategoryModel, CategoryData>
{

	@Override
	public void populate(final CategoryModel source, final CategoryData target)
	{
		target.setDisplayName(source.getDisplayName());
	}

}
