package com.sacc.saccproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductDiscountPopulator implements Populator<SearchResultValueData, ProductData>
{
	private static final Logger LOG = Logger.getLogger(SearchResultProductDiscountPopulator.class);

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productDiscountPopulator")
	private Populator<ProductModel, ProductData> productDiscountPopulator;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			if (productModel != null)
			{
				productDiscountPopulator.populate(productModel, target);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}
	}



}