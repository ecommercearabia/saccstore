/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import com.sacc.saccproduct.model.GroceryVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductUnitOfMeasurePopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source instanceof GroceryVariantProductModel)
		{
			final GroceryVariantProductModel variant = (GroceryVariantProductModel) source;
			target.setUnitOfMeasure(variant.getUnitOfMeasure());
			target.setUnitOfMeasureDescription(variant.getUnitOfMeasureDescription());
		}
	}

}
