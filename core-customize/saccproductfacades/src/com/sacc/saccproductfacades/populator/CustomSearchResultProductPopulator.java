/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.entitlementfacades.product.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author monzer
 *
 */
public class CustomSearchResultProductPopulator extends SearchResultProductPopulator<SearchResultValueData, ProductData>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomSearchResultProductPopulator.class);

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("source", source);
		ServicesUtil.validateParameterNotNullStandardMessage("target", target);
		ProductModel productModel = null;
		try
		{
			productModel = getProductService().getProductForCode(target.getCode());
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product not found or not Approved");
		}
		if (productModel == null)
		{
			return;
		}
		getProductEntitlementCollectionPopulator().populate(productModel, target);
	}

}