/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.constants;

/**
 * Global class for all Saccproductfacades constants. You can add global constants for your extension into this class.
 */
public final class SaccproductfacadesConstants extends GeneratedSaccproductfacadesConstants
{
	public static final String EXTENSIONNAME = "saccproductfacades";

	private SaccproductfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccproductfacadesPlatformLogo";
}
