/**
 *
 */
package com.sacc.saccstorecredit.dao;

import com.sacc.saccstorecredit.enums.StoreCreditModeType;
import com.sacc.saccstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeDao
{
	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);
}
