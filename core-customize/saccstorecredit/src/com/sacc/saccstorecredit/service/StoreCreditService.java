/**
 *
 */
package com.sacc.saccstorecredit.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;

import java.math.BigDecimal;
import java.util.List;

import com.sacc.saccstorecredit.model.StoreCreditHistoryModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditService
{
	/**
	 * @param userModel
	 * @return List<StoreCreditHistoryModel>
	 */
	public List<StoreCreditHistoryModel> getStoreCreditHistory(UserModel userModel);

	/**
	 * @return List<StoreCreditHistoryModel>
	 */
	public List<StoreCreditHistoryModel> getStoreCreditHistoryByCurrentUser();

	/**
	 * @param userModel
	 * @param baseStoreModel
	 * @return BigDecimal
	 */
	public BigDecimal getStoreCreditAmount(UserModel userModel, BaseStoreModel baseStoreModel);

	/**
	 * @param baseStoreModel
	 * @return BigDecimal
	 */
	public BigDecimal getStoreCreditAmountByCurrentUser(BaseStoreModel baseStoreModel);

	/**
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getStoreCreditAmountByCurrentUserAndCurrentBaseStore();

	/**
	 * @param baseStoreModel
	 * @return boolean
	 */

	public boolean getStoreCreditEnabled(BaseStoreModel baseStoreModel);

	/**
	 * @return boolean
	 */

	public boolean getStoreCreditEnabledByCurrentBaseStore();

	/**
	 *
	 */

	public void redeemStoreCreditAmount(AbstractOrderModel abstractOrderModel);

	public BigDecimal getStoreCreditAmount(AbstractOrderModel abstractOrderModel);




}
