/**
 *
 */
package com.sacc.saccstorecredit.service.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import com.sacc.saccstorecredit.dao.StoreCreditModeDao;
import com.sacc.saccstorecredit.enums.StoreCreditModeType;
import com.sacc.saccstorecredit.exception.StoreCreditException;
import com.sacc.saccstorecredit.exception.enums.StoreCreditExceptionType;
import com.sacc.saccstorecredit.model.StoreCreditModeModel;
import com.sacc.saccstorecredit.service.StoreCreditModeService;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditModeService implements StoreCreditModeService
{
	@Resource(name = "storeCreditModeDao")
	private StoreCreditModeDao storeCreditModeDao;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.saccstorecredit.service.StoreCreditModeService#getStoreCreditMode(com.sacc.saccstorecredit.
	 * enums .StoreCreditModeType)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final StoreCreditModeType storeCreditModeType)
	{
		if (storeCreditModeType == null)
		{
			throw new IllegalArgumentException("storeCreditModeType can't be null");
		}
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (!currentBaseStore.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}
		return storeCreditModeDao.getStoreCreditMode(storeCreditModeType);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.saccstorecredit.service.StoreCreditModeService#getSupportedStoreCreditModes(de.hybris.platform.
	 * store .BaseStoreModel);
	 */
	@Override
	public List<StoreCreditModeModel> getSupportedStoreCreditModes(final BaseStoreModel baseStoreModel)
	{
		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel can't be null");
		}
		else if (baseStoreModel.getStoreCreditEnabled() == null || !baseStoreModel.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}

		return baseStoreModel.getSupportedStoreCreditModes();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.sacc.saccstorecredit.service.StoreCreditModeService#getSupportedStoreCreditModesCurrentBaseStore()
	 */
	@Override
	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore()
	{
		return getSupportedStoreCreditModes(baseStoreService.getCurrentBaseStore());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.sacc.saccstorecredit.service.StoreCreditModeService#getStoreCreditMode(java.lang.String)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final String storeCreditModeTypeCode)
	{
		if (storeCreditModeTypeCode == null)
		{
			throw new IllegalArgumentException("storeCreditModeTypeCode can't be null");
		}

		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (currentBaseStore.getStoreCreditEnabled() == null || !currentBaseStore.getStoreCreditEnabled())
		{
			throw new StoreCreditException("Store Credit is not available", StoreCreditExceptionType.NOT_AVAILABLE);
		}

		final StoreCreditModeType storeCreditModeType = StoreCreditModeType.valueOf(storeCreditModeTypeCode);
		if (storeCreditModeType == null)
		{
			return null;
		}
		return getStoreCreditMode(storeCreditModeType);
	}


}
