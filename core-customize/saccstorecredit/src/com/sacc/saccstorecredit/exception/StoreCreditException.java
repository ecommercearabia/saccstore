/**
 *
 */
package com.sacc.saccstorecredit.exception;

import com.sacc.saccstorecredit.exception.enums.StoreCreditExceptionType;


/**
 * @author yhammad
 *
 */
public class StoreCreditException extends RuntimeException
{

	StoreCreditExceptionType storeCreditExceptionType;


	public StoreCreditException(final String message, final StoreCreditExceptionType type)
	{
		super(message);
		this.storeCreditExceptionType = type;
	}

	/**
	 * @return the type
	 */
	public StoreCreditExceptionType getStoreCreditExceptionType()
	{
		return storeCreditExceptionType;
	}
}
