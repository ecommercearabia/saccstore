/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomcronjobs.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.assertj.core.util.Preconditions;

import com.sacc.core.service.FillProductCarouselService;
import com.sacc.sacccustomcronjobs.enums.FillCarouselComponentType;
import com.sacc.sacccustomcronjobs.model.FillProductCarouselCronJobModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class LimitedStockJob extends AbstractJobPerformable<FillProductCarouselCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(LimitedStockJob.class);
	private static final String CRONJOB_FINISHED = "LimitedStockJob is Finished ...";


	@Resource(name = "fillProductCarouselService")
	private FillProductCarouselService fillProductCarouselService;

	/**
	 * @return the fillProductCarouselService
	 */
	public FillProductCarouselService getFillProductCarouselService()
	{
		return fillProductCarouselService;
	}

	@Override
	public PerformResult perform(final FillProductCarouselCronJobModel arg0)
	{
		Preconditions.checkNotNull(arg0, "FillProductCarouselCronJobModel is null");
		LOG.info("LimitedStockJob is Starting ...");

		if (arg0.getProductCarousel() == null)
		{
			LOG.info("No product carousel found");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}
		try
		{

			fillProductCarouselService.updateProduct(FillCarouselComponentType.LIMITED_STOCK, arg0.getProductCarousel(),
					arg0.getStore(), arg0.getCatalogVersion(), arg0.getLimitedStock(), arg0.getSize(), arg0.isSyncContentCatalog());






		}
		catch (final Exception e)
		{
			LOG.info("An error has occured, error of type: " + e.getClass() + ", the message is:" + e.getMessage());
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}


		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}




}
