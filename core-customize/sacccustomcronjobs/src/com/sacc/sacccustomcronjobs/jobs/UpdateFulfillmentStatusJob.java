/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomcronjobs.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.sacccustomcronjobs.model.OrderProcessingCronJobModel;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.service.CustomConsignmentService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateFulfillmentStatusJob extends AbstractJobPerformable<OrderProcessingCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(UpdateFulfillmentStatusJob.class);
	private static final String CRONJOB_FINISHED = "UpdateFulfillmentStatusJob is Finished ...";
	private static final String FULFILLMENT_TYPE_MUSTN_T_BE_NULL = "type mustn't be null";
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public PerformResult perform(final OrderProcessingCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");

		final List<ConsignmentModel> consignments = customConsignmentService
				.getConsignmentsByNotStatus(ConsignmentStatus.DELIVERY_COMPLETED);


		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		consignments.stream().filter(Objects::nonNull).forEach(c -> checkAndChangeConsignmentStatus(c));
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
	{
		FulfillmentProviderType type = null;
		try
		{
			type = consignment.getCarrierDetails().getFulfillmentProviderType();
			if (type == null)
			{
				LOG.error("Empty type for consignment: " + consignment.getCode());
				return;
			}
			final Optional<ConsignmentStatus> optionalStatus = fulfillmentContext.updateStatus(consignment, type);
			if (optionalStatus.isEmpty())
			{
				LOG.error("Empty status for consignment: " + consignment.getCode());
				return;
			}
			if (ConsignmentStatus.DELIVERY_COMPLETED.equals(optionalStatus.get()))
			{
				saccERPService.updateSaleOrderDeliveredStatus(consignment);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			String msg = e.getMessage();
			if (e.getCause() != null)
			{
				msg = e.getCause().getMessage();
				if (e.getCause().getCause() != null)
				{
					msg = e.getCause().getCause().getMessage();
				}
			}
			getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(type), consignment.getOrder(), msg));
			return;
		}
	}

	private IntegrationProvider getIntegrationProviderType(final FulfillmentProviderType type)
	{
		Preconditions.checkArgument(type != null, FULFILLMENT_TYPE_MUSTN_T_BE_NULL);
		switch (type)
		{
			case SMSA:
				return IntegrationProvider.SMSA;
			case SAEE:
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


}