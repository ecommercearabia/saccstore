/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwishlistfacade.constants;

/**
 * Global class for all Saccwishlistfacade constants. You can add global constants for your extension into this class.
 */
public final class SaccwishlistfacadeConstants extends GeneratedSaccwishlistfacadeConstants
{
	public static final String EXTENSIONNAME = "saccwishlistfacade";

	private SaccwishlistfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccwishlistfacadePlatformLogo";
}
