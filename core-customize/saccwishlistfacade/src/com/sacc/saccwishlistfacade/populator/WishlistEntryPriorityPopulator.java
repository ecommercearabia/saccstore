/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwishlistfacade.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;

import javax.annotation.Resource;

import com.sacc.saccwishlistfacade.data.WishlistEntryPriorityData;


/**
 * @author mohammad-abu muhasien
 */
public class WishlistEntryPriorityPopulator implements Populator<Wishlist2EntryPriority, WishlistEntryPriorityData>
{

	@Resource(name = "typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final Wishlist2EntryPriority source, final WishlistEntryPriorityData target)
	{
		final Wishlist2EntryPriority wishlist2EntryPriority = source;
		if (wishlist2EntryPriority != null)
		{
			target.setCode(source.getCode());
			target.setDisplayName(getTypeService().getEnumerationValue(source).getName());
		}

	}
}
