/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacctimeslot.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import com.sacc.sacctimeslot.model.TimeSlotInfoModel;
import com.sacc.sacctimeslot.model.TimeSlotModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface TimeSlotService
{
	/**
	 * Retrieves TimeSlot by the related ZoneDeliveryMode code if active equals true.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlot, Optional<TimeSlotModel>
	 */
	public Optional<TimeSlotModel> getActive(String zoneDeliveryModeCode);

	/**
	 * Returns true if the TimeSlot related to the ZoneDeliveryMode code is active.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlotEnabled, boolean
	 */
	public boolean isTimeSlotEnabled(String zoneDeliveryModeCode);

	/**
	 * @param store,
	 *           BaseStoreModel
	 * @param start,
	 *           String
	 * @param end,
	 *           String
	 * @param date,
	 *           String
	 * @param timezone,
	 *           String
	 *
	 * @return numberOfOrders, int
	 */
	public int getNumberOfOrdersByTimeSlot(BaseStoreModel store, String start, String end, String date, String timezone);

	/**
	 * @param store,
	 *           BaseStoreModel
	 * @param timezone,
	 *           String
	 * @param interval,
	 *           int
	 *
	 * @return List of orders, List<OrderModel>
	 */
	public List<OrderModel> getOrdersByInterval(BaseStoreModel store, String timezone, final long interval);

	/**
	 *
	 */
	public void saveTimeSlotInfo(TimeSlotInfoModel info, CartModel cart, String date, LocalTime start);

}
