/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccpaymentbackoffice.services;

/**
 * Hello World SaccpaymentbackofficeService
 */
public class SaccpaymentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
