/**
 *
 */
package com.sacc.saccexport.interceptors.impl;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import javax.annotation.Resource;

import com.sacc.saccexport.model.CSVImportCronJobModel;
import com.sacc.saccexport.validators.ErabiaCSVValidator;



/**
 * @author jaafarNaddaf
 *
 */
public class DefaultErabiaCSVInterceptor implements ValidateInterceptor<CSVImportCronJobModel>
{
	@Resource(name = "erabiaCSVValidator")
	private ErabiaCSVValidator erabiaCSVValidator;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onValidate(final CSVImportCronJobModel job, final InterceptorContext ctx) throws InterceptorException
	{
		if (!erabiaCSVValidator.hasFile(job))
		{
			throw new InterceptorException("One of the files (FTP/Media/Script) must exist");
		}


		if (!erabiaCSVValidator.validateMail(job.getMailAddress()))
		{
			throw new InterceptorException("Enter a valid mail address");
		}
	}
}