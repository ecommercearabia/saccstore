/**
 *
 */
package com.sacc.saccexport.validators.impl;


import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.sacc.saccexport.model.CSVFieldModel;
import com.sacc.saccexport.model.CSVImportCronJobModel;
import com.sacc.saccexport.model.ImpExScriptModel;
import com.sacc.saccexport.services.ErabiaCSVService;
import com.sacc.saccexport.validators.ErabiaCSVValidator;


/**
 * @author jaafarNaddaf
 *
 */
public class DefaultErabiaCSVValidator implements ErabiaCSVValidator
{
	@Resource(name = "erabiaCSVService")
	private ErabiaCSVService erabiaCSVService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	public static final String MAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.saccexport.validators.ErabiaCSVValidator#validate(com.sacc.saccexport.model.CSVImportCronJobModel )
	 */
	@Override
	public Map<String, String> validate(final StringBuilder data, final Set<ImpExScriptModel> scripts)
	{
		final Map<String, String> errors = new HashMap<>();

		if (data == null)
		{
			errors.put("CONTENT", "Couldn't read file or file is empty");
		}

		return errors;
	}

	/**
	 * @param job
	 * @return boolean whether the cronjob has a file or not
	 */
	@Override
	public boolean hasFile(final CSVImportCronJobModel job)
	{
		return hasFTP(job) || hasScript(job.getScript());
	}

	/**
	 * @param script
	 * @return boolean
	 */
	@Override
	public boolean hasScript(final String script)
	{
		return !isBlankOrNull(script);
	}

	/**
	 * @param job
	 * @return boolean
	 */
	@Override
	public boolean hasFTP(final CSVImportCronJobModel job)
	{
		return !isBlankOrNull(job.getHostname()) && !isBlankOrNull(job.getPath()) && !isBlankOrNull(job.getUsername())
				&& !isBlankOrNull(job.getPassword());
	}

	@Override
	public boolean validateMail(final String mailAddress)
	{
		return isBlankOrNull(mailAddress) || mailAddress.matches(MAIL_REGEX);
	}

	@Override
	public Map<String, String> isValid(final List<List<String>> csv, final Set<CSVFieldModel> fields)
	{
		final Map<String, String> errors = new HashMap<>();
		for (final List<String> value : csv)
		{
			for (final CSVFieldModel field : fields)
			{
				try
				{
					if (field.getRequired().booleanValue() && value.get(field.getPosition().intValue()) != null
							&& value.get(field.getPosition().intValue()).trim().isEmpty())
					{
						errors.put("Required", "Missing required field value");
					}
				}
				catch (final ArrayIndexOutOfBoundsException obe)
				{
					errors.put("Index", "Index {" + obe.getMessage() + "} doesn't exist in CSV");
					break;
				}
			}
		}

		return errors;
	}

	private boolean isBlankOrNull(final String string)
	{
		return string == null || string.isEmpty();
	}
}
