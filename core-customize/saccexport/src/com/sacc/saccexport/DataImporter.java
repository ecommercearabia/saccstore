/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sacc.saccexport;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.ImportCronJobResult;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.sacc.saccexport.bean.AddressBean;
import com.sacc.saccexport.bean.EmailBean;
import com.sacc.saccexport.model.CSVFieldModel;
import com.sacc.saccexport.model.CSVImportCronJobModel;
import com.sacc.saccexport.model.ImpExScriptModel;
import com.sacc.saccexport.services.ErabiaCSVService;
import com.sacc.saccexport.services.ErabiaExportService;
import com.sacc.saccexport.validators.ErabiaCSVValidator;


/**
 * @author jaafarNaddaf
 *
 */
public class DataImporter extends AbstractJobPerformable<CSVImportCronJobModel>
{
	@Resource(name = "erabiaCSVService")
	private ErabiaCSVService erabiaCSVService;

	@Resource(name = "erabiaExportService")
	private ErabiaExportService erabiaExportService;

	@Resource(name = "erabiaCSVValidator")
	private ErabiaCSVValidator erabiaCSVValidator;

	@Resource(name = "importService")
	private ImportService importService;

	@Resource(name = "indexerService")
	private IndexerService indexerService;

	@Resource(name = "facetSearchConfigService")
	private FacetSearchConfigService facetSearchConfigService;

	private static final Logger LOG = Logger.getLogger(DataImporter.class.getName());
	private static final String HOTFOLDER_ERROR = "hotfolder-error";
	private static final String HOTFOLDER_ARCHIVED = "hotfolder-archived";
	private static final String FAILURE_RESULT = "[] error occurred during import";
	private static final String SUCCESS_RESULT = "[] has been imported successfully";


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.
	 * CronJobModel )
	 */
	@Override
	public PerformResult perform(final CSVImportCronJobModel job)
	{
		return performJob(job);
	}

	/**
	 * @param job
	 * @return PerformResult
	 */
	private PerformResult performJob(final CSVImportCronJobModel job)
	{
		CronJobResult cronJobResult = CronJobResult.SUCCESS;
		final StringBuilder data = readFiles(job);

		final Set<ImpExScriptModel> scripts = new HashSet<>(job.getImpExScripts());
		final Map<String, String> errors = erabiaCSVValidator.validate(data, scripts);

		if (errors != null && !errors.isEmpty())
		{
			LOG.error(StringUtils.join(errors.values(), " - "));
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
		}

		final List<List<String>> csv = erabiaCSVService.readerToCSV(data, job.getSeparator());
		final Set<CSVFieldModel> fields = erabiaCSVService.getFields(scripts);

		errors.putAll(erabiaCSVValidator.isValid(csv, fields));

		if (!errors.isEmpty())
		{
			LOG.error(StringUtils.join(errors.values(), " - "));
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		final List<String> impExResults = new ArrayList<>();

		for (final ImpExScriptModel script : job.getImpExScripts())
		{
			final StringBuilder impEx = new StringBuilder();
			impEx.append(script.getImpExHeader() + "\n" + erabiaCSVService.csvToImpEx(csv, script, fields));
			final ImportConfig config = erabiaCSVService.prepareImportConfig(impEx);
			final ImportCronJobResult result = (ImportCronJobResult) importService.importData(config);
			String folderName = "";
			if (result.isError())
			{
				folderName = HOTFOLDER_ERROR;
				impExResults.add(FAILURE_RESULT.replace("[]", "[" + script.getCode() + "]"));
				erabiaExportService.impExToMedia(job.getCode() + "-" + script.getCode(), impEx, folderName);
				cronJobResult = CronJobResult.FAILURE;
			}
			else
			{
				folderName = HOTFOLDER_ARCHIVED;
				impExResults.add(SUCCESS_RESULT.replace("[]", "[" + script.getCode() + "]"));
				erabiaExportService.impExToMedia(job.getCode() + "-" + script.getCode(), impEx, folderName);
				cronJobResult = CronJobResult.SUCCESS;
			}
		}

		if (job.getSendMail() != null && job.getSendMail().booleanValue() && job.getMailAddress() != null
				&& !job.getMailAddress().trim().isEmpty())
		{
			sendMail(job, impExResults);
		}

		if (job.getSynchronize() != null)
		{
			erabiaExportService.synchronize(job.getSynchronize().getId());
		}
		if (job.getDeleteSource() != null && job.getDeleteSource().booleanValue())
		{
			clearSource(job);
		}
		final SolrFacetSearchConfigModel solrFacetSearchConfig = job.getSolrFacetSearchConfig();
		if (solrFacetSearchConfig != null)
		{
			try
			{
				indexerService.performFullIndex(getFacetSearchConfig(solrFacetSearchConfig));
			}
			catch (final IndexerException e)
			{
				LOG.error(e.getMessage());
				return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
			}
		}
		return new PerformResult(cronJobResult, CronJobStatus.FINISHED);
	}

	protected FacetSearchConfig getFacetSearchConfig(final SolrFacetSearchConfigModel facetSearchConfigModel)
	{
		try
		{
			return this.facetSearchConfigService.getConfiguration(facetSearchConfigModel.getName());
		}
		catch (final FacetConfigServiceException var3)
		{
			LOG.warn("Configuration service could not load configuration with name: " + facetSearchConfigModel.getName(), var3);
			return null;
		}
	}

	/**
	 * @param job
	 */
	private String clearSource(final CSVImportCronJobModel job)
	{
		String results = StringUtils.EMPTY;

		if (erabiaCSVValidator.hasFTP(job))
		{
			final String server = job.getHostname();
			final String user = job.getUsername();
			final String pass = job.getPassword();

			final FTPClient ftpClient = new FTPClient();
			try
			{
				ftpClient.connect(server);
				ftpClient.login(user, pass);
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

				final String ftpFile = job.getPath();
				ftpClient.deleteFile(ftpFile);
				results += "FTP file [" + job.getPath() + "] is deleted successfully";
			}
			catch (final IOException ex)
			{
				results += "FTP file [" + job.getPath() + "] couldn't be deleted";
			}
			finally
			{
				try
				{
					if (ftpClient.isConnected())
					{
						ftpClient.logout();
						ftpClient.disconnect();
					}
				}
				catch (final IOException ex)
				{
					LOG.error(ex.getMessage());
				}
			}
		}

		return results;
	}

	/**
	 * @param job
	 * @param impExResults
	 */
	private void sendMail(final CSVImportCronJobModel job, final List<String> impExResults)
	{
		final EmailBean mail = new EmailBean();
		mail.setSubject(job.getCode() + " Results");
		mail.setBody(StringUtils.join(impExResults, "\n\n"));
		final AddressBean toAddress = new AddressBean();
		toAddress.setAddress(job.getMailAddress());
		toAddress.setDisplayName(job.getMailAddress());
		mail.getToAddresses().add(toAddress);
		erabiaExportService.sendMail(mail);
	}

	private StringBuilder readFiles(final CSVImportCronJobModel job)
	{
		final StringBuilder data = new StringBuilder();


		if (job.getReadFromFTPServer() != null && job.getReadFromFTPServer().booleanValue() && erabiaCSVValidator.hasFTP(job))
		{
			if (erabiaCSVService.isFTPDirectory(job.getHostname(), job.getPath(), job.getUsername(), job.getPassword())
					&& job.getSequentialFiles() != null && job.getSequentialFiles().booleanValue())
			{
				data.append(erabiaCSVService.readFTPDirectory(job.getHostname(), job.getPath(), job.getUsername(), job.getPassword(),
						job.getRegEx()));
				data.append("\n");
			}
			else
			{
				data.append(erabiaCSVService.readFTP(job.getHostname(), job.getPath(), job.getUsername(), job.getPassword()));
				data.append("\n");
			}
		}

		if (erabiaCSVValidator.hasScript(job.getScript()))
		{
			data.append(erabiaCSVService.readScript(job.getScript()));
		}

		return data;
	}

}
