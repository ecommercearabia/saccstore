package com.sacc.saccexport.process.email.actions;


import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.sacc.saccexport.bean.AddressBean;
import com.sacc.saccexport.bean.EmailBean;


/**
 * A process action to send emails.
 */
public class SendEmailAction
{


	private EmailService emailService;
	private ConfigurationService configurationService;
	private ModelService modelService;

	/**
	 * @return the modelService
	 */

	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected EmailService getEmailService()
	{
		return emailService;
	}

	@Required
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public boolean sendEmail(final EmailBean emailBean)
	{
		final EmailMessageModel email = getModelService().create(EmailMessageModel.class);

		if (emailBean.getAttachments() != null && !emailBean.getAttachments().isEmpty())
		{
			email.setAttachments(emailBean.getAttachments());
		}

		final List<EmailAddressModel> toAddresses = new ArrayList<>();

		if (emailBean.getToAddresses() != null && !emailBean.getToAddresses().isEmpty())
		{
			for (final AddressBean address : emailBean.getToAddresses())
			{
				final EmailAddressModel toEmailAddress = getEmailService().getOrCreateEmailAddressForEmail(address.getAddress(),
						address.getDisplayName());
				toAddresses.add(toEmailAddress);
			}
		}

		final EmailAddressModel fromEmailAddress = getEmailService().getOrCreateEmailAddressForEmail(
				emailBean.getFromAddress().getAddress(), emailBean.getFromAddress().getDisplayName());
		email.setToAddresses(toAddresses);
		email.setFromAddress(fromEmailAddress);
		email.setSubject(emailBean.getSubject());
		email.setBody(emailBean.getBody());
		email.setReplyToAddress(emailBean.getReplyToAddres());
		return getEmailService().send(email);
	}
}
