/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccuser.constants;

/**
 * Global class for all Saccuser constants. You can add global constants for your extension into this class.
 */
public final class SaccuserConstants extends GeneratedSaccuserConstants
{
	public static final String EXTENSIONNAME = "saccuser";

	private SaccuserConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccuserPlatformLogo";
}
