/**
 *
 */
package com.sacc.saccuser.area.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.sacc.saccuser.area.dao.AreaDao;
import com.sacc.saccuser.city.service.CityService;
import com.sacc.saccuser.model.AreaModel;
import com.sacc.saccuser.model.CityModel;


/**
 * The Class DefaultAreaDao.
 *
 * @author mnasro
 */
public class DefaultAreaDao extends DefaultGenericDao<AreaModel> implements AreaDao
{
	/** The city service. */
	@Resource(name = "cityService")
	private CityService cityService;


	/**
	 * Instantiates a new default area dao.
	 */
	public DefaultAreaDao()
	{
		super(AreaModel._TYPECODE);
	}


	/**
	 * Find areas bycode.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the list
	 */
	@Override
	public Optional<List<AreaModel>> findAreasBycode(final String cityCode)
	{
		ServicesUtil.validateParameterNotNull(cityCode, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		final Optional<CityModel> optional = getCityService().get(cityCode);
		if (!optional.isPresent())
		{
			Optional.ofNullable(Collections.emptyList());
		}
		params.put(AreaModel.CITY, optional.get());
		return Optional.ofNullable(find(params));
	}

	/**
	 * Find.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaModel> find(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(AreaModel.CODE, code);
		final List<AreaModel> find = find(params);
		return find.stream().findFirst();
	}

	/**
	 * Gets the city service.
	 *
	 * @return the city service
	 */
	protected CityService getCityService()
	{
		return cityService;
	}


	@Override
	public Optional<List<AreaModel>> findAll()
	{
		final List<AreaModel> find = find();
		return CollectionUtils.isEmpty(find) ? Optional.empty() : Optional.ofNullable(find);
	}
}
