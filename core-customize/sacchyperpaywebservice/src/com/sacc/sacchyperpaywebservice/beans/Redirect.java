
package com.sacc.sacchyperpaywebservice.beans;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Redirect
{

	@Expose
	@SerializedName("parameters")
	private List<Object> parameters = null;

	public List<Object> getParameters()
	{
		return parameters;
	}

	public void setParameters(final List<Object> parameters)
	{
		this.parameters = parameters;
	}

}
