
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PaymentPayLoad
{
	@Expose
	@SerializedName("id")
	private String id;

	@Expose
	@SerializedName("paymentType")
	private String paymentType;

	@Expose
	@SerializedName("paymentBrand")
	private String paymentBrand;

	@Expose
	@SerializedName("amount")
	private String amount;

	@Expose
	@SerializedName("currency")
	private String currency;

	@Expose
	@SerializedName("presentationAmount")
	private String presentationAmount;

	@Expose
	@SerializedName("presentationCurrency")
	private String presentationCurrency;

	@Expose
	@SerializedName("descriptor")
	private String descriptor;

	@Expose
	@SerializedName("result")
	private Result result;

	@Expose
	@SerializedName("authentication")
	private Authentication authentication;

	@Expose
	@SerializedName("card")
	private Card card;

	@Expose
	@SerializedName("customer")
	private Customer customer;

	@Expose
	@SerializedName("customParameters")
	private CustomParameters customParameters;

	@Expose
	@SerializedName("risk")
	private Risk risk;

	@Expose
	@SerializedName("buildNumber")
	private String buildNumber;

	@Expose
	@SerializedName("timestamp")
	private String timestamp;

	@Expose
	@SerializedName("ndc")
	private String ndc;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getPaymentBrand()
	{
		return paymentBrand;
	}

	public void setPaymentBrand(final String paymentBrand)
	{
		this.paymentBrand = paymentBrand;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getPresentationAmount()
	{
		return presentationAmount;
	}

	public void setPresentationAmount(final String presentationAmount)
	{
		this.presentationAmount = presentationAmount;
	}

	public String getPresentationCurrency()
	{
		return presentationCurrency;
	}

	public void setPresentationCurrency(final String presentationCurrency)
	{
		this.presentationCurrency = presentationCurrency;
	}

	public String getDescriptor()
	{
		return descriptor;
	}

	public void setDescriptor(final String descriptor)
	{
		this.descriptor = descriptor;
	}

	public Result getResult()
	{
		return result;
	}

	public void setResult(final Result result)
	{
		this.result = result;
	}

	public Authentication getAuthentication()
	{
		return authentication;
	}

	public void setAuthentication(final Authentication authentication)
	{
		this.authentication = authentication;
	}

	public Card getCard()
	{
		return card;
	}

	public void setCard(final Card card)
	{
		this.card = card;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(final Customer customer)
	{
		this.customer = customer;
	}

	public CustomParameters getCustomParameters()
	{
		return customParameters;
	}

	public void setCustomParameters(final CustomParameters customParameters)
	{
		this.customParameters = customParameters;
	}

	public Risk getRisk()
	{
		return risk;
	}

	public void setRisk(final Risk risk)
	{
		this.risk = risk;
	}

	public String getBuildNumber()
	{
		return buildNumber;
	}

	public void setBuildNumber(final String buildNumber)
	{
		this.buildNumber = buildNumber;
	}

	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(final String timestamp)
	{
		this.timestamp = timestamp;
	}

	public String getNdc()
	{
		return ndc;
	}

	public void setNdc(final String ndc)
	{
		this.ndc = ndc;
	}

}
