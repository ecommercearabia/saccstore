
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sacc.saccpayment.hyperpay.enums.CustomerSex;


public class Customer
{

	@Expose
	@SerializedName("givenName")
	private String givenName;
	@Expose
	@SerializedName("surname")
	private String surname;
	@Expose
	@SerializedName("merchantCustomerId")
	private String merchantCustomerId;
	@Expose
	@SerializedName("sex")
	private CustomerSex sex;
	@Expose
	@SerializedName("email")
	private String email;

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(final String givenName)
	{
		this.givenName = givenName;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(final String surname)
	{
		this.surname = surname;
	}

	public String getMerchantCustomerId()
	{
		return merchantCustomerId;
	}

	public void setMerchantCustomerId(final String merchantCustomerId)
	{
		this.merchantCustomerId = merchantCustomerId;
	}

	public CustomerSex getSex()
	{
		return sex;
	}

	public void setSex(final CustomerSex sex)
	{
		this.sex = sex;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

}
