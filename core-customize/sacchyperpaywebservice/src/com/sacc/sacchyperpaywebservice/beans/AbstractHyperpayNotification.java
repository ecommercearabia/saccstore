/**
 *
 */
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public abstract class AbstractHyperpayNotification
{

	@SerializedName("type")
	@Expose
	private String type;



	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 *           the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}



}
