/**
 *
 */
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class HyperpayRiskNotification extends AbstractHyperpayNotification
{
	@Expose
	@SerializedName("payload")
	private RiskPayload payload;

	/**
	 * @return the payload
	 */
	public RiskPayload getPayload()
	{
		return payload;
	}

	/**
	 * @param payload
	 *           the payload to set
	 */
	public void setPayload(final RiskPayload payload)
	{
		this.payload = payload;
	}


}
