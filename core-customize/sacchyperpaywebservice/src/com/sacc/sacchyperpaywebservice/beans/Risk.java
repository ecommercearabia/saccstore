
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Risk
{
	@Expose
	@SerializedName("score")
	private String score;

	public String getScore()
	{
		return score;
	}

	public void setScore(final String score)
	{
		this.score = score;
	}

}
