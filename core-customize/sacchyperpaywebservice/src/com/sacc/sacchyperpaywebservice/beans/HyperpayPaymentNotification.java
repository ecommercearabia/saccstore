/**
 *
 */
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class HyperpayPaymentNotification extends AbstractHyperpayNotification
{
	@Expose
	@SerializedName("payload")
	private PaymentPayLoad payload;

	/**
	 * @return the payload
	 */
	public PaymentPayLoad getPayload()
	{
		return payload;
	}

	/**
	 * @param payload
	 *           the payload to set
	 */
	public void setPayload(final PaymentPayLoad payload)
	{
		this.payload = payload;
	}

}
