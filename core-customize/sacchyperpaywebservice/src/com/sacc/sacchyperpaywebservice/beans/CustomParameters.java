
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CustomParameters
{

	@SerializedName("SHOPPER_promoCode")
	@Expose
	private String shopperPromoCode;

	public String getSHOPPERPromoCode()
	{
		return shopperPromoCode;
	}

	public void setSHOPPERPromoCode(final String sHOPPERPromoCode)
	{
		this.shopperPromoCode = sHOPPERPromoCode;
	}

}
