
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RegistrationPayload
{
	@Expose
	@SerializedName("id")
	private String id;

	@Expose
	@SerializedName("paymentBrand")
	private String paymentBrand;

	@Expose
	@SerializedName("result")
	private Result result;

	@Expose
	@SerializedName("card")
	private Card card;

	@Expose
	@SerializedName("authentication")
	private Authentication authentication;

	@Expose
	@SerializedName("redirect")
	private Redirect redirect;

	@Expose
	@SerializedName("risk")
	private Risk risk;

	@Expose
	@SerializedName("timestamp")
	private String timestamp;

	@Expose
	@SerializedName("ndc")
	private String ndc;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPaymentBrand()
	{
		return paymentBrand;
	}

	public void setPaymentBrand(final String paymentBrand)
	{
		this.paymentBrand = paymentBrand;
	}

	public Result getResult()
	{
		return result;
	}

	public void setResult(final Result result)
	{
		this.result = result;
	}

	public Card getCard()
	{
		return card;
	}

	public void setCard(final Card card)
	{
		this.card = card;
	}

	public Authentication getAuthentication()
	{
		return authentication;
	}

	public void setAuthentication(final Authentication authentication)
	{
		this.authentication = authentication;
	}

	public Redirect getRedirect()
	{
		return redirect;
	}

	public void setRedirect(final Redirect redirect)
	{
		this.redirect = redirect;
	}

	public Risk getRisk()
	{
		return risk;
	}

	public void setRisk(final Risk risk)
	{
		this.risk = risk;
	}

	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(final String timestamp)
	{
		this.timestamp = timestamp;
	}

	public String getNdc()
	{
		return ndc;
	}

	public void setNdc(final String ndc)
	{
		this.ndc = ndc;
	}

}
