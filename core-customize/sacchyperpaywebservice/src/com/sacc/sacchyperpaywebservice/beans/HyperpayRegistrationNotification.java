/**
 *
 */
package com.sacc.sacchyperpaywebservice.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class HyperpayRegistrationNotification extends AbstractHyperpayNotification
{
	@Expose
	@SerializedName("payload")
	private RegistrationPayload payload;

	@SerializedName("action")
	@Expose
	private String action;

	/**
	 * @return the payload
	 */
	public RegistrationPayload getPayload()
	{
		return payload;
	}

	/**
	 * @param payload
	 *           the payload to set
	 */
	public void setPayload(final RegistrationPayload payload)
	{
		this.payload = payload;
	}

	/**
	 * @return the action
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param action
	 *           the action to set
	 */
	public void setAction(final String action)
	{
		this.action = action;
	}

}
