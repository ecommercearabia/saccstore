/**
 *
 */
package com.sacc.sacchyperpaywebservice.enums;

/**
 * @author core
 *
 */
public enum HyperPayBrands
{
	APPLEPAY, CC, MADA;
}
