/**
 *
 */
package com.sacc.sacchyperpaywebservice.util;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.ArrayUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.sacc.sacchyperpaywebservice.exception.HyperpayClientException;
import com.sacc.sacchyperpaywebservice.exception.type.HyperpayClientExceptionType;


/**
 * @author mbaker
 *
 */
public class HyperpayEncryptionUtil
{

	private HyperpayEncryptionUtil()
	{

	}

	public static String decrypt(final String vector, final String authenticationTag, final String data, final String decryptKey)
			throws HyperpayClientException
	{
		Security.addProvider(new BouncyCastleProvider());

		// Convert data to process
		final byte[] key = DatatypeConverter.parseHexBinary(decryptKey);
		final byte[] iv = DatatypeConverter.parseHexBinary(vector);
		final byte[] authTag = DatatypeConverter.parseHexBinary(authenticationTag);
		final byte[] encryptedText = DatatypeConverter.parseHexBinary(data);

		// Unlike other programming language, We have to append auth tag at the end of encrypted text in Java
		final byte[] cipherText = ArrayUtils.addAll(encryptedText, authTag);

		// Prepare decryption
		final SecretKeySpec keySpec = new SecretKeySpec(key, 0, 32, "AES");
		Cipher cipher;
		try
		{
			cipher = Cipher.getInstance("AES/GCM/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));

			// Decrypt
			final byte[] bytes = cipher.doFinal(cipherText);
			return new String(bytes, StandardCharsets.UTF_8);
		}
		catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException e)
		{

			throw new HyperpayClientException(HyperpayClientExceptionType.DECRYPTION_ERROR, e.getMessage(), e);
		}


	}
}
