/**
 *
 */
package com.sacc.sacchyperpaywebservice.services.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sacc.core.model.MerchantTransactionModel;
import com.sacc.core.service.CustomCartService;
import com.sacc.core.service.CustomOrderService;
import com.sacc.core.service.MerchantTransactionService;
import com.sacc.facades.facade.CustomAcceleratorCheckoutFacade;
import com.sacc.sacchyperpaywebservice.enums.HyperPayBrands;
import com.sacc.sacchyperpaywebservice.exception.HyperpayClientException;
import com.sacc.sacchyperpaywebservice.exception.type.HyperpayClientExceptionType;
import com.sacc.sacchyperpaywebservice.services.HyperpayClientService;
import com.sacc.sacchyperpaywebservice.util.HyperpayEncryptionUtil;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.context.PaymentContext;
import com.sacc.saccpayment.hyperpay.enums.TransactionStatus;
import com.sacc.saccpayment.hyperpay.service.HyperpayService;
import com.sacc.saccpayment.model.HyperpayApplePayPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayMadaPaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.service.PaymentProviderService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultHyperpayClientService implements HyperpayClientService
{

	private static final String MERCHANT_TRANSACTION_ID = "merchantTransactionId";
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHyperpayClientService.class);
	private static final Gson GSON = new GsonBuilder().create();
	private static final String BASE_STORE_CODE = "skysales-sa";

	@Resource(name = "paymentProviderService")
	private PaymentProviderService paymentProviderService;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "defaultCartServiceForAccelerator")
	private CustomCartService customCartService;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade customAcceleratorCheckoutFacade;

	@Resource(name = "hyperpayService")
	private HyperpayService hyperpayService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	@Override
	public void updatePayment(final String data, final String vector, final String authTag, final HyperPayBrands brand)
			throws HyperpayClientException
	{
		Preconditions.checkArgument(Strings.isNotBlank(data), "data is empty");
		Preconditions.checkArgument(Strings.isNotBlank(vector), "vector is empty");
		Preconditions.checkArgument(Strings.isNotBlank(authTag), "authTag is empty");


		final HyperpayPaymentProviderModel paymentProvider = getProviderByBrand(brand);
		if (paymentProvider == null)
		{
			throw new IllegalArgumentException("Cannot Get Payment Provider for brand[" + brand + "]");
		}

		final String encryptKey = paymentProvider.getEncryptKey();
		if (Strings.isBlank(encryptKey))
		{
			LOG.error("Cannot find encryptKey on paymentProvidor");
			throw new HyperpayClientException(HyperpayClientExceptionType.DECRYPTION_ERROR, "encryptKey not found", null);
		}

		final String jsonRequest = HyperpayEncryptionUtil.decrypt(vector, authTag, data, encryptKey);
		final Map<String, Object> request = GSON.fromJson(jsonRequest, Map.class);


		if (!request.containsKey("type"))
		{
			LOG.error("Request doesn't contain 'type'\nRequest: {}", jsonRequest);
			throw new IllegalArgumentException("Request doesn't contain 'type'");
		}


		// First check if it's ping data
		if (((String) request.getOrDefault("type", "")).equalsIgnoreCase("test"))
		{
			LOG.info("Recived PING notification \n{}", jsonRequest);
			return;
		}

		// We only expect 'PAYMENT' as type for the notifications
		if (!((String) request.getOrDefault("type", "")).equalsIgnoreCase("PAYMENT"))
		{
			LOG.error("Unhandeled Notification type: [{}]", request.get("type"));
			throw new IllegalArgumentException("Unhandeled Notification 'type'");
		}

		final Map<String, Object> payload = (Map<String, Object>) request.getOrDefault("payload", null);
		if (payload == null)
		{
			LOG.error("PayLoad is null type[{}]", jsonRequest);
			throw new IllegalArgumentException("Payload of the notification is null");
		}



		try
		{
			if (getPaymentContext().isWebHookNotificationPending(paymentProvider, payload))
			{
				LOG.info("Recived Pending Notification for merchantTransactionId[{}]",
						payload.getOrDefault(MERCHANT_TRANSACTION_ID, null));
				return;
			}
		}
		catch (final PaymentException e)
		{
			throw new HyperpayClientException(HyperpayClientExceptionType.BAD_REQUEST, e.getMessage(), e);
		}


		sleepRandomTime();

		final String merchantTransactionId = (String) payload.getOrDefault(MERCHANT_TRANSACTION_ID, Strings.EMPTY);
		if (Strings.isBlank(merchantTransactionId))
		{
			throw new HyperpayClientException(HyperpayClientExceptionType.BAD_REQUEST, "Merchant Transaction ID is null", payload);
		}

		final String orderCode = merchantTransactionId.split("_")[0];
		final MerchantTransactionModel merchantTransaction = trySaveMerchantTransaction(merchantTransactionId);
		if (merchantTransaction == null)
		{
			return;
		}

		final List<CartModel> cartByMerchantTransactionId = getCustomCartService()
				.getCartByMerchantTransactionId(merchantTransaction.getId());

		if (Collections.isEmpty(cartByMerchantTransactionId))
		{
			LOG.info("Cannot Find cart with the orderCode[{}]", orderCode);
			// Check if cart already changed into order
			if (getCustomOrderService().getOrderForCode(orderCode) == null)
			{
				LOG.error("Nor the cart nor the order can be found ");
			}
			return;
		}

		if (cartByMerchantTransactionId.size() > 1)
		{
			LOG.warn("Found Multiple Carts[{}] with the same merchantTransactionId[{}], taking the first one",
					cartByMerchantTransactionId.stream().map(e -> e.getCode()).collect(Collectors.toList()), merchantTransactionId);
		}

		final CartModel cartModel = cartByMerchantTransactionId.get(0);

		final TransactionStatus transactionStatus = getHyperpayService().getTransactionStatus(payload);
		if (!TransactionStatus.SUCCESS.equals(transactionStatus))
		{
			merchantTransaction.setDecision("1");
			merchantTransaction.setResultCode((String) ((Map<String, Object>) payload.get("result")).get("code"));
			merchantTransaction.setVersion(merchantTransactionId);
			getModelService().save(merchantTransaction);
			getModelService().refresh(merchantTransaction);
			return;
		}

		LOG.info(jsonRequest);
		try
		{
			getCustomAcceleratorCheckoutFacade().completeCardOrderModelUsingWebhook(cartModel, payload);
		}
		catch (final InvalidCartException e)
		{
			LOG.error("{}", e.getStackTrace());
			throw new HyperpayClientException(HyperpayClientExceptionType.BAD_REQUEST, e.getMessage(), payload);
		}

	}


	private void sleepRandomTime()
	{
		try
		{
			final double random = Math.random();
			final long time = (long) (random * 4000 + 1000);
			Thread.sleep(time);
		}
		catch (final InterruptedException e1)
		{
			LOG.warn(e1.getMessage());
			Thread.currentThread().interrupt();
		}
	}


	/**
	 * @param brand
	 * @return
	 * @throws HyperpayClientException
	 */
	private HyperpayPaymentProviderModel getProviderByBrand(final HyperPayBrands brand) throws HyperpayClientException
	{
		switch (brand)
		{
			case APPLEPAY:
				return getPaymentProvider(HyperpayApplePayPaymentProviderModel.class);
			case CC:
				return getPaymentProvider(HyperpayPaymentProviderModel.class);
			case MADA:
				return getPaymentProvider(HyperpayMadaPaymentProviderModel.class);
			default:
				break;
		}
		return null;
	}


	private MerchantTransactionModel getMerchantTransactionIfExistById(final String merchantTransactionId)
	{
		return getMerchantTransactionService().getMerchantTransactionById(merchantTransactionId);
	}


	private MerchantTransactionModel trySaveMerchantTransaction(final String merchantTransactionId)
	{
		final String orderCode = merchantTransactionId.split("_")[0];
		final MerchantTransactionModel merchantTransactionIfExist = getMerchantTransactionIfExistById(orderCode);
		if (merchantTransactionIfExist != null && (merchantTransactionIfExist.getVersion() == null
				|| !merchantTransactionIfExist.getVersion().equalsIgnoreCase(merchantTransactionId)))
		{
			getModelService().remove(merchantTransactionIfExist);
		}

		try
		{
			final MerchantTransactionModel merchantTrasnaction = getModelService().create(MerchantTransactionModel.class);
			merchantTrasnaction.setId(orderCode);
			merchantTrasnaction.setSource("WEBHOOK");
			merchantTrasnaction.setVersion(merchantTransactionId);
			getModelService().save(merchantTrasnaction);
			return merchantTrasnaction;
		}
		catch (final Exception e)
		{
			LOG.info(e.toString());
			LOG.warn("MerchantTransactionModel already exist for {}", merchantTransactionId);
		}
		return null;
	}

	private <T> T getPaymentProvider(final Class<T> clazz) throws HyperpayClientException
	{
		final Optional<PaymentProviderModel> paymentProvider = getPaymentProviderService().getActive(BASE_STORE_CODE, clazz);

		if (paymentProvider.isEmpty())
		{
			throw new HyperpayClientException(HyperpayClientExceptionType.PROVIDER_NOT_FOUND, "provider not found", null);
		}
		return (T) paymentProvider.get();
	}

	/**
	 * @return the paymentProviderService
	 */
	protected PaymentProviderService getPaymentProviderService()
	{
		return paymentProviderService;
	}


	/**
	 * @return the paymentContext
	 */
	public PaymentContext getPaymentContext()
	{
		return paymentContext;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the customCartService
	 */
	public CustomCartService getCustomCartService()
	{
		return customCartService;
	}


	/**
	 * @return the customAcceleratorCheckoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCustomAcceleratorCheckoutFacade()
	{
		return customAcceleratorCheckoutFacade;
	}


	/**
	 * @return the hyperpayService
	 */
	public HyperpayService getHyperpayService()
	{
		return hyperpayService;
	}


	/**
	 * @return the customOrderService
	 */
	public CustomOrderService getCustomOrderService()
	{
		return customOrderService;
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}


}
