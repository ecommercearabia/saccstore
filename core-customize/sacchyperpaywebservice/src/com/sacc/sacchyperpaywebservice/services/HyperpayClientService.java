/**
 *
 */
package com.sacc.sacchyperpaywebservice.services;

import com.sacc.sacchyperpaywebservice.enums.HyperPayBrands;
import com.sacc.sacchyperpaywebservice.exception.HyperpayClientException;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface HyperpayClientService
{
	public void updatePayment(final String data, final String vector, final String authTag, final HyperPayBrands brand)
			throws HyperpayClientException;

}
