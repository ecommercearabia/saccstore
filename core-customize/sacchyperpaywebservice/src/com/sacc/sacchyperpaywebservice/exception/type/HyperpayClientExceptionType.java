/**
 *
 */
package com.sacc.sacchyperpaywebservice.exception.type;

/**
 * @author mbaker
 *
 */
public enum HyperpayClientExceptionType
{
	DECRYPTION_ERROR, PROVIDER_NOT_FOUND, BAD_REQUEST;

}
