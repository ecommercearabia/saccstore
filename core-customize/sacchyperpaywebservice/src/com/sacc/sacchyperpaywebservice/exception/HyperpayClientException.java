/**
 *
 */
package com.sacc.sacchyperpaywebservice.exception;

import com.sacc.sacchyperpaywebservice.exception.type.HyperpayClientExceptionType;


/**
 * @author mbaker
 *
 */
public class HyperpayClientException extends Exception
{
	private final Object data;

	private final HyperpayClientExceptionType type;

	/**
	 * @param message
	 */
	public HyperpayClientException(final HyperpayClientExceptionType type, final String message, final Object data)
	{
		super(message);
		this.data = data;
		this.type = type;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @return the type
	 */
	public HyperpayClientExceptionType getType()
	{
		return type;
	}

}
