/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacchyperpaywebservice;

import org.apache.log4j.Logger;


/**
 * Simple test class to demonstrate how to include utility classes to your webmodule.
 */
public class SacchyperpaywebserviceWebHelper
{
	/** Edit the local|project.properties to change logging behavior (properties log4j.*). */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SacchyperpaywebserviceWebHelper.class.getName());

	private SacchyperpaywebserviceWebHelper()
	{
	}

	public static final String getTestOutput()
	{
		return "testoutput";
	}
}
