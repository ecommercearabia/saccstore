/**
 *
 */
package com.sacc.sacchyperpaywebservice.controllers;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sacc.sacchyperpaywebservice.enums.HyperPayBrands;
import com.sacc.sacchyperpaywebservice.exception.HyperpayClientException;
import com.sacc.sacchyperpaywebservice.services.HyperpayClientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author mbaker
 *
 */
@Controller
@RequestMapping(value = "/")
@Api(tags = "HYPERPAY")
public class HyperpayController
{

	private static final Logger LOG = LoggerFactory.getLogger(HyperpayController.class);
	private static final Gson GSON = new GsonBuilder().create();

	@Resource(name = "hyperpayClientService")
	private HyperpayClientService hyperpayClientService;

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/notification", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@ApiOperation(value = "Payment Notification", notes = "Payment Notification", produces = "application/json", consumes = "text/plain")
	public ResponseEntity<String> notification(@RequestBody(required = false)
	final String body, @RequestHeader(required = false, name = "X-Initialization-Vector")
	final String vector, @RequestHeader(required = false, name = "X-Authentication-Tag")
	final String authTag, final HttpServletRequest httpServletRequest)
	{
		logInfo(body, vector, authTag, httpServletRequest);
		return callUpdatePayment(body, vector, authTag, HyperPayBrands.CC);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/notification/mada", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@ApiOperation(value = "Payment Notification", notes = "Payment Notification", produces = "application/json", consumes = "text/plain")
	public ResponseEntity<String> notificationMada(@RequestBody(required = false)
	final String body, @RequestHeader(required = false, name = "X-Initialization-Vector")
	final String vector, @RequestHeader(required = false, name = "X-Authentication-Tag")
	final String authTag, final HttpServletRequest httpServletRequest)
	{
		logInfo(body, vector, authTag, httpServletRequest);
		return callUpdatePayment(body, vector, authTag, HyperPayBrands.MADA);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/notification/applepay", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@ApiOperation(value = "Payment Notification", notes = "Payment Notification", produces = "application/json", consumes = "text/plain")
	public ResponseEntity<String> notificationApplePay(@RequestBody(required = false)
	final String body, @RequestHeader(required = false, name = "X-Initialization-Vector")
	final String vector, @RequestHeader(required = false, name = "X-Authentication-Tag")
	final String authTag, final HttpServletRequest httpServletRequest)
	{
		logInfo(body, vector, authTag, httpServletRequest);
		return callUpdatePayment(body, vector, authTag, HyperPayBrands.APPLEPAY);
	}




	private ResponseEntity<String> callUpdatePayment(final String body, final String vector, final String authTag,
			final HyperPayBrands brand)
	{
		try
		{
			getHyperpayClientService().updatePayment(body, vector, authTag, brand);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		catch (final HyperpayClientException e)
		{
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void logInfo(final String body, final String vector, final String authTag, final HttpServletRequest httpServletRequest)
	{
		LOG.info("body = {}", body);
		LOG.info("vector = {}", vector);
		LOG.info("authTag = {}", authTag);
		final String parameterMap = GSON.toJson(httpServletRequest.getParameterMap());
		LOG.info("Request Param Map = {}", parameterMap);
		logHeaders(httpServletRequest);
	}

	private void logHeaders(final HttpServletRequest httpServletRequest)
	{
		final Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
		final Map<String, String> map = new HashMap<>();
		while (headerNames.hasMoreElements())
		{
			final String key = headerNames.nextElement();
			final String value = httpServletRequest.getHeader(key);
			map.put(key, value);
		}

		final String json = GSON.toJson(map);
		LOG.info(json);
	}

	/**
	 * @return the hyperpayClientService
	 */
	protected HyperpayClientService getHyperpayClientService()
	{
		return hyperpayClientService;
	}

}
