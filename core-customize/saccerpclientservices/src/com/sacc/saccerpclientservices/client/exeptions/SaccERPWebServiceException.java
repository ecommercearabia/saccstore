/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2020 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sacc.saccerpclientservices.client.exeptions;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;


/**
 *
 * @author Baha Almtoor
 *
 */
public class SaccERPWebServiceException extends Exception
{
	private static final long serialVersionUID = 1L;

	private SaccERPExeptionType type;

	public SaccERPWebServiceException(final String message)
	{
		super(message);
	}

	public SaccERPWebServiceException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public SaccERPWebServiceException(final String message, final SaccERPExeptionType type)
	{
		this(message);
		this.type = type;
	}

	public SaccERPWebServiceException(final String message, final Throwable cause, final SaccERPExeptionType type)
	{
		this(message, cause);
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public SaccERPExeptionType getType()
	{
		return type;
	}
}
