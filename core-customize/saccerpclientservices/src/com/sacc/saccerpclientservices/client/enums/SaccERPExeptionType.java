package com.sacc.saccerpclientservices.client.enums;

/**
 *
 * @author Baha Almtoor
 *
 */
public enum SaccERPExeptionType
{
	CLIENT_FORBIDDEN(403, "Forbidden"), CONFIGURATION_NOT_FOUND(404, "Configuration not found"), BAD_REQUEST(400,
			"Bad request"), INTERNAL_SERVER_ERROR(500,
					"Internal server error"), INVALID_PRODUCT(400,
							"Product not found"), INSUFFICIENT_INVENTORY(400, "Insufficient stock level");

	private int code;
	private String message;

	SaccERPExeptionType(final int code, final String message)
	{
		this.code = code;
		this.message = message;
	}

	public int getCode()
	{
		return code;
	}

	public String getMessage()
	{
		return message;
	}
}
