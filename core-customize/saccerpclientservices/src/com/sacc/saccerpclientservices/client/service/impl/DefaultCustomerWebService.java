/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPCustomerWebService;
import com.sacc.saccerpclientservices.model.SaccERPClientCustomerWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;

import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebService;
import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultCustomerWebService extends DefaultWebService implements SaccERPCustomerWebService
{
	private static final Logger LOG = Logger.getLogger(DefaultCustomerWebService.class);

	protected CustomerWebService buildCustomerWebService(final String no, final String username, final String password)
			throws SaccERPWebServiceException
	{
		final CustomerWebService customerWebService = new CustomerWebService();
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientCustomerWebServiceModel.class);

		if (model != null)
		{
			customerWebService.setTestEnv(model.isTestEnvironment());
		}
		customerWebService.setTimeout(getTimeout());
		customerWebService.setNo(no);
		customerWebService.setUsername(username);
		customerWebService.setPassword(password);


		return customerWebService;
	}

	protected CustomerWebService buildCustomerWebService(final ConsignmentModel consignment,
			final SaccERPClientCustomerWebServiceModel model) throws SaccERPWebServiceException
	{
		validateUser(consignment.getOrder().getUser());

		final OrderModel order = (OrderModel) consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		final CustomerWebService customerWebService = buildCustomerWebService(customer.getPk().toString(), model.getUserName(),
				model.getPassword());
		customerWebService.setName(customer.getName());
		customerWebService.setPhoneNo(customer.getMobileNumber());
		customerWebService.setEMail(customer.getContactEmail());

		return customerWebService;
	}

	protected void checkInstance(final SaccERPClientWebServiceModel model) throws SaccERPWebServiceException
	{
		if (!(model instanceof SaccERPClientCustomerWebServiceModel))
		{
			throw new SaccERPWebServiceException(
					String.format("Model not instance from[%]", SaccERPClientCustomerWebServiceModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}
	}

	protected SaccERPClientCustomerWebServiceModel getModel(final String modelCode) throws SaccERPWebServiceException
	{
		validate(modelCode, String.format("Invalid modelCode[%s]", modelCode), SaccERPExeptionType.BAD_REQUEST);

		final Optional<SaccERPClientWebServiceModel> result = getSaccERPModelService().find(modelCode, true,
				SaccERPClientCustomerWebServiceModel.class);
		validateOp(result,
				String.format("[%s] not found where code[%s]", SaccERPClientCustomerWebServiceModel._TYPECODE, modelCode),
				SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = result.get();
		checkInstance(model);
		validateCredentials(model.getUserName(), model.getPassword());

		return (SaccERPClientCustomerWebServiceModel) model;
	}

	@Override
	public List<CustomerWebService> readMultiple(final int size, final List<CustomerWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientCustomerWebServiceModel.class);
		final CustomerWebService customerWebService = buildCustomerWebService("", model.getUserName(), model.getPassword());

		return customerWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<CustomerWebService> readMultiple(final String username, final String password, final int size,
			final List<CustomerWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, "notReq");
		final CustomerWebService customerWebService = buildCustomerWebService("", username, password);

		return customerWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<CustomerWebService> readMultiple(final int size, final String modelCode,
			final List<CustomerWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientCustomerWebServiceModel model = getModel(modelCode);
		final CustomerWebService customerWebService = buildCustomerWebService("", model.getUserName(), model.getPassword());

		return customerWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<CustomerWebService> readMultiple(final int size, final SaccERPClientCustomerWebServiceModel model,
			final List<CustomerWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validate(model, String.format("Invalid [%s]", SaccERPClientCustomerWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		validateEssentialData(model.getUserName(), model.getPassword(), "notReq");
		final CustomerWebService customerWebService = buildCustomerWebService("", model.getUserName(), model.getPassword());

		return customerWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public CustomerWebService read(final String no) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientCustomerWebServiceModel.class);
		final CustomerWebService customerWebService = buildCustomerWebService(no, model.getUserName(), model.getPassword());

		return customerWebService.read();
	}

	@Override
	public CustomerWebService read(final String no, final String username, final String password) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, no);
		final CustomerWebService customerWebService = buildCustomerWebService(no, username, password);

		return customerWebService.read();
	}

	@Override
	public CustomerWebService read(final String no, final String modelCode) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientCustomerWebServiceModel model = getModel(modelCode);
		final CustomerWebService customerWebService = buildCustomerWebService(no, model.getUserName(), model.getPassword());

		return customerWebService.read();
	}

	@Override
	public CustomerWebService read(final String no, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(no, String.format("Invalid number", no), SaccERPExeptionType.BAD_REQUEST);
		validate(model, String.format("Invalid [%s]", SaccERPClientCustomerWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		final CustomerWebService customerWebService = buildCustomerWebService(no, model.getUserName(), model.getPassword());

		return customerWebService.read();
	}

	protected CustomerWebService executeUpdate(final CustomerWebService customerWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP update customerWebService request[%s]", marshal(customerWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return customerWebService.update();
	}

	protected CustomerWebService update(final CustomerWebService customerWebService, SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		if (model == null)
		{
			model = findModel(SaccERPClientCustomerWebServiceModel.class);
			customerWebService.setUsername(model.getUserName());
			customerWebService.setPassword(model.getPassword());
			customerWebService.setTestEnv(model.isTestEnvironment());
		}
		customerWebService.setTimeout(getTimeout());
		if (StringUtils.isBlank(customerWebService.getKey()))
		{
			final CustomerWebService getCustomerWebService = customerWebService.read();
			if (getCustomerWebService == null || StringUtils.isBlank(getCustomerWebService.getKey()))
			{
				throw new SaccERPWebServiceException(
						String.format("Invalid getCustomerWebService where number=[%s]", customerWebService.getNo()),
						SaccERPExeptionType.CLIENT_FORBIDDEN);
			}

			customerWebService.setKey(getCustomerWebService.getKey());
		}

		return executeUpdate(customerWebService);
	}

	@Override
	public CustomerWebService update(final CustomerWebService customerWebService) throws SaccERPWebServiceException
	{
		validate(customerWebService, "Invalid customerWebService", SaccERPExeptionType.BAD_REQUEST);
		validate(customerWebService.getNo(), "Invalid customerWebService.no", SaccERPExeptionType.BAD_REQUEST);

		return update(customerWebService, null);
	}

	@Override
	public CustomerWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SaccERPClientCustomerWebServiceModel model = getModel(modelCode);
		final CustomerWebService customerWebService = buildCustomerWebService(consignment, model);

		return update(customerWebService, model);
	}

	@Override
	public CustomerWebService update(final ConsignmentModel consignment, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final CustomerWebService customerWebService = buildCustomerWebService(consignment, model);

		return update(customerWebService, model);
	}

	protected CustomerWebService executeCreate(final CustomerWebService customerWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP create customerWebService request[%s]", marshal(customerWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientCustomerWebServiceModel.class);

		return customerWebService.create();
	}

	@Override
	public CustomerWebService create(final CustomerWebService customerWebService) throws SaccERPWebServiceException
	{
		validate(customerWebService, "Invalid customerWebService", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientCustomerWebServiceModel.class);
		customerWebService.setUsername(model.getUserName());
		customerWebService.setPassword(model.getPassword());
		customerWebService.setTestEnv(model.isTestEnvironment());
		customerWebService.setTimeout(getTimeout());
		return executeCreate(customerWebService);
	}

	@Override
	public CustomerWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final CustomerWebService customerWebService = buildCustomerWebService(consignment, getModel(modelCode));

		return executeCreate(customerWebService);
	}

	@Override
	public CustomerWebService create(final ConsignmentModel consignment, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final CustomerWebService customerWebService = buildCustomerWebService(consignment, model);

		return executeCreate(customerWebService);
	}
}