/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import java.util.Collection;
import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccerpclientservices.client.dao.SaccERPModelDao;
import com.sacc.saccerpclientservices.client.service.SaccERPModelService;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultSaccERPModelService implements SaccERPModelService
{
	@Resource(name = "saccERPModelDao")
	private SaccERPModelDao saccERPModelDao;

	protected SaccERPModelDao getSaccERPModelDao()
	{
		return saccERPModelDao;
	}

	@Override
	public Optional<SaccERPClientWebServiceModel> find(final String code, final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		return getSaccERPModelDao().find(code, active, model);
	}

	public Optional<SaccERPClientWebServiceModel> findFirst(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		return getSaccERPModelDao().findFirst(active, model);
	}

	@Override
	public Collection<SaccERPClientWebServiceModel> findAll(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		return getSaccERPModelDao().findAll(active, model);
	}
}
