/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPReturnWebService;
import com.sacc.saccerpclientservices.model.SaccERPClientReturnWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;

import schemas.dynamics.microsoft.page.return_ws.ReturnWS;
import schemas.dynamics.microsoft.page.return_ws.ReturnWSFilter;
import schemas.dynamics.microsoft.page.return_ws.WSReturnOrderLine;
import schemas.dynamics.microsoft.page.return_ws.WSReturnOrderLineList;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultReturnWebService extends DefaultWebService implements SaccERPReturnWebService
{
	private static final Logger LOG = Logger.getLogger(DefaultReturnWebService.class);

	protected void setWSReturnOrderLine(final ReturnWS returnWebService, final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			returnWebService.setWSReturnOrderLine(new WSReturnOrderLineList());
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry != null)
				{
					final WSReturnOrderLine wsReturnOrderLine = new WSReturnOrderLine();
					if (entry.getProduct() != null)
					{
						wsReturnOrderLine.setBarcodeNo(entry.getProduct().getCode());
					}
					if (entry.getQuantity() != null)
					{
						wsReturnOrderLine.setQuantity(String.valueOf(entry.getQuantity()));
					}
					if (entry.getBasePrice() != null)
					{
						wsReturnOrderLine.setUnitPrice(String.valueOf(entry.getBasePrice()));
					}

					returnWebService.getWSReturnOrderLine().getWSReturnOrderLine().add(wsReturnOrderLine);
				}
			}
		}
	}

	protected Date getDeliveryDateGap(final SaccERPClientReturnWebServiceModel saccERPClientReturnWebService)
	{
		final Integer deliveryDateGap = saccERPClientReturnWebService.getDeliveryDateGap();
		if (deliveryDateGap != null && deliveryDateGap >= 0)
		{
			final Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, deliveryDateGap);

			return calendar.getTime();
		}

		return null;
	}

	protected ReturnWS buildReturnWebService(final String no, final String username, final String password)
			throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientReturnWebServiceModel.class);
		final ReturnWS returnWebService = new ReturnWS();
		if (model != null)
		{
			returnWebService.setTestEnv(model.isTestEnvironment());
		}
		returnWebService.setTimeout(getTimeout());

		returnWebService.setNo(no);
		returnWebService.setUsername(username);
		returnWebService.setPassword(password);

		return returnWebService;
	}

	protected ReturnWS buildReturnWebService(final ConsignmentModel consignment,
			final SaccERPClientReturnWebServiceModel saccERPClientReturnWebService) throws SaccERPWebServiceException
	{
		validateUser(consignment.getOrder().getUser());
		final OrderModel order = (OrderModel) consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();
		final ReturnWS returnWebService = buildReturnWebService(order.getCode(), saccERPClientReturnWebService.getUserName(),
				saccERPClientReturnWebService.getPassword());
		returnWebService.setExternalDocumentNo(order.getCode());
		returnWebService.setSellToCustomerNo(customer.getPk().toString());
		returnWebService.setOrderDate(convertDateToXMLGregorianCalendar(order.getCreationtime()));
		setWSReturnOrderLine(returnWebService, order);

		return returnWebService;
	}

	protected void checkInstance(final SaccERPClientWebServiceModel model) throws SaccERPWebServiceException
	{
		if (!(model instanceof SaccERPClientReturnWebServiceModel))
		{
			throw new SaccERPWebServiceException(
					String.format("Model not instance from[%]", SaccERPClientReturnWebServiceModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}
	}

	protected SaccERPClientReturnWebServiceModel getModel(final String modelCode) throws SaccERPWebServiceException
	{
		validate(modelCode, String.format("Invalid modelCode[%s]", modelCode), SaccERPExeptionType.BAD_REQUEST);

		final Optional<SaccERPClientWebServiceModel> result = getSaccERPModelService().find(modelCode, true,
				SaccERPClientReturnWebServiceModel.class);
		validateOp(result, String.format("[%s] not found where code[%s]", SaccERPClientReturnWebServiceModel._TYPECODE, modelCode),
				SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = result.get();
		checkInstance(model);
		validateCredentials(model.getUserName(), model.getPassword());

		return (SaccERPClientReturnWebServiceModel) model;
	}

	@Override
	public List<ReturnWS> readMultiple(final int size, final List<ReturnWSFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientReturnWebServiceModel.class);
		final ReturnWS returnWebService = buildReturnWebService("", model.getUserName(), model.getPassword());

		return returnWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ReturnWS> readMultiple(final String username, final String password, final int size,
			final List<ReturnWSFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, "notReq");
		final ReturnWS returnWebService = buildReturnWebService("", username, password);

		return returnWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ReturnWS> readMultiple(final int size, final String modelCode, final List<ReturnWSFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientReturnWebServiceModel model = getModel(modelCode);
		final ReturnWS returnWebService = buildReturnWebService("", model.getUserName(), model.getPassword());

		return returnWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ReturnWS> readMultiple(final int size, final SaccERPClientReturnWebServiceModel model,
			final List<ReturnWSFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validate(model, String.format("Invalid [%s]", SaccERPClientReturnWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		validateEssentialData(model.getUserName(), model.getPassword(), "notReq");
		final ReturnWS returnWebService = buildReturnWebService("", model.getUserName(), model.getPassword());

		return returnWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public ReturnWS read(final String no) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientReturnWebServiceModel.class);
		final ReturnWS returnWebService = buildReturnWebService(no, model.getUserName(), model.getPassword());

		return returnWebService.read();
	}

	@Override
	public ReturnWS read(final String no, final String username, final String password) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, no);
		final ReturnWS returnWebService = buildReturnWebService(no, username, password);

		return returnWebService.read();
	}

	@Override
	public ReturnWS read(final String no, final String modelCode) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientReturnWebServiceModel model = getModel(modelCode);
		final ReturnWS returnWebService = buildReturnWebService(no, model.getUserName(), model.getPassword());

		return returnWebService.read();
	}

	@Override
	public ReturnWS read(final String no, final SaccERPClientReturnWebServiceModel model) throws SaccERPWebServiceException
	{
		validate(no, String.format("Invalid number", no), SaccERPExeptionType.BAD_REQUEST);
		validate(model, String.format("Invalid [%s]", SaccERPClientReturnWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		final ReturnWS returnWebService = buildReturnWebService(no, model.getUserName(), model.getPassword());

		return returnWebService.read();
	}

	protected ReturnWS executeUpdate(final ReturnWS returnWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP update returnWebService request[%s]", marshal(returnWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return returnWebService.update();
	}

	protected ReturnWS update(final ReturnWS returnWebService, SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		if (model == null)
		{
			model = findModel(SaccERPClientReturnWebServiceModel.class);
			returnWebService.setUsername(model.getUserName());
			returnWebService.setPassword(model.getPassword());
			returnWebService.setTestEnv(model.isTestEnvironment());
		}
		returnWebService.setTimeout(getTimeout());
		if (StringUtils.isBlank(returnWebService.getKey()))
		{
			final ReturnWS getReturnWebService = returnWebService.read();
			if (getReturnWebService == null || StringUtils.isBlank(getReturnWebService.getKey()))
			{
				throw new SaccERPWebServiceException(
						String.format("Invalid getReturnWebService where number=[%s]", returnWebService.getNo()),
						SaccERPExeptionType.CLIENT_FORBIDDEN);
			}

			returnWebService.setKey(getReturnWebService.getKey());
		}

		try
		{
			LOG.info(String.format("Sacc ERP update returnWebService request[%s]", marshal(returnWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return executeUpdate(returnWebService);
	}

	@Override
	public ReturnWS update(final ReturnWS returnWebService) throws SaccERPWebServiceException
	{
		validate(returnWebService, "Invalid returnWebService", SaccERPExeptionType.BAD_REQUEST);
		validate(returnWebService.getNo(), "Invalid returnWebService.no", SaccERPExeptionType.BAD_REQUEST);

		return update(returnWebService, null);
	}

	@Override
	public ReturnWS update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SaccERPClientReturnWebServiceModel model = getModel(modelCode);
		final ReturnWS returnWebService = buildReturnWebService(consignment, model);

		return update(returnWebService, model);
	}

	@Override
	public ReturnWS update(final ConsignmentModel consignment, final SaccERPClientReturnWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final ReturnWS returnWebService = buildReturnWebService(consignment, model);

		return update(returnWebService, model);
	}

	protected ReturnWS executeCreate(final ReturnWS returnWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP create returnWebService request[%s]", marshal(returnWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientReturnWebServiceModel.class);
		returnWebService.setTestEnv(model.isTestEnvironment());
		returnWebService.setTimeout(getTimeout());
		return returnWebService.create();
	}

	@Override
	public ReturnWS create(final ReturnWS returnWebService) throws SaccERPWebServiceException
	{
		validate(returnWebService, "Invalid returnWebService", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientReturnWebServiceModel.class);
		returnWebService.setUsername(model.getUserName());
		returnWebService.setPassword(model.getPassword());

		return executeCreate(returnWebService);
	}

	@Override
	public ReturnWS create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final ReturnWS returnWebService = buildReturnWebService(consignment, getModel(modelCode));

		return executeCreate(returnWebService);
	}

	@Override
	public ReturnWS create(final ConsignmentModel consignment, final SaccERPClientReturnWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final ReturnWS returnWebService = buildReturnWebService(consignment, model);

		return executeCreate(returnWebService);
	}
}