/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.model.SaccERPClientItemWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;
import schemas.dynamics.microsoft.page.item_web_service.ItemWebServiceFields;
import schemas.dynamics.microsoft.page.item_web_service.ItemWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultItemWebService extends DefaultWebService implements SaccERPItemWebService
{
	private static final Logger LOG = Logger.getLogger(DefaultItemWebService.class);

	protected ItemWebService buildItemWebService(final String no, final String username, final String password)
			throws SaccERPWebServiceException
	{
		final ItemWebService itemWebService = new ItemWebService();
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		if (model != null)
		{
			itemWebService.setTestEnv(model.isTestEnvironment());
		}
		itemWebService.setTimeout(getTimeout());
		itemWebService.setNo(no);
		itemWebService.setUsername(username);
		itemWebService.setPassword(password);

		return itemWebService;
	}

	protected ItemWebService buildItemWebService(final ConsignmentModel consignment,
			final SaccERPClientItemWebServiceModel saccERPClientItemWebService) throws SaccERPWebServiceException
	{
		validateUser(consignment.getOrder().getUser());

		final OrderModel order = (OrderModel) consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();
		final ItemWebService itemWebService = buildItemWebService(customer.getPk().toString(),
				saccERPClientItemWebService.getUserName(), saccERPClientItemWebService.getPassword());

		return itemWebService;
	}

	protected void checkInstance(final SaccERPClientWebServiceModel model) throws SaccERPWebServiceException
	{
		if (!(model instanceof SaccERPClientItemWebServiceModel))
		{
			throw new SaccERPWebServiceException(
					String.format("Model not instance from[%]", SaccERPClientItemWebServiceModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}
	}

	protected SaccERPClientItemWebServiceModel getModel(final String modelCode) throws SaccERPWebServiceException
	{
		validate(modelCode, String.format("Invalid modelCode[%s]", modelCode), SaccERPExeptionType.BAD_REQUEST);

		final Optional<SaccERPClientWebServiceModel> result = getSaccERPModelService().find(modelCode, true,
				SaccERPClientItemWebServiceModel.class);
		validateOp(result, String.format("[%s] not found where code[%s]", SaccERPClientItemWebServiceModel._TYPECODE, modelCode),
				SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = result.get();
		checkInstance(model);
		validateCredentials(model.getUserName(), model.getPassword());

		return (SaccERPClientItemWebServiceModel) model;
	}

	@Override
	public List<ItemWebService> readMultiple(final int size, final String barcodeNo) throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		final ItemWebService itemWebService = buildItemWebService("", model.getUserName(), model.getPassword());
		final ItemWebServiceFilter filter = new ItemWebServiceFilter();
		filter.setCriteria(barcodeNo);
		filter.setField(ItemWebServiceFields.BAR_CODE_NO);
		final List<ItemWebServiceFilter> filters = new ArrayList<>();
		filters.add(filter);

		return itemWebService.readMultiple(filters, "", size);
	}

	@Override
	public List<ItemWebService> readMultiple(final int size, final List<ItemWebServiceFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		final ItemWebService itemWebService = buildItemWebService("", model.getUserName(), model.getPassword());

		return itemWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ItemWebService> readMultiple(final String username, final String password, final int size,
			final List<ItemWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, "notReq");
		final ItemWebService itemWebService = buildItemWebService("", username, password);

		return itemWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ItemWebService> readMultiple(final int size, final String modelCode, final List<ItemWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientItemWebServiceModel model = getModel(modelCode);
		final ItemWebService itemWebService = buildItemWebService("", model.getUserName(), model.getPassword());

		return itemWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<ItemWebService> readMultiple(final int size, final SaccERPClientItemWebServiceModel model,
			final List<ItemWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validate(model, String.format("Invalid [%s]", SaccERPClientItemWebServiceModel._TYPECODE), SaccERPExeptionType.BAD_REQUEST);
		validateEssentialData(model.getUserName(), model.getPassword(), "notReq");
		final ItemWebService itemWebService = buildItemWebService("", model.getUserName(), model.getPassword());

		return itemWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public ItemWebService read(final String no) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		final ItemWebService itemWebService = buildItemWebService(no, model.getUserName(), model.getPassword());

		return itemWebService.read();
	}

	@Override
	public ItemWebService read(final String no, final String username, final String password) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, no);
		final ItemWebService itemWebService = buildItemWebService(no, username, password);

		return itemWebService.read();
	}

	@Override
	public ItemWebService read(final String no, final String modelCode) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientItemWebServiceModel model = getModel(modelCode);
		final ItemWebService itemWebService = buildItemWebService(no, model.getUserName(), model.getPassword());

		return itemWebService.read();
	}

	@Override
	public ItemWebService read(final String no, final SaccERPClientItemWebServiceModel model) throws SaccERPWebServiceException
	{
		validate(no, String.format("Invalid number", no), SaccERPExeptionType.BAD_REQUEST);
		validate(model, String.format("Invalid [%s]", SaccERPClientItemWebServiceModel._TYPECODE), SaccERPExeptionType.BAD_REQUEST);
		final ItemWebService itemWebService = buildItemWebService(no, model.getUserName(), model.getPassword());

		return itemWebService.read();
	}

	protected ItemWebService executeUpdate(final ItemWebService itemWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP update itemWebService request[%s]", marshal(itemWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return itemWebService.update();
	}

	protected ItemWebService update(final ItemWebService itemWebService, SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		if (model == null)
		{
			model = findModel(SaccERPClientItemWebServiceModel.class);
			itemWebService.setUsername(model.getUserName());
			itemWebService.setPassword(model.getPassword());
			itemWebService.setTestEnv(model.isTestEnvironment());
		}
		itemWebService.setTimeout(getTimeout());
		if (StringUtils.isBlank(itemWebService.getKey()))
		{
			final ItemWebService getItemWebService = itemWebService.read();
			if (getItemWebService == null || StringUtils.isBlank(getItemWebService.getKey()))
			{
				throw new SaccERPWebServiceException(
						String.format("Invalid getItemWebService where number=[%s]", itemWebService.getNo()),
						SaccERPExeptionType.CLIENT_FORBIDDEN);
			}

			itemWebService.setKey(getItemWebService.getKey());
		}

		return executeUpdate(itemWebService);
	}

	@Override
	public ItemWebService update(final ItemWebService itemWebService) throws SaccERPWebServiceException
	{
		validate(itemWebService, "Invalid itemWebService", SaccERPExeptionType.BAD_REQUEST);
		validate(itemWebService.getNo(), "Invalid itemWebService.no", SaccERPExeptionType.BAD_REQUEST);

		return update(itemWebService, null);
	}

	@Override
	public ItemWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SaccERPClientItemWebServiceModel model = getModel(modelCode);
		final ItemWebService itemWebService = buildItemWebService(consignment, model);

		return update(itemWebService, model);
	}

	@Override
	public ItemWebService update(final ConsignmentModel consignment, final SaccERPClientItemWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final ItemWebService itemWebService = buildItemWebService(consignment, model);

		return update(itemWebService, model);
	}

	protected ItemWebService executeCreate(final ItemWebService itemWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP create itemWebService request[%s]", marshal(itemWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		itemWebService.setTestEnv(model.isTestEnvironment());
		itemWebService.setTimeout(getTimeout());
		return itemWebService.create();
	}

	@Override
	public ItemWebService create(final ItemWebService itemWebService) throws SaccERPWebServiceException
	{
		validate(itemWebService, "Invalid itemWebService", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientItemWebServiceModel.class);
		itemWebService.setUsername(model.getUserName());
		itemWebService.setPassword(model.getPassword());

		return executeCreate(itemWebService);
	}

	@Override
	public ItemWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final ItemWebService itemWebService = buildItemWebService(consignment, getModel(modelCode));

		return executeCreate(itemWebService);
	}

	@Override
	public ItemWebService create(final ConsignmentModel consignment, final SaccERPClientItemWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final ItemWebService itemWebService = buildItemWebService(consignment, model);

		return executeCreate(itemWebService);
	}
}