/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.StringWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPModelService;
import com.sacc.saccerpclientservices.client.validators.SaccERPValidator;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public abstract class DefaultWebService implements SaccERPValidator
{
	private static final Logger LOG = Logger.getLogger(DefaultWebService.class);

	@Resource(name = "saccERPModelService")
	private SaccERPModelService saccERPModelService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	protected int getTimeout()
	{
		int timeout = 20000;
		try
		{
			timeout = configurationService.getConfiguration().getInt("sacc.erp.connection.timeout");
		}
		catch (final Exception e)
		{
			LOG.warn("Property sacc.erp.connection.timeout not found! Setting SACC ERP connection timeout as default.");
		}
		LOG.info("SACC ERP Connection timeout in milliseconds: " + timeout);
		return timeout;
	}

	protected SaccERPModelService getSaccERPModelService()
	{
		return saccERPModelService;
	}

	protected <O> String marshal(final O obj) throws JAXBException
	{
		final JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
		final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		final StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(obj, sw);

		return sw.toString();
	}

	protected XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) throws SaccERPWebServiceException
	{
		validate(date, "Invalid date", SaccERPExeptionType.BAD_REQUEST);

		XMLGregorianCalendar xmlGregorianCalendar = null;
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		try
		{
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
			xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		}
		catch (final DatatypeConfigurationException ex)
		{
			throw new SaccERPWebServiceException("Can't convert from Date to XMLGregorianCalendar", ex,
					SaccERPExeptionType.INTERNAL_SERVER_ERROR);
		}

		return xmlGregorianCalendar;
	}

	protected void validateCredentials(final String username, final String password) throws SaccERPWebServiceException
	{
		validate(username, "Invalid username", SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
		validate(username, "Invalid password", SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
	}

	protected void validateEssentialData(final String username, final String password, final String no)
			throws SaccERPWebServiceException
	{
		validateCredentials(username, password);
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
	}

	protected void validate(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment);
		validate(modelCode, "Invalid modelCode", SaccERPExeptionType.BAD_REQUEST);
	}

	protected void validate(final ConsignmentModel consignment) throws SaccERPWebServiceException
	{
		validate(consignment, "Invalid consignment", SaccERPExeptionType.BAD_REQUEST);
		validate(consignment.getOrder(), "Invalid order", SaccERPExeptionType.BAD_REQUEST);
	}

	protected void validate(final ConsignmentModel consignment, final SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment);
		validate(model, "Invalid model", SaccERPExeptionType.BAD_REQUEST);
		validateCredentials(model.getUserName(), model.getPassword());
	}

	protected void validateUser(final UserModel user) throws SaccERPWebServiceException
	{
		if (!(user instanceof CustomerModel))
		{
			throw new SaccERPWebServiceException(String.format("Model not instance from[%s]", CustomerModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}

		validate(((CustomerModel) user).getPk(), String.format("Invalid customer.pk[%s]", ((CustomerModel) user).getPk()),
				SaccERPExeptionType.BAD_REQUEST);
	}

	protected SaccERPClientWebServiceModel findModel(final Class<? extends SaccERPClientWebServiceModel> clazz)
			throws SaccERPWebServiceException
	{
		final Optional<SaccERPClientWebServiceModel> optional = getSaccERPModelService().findFirst(true, clazz);
		validateOp(optional, "Model not found", SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = optional.get();
		validateCredentials(model.getUserName(), model.getPassword());

		return model;
	}
}