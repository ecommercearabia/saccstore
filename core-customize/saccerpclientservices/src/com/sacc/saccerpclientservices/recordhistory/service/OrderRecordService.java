/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.util.List;


/**
 *
 */
public interface OrderRecordService
{
	OrderModel getOrderByCode(String orderCode);

	List<ReturnRequestModel> getReturnRequestByOrderCode(String orderCode);
}
