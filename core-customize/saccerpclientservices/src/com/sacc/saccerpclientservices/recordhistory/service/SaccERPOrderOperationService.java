/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service;

import java.util.List;

import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;


/**
 *
 */
public interface SaccERPOrderOperationService
{

	void saveRecord(ERPWebServiceType webServiceType, String orderCode, String orderVersionId, String consignmentCode,
			String requestData, String responseData, Boolean done);

	void saveRecord(String orderCode, String productBarcode, String orderVersionId, String consignmentCode, String requestData,
			String responseData, Boolean done);

	public List<SaccERPOrderOperationRecordModel> getAllOrderRecords(final List<ERPWebServiceType> webServiceTypes,
			final String orderCode, final String orderVersionId, final String consignmentCode, final Boolean done);

	List<SaccERPOrderOperationRecordModel> getAll();

	List<SaccERPOrderOperationRecordModel> getAllFialedRecords();

	void updateRecord(final SaccERPOrderOperationRecordModel operationRecord, final String requestData, final String responseData,
			final boolean done);
}
