/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.dao.ReturnRequestDao;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.util.List;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.sacc.saccerpclientservices.recordhistory.dao.OrderRecordDao;
import com.sacc.saccerpclientservices.recordhistory.service.OrderRecordService;


/**
 *
 */
public class DefaultOrderRecordService implements OrderRecordService
{

	@Resource(name = "orderRecordDao")
	private OrderRecordDao orderRecordDao;

	@Resource(name = "returnRequestDao")
	private ReturnRequestDao returnRequestDao;

	@Override
	public OrderModel getOrderByCode(final String orderCode)
	{
		Preconditions.checkArgument(orderCode != null, "Order code cannot be null");
		final OrderModel orderByCode = orderRecordDao.getOrderByCode(orderCode);
		return orderByCode;
	}

	@Override
	public List<ReturnRequestModel> getReturnRequestByOrderCode(final String orderCode)
	{
		Preconditions.checkArgument(orderCode != null, "Order code cannot be null");
		return getReturnRequestDao().getReturnRequests(orderCode);
	}

	/**
	 * @return the returnRequestDao
	 */
	public ReturnRequestDao getReturnRequestDao()
	{
		return returnRequestDao;
	}



}
