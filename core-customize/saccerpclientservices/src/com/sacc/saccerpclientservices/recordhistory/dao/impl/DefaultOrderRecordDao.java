/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.saccerpclientservices.recordhistory.dao.OrderRecordDao;


/**
 *
 */
public class DefaultOrderRecordDao implements OrderRecordDao
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderRecordDao.class);

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	private static final String QUERY = "SELECT {" + OrderModel.PK + "} from {" + OrderModel._TYPECODE + "} where {"
			+ OrderModel.CODE + "}=?code";

	@Override
	public OrderModel getOrderByCode(final String orderCode)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY);
		query.addQueryParameter("code", orderCode);
		final List<OrderModel> result = flexibleSearchService.<OrderModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			LOG.info("Could not find order model with the order code [{}]", orderCode);
			return null;
		}

		if (result.size() > 0)
		{
			LOG.info("Found more than one order with the order code [{}], returning the first oneF", orderCode);
		}

		return result.get(0);
	}

}
