/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.dao.SaccERPOrderOperationDao;

/**
 *
 */
public class DefaultSaccERPOrderOperationDao implements SaccERPOrderOperationDao
{

	private static final String QUERY_START = "select {" + SaccERPOrderOperationRecordModel.PK + "} from {"
			+ SaccERPOrderOperationRecordModel._TYPECODE + "}";
	private static final String WHERE_ORDER_CODE = " and {" + SaccERPOrderOperationRecordModel.ORDERCODE + "}=?orderCode";
	private static final String WHERE_WEB_SERVICE_TYPE = " and {" + SaccERPOrderOperationRecordModel.WEBSERVICETYPE
			+ "} in (?webServiceType)";
	private static final String WHERE_ORDER_VERSION_ID = " and {" + SaccERPOrderOperationRecordModel.ORDERVERSIONID
			+ "}=?orderVersionID";
	private static final String WHERE_CONSIGNMENT_CODE = " and {" + SaccERPOrderOperationRecordModel.CONSIGNMENTCODE
			+ "}=?consignmentCode";
	private static final String WHERE_DONE = " where {" + SaccERPOrderOperationRecordModel.DONE + "}=?done";

	private static final String WEB_SERVICE_TYPE = SaccERPOrderOperationRecordModel.WEBSERVICETYPE;
	private static final String ORDER_CODE = SaccERPOrderOperationRecordModel.ORDERCODE;
	private static final String ORDER_VERSION_ID = SaccERPOrderOperationRecordModel.ORDERVERSIONID;
	private static final String CONSIGNMENT_CODE = SaccERPOrderOperationRecordModel.CONSIGNMENTCODE;
	private static final String DONE = SaccERPOrderOperationRecordModel.DONE;

	private FlexibleSearchService searchService;

	@Override
	public List<SaccERPOrderOperationRecordModel> getAllRecords()
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(QUERY_START);
		final List<SaccERPOrderOperationRecordModel> results = searchService.<SaccERPOrderOperationRecordModel> search(searchQuery)
				.getResult();
		return results;
	}

	@Override
	public List<SaccERPOrderOperationRecordModel> getAllRecordsByOrderCode(final List<ERPWebServiceType> webServiceTypes,
			final String orderCode, final String orderVersionId,
			final String consignmentCode, final Boolean done)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		final String query = buildQuery(webServiceTypes, orderCode, orderVersionId, consignmentCode, done, params);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameters(params);

		final List<SaccERPOrderOperationRecordModel> results = searchService.<SaccERPOrderOperationRecordModel> search(searchQuery)
				.getResult();
		return results;
	}

	private String buildQuery(final List<ERPWebServiceType> webServiceTypes, final String orderCode, final String orderVersionId,
			final String consignmentCode, final Boolean done, final Map<String, Object> params)
	{
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(QUERY_START);
		if (done != null)
		{
			queryBuilder.append(WHERE_DONE);
			params.put(DONE, done);
		}
		if (StringUtils.isNotBlank(orderCode))
		{
			queryBuilder.append(WHERE_ORDER_CODE);
			params.put(ORDER_CODE, orderCode);
		}
		if (webServiceTypes != null)
		{
			queryBuilder.append(WHERE_WEB_SERVICE_TYPE);
			params.put(WEB_SERVICE_TYPE, webServiceTypes);
		}
		if (StringUtils.isNotBlank(orderVersionId))
		{
			queryBuilder.append(WHERE_ORDER_VERSION_ID);
			params.put(ORDER_VERSION_ID, orderVersionId);
		}
		if (StringUtils.isNotBlank(consignmentCode))
		{
			queryBuilder.append(WHERE_CONSIGNMENT_CODE);
			params.put(CONSIGNMENT_CODE, consignmentCode);
		}

		return queryBuilder.toString();
	}

	/**
	 * @return the searchService
	 */
	public FlexibleSearchService getSearchService()
	{
		return searchService;
	}

	/**
	 * @param searchService
	 *           the searchService to set
	 */
	public void setSearchService(final FlexibleSearchService searchService)
	{
		this.searchService = searchService;
	}

	@Override
	public List<SaccERPOrderOperationRecordModel> getAllFialedRecords()
	{
		final String query = "SELECT {" + SaccERPOrderOperationRecordModel.PK + "} from {"
				+ SaccERPOrderOperationRecordModel._TYPECODE + "} where {" + SaccERPOrderOperationRecordModel.DONE + "}=false";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final List<SaccERPOrderOperationRecordModel> result = getSearchService()
				.<SaccERPOrderOperationRecordModel> search(searchQuery).getResult();

		return result;
	}



}
