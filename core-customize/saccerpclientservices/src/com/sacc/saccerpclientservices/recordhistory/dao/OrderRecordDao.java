/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.dao;

import de.hybris.platform.core.model.order.OrderModel;

/**
 *
 */
public interface OrderRecordDao
{
	OrderModel getOrderByCode(String orderCode);
}
