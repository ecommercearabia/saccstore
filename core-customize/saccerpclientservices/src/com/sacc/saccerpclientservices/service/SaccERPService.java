/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;


// TODO: Auto-generated Javadoc
/**
 * The Interface SaccERPService.
 *
 * @author mnasro The Interface SaccERPService.
 */
public interface SaccERPService
{

	/**
	 * Creates the sales order action.
	 *
	 * @param orderModel
	 *           the order model
	 */
	public void createSalesOrderAction(final OrderModel orderModel);

	/**
	 * Creates the sales order.
	 *
	 * @param orderModel
	 *           the order model
	 * @return true, if successful
	 */
	boolean createSalesOrder(OrderModel orderModel);

	/**
	 * Creates the customer.
	 *
	 * @param orderModel
	 *           the order model
	 * @return true, if successful
	 */
	boolean createCustomer(OrderModel orderModel);

	/**
	 * Creates the payment.
	 *
	 * @param orderModel
	 *           the order model
	 * @return true, if successful
	 */
	boolean createPayment(OrderModel orderModel);

	/**
	 * Update sales order.
	 *
	 * @param orderModel
	 *           the order model
	 * @param webServiceType
	 *           the web service type
	 * @throws SaccERPWebServiceException
	 */
	boolean updateSalesOrderStatus(OrderModel orderModel, ERPWebServiceType webServiceType) throws SaccERPWebServiceException;

	/**
	 * Update sales order lines without change the saleo rder status.
	 *
	 * @param orderModel
	 *           the order model
	 * @return
	 * @throws SaccERPWebServiceException
	 *            the sacc ERP web service exception
	 */
	boolean pickSaleOrder(OrderModel orderModel);

	boolean packSaleOrder(OrderModel orderModel);


	/**
	 * Creates the return request.
	 *
	 * @param returnRequest
	 *           the returnRequest model
	 */

	void createReturnRequest(ReturnRequestModel returnRequest) throws SaccERPWebServiceException;

	/**
	 * Preform partial cancel sale order line. by give it a list of entries to be canceled
	 *
	 * @param order
	 *           the order
	 * @param entries
	 *           the entries
	 */
	void performPartialCancelSaleOrderLine(OrderModel order, List<OrderCancelEntry> entries);

	boolean performPartialSaleOrderLineCancellationByModificationRecord(OrderModel order,
			List<OrderEntryModificationRecordEntryModel> entries, SaccERPOrderOperationRecordModel orderRecord);

	/**
	 * Preform fully cancel sale order.
	 *
	 * @param order
	 *           the order
	 * @return
	 */
	boolean performFullyCancelSaleOrder(OrderModel order);

	/**
	 * Update sale order delivered status.Based On Record Status
	 *
	 * @param consignments
	 *           the consignments
	 * @return
	 */
	boolean updateSaleOrderDeliveredStatus(ConsignmentModel consignment);
}
