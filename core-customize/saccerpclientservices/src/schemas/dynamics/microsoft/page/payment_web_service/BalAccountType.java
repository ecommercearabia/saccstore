
package schemas.dynamics.microsoft.page.payment_web_service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Bal_Account_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Bal_Account_Type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="G_L_Account"/>
 *     &lt;enumeration value="Customer"/>
 *     &lt;enumeration value="Vendor"/>
 *     &lt;enumeration value="Bank_Account"/>
 *     &lt;enumeration value="Fixed_Asset"/>
 *     &lt;enumeration value="IC_Partner"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Bal_Account_Type")
@XmlEnum
public enum BalAccountType {

    @XmlEnumValue("G_L_Account")
    G_L_ACCOUNT("G_L_Account"),
    @XmlEnumValue("Customer")
    CUSTOMER("Customer"),
    @XmlEnumValue("Vendor")
    VENDOR("Vendor"),
    @XmlEnumValue("Bank_Account")
    BANK_ACCOUNT("Bank_Account"),
    @XmlEnumValue("Fixed_Asset")
    FIXED_ASSET("Fixed_Asset"),
    @XmlEnumValue("IC_Partner")
    IC_PARTNER("IC_Partner");
    private final String value;

    BalAccountType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BalAccountType fromValue(String v) {
        for (BalAccountType c: BalAccountType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
