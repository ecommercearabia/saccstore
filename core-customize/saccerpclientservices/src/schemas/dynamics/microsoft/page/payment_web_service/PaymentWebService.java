
package schemas.dynamics.microsoft.page.payment_web_service;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sun.xml.ws.client.BindingProviderProperties;

import schemas.dynamics.microsoft.page.AbstractSaccERPWebService;


/**
 * <p>
 * Java class for Payment_Web_Service complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Payment_Web_Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Customer_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="External_Document_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Posting_Date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Bal_Account_Type" type="{urn:microsoft-dynamics-schemas/page/payment_web_service}Bal_Account_Type" minOccurs="0"/>
 *         &lt;element name="Bal_Account_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Post" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Online_Posted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

/**
 *
 * @author Baha Almtoor
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment_Web_Service", propOrder =
{ "key", "customerNo", "externalDocumentNo", "amount", "postingDate", "balAccountType", "balAccountNo", "post", "onlinePosted" })
@XmlRootElement
public class PaymentWebService extends AbstractSaccERPWebService
{
	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "Customer_No")
	protected String customerNo = "";
	@XmlElement(name = "External_Document_No")
	protected String externalDocumentNo = "";
	@XmlElement(name = "Amount")
	protected String amount = "0";
	@XmlElement(name = "Posting_Date")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar postingDate;
	@XmlElement(name = "Bal_Account_Type")
	@XmlSchemaType(name = "string")
	protected BalAccountType balAccountType;
	@XmlElement(name = "Bal_Account_No")
	protected String balAccountNo = "";
	@XmlElement(name = "Post")
	protected Boolean post;
	@XmlElement(name = "Online_Posted")
	protected Boolean onlinePosted;

	private void setTimeoutValue(final PaymentWebServicePort paymentWebServicePort)
	{
		final BindingProvider provider = (BindingProvider) paymentWebServicePort;
		final Map<String, Object> requestContext = provider.getRequestContext();
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getTimeout());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getTimeout());
	}

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the customerNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getCustomerNo()
	{
		return customerNo;
	}

	/**
	 * Sets the value of the customerNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setCustomerNo(final String value)
	{
		this.customerNo = value;
	}

	/**
	 * Gets the value of the externalDocumentNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getExternalDocumentNo()
	{
		return externalDocumentNo;
	}

	/**
	 * Sets the value of the externalDocumentNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setExternalDocumentNo(final String value)
	{
		this.externalDocumentNo = value;
	}

	/**
	 * Gets the value of the amount property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getAmount()
	{
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setAmount(final String value)
	{
		this.amount = value;
	}

	/**
	 * Gets the value of the postingDate property.
	 *
	 * @return possible object is {@link XMLGregorianCalendar }
	 *
	 */
	public XMLGregorianCalendar getPostingDate()
	{
		return postingDate;
	}

	protected XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) throws SaccERPWebServiceException
	{

		XMLGregorianCalendar xmlGregorianCalendar = null;
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		try
		{
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
			xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		}
		catch (final DatatypeConfigurationException ex)
		{
			ex.printStackTrace();
		}

		return xmlGregorianCalendar;
	}

	/**
	 * Sets the value of the postingDate property.
	 *
	 * @param value
	 *           allowed object is {@link XMLGregorianCalendar }
	 *
	 */
	public void setPostingDate(final XMLGregorianCalendar value)
	{
		this.postingDate = value;
	}

	public void setPostingDate(final Date value)
	{
		try
		{
			this.postingDate = convertDateToXMLGregorianCalendar(value);
		}
		catch (final SaccERPWebServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the value of the balAccountType property.
	 *
	 * @return possible object is {@link BalAccountType }
	 *
	 */
	public BalAccountType getBalAccountType()
	{
		return balAccountType;
	}

	/**
	 * Sets the value of the balAccountType property.
	 *
	 * @param value
	 *           allowed object is {@link BalAccountType }
	 *
	 */
	public void setBalAccountType(final BalAccountType value)
	{
		this.balAccountType = value;
	}

	/**
	 * Gets the value of the balAccountNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBalAccountNo()
	{
		return balAccountNo;
	}

	/**
	 * Sets the value of the balAccountNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setBalAccountNo(final String value)
	{
		this.balAccountNo = value;
	}

	/**
	 * Gets the value of the post property.
	 *
	 * @return possible object is {@link Boolean }
	 *
	 */
	public Boolean isPost()
	{
		return post;
	}

	/**
	 * Sets the value of the post property.
	 *
	 * @param value
	 *           allowed object is {@link Boolean }
	 *
	 */
	public void setPost(final Boolean value)
	{
		this.post = value;
	}

	/**
	 * Gets the value of the onlinePosted property.
	 *
	 * @return possible object is {@link Boolean }
	 *
	 */
	public Boolean isOnlinePosted()
	{
		return onlinePosted;
	}

	/**
	 * Sets the value of the onlinePosted property.
	 *
	 * @param value
	 *           allowed object is {@link Boolean }
	 *
	 */
	public void setOnlinePosted(final Boolean value)
	{
		this.onlinePosted = value;
	}

	protected PaymentWebServicePort getPaymentWebServicePort()
	{
		return new PaymentWebServiceService(isTestEnv()).getPaymentWebServicePort();
	}

	public List<PaymentWebService> readMultiple(final List<PaymentWebServiceFilter> filters, final String bookmarkKey,
			final int size) throws SaccERPWebServiceException
	{
		try
		{
			final PaymentWebServicePort paymentWebServicePort = getPaymentWebServicePort();
			setAuthenticator();
			setTimeoutValue(paymentWebServicePort);
			final PaymentWebServiceList paymentWebServiceList = paymentWebServicePort.readMultiple(filters, bookmarkKey, size);
			if (paymentWebServiceList != null)
			{
				return paymentWebServiceList.getPaymentWebService();
			}

			return Collections.emptyList();
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't readMultiple[PaymentWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public PaymentWebService read() throws SaccERPWebServiceException
	{
		try
		{
			final PaymentWebServicePort paymentWebServicePort = getPaymentWebServicePort();
			setAuthenticator();
			setTimeoutValue(paymentWebServicePort);
			final String recIdFromKey = paymentWebServicePort.getRecIdFromKey(getKey());
			return paymentWebServicePort.readByRecId(recIdFromKey);
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't read[PaymentWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public PaymentWebService update() throws SaccERPWebServiceException
	{
		throw new SaccERPWebServiceException("Service update[PaymentWebService] not found", SaccERPExeptionType.CLIENT_FORBIDDEN);
	}

	@Override
	public PaymentWebService create() throws SaccERPWebServiceException
	{
		try
		{
			final PaymentWebServicePort paymentWebServicePort = getPaymentWebServicePort();
			setAuthenticator();
			setTimeoutValue(paymentWebServicePort);
			final Holder<PaymentWebService> paymentWebServiceHolder = new Holder<PaymentWebService>(this);
			paymentWebServicePort.create(paymentWebServiceHolder);
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't create[PaymentWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public String toString()
	{
		return "PaymentWebService [key=" + key + ", customerNo=" + customerNo + ", externalDocumentNo=" + externalDocumentNo
				+ ", amount=" + amount + ", postingDate=" + postingDate + ", balAccountType=" + balAccountType + ", balAccountNo="
				+ balAccountNo + ", post=" + post + ", onlinePosted=" + onlinePosted + "]";
	}

}
