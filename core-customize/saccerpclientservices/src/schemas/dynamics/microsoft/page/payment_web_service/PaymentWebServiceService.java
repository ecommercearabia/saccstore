
package schemas.dynamics.microsoft.page.payment_web_service;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import org.apache.log4j.Logger;


/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.9-b130926.1035 Generated source version: 2.2
 *
 */
@WebServiceClient(name = "Payment_Web_Service_Service", targetNamespace = "urn:microsoft-dynamics-schemas/page/payment_web_service")
public class PaymentWebServiceService extends Service
{
	private static final Logger LOG = Logger.getLogger(PaymentWebServiceService.class);

	private static URL PAYMENTWEBSERVICESERVICEWSDLLOCATION;
	private static WebServiceException PAYMENTWEBSERVICESERVICEEXCEPTION;
	private final static QName PAYMENTWEBSERVICESERVICE_QNAME = new QName(
			"urn:microsoft-dynamics-schemas/page/payment_web_service", "Payment_Web_Service_Service");
	private final static String LIVE_WSDL_FILE_LOCATION = "/wsdls/live/Payment_Web_Service.wsdl";
	private final static String TEST_WSDL_FILE_LOCATION = "/wsdls/test/Payment_Web_Service.wsdl";

	public PaymentWebServiceService(final boolean flag)
	{
		super(__getWsdlLocation(flag), PAYMENTWEBSERVICESERVICE_QNAME);
	}

	public PaymentWebServiceService(final boolean flag, final WebServiceFeature... features)
	{
		super(__getWsdlLocation(flag), PAYMENTWEBSERVICESERVICE_QNAME, features);
	}

	public PaymentWebServiceService(final URL wsdlLocation)
	{
		super(wsdlLocation, PAYMENTWEBSERVICESERVICE_QNAME);
	}

	public PaymentWebServiceService(final URL wsdlLocation, final WebServiceFeature... features)
	{
		super(wsdlLocation, PAYMENTWEBSERVICESERVICE_QNAME, features);
	}

	public PaymentWebServiceService(final URL wsdlLocation, final QName serviceName)
	{
		super(wsdlLocation, serviceName);
	}

	public PaymentWebServiceService(final URL wsdlLocation, final QName serviceName, final WebServiceFeature... features)
	{
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns PaymentWebServicePort
	 */
	@WebEndpoint(name = "Payment_Web_Service_Port")
	public PaymentWebServicePort getPaymentWebServicePort()
	{
		return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/payment_web_service", "Payment_Web_Service_Port"),
				PaymentWebServicePort.class);
	}

	/**
	 *
	 * @param features
	 *           A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy. Supported features not in
	 *           the <code>features</code> parameter will have their default values.
	 * @return returns PaymentWebServicePort
	 */
	@WebEndpoint(name = "Payment_Web_Service_Port")
	public PaymentWebServicePort getPaymentWebServicePort(final WebServiceFeature... features)
	{
		return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/payment_web_service", "Payment_Web_Service_Port"),
				PaymentWebServicePort.class, features);
	}

	private static URL __getWsdlLocation(final boolean flag)
	{
		URL url = null;
		WebServiceException e = null;
		try
		{
			if (flag)
			{
				url = PaymentWebServiceService.class.getResource(TEST_WSDL_FILE_LOCATION);
				LOG.info("test env is value : " + flag + "  and the below url will be used : " + url);
			}
			else
			{
				url = url = PaymentWebServiceService.class.getResource(LIVE_WSDL_FILE_LOCATION);
				LOG.info("test env is value  : " + flag + "  and the below url will be used : " + url);
			}
		}
		catch (final Exception ex)
		{
			e = new WebServiceException(ex);
		}
		PAYMENTWEBSERVICESERVICEWSDLLOCATION = url;
		PAYMENTWEBSERVICESERVICEEXCEPTION = e;



		if (PAYMENTWEBSERVICESERVICEEXCEPTION != null)
		{
			throw PAYMENTWEBSERVICESERVICEEXCEPTION;
		}
		return PAYMENTWEBSERVICESERVICEWSDLLOCATION;
	}
}
