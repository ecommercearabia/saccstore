
package schemas.dynamics.microsoft.page.item_web_service;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for Item_Web_Service_Fields.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 *
 * <pre>
 * &lt;simpleType name="Item_Web_Service_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Description_2"/>
 *     &lt;enumeration value="Unit_Cost"/>
 *     &lt;enumeration value="Sales_Unit_of_Measure"/>
 *     &lt;enumeration value="Unit_Price"/>
 *     &lt;enumeration value="Base_Unit_of_Measure"/>
 *     &lt;enumeration value="Stockkeeping_Unit_Exists"/>
 *     &lt;enumeration value="Inventory"/>
 *     &lt;enumeration value="Net_Weight"/>
 *     &lt;enumeration value="Summary"/>
 *     &lt;enumeration value="Arabic_Summary"/>
 *     &lt;enumeration value="IsBaseProduct"/>
 *     &lt;enumeration value="Warranty"/>
 *     &lt;enumeration value="Product_Label"/>
 *     &lt;enumeration value="BarCode_No"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "Item_Web_Service_Fields")
@XmlEnum
public enum ItemWebServiceFields
{

	@XmlEnumValue("No")
	NO("No"), @XmlEnumValue("Description")
	DESCRIPTION("Description"), @XmlEnumValue("Description_2")
	DESCRIPTION_2("Description_2"), @XmlEnumValue("Unit_Cost")
	UNIT_COST("Unit_Cost"), @XmlEnumValue("Sales_Unit_of_Measure")
	SALES_UNIT_OF_MEASURE("Sales_Unit_of_Measure"), @XmlEnumValue("Unit_Price")
	UNIT_PRICE("Unit_Price"), @XmlEnumValue("Base_Unit_of_Measure")
	BASE_UNIT_OF_MEASURE("Base_Unit_of_Measure"), @XmlEnumValue("Stockkeeping_Unit_Exists")
	STOCKKEEPING_UNIT_EXISTS("Stockkeeping_Unit_Exists"), @XmlEnumValue("Inventory")
	INVENTORY("Inventory"), @XmlEnumValue("Net_Weight")
	NET_WEIGHT("Net_Weight"), @XmlEnumValue("Summary")
	SUMMARY("Summary"), @XmlEnumValue("Arabic_Summary")
	ARABIC_SUMMARY("Arabic_Summary"), @XmlEnumValue("IsBaseProduct")
	IS_BASE_PRODUCT("IsBaseProduct"), @XmlEnumValue("Warranty")
	WARRANTY("Warranty"), @XmlEnumValue("Product_Label")
	PRODUCT_LABEL("Product_Label"), @XmlEnumValue("BarCode_No")
	BAR_CODE_NO("BarCode_No"), @XmlEnumValue("Actual_Inventory")
	ACTUAL_INVENTORY("Actual_Inventory");

	private final String value;

	@XmlEnumValue("Online_Vat_Percent")
	protected BigDecimal onlineVatPercent;


	ItemWebServiceFields(final String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static ItemWebServiceFields fromValue(final String v)
	{
		for (final ItemWebServiceFields c : ItemWebServiceFields.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
