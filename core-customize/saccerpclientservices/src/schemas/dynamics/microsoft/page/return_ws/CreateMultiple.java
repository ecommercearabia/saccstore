
package schemas.dynamics.microsoft.page.return_ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Return_WS_List" type="{urn:microsoft-dynamics-schemas/page/return_ws}Return_WS_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnWSList"
})
@XmlRootElement(name = "CreateMultiple")
public class CreateMultiple {

    @XmlElement(name = "Return_WS_List", required = true)
    protected ReturnWSList returnWSList;

    /**
     * Gets the value of the returnWSList property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnWSList }
     *     
     */
    public ReturnWSList getReturnWSList() {
        return returnWSList;
    }

    /**
     * Sets the value of the returnWSList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnWSList }
     *     
     */
    public void setReturnWSList(ReturnWSList value) {
        this.returnWSList = value;
    }

}
