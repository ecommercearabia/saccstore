
package schemas.dynamics.microsoft.page.return_ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for WS_Return_Order_Line complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="WS_Return_Order_Line">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Barcode_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Unit_Price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Line_Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_Return_Order_Line", propOrder =
{ "key", "barcodeNo", "quantity", "unitPrice", "lineAmount", "lineDiscountAmount" })
public class WSReturnOrderLine
{

	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "Barcode_No")
	protected String barcodeNo = "";
	@XmlElement(name = "Quantity")
	protected String quantity = "0";
	@XmlElement(name = "Unit_Price")
	protected String unitPrice = "0";
	@XmlElement(name = "Line_Amount")
	protected String lineAmount = "0";
	@XmlElement(name = "Line_Discount_Amount")
	protected String lineDiscountAmount = "0";

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the barcodeNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBarcodeNo()
	{
		return barcodeNo;
	}

	/**
	 * Sets the value of the barcodeNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setBarcodeNo(final String value)
	{
		this.barcodeNo = value;
	}

	/**
	 * Gets the value of the quantity property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * Sets the value of the quantity property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setQuantity(final String value)
	{
		this.quantity = value;
	}

	/**
	 * Gets the value of the unitPrice property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * Sets the value of the unitPrice property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setUnitPrice(final String value)
	{
		this.unitPrice = value;
	}

	/**
	 * Gets the value of the lineAmount property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getLineAmount()
	{
		return lineAmount;
	}

	/**
	 * Sets the value of the lineAmount property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setLineAmount(final String value)
	{
		this.lineAmount = value;
	}

	/**
	 * @param lineDiscountAmount
	 *           the lineDiscountAmount to set
	 */
	public void setLineDiscountAmount(final String lineDiscountAmount)
	{
		this.lineDiscountAmount = lineDiscountAmount;
	}

	/**
	 * @return the lineDiscountAmount
	 */
	public String getLineDiscountAmount()
	{
		return lineDiscountAmount;
	}

	@Override
	public String toString()
	{
		return "WSReturnOrderLine [key=" + key + ", barcodeNo=" + barcodeNo + ", quantity=" + quantity + ", unitPrice=" + unitPrice
				+ ", lineAmount=" + lineAmount + ", lineDiscountAmount=" + lineDiscountAmount + "]";
	}

}
