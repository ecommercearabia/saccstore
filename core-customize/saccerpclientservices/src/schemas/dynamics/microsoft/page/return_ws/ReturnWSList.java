
package schemas.dynamics.microsoft.page.return_ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Return_WS_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Return_WS_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Return_WS" type="{urn:microsoft-dynamics-schemas/page/return_ws}Return_WS" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Return_WS_List", propOrder = {
    "returnWS"
})
public class ReturnWSList {

    @XmlElement(name = "Return_WS", required = true)
    protected List<ReturnWS> returnWS;

    /**
     * Gets the value of the returnWS property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the returnWS property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturnWS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReturnWS }
     * 
     * 
     */
    public List<ReturnWS> getReturnWS() {
        if (returnWS == null) {
            returnWS = new ArrayList<ReturnWS>();
        }
        return this.returnWS;
    }

}
