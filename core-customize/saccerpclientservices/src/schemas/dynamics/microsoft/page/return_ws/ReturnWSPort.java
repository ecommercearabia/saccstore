
package schemas.dynamics.microsoft.page.return_ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "Return_WS_Port", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ReturnWSPort {


    /**
     * 
     * @param no
     * @return
     *     returns schemas.dynamics.microsoft.page.return_ws.ReturnWS
     */
    @WebMethod(operationName = "Read", action = "urn:microsoft-dynamics-schemas/page/return_ws:Read")
    @WebResult(name = "Return_WS", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "Read", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.Read")
    @ResponseWrapper(localName = "Read_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.ReadResult")
    public ReturnWS read(
        @WebParam(name = "No", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String no);

    /**
     * 
     * @param recId
     * @return
     *     returns schemas.dynamics.microsoft.page.return_ws.ReturnWS
     */
    @WebMethod(operationName = "ReadByRecId", action = "urn:microsoft-dynamics-schemas/page/return_ws:ReadByRecId")
    @WebResult(name = "Return_WS", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "ReadByRecId", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.ReadByRecId")
    @ResponseWrapper(localName = "ReadByRecId_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.ReadByRecIdResult")
    public ReturnWS readByRecId(
        @WebParam(name = "recId", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String recId);

    /**
     * 
     * @param filter
     * @param setSize
     * @param bookmarkKey
     * @return
     *     returns schemas.dynamics.microsoft.page.return_ws.ReturnWSList
     */
    @WebMethod(operationName = "ReadMultiple", action = "urn:microsoft-dynamics-schemas/page/return_ws:ReadMultiple")
    @WebResult(name = "ReadMultiple_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "ReadMultiple", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.ReadMultiple")
    @ResponseWrapper(localName = "ReadMultiple_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.ReadMultipleResult")
    public ReturnWSList readMultiple(
        @WebParam(name = "filter", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        List<ReturnWSFilter> filter,
        @WebParam(name = "bookmarkKey", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String bookmarkKey,
        @WebParam(name = "setSize", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        int setSize);

    /**
     * 
     * @param key
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "IsUpdated", action = "urn:microsoft-dynamics-schemas/page/return_ws:IsUpdated")
    @WebResult(name = "IsUpdated_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "IsUpdated", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.IsUpdated")
    @ResponseWrapper(localName = "IsUpdated_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.IsUpdatedResult")
    public boolean isUpdated(
        @WebParam(name = "Key", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String key);

    /**
     * 
     * @param key
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "GetRecIdFromKey", action = "urn:microsoft-dynamics-schemas/page/return_ws:GetRecIdFromKey")
    @WebResult(name = "GetRecIdFromKey_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "GetRecIdFromKey", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.GetRecIdFromKey")
    @ResponseWrapper(localName = "GetRecIdFromKey_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.GetRecIdFromKeyResult")
    public String getRecIdFromKey(
        @WebParam(name = "Key", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String key);

    /**
     * 
     * @param returnWS
     */
    @WebMethod(operationName = "Create", action = "urn:microsoft-dynamics-schemas/page/return_ws:Create")
    @RequestWrapper(localName = "Create", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.Create")
    @ResponseWrapper(localName = "Create_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.CreateResult")
    public void create(
        @WebParam(name = "Return_WS", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", mode = WebParam.Mode.INOUT)
        Holder<ReturnWS> returnWS);

    /**
     * 
     * @param returnWSList
     */
    @WebMethod(operationName = "CreateMultiple", action = "urn:microsoft-dynamics-schemas/page/return_ws:CreateMultiple")
    @RequestWrapper(localName = "CreateMultiple", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.CreateMultiple")
    @ResponseWrapper(localName = "CreateMultiple_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.CreateMultipleResult")
    public void createMultiple(
        @WebParam(name = "Return_WS_List", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", mode = WebParam.Mode.INOUT)
        Holder<ReturnWSList> returnWSList);

    /**
     * 
     * @param returnWS
     */
    @WebMethod(operationName = "Update", action = "urn:microsoft-dynamics-schemas/page/return_ws:Update")
    @RequestWrapper(localName = "Update", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.Update")
    @ResponseWrapper(localName = "Update_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.UpdateResult")
    public void update(
        @WebParam(name = "Return_WS", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", mode = WebParam.Mode.INOUT)
        Holder<ReturnWS> returnWS);

    /**
     * 
     * @param returnWSList
     */
    @WebMethod(operationName = "UpdateMultiple", action = "urn:microsoft-dynamics-schemas/page/return_ws:UpdateMultiple")
    @RequestWrapper(localName = "UpdateMultiple", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.UpdateMultiple")
    @ResponseWrapper(localName = "UpdateMultiple_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.UpdateMultipleResult")
    public void updateMultiple(
        @WebParam(name = "Return_WS_List", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", mode = WebParam.Mode.INOUT)
        Holder<ReturnWSList> returnWSList);

    /**
     * 
     * @param key
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "Delete", action = "urn:microsoft-dynamics-schemas/page/return_ws:Delete")
    @WebResult(name = "Delete_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "Delete", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.Delete")
    @ResponseWrapper(localName = "Delete_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.DeleteResult")
    public boolean delete(
        @WebParam(name = "Key", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String key);

    /**
     * 
     * @param wsReturnOrderLineKey
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "Delete_WS_Return_Order_Line", action = "urn:microsoft-dynamics-schemas/page/return_ws:Delete_WS_Return_Order_Line")
    @WebResult(name = "Delete_WS_Return_Order_Line_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
    @RequestWrapper(localName = "Delete_WS_Return_Order_Line", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.DeleteWSReturnOrderLine")
    @ResponseWrapper(localName = "Delete_WS_Return_Order_Line_Result", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws", className = "schemas.dynamics.microsoft.page.return_ws.DeleteWSReturnOrderLineResult")
    public boolean deleteWSReturnOrderLine(
        @WebParam(name = "WS_Return_Order_Line_Key", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
        String wsReturnOrderLineKey);

}
