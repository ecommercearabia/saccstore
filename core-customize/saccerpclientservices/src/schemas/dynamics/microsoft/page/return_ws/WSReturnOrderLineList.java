
package schemas.dynamics.microsoft.page.return_ws;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections.CollectionUtils;


/**
 * <p>
 * Java class for WS_Return_Order_Line_List complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="WS_Return_Order_Line_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_Return_Order_Line" type="{urn:microsoft-dynamics-schemas/page/return_ws}WS_Return_Order_Line" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_Return_Order_Line_List", propOrder =
{ "wsReturnOrderLine" })
public class WSReturnOrderLineList
{

	@XmlElement(name = "WS_Return_Order_Line", required = true)
	protected List<WSReturnOrderLine> wsReturnOrderLine;

	/**
	 * Gets the value of the wsReturnOrderLine property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the wsReturnOrderLine property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 *
	 * <pre>
	 * getWSReturnOrderLine().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link WSReturnOrderLine }
	 *
	 *
	 */
	public List<WSReturnOrderLine> getWSReturnOrderLine()
	{
		if (wsReturnOrderLine == null)
		{
			wsReturnOrderLine = new ArrayList<WSReturnOrderLine>();
		}
		return this.wsReturnOrderLine;
	}

	@Override
	public String toString()
	{
		if (CollectionUtils.isNotEmpty(wsReturnOrderLine))
		{
			String list = "WSReturnOrderLineList [";
			for (final WSReturnOrderLine line : wsReturnOrderLine)
			{
				list = list + "[wsReturnOrderLine=" + line + "]";
			}
			list = list + "]";
			return list;
		}
		return "WSReturnOrderLineList []";
	}


}
