
package schemas.dynamics.microsoft.page.sales_order_web_services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections.CollectionUtils;


/**
 * <p>
 * Java class for WS_Sales_Order_Line_List complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="WS_Sales_Order_Line_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_Sales_Order_Line" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}WS_Sales_Order_Line" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_Sales_Order_Line_List", propOrder =
{ "wsSalesOrderLine" })
public class WSSalesOrderLineList
{

	@XmlElement(name = "WS_Sales_Order_Line", required = true)
	protected List<WSSalesOrderLine> wsSalesOrderLine;

	/**
	 * Gets the value of the wsSalesOrderLine property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the wsSalesOrderLine property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 *
	 * <pre>
	 * getWSSalesOrderLine().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link WSSalesOrderLine }
	 * 
	 * 
	 */
	public List<WSSalesOrderLine> getWSSalesOrderLine()
	{
		if (wsSalesOrderLine == null)
		{
			wsSalesOrderLine = new ArrayList<WSSalesOrderLine>();
		}
		return this.wsSalesOrderLine;
	}

	@Override
	public String toString()
	{
		if (CollectionUtils.isNotEmpty(wsSalesOrderLine))
		{
			String list = "WSSalesOrderLineList [";
			for (final WSSalesOrderLine line : wsSalesOrderLine)
			{
				list = list + "[wsSalesOrderLine=" + line + "]";
			}
			list = list + "]";
			return list;
		}
		return "WSSalesOrderLineList []";
	}
}
