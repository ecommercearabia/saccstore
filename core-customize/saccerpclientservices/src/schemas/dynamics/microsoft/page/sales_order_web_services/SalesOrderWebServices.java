
package schemas.dynamics.microsoft.page.sales_order_web_services;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sun.xml.ws.client.BindingProviderProperties;

import schemas.dynamics.microsoft.page.AbstractSaccERPWebService;


/**
 * <p>
 * Java class for Sales_Order_Web_Services complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Sales_Order_Web_Services">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="External_Document_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sell_to_Customer_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Order_Date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Invoice_Discount_Calculation" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Invoice_Discount_Calculation" minOccurs="0"/>
 *         &lt;element name="Invoice_Discount_Value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Online_Status" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Online_Status" minOccurs="0"/>
 *         &lt;element name="WS_Sales_Order_Line" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}WS_Sales_Order_Line_List" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

/**
 *
 * @author Baha Almtoor
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sales_Order_Web_Services", propOrder =
{ "key", "no", "externalDocumentNo", "sellToCustomerNo", "orderDate", "invoiceDiscountCalculation", "invoiceDiscountValue",
		"onlineStatus", "wsSalesOrderLine", "orderRefId" })
@XmlRootElement
public class SalesOrderWebServices extends AbstractSaccERPWebService
{
	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "No")
	protected String no = "";
	@XmlElement(name = "External_Document_No")
	protected String externalDocumentNo = "";
	@XmlElement(name = "Sell_to_Customer_No")
	protected String sellToCustomerNo = "";
	@XmlElement(name = "Order_Date")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar orderDate;
	@XmlElement(name = "Invoice_Discount_Calculation")
	@XmlSchemaType(name = "string")
	protected InvoiceDiscountCalculation invoiceDiscountCalculation;
	@XmlElement(name = "Invoice_Discount_Value")
	protected String invoiceDiscountValue = "0";
	@XmlElement(name = "Online_Status")
	@XmlSchemaType(name = "string")
	protected OnlineStatus onlineStatus;
	@XmlElement(name = "WS_Sales_Order_Line")
	protected WSSalesOrderLineList wsSalesOrderLine;
	@XmlElement(name = "Order_Ref_ID")
	protected String orderRefId = "";

	private void setTimeoutValue(final SalesOrderWebServicesPort salesOrderWebServicesPort)
	{
		final BindingProvider provider = (BindingProvider) salesOrderWebServicesPort;
		final Map<String, Object> requestContext = provider.getRequestContext();
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getTimeout());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getTimeout());
	}

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the no property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getNo()
	{
		return no;
	}

	/**
	 * Sets the value of the no property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setNo(final String value)
	{
		this.no = value;
	}

	/**
	 * Gets the value of the externalDocumentNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getExternalDocumentNo()
	{
		return externalDocumentNo;
	}

	/**
	 * Sets the value of the externalDocumentNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setExternalDocumentNo(final String value)
	{
		this.externalDocumentNo = value;
	}

	/**
	 * Gets the value of the sellToCustomerNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getSellToCustomerNo()
	{
		return sellToCustomerNo;
	}

	/**
	 * Sets the value of the sellToCustomerNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setSellToCustomerNo(final String value)
	{
		this.sellToCustomerNo = value;
	}

	/**
	 * Gets the value of the orderDate property.
	 *
	 * @return possible object is {@link XMLGregorianCalendar }
	 *
	 */
	public XMLGregorianCalendar getOrderDate()
	{
		return orderDate;
	}

	/**
	 * Sets the value of the orderDate property.
	 *
	 * @param value
	 *           allowed object is {@link XMLGregorianCalendar }
	 *
	 */
	public void setOrderDate(final XMLGregorianCalendar value)
	{
		this.orderDate = value;
	}

	public void setOrderDate(final Date value)
	{
		try
		{
			this.orderDate = convertDateToXMLGregorianCalendar(value);
		}
		catch (final SaccERPWebServiceException e)
		{
			e.printStackTrace();
		}
	}

	protected XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) throws SaccERPWebServiceException
	{

		XMLGregorianCalendar xmlGregorianCalendar = null;
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		try
		{
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
			xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		}
		catch (final DatatypeConfigurationException ex)
		{
			ex.printStackTrace();
		}

		return xmlGregorianCalendar;
	}

	/**
	 * Gets the value of the invoiceDiscountCalculation property.
	 *
	 * @return possible object is {@link InvoiceDiscountCalculation }
	 *
	 */
	public InvoiceDiscountCalculation getInvoiceDiscountCalculation()
	{
		return invoiceDiscountCalculation;
	}

	/**
	 * Sets the value of the invoiceDiscountCalculation property.
	 *
	 * @param value
	 *           allowed object is {@link InvoiceDiscountCalculation }
	 *
	 */
	public void setInvoiceDiscountCalculation(final InvoiceDiscountCalculation value)
	{
		this.invoiceDiscountCalculation = value;
	}

	/**
	 * Gets the value of the invoiceDiscountValue property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getInvoiceDiscountValue()
	{
		return invoiceDiscountValue;
	}

	/**
	 * Sets the value of the invoiceDiscountValue property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setInvoiceDiscountValue(final String value)
	{
		this.invoiceDiscountValue = value;
	}

	/**
	 * Gets the value of the onlineStatus property.
	 *
	 * @return possible object is {@link OnlineStatus }
	 *
	 */
	public OnlineStatus getOnlineStatus()
	{
		return onlineStatus;
	}

	/**
	 * Sets the value of the onlineStatus property.
	 *
	 * @param value
	 *           allowed object is {@link OnlineStatus }
	 *
	 */
	public void setOnlineStatus(final OnlineStatus value)
	{
		this.onlineStatus = value;
	}

	/**
	 * Gets the value of the wsSalesOrderLine property.
	 *
	 * @return possible object is {@link WSSalesOrderLineList }
	 *
	 */
	public WSSalesOrderLineList getWSSalesOrderLine()
	{
		return wsSalesOrderLine;
	}

	/**
	 * Sets the value of the wsSalesOrderLine property.
	 *
	 * @param value
	 *           allowed object is {@link WSSalesOrderLineList }
	 *
	 */
	public void setWSSalesOrderLine(final WSSalesOrderLineList value)
	{
		this.wsSalesOrderLine = value;
	}

	/**
	 * @return the orderRefId
	 */
	public String getOrderRefId()
	{
		return orderRefId;
	}

	/**
	 * @param orderRefId
	 *           the orderRefId to set
	 */
	public void setOrderRefId(final String orderRefId)
	{
		this.orderRefId = orderRefId;
	}

	protected SalesOrderWebServicesPort getSalesOrderWebServicesPort()
	{
		return new SalesOrderWebServicesService(isTestEnv()).getSalesOrderWebServicesPort();
	}

	public List<SalesOrderWebServices> readMultiple(final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey,
			final int size) throws SaccERPWebServiceException
	{
		try
		{
			setAuthenticator();
			final SalesOrderWebServicesList salesOrderWebServicesList = getSalesOrderWebServicesPort().readMultiple(filters,
					bookmarkKey, size);
			if (salesOrderWebServicesList != null)
			{
				return salesOrderWebServicesList.getSalesOrderWebServices();
			}

			return Collections.emptyList();
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't readMultiple[SalesOrderWebServices]", ex,
					SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public SalesOrderWebServices read() throws SaccERPWebServiceException
	{
		try
		{
			final SalesOrderWebServicesPort salesOrderWebServicesPort = getSalesOrderWebServicesPort();
			setAuthenticator();
			setTimeoutValue(salesOrderWebServicesPort);
			return salesOrderWebServicesPort.read(getNo());
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't read[SalesOrderWebServices]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public SalesOrderWebServices update() throws SaccERPWebServiceException
	{
		try
		{
			final SalesOrderWebServicesPort salesOrderWebServicesPort = getSalesOrderWebServicesPort();
			setAuthenticator();
			setTimeoutValue(salesOrderWebServicesPort);
			final Holder<SalesOrderWebServices> salesOrderWebServiceHolder = new Holder<SalesOrderWebServices>(this);
			salesOrderWebServicesPort.update(salesOrderWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't update[SalesOrderWebServices]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public SalesOrderWebServices create() throws SaccERPWebServiceException
	{
		try
		{
			final SalesOrderWebServicesPort salesOrderWebServicesPort = getSalesOrderWebServicesPort();
			setAuthenticator();
			setTimeoutValue(salesOrderWebServicesPort);
			final Holder<SalesOrderWebServices> salesOrderWebServiceHolder = new Holder<SalesOrderWebServices>(this);
			salesOrderWebServicesPort.create(salesOrderWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't create[SalesOrderWebServices]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public String toString()
	{
		return "SalesOrderWebServices [key=" + key + ", no=" + no + ", externalDocumentNo=" + externalDocumentNo
				+ ", sellToCustomerNo=" + sellToCustomerNo + ", orderDate=" + orderDate + ", invoiceDiscountCalculation="
				+ invoiceDiscountCalculation + ", invoiceDiscountValue=" + invoiceDiscountValue + ", onlineStatus=" + onlineStatus
				+ ", wsSalesOrderLine=" + wsSalesOrderLine + ", orderRefId=" + orderRefId + "]";
	}

}
