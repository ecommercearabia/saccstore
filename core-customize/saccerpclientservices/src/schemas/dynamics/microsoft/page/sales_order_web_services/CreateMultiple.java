
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sales_Order_Web_Services_List" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Sales_Order_Web_Services_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "salesOrderWebServicesList"
})
@XmlRootElement(name = "CreateMultiple")
public class CreateMultiple {

    @XmlElement(name = "Sales_Order_Web_Services_List", required = true)
    protected SalesOrderWebServicesList salesOrderWebServicesList;

    /**
     * Gets the value of the salesOrderWebServicesList property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderWebServicesList }
     *     
     */
    public SalesOrderWebServicesList getSalesOrderWebServicesList() {
        return salesOrderWebServicesList;
    }

    /**
     * Sets the value of the salesOrderWebServicesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderWebServicesList }
     *     
     */
    public void setSalesOrderWebServicesList(SalesOrderWebServicesList value) {
        this.salesOrderWebServicesList = value;
    }

}
