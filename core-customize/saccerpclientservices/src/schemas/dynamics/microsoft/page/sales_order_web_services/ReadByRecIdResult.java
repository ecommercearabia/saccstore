
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sales_Order_Web_Services" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Sales_Order_Web_Services" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "salesOrderWebServices"
})
@XmlRootElement(name = "ReadByRecId_Result")
public class ReadByRecIdResult {

    @XmlElement(name = "Sales_Order_Web_Services")
    protected SalesOrderWebServices salesOrderWebServices;

    /**
     * Gets the value of the salesOrderWebServices property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderWebServices }
     *     
     */
    public SalesOrderWebServices getSalesOrderWebServices() {
        return salesOrderWebServices;
    }

    /**
     * Sets the value of the salesOrderWebServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderWebServices }
     *     
     */
    public void setSalesOrderWebServices(SalesOrderWebServices value) {
        this.salesOrderWebServices = value;
    }

}
