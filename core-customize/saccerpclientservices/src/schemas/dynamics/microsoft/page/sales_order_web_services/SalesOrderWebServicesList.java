
package schemas.dynamics.microsoft.page.sales_order_web_services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Sales_Order_Web_Services_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sales_Order_Web_Services_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sales_Order_Web_Services" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Sales_Order_Web_Services" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sales_Order_Web_Services_List", propOrder = {
    "salesOrderWebServices"
})
public class SalesOrderWebServicesList {

    @XmlElement(name = "Sales_Order_Web_Services", required = true)
    protected List<SalesOrderWebServices> salesOrderWebServices;

    /**
     * Gets the value of the salesOrderWebServices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the salesOrderWebServices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSalesOrderWebServices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderWebServices }
     * 
     * 
     */
    public List<SalesOrderWebServices> getSalesOrderWebServices() {
        if (salesOrderWebServices == null) {
            salesOrderWebServices = new ArrayList<SalesOrderWebServices>();
        }
        return this.salesOrderWebServices;
    }

}
