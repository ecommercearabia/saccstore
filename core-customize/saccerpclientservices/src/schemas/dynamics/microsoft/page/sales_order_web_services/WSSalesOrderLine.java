
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for WS_Sales_Order_Line complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="WS_Sales_Order_Line">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Barcode_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Unit_Price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Line_Discount_Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Line_Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Line_Online_Status" type="{urn:microsoft-dynamics-schemas/page/sales_order_web_services}Line_Online_Status" minOccurs="0"/>
 *         &lt;element name="Cancel_Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Picked_Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WS_Sales_Order_Line", propOrder =
{ "key", "barcodeNo", "quantity", "unitPrice", "lineDiscountAmount", "lineAmount", "cancelQuantity", "pickedQuantity" })
public class WSSalesOrderLine
{

	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "Barcode_No")
	protected String barcodeNo = "";
	@XmlElement(name = "Quantity")
	protected String quantity = "0";
	@XmlElement(name = "Unit_Price")
	protected String unitPrice = "0";
	@XmlElement(name = "Line_Discount_Amount")
	protected String lineDiscountAmount = "0";
	@XmlElement(name = "Line_Amount")
	protected String lineAmount = "0";
	@XmlElement(name = "Cancel_Quantity")
	protected String cancelQuantity = "0";
	@XmlElement(name = "Picked_Quantity")
	protected String pickedQuantity = "0";

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the barcodeNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBarcodeNo()
	{
		return barcodeNo;
	}

	/**
	 * Sets the value of the barcodeNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setBarcodeNo(final String value)
	{
		this.barcodeNo = value;
	}

	/**
	 * Gets the value of the quantity property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * Sets the value of the quantity property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setQuantity(final String value)
	{
		this.quantity = value;
	}

	/**
	 * Gets the value of the unitPrice property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * Sets the value of the unitPrice property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setUnitPrice(final String value)
	{
		this.unitPrice = value;
	}

	/**
	 * Gets the value of the lineDiscountAmount property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getLineDiscountAmount()
	{
		return lineDiscountAmount;
	}

	/**
	 * Sets the value of the lineDiscountAmount property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setLineDiscountAmount(final String value)
	{
		this.lineDiscountAmount = value;
	}

	/**
	 * Gets the value of the lineAmount property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getLineAmount()
	{
		return lineAmount;
	}

	/**
	 * Sets the value of the lineAmount property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setLineAmount(final String value)
	{
		this.lineAmount = value;
	}


	/**
	 * Gets the value of the cancelQuantity property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getCancelQuantity()
	{
		return cancelQuantity;
	}

	/**
	 * Sets the value of the cancelQuantity property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setCancelQuantity(final String value)
	{
		this.cancelQuantity = value;
	}

	/**
	 * Gets the value of the pickedQuantity property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getPickedQuantity()
	{
		return pickedQuantity;
	}

	/**
	 * Sets the value of the pickedQuantity property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setPickedQuantity(final String value)
	{
		this.pickedQuantity = value;
	}

	@Override
	public String toString()
	{
		return "WSSalesOrderLine [key=" + key + ", barcodeNo=" + barcodeNo + ", quantity=" + quantity + ", unitPrice=" + unitPrice
				+ ", lineDiscountAmount=" + lineDiscountAmount + ", lineAmount=" + lineAmount + ", cancelQuantity=" + cancelQuantity
				+ ", pickedQuantity=" + pickedQuantity + "]";
	}


}
