
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Delete_WS_Sales_Order_Line_Result" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteWSSalesOrderLineResult"
})
@XmlRootElement(name = "Delete_WS_Sales_Order_Line_Result")
public class DeleteWSSalesOrderLineResult {

    @XmlElement(name = "Delete_WS_Sales_Order_Line_Result")
    protected boolean deleteWSSalesOrderLineResult;

    /**
     * Gets the value of the deleteWSSalesOrderLineResult property.
     * 
     */
    public boolean isDeleteWSSalesOrderLineResult() {
        return deleteWSSalesOrderLineResult;
    }

    /**
     * Sets the value of the deleteWSSalesOrderLineResult property.
     * 
     */
    public void setDeleteWSSalesOrderLineResult(boolean value) {
        this.deleteWSSalesOrderLineResult = value;
    }

}
