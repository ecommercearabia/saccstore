
package schemas.dynamics.microsoft.page.customer_web_service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Customer_Web_Service_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Customer_Web_Service_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Name"/>
 *     &lt;enumeration value="Search_Name"/>
 *     &lt;enumeration value="Name_2"/>
 *     &lt;enumeration value="Address"/>
 *     &lt;enumeration value="Address_2"/>
 *     &lt;enumeration value="City"/>
 *     &lt;enumeration value="Contact"/>
 *     &lt;enumeration value="Phone_No"/>
 *     &lt;enumeration value="E_Mail"/>
 *     &lt;enumeration value="Territory_Code"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Customer_Web_Service_Fields")
@XmlEnum
public enum CustomerWebServiceFields {

    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("Name")
    NAME("Name"),
    @XmlEnumValue("Search_Name")
    SEARCH_NAME("Search_Name"),
    @XmlEnumValue("Name_2")
    NAME_2("Name_2"),
    @XmlEnumValue("Address")
    ADDRESS("Address"),
    @XmlEnumValue("Address_2")
    ADDRESS_2("Address_2"),
    @XmlEnumValue("City")
    CITY("City"),
    @XmlEnumValue("Contact")
    CONTACT("Contact"),
    @XmlEnumValue("Phone_No")
    PHONE_NO("Phone_No"),
    @XmlEnumValue("E_Mail")
    E_MAIL("E_Mail"),
    @XmlEnumValue("Territory_Code")
    TERRITORY_CODE("Territory_Code");
    private final String value;

    CustomerWebServiceFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerWebServiceFields fromValue(String v) {
        for (CustomerWebServiceFields c: CustomerWebServiceFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
