/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.strategy;

import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationStrategy
{
	Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getFormData(Object data, ThirdPartyAuthenticationProviderModel provider, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;
}
