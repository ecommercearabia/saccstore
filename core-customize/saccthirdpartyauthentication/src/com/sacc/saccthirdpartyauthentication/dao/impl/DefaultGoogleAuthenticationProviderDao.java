/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.dao.impl;

import com.sacc.saccthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.sacc.saccthirdpartyauthentication.model.GoogleAuthenticationProviderModel;

/**
 *
 */
public class DefaultGoogleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultGoogleAuthenticationProviderDao()
	{
		super(GoogleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return GoogleAuthenticationProviderModel._TYPECODE;
	}


}
