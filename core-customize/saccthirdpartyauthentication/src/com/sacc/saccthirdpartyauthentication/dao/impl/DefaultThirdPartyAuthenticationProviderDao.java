/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.dao.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.site.BaseSiteService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public abstract class DefaultThirdPartyAuthenticationProviderDao extends DefaultGenericDao<ThirdPartyAuthenticationProviderModel>
		implements ThirdPartyAuthenticationProviderDao
{
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";
	private static final String SITES = "sites";
	private static final String CODE = "code";
	private static final String ACTIVE = "active";

	/**
	 *
	 */
	public DefaultThirdPartyAuthenticationProviderDao(final String typecode)
	{
		super(typecode);
	}

	protected abstract String getModelName();

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<ThirdPartyAuthenticationProviderModel> find = find(params);
		final Optional<ThirdPartyAuthenticationProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(final String cmsSiteUid)
	{
		return getActive((CMSSiteModel) baseSiteService.getBaseSiteForUID(cmsSiteUid));
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, "cmsSiteModel must not be null");
		final Optional<Collection<ThirdPartyAuthenticationProviderModel>> find = find(Arrays.asList(cmsSiteModel), Boolean.TRUE, 1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveByCurrentSite()
	{
		return getActive(cmsSiteService.getCurrentSite());
	}



	protected Optional<Collection<ThirdPartyAuthenticationProviderModel>> find(final List<CMSSiteModel> sites,
			final Boolean active, final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("SELECT {thap.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS thap ");


		if (CollectionUtils.isNotEmpty(sites))
		{
			query.append("JOIN ThirdPartyAuthProvidForCMSSite AS bsfpRel ON {bsfpRel.target}={thap.PK} ");
			query.append("JOIN CMSSite AS bs ON {bsfpRel.source}={bs.PK} AND {bs.PK} IN (?sites) ");
			params.put(SITES, sites);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {thap.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {thap.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<ThirdPartyAuthenticationProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

}
