/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.dao.impl;

import com.sacc.saccthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.sacc.saccthirdpartyauthentication.model.FacebookAuthenticationProviderModel;

/**
 *
 */
public class DefaultFacebookAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	 *
	 */
	public DefaultFacebookAuthenticationProviderDao()
	{
		super(FacebookAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return FacebookAuthenticationProviderModel._TYPECODE;
	}

}
