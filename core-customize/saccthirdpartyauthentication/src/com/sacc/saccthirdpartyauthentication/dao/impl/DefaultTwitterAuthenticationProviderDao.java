/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.dao.impl;

import com.sacc.saccthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.sacc.saccthirdpartyauthentication.model.TwitterAuthenticationProviderModel;


/**
 *
 */
public class DefaultTwitterAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultTwitterAuthenticationProviderDao()
	{
		super(TwitterAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return TwitterAuthenticationProviderModel._TYPECODE;
	}


}
