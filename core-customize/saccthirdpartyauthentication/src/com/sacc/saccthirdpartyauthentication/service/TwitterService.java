package com.sacc.saccthirdpartyauthentication.service;

import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;


public interface TwitterService
{
	public Optional<ThirdPartyAuthenticationUserData> getData(String verifier, String consumerKey, String consumerSecret,
			String accessToken, String tokenSecret) throws ThirdPartyAuthenticationException;

	/**
	 * @throws ThirdPartyAuthenticationException
	 *
	 */
	public Optional<TwitterFormData> getTwitterFormData(String consumerKey, String consumerSecret, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	public boolean verifyThirdPartyAccessToken(String consumerKey, String consumerKeySecret, String id, String token)
			throws ThirdPartyAuthenticationException;

}
