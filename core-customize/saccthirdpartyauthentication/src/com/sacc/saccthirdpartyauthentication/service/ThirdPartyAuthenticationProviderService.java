package com.sacc.saccthirdpartyauthentication.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;



public interface ThirdPartyAuthenticationProviderService
{

	public Optional<ThirdPartyAuthenticationProviderModel> get(String code, final Class<?> providerClass);

	public Optional<ThirdPartyAuthenticationProviderModel> getActive(String cmsSiteUid, final Class<?> providerClass);

	public Optional<ThirdPartyAuthenticationProviderModel> getActive(CMSSiteModel cmsSiteModel, final Class<?> providerClass);

	public Optional<ThirdPartyAuthenticationProviderModel> getActiveByCurrentSite(final Class<?> providerClass);
}