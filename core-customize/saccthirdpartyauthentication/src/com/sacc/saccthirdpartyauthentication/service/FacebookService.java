package com.sacc.saccthirdpartyauthentication.service;

import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;


public interface FacebookService
{
	public Optional<ThirdPartyAuthenticationUserData> getData(String token, String appSecret)
			throws ThirdPartyAuthenticationException;

	public boolean verifyThirdPartyAccessToken(String id, String token, String appSecret)
			throws ThirdPartyAuthenticationException;
}
