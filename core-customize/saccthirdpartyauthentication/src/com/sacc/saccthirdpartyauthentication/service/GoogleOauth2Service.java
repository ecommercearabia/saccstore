package com.sacc.saccthirdpartyauthentication.service;

import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;


public interface GoogleOauth2Service
{

	public Optional<ThirdPartyAuthenticationUserData> getData(String code, String clientId, String clientSecret)
			throws ThirdPartyAuthenticationException;

	public boolean verifyThirdPartyAccessToken(String id, String appId, String appSecret)
			throws ThirdPartyAuthenticationException;
}
