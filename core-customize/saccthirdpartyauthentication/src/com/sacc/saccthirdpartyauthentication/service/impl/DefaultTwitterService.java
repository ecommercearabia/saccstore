/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.service.impl;

import java.util.Optional;

import org.springframework.util.StringUtils;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.service.TwitterService;

import twitter4j.HttpClient;
import twitter4j.HttpClientFactory;
import twitter4j.HttpResponse;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.OAuth2Token;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;


/**
 *
 */
public class DefaultTwitterService implements TwitterService
{
	@Override
	public Optional<TwitterFormData> getTwitterFormData(final String consumerKey, final String consumerSecret,
			final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		final ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setIncludeEmailEnabled(true);
		cb.setDebugEnabled(true).setOAuthConsumerKey(consumerKey).setOAuthConsumerSecret(consumerSecret);
		final TwitterFactory tf = new TwitterFactory(cb.build());
		final Twitter twitter = tf.getInstance();


		RequestToken requestToken;
		try
		{
			requestToken = twitter.getOAuthRequestToken(callbackUrl);
		}
		catch (final TwitterException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED, e.getErrorMessage());
		}
		final String authenticationURL = requestToken.getAuthenticationURL();
		final TwitterFormData twitterFormData = new TwitterFormData(authenticationURL, requestToken.getToken(),
				requestToken.getTokenSecret());


		return Optional.ofNullable(twitterFormData);
	}



	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String verifier, final String consumerKey,
			final String consumerSecret, final String oauthToken, final String oauthTokenSecret)
			throws ThirdPartyAuthenticationException
	{
		final ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setIncludeEmailEnabled(true);
		cb.setDebugEnabled(true).setOAuthConsumerKey(consumerKey).setOAuthConsumerSecret(consumerSecret);
		final TwitterFactory tf = new TwitterFactory(cb.build());
		final Twitter twitter = tf.getInstance();

		final RequestToken requestToken = new RequestToken(oauthToken, oauthTokenSecret);

		if (twitter == null || verifier == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}


		try
		{
			twitter.getOAuthAccessToken(requestToken, verifier);
		}
		catch (final TwitterException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED, e.getErrorMessage());
		}

		User user = null;
		try
		{
			user = twitter.verifyCredentials();
		}
		catch (final TwitterException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED, e.getErrorMessage());
		}

		if (user == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED, "User Is null");
		}

		final ThirdPartyAuthenticationUserData userData = new ThirdPartyAuthenticationUserData();
		userData.setId(String.valueOf(user.getId()));
		userData.setName(user.getName());
		userData.setEmail(user.getEmail());

		return Optional.ofNullable(userData);
	}



	@Override
	public boolean verifyThirdPartyAccessToken(final String consumerKey, final String consumerKeySecret, final String id,
			final String token) throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(consumerKey) || StringUtils.isEmpty(consumerKeySecret) || StringUtils.isEmpty(id)
				|| StringUtils.isEmpty(token))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final ConfigurationBuilder builder = new ConfigurationBuilder();

		builder.setApplicationOnlyAuthEnabled(true);

		final Twitter twitter = new TwitterFactory(builder.build()).getInstance();

		twitter.setOAuthConsumer(consumerKey, consumerKeySecret);

		try
		{
			final OAuth2Token oAuth2Token = twitter.getOAuth2Token();
			final boolean isBearerTokenAuthenticated = oAuth2Token.getAccessToken().equals(token);
			final HttpClient client = HttpClientFactory.getInstance();
			final HttpResponse httpResponse = client.get("https://api.twitter.com/2/users/" + id, null, twitter.getAuthorization(),
					null);
			final String dataId = httpResponse.asJSONObject().getJSONObject("data").getString("id");
			return isBearerTokenAuthenticated && dataId.equals(id);
		}
		catch (final TwitterException e)
		{
			return false;
		}
	}

}
