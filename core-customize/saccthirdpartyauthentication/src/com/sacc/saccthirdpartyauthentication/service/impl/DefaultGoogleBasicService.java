
package com.sacc.saccthirdpartyauthentication.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.service.GoogleBasicService;


public class DefaultGoogleBasicService implements GoogleBasicService
{


	private static final Logger LOG = Logger.getLogger(DefaultGoogleBasicService.class);

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String token, final String clientId)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("GoogleBasicUserServiceImpl getData()");
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(clientId))
		{
			LOG.error("token or clientId is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(),
				JacksonFactory.getDefaultInstance()).setAudience(Collections.singletonList(clientId)).build();

		GoogleIdToken idToken = null;
		try
		{
			idToken = verifier.verify(token);
		}
		catch (GeneralSecurityException | IOException e)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}
		if (idToken == null)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final Payload payload = idToken.getPayload();

		final String userId = payload.getSubject();
		final String email = payload.getEmail();
		final String name = (String) payload.get("name");
		final String familyName = (String) payload.get("family_name");
		final String givenName = (String) payload.get("given_name");

		final ThirdPartyAuthenticationUserData user = new ThirdPartyAuthenticationUserData();
		user.setId(userId);
		user.setEmail(email);
		user.setName(name);
		user.setLastName(familyName);
		user.setFirstName(givenName);

		return Optional.ofNullable(user);

	}

	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(token, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}
