/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.service.impl;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;

import com.google.api.client.util.Preconditions;
import com.sacc.saccthirdpartyauthentication.service.ThirdPartyUserService;
import com.sacc.saccuserfacades.customer.facade.CustomCustomerFacade;


/**
 *
 */
public class DefaultThirdPartyUserService implements ThirdPartyUserService
{

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "titleConverter")
	private Converter<TitleModel, TitleData> titleConverter;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Override
	public Boolean isUserExist(final String cutomerId)
	{
		Preconditions.checkArgument(cutomerId != null || cutomerId.isBlank(), "User is null");
		return customCustomerFacade.isCustomerExists(cutomerId);
	}

	@Override
	public void saveUser(final CustomerData user)
	{
		Preconditions.checkArgument(user != null, "User is null");
		Preconditions.checkArgument(user.getCustomerId() != null, "User ID Is null");
		Preconditions.checkArgument(user.getUid() != null, "User email is null");
		Preconditions.checkArgument(user.getMobileCountry() != null, "User email is null");
		Preconditions.checkArgument(user.getMobileNumber() != null, "User email is null");
		Preconditions.checkArgument(user.getFirstName() != null, "User email is null");
		Preconditions.checkArgument(user.getLastName() != null, "User email is null");

		final CustomerModel model = modelService.create(CustomerModel.class);

		final TitleModel title = userService.getTitleForCode(user.getTitleCode());
		model.setTitle(title);
		model.setName(user.getName());
		final CountryModel country = commonI18NService.getCountry(user.getMobileCountry().getIsocode());
		model.setMobileCountry(country);
		model.setMobileNumber(user.getMobileNumber());
		model.setUid(user.getUid());
		model.setCustomerID(user.getCustomerId());
		model.setType(CustomerType.THIRD_PARTY);
		model.setThirdPartyType(user.getThirdPartyType());
		modelService.save(model);
		modelService.refresh(model);

		eventService.publishEvent(initializeEvent(new RegisterEvent(), model));
	}

	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event, final CustomerModel customerModel)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
