/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.List;
import java.util.Optional;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;


/**
 *
 */
public interface ThirdPartyAuthenticationContext
{
	List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderData(CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

	List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderDataByCurrentStore()
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserDataByCurrentStore(Object data, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getThirdPartyFormData(Object data, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getThirdPartyFormDataByCurrentStore(Object data, ThirdPartyAuthenticationType type,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdPartyByCurrentStore(Object data, Object token, ThirdPartyAuthenticationType type,
			String callbackUrl)
			throws ThirdPartyAuthenticationException;


}
