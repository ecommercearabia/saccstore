/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccconsignmenttrackingocccustomaddon.controllers;

/**
 */
public interface SaccconsignmenttrackingocccustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
