/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproduct.constants;

/**
 * Global class for all Saccproduct constants. You can add global constants for your extension into this class.
 */
public final class SaccproductConstants extends GeneratedSaccproductConstants
{
	public static final String EXTENSIONNAME = "saccproduct";

	private SaccproductConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccproductPlatformLogo";
}
