/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccfulfillmentbackoffice.services;

/**
 * Hello World SaccfulfillmentbackofficeService
 */
public class SaccfulfillmentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
