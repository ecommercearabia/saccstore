package com.sacc.saccfulfillmentbackoffice.util;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.process.BusinessProcessException;
import de.hybris.platform.workflow.exceptions.WorkflowActionDecideException;

import javax.annotation.Resource;

import org.zkoss.zul.Messagebox;

import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;


/**
 * @author amjad.shati@erabia.com
 *
 */
public abstract class AbstractConsignmentWorkflow
{
	protected static final int RETRIES = 500;
	@Resource
	private ModelService modelService;
	@Resource
	private NotificationService notificationService;

	@Resource(name = "eventService")
	private EventService eventService;

	protected ActionResult<ConsignmentModel> getConsignmentActionResult(final ActionContext<ConsignmentModel> actionContext,
			final String successMessage, final String failedMessage, final ConsignmentStatus expectedStatus)
	{
		String result;
		final ConsignmentModel consignment = actionContext.getData();
		try
		{
			// Needed some time before continue.
			for (int count = 0; !expectedStatus.equals(getUpdatedConsignmentStatus(consignment)) && count < 500; ++count)
			{
			}
			getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
			{ actionContext.getLabel(successMessage) });
			result = ActionResult.SUCCESS;
		}
		catch (BusinessProcessException | WorkflowActionDecideException throwable)
		{
			getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
			{ actionContext.getLabel(failedMessage) });
			result = ActionResult.ERROR;
		}
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(result);
		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		return actionResult;
	}

	protected ActionResult<ConsignmentModel> getConsignmentActionNotificationWithResult(
			final ActionResult<ConsignmentModel> actionResult, final ActionContext<ConsignmentModel> actionContext,
			final String successMessage, final String failedMessage, final ConsignmentStatus expectedStatus, final Throwable throwz)
	{

		final String resultCode = actionResult.getResultCode();

		final String result;
		switch (resultCode)
		{
			case ActionResult.ERROR:
				String message;
				if (throwz instanceof SaccERPWebServiceException)
				{
					message = throwz.getCause() == null ? throwz.getMessage() : throwz.getCause().getMessage();
				}
				else if (throwz instanceof SaccERPWebServiceException)
				{
					message = throwz.getMessage();
				}
				else
				{
					message = actionContext.getLabel(failedMessage);
				}
				this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
				{ actionContext.getLabel(failedMessage) });
				Messagebox.show(message, actionContext.getLabel(failedMessage), Messagebox.OK, Messagebox.ERROR, 0, null);
				break;

			case ActionResult.SUCCESS:
				this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
				{ actionContext.getLabel(successMessage) });
				Messagebox.show(actionContext.getLabel(successMessage), "Info", Messagebox.OK, Messagebox.INFORMATION, 0, null);
				break;
		}

		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		return actionResult;
	}

	protected ConsignmentStatus getUpdatedConsignmentStatus(final ConsignmentModel consignmentModel)
	{
		return ((ConsignmentModel) getModelService().get(consignmentModel.getPk())).getStatus();
	}

	protected boolean isFulfillmentExternal(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getFulfillmentSystemConfig() != null;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected NotificationService getNotificationService()
	{
		return notificationService;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}
}
