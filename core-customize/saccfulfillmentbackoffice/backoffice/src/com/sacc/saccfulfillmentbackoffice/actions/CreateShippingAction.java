package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.google.common.base.Preconditions;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.sacc.Ordermanagement.model.SendingOrderTrackingIdProcessModel;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.core.service.BarcodeGenaratorService;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public class CreateShippingAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateShippingAction.class);
	private static final String TRACKING_ID_NOT_FOUND = "Tracking id not found";
	private static final String ERROR_GENERATING_BARCODE = "Error generating barcode";
	private static final String ERROR_SAVING_CONSIGNMENT = "Error saving consignment";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";
	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;
	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "businessProcessService")
	/** The business process service. */
	private BusinessProcessService businessProcessService;
	@Resource(name = "processCodeGenerator")
	/** The process code generator. */
	private KeyGenerator processCodeGenerator;

	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();
		try
		{
			final Optional<String> trackingId = getFulfillmentContext().createShipment(consignment);

			final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
			statusFlags.add(StatusFlag.OBJECT_MODIFIED);
			actionResult.setStatusFlags(statusFlags);
			if (trackingId.isPresent())
			{
				Messagebox.show(String.format("Tracking ID : %s", trackingId.get()));
				final SendingOrderTrackingIdProcessModel sendingOrderTrackingIdProcessModel = (SendingOrderTrackingIdProcessModel) getBusinessProcessService()
						.createProcess("sendingOrderTrackingIdEmail-process-" + consignment.getCode() + "-"
								+ processCodeGenerator.generate().toString(), "sendingOrderTrackingIdEmail-process");
				sendingOrderTrackingIdProcessModel.setConsignment(consignment);
				getModelService().save(sendingOrderTrackingIdProcessModel);
				getBusinessProcessService().startProcess(sendingOrderTrackingIdProcessModel);

				modelService.refresh(consignment);
				try
				{
					barcodeGenaratorService.generateBarcodeAsMedia(consignment);
				}
				catch (final IOException e)
				{
					LOG.error(e.getMessage());
					Messagebox.show(ERROR_GENERATING_BARCODE);
				}
			}
			else
			{
				Messagebox.show(TRACKING_ID_NOT_FOUND);
			}
		}
		catch (final com.sacc.saccfulfillment.exception.FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(consignment.getOrder().getStore()),
					consignment.getOrder(), ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage()));
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{

		final ConsignmentModel consignment = ctx.getData();
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		final boolean enable = getEnableProperty(consignment);

		return enable && (consignment != null);

	}

	/**
	 * @param consignment
	 * @return
	 */
	private boolean getEnableProperty(final ConsignmentModel consignment)
	{
		final String property = "backoffice.createshipment.enable." + consignment.getOrder().getStore().getUid();
		return configurationService.getConfiguration().getBoolean(property, false);
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		if (StringUtils.isNotBlank(ctx.getData().getTrackingID()))
		{
			return "Do you want to create new shipment?";
		}
		else
		{
			return "Do you want to create shipment?";
		}
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}


	/**
	 * @return the processCodeGenerator
	 */
	protected KeyGenerator getProcessCodeGenerator()
	{
		return processCodeGenerator;
	}

	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SMSAFULFILLMENTPROVIDER":
				return IntegrationProvider.SMSA;
			case "SAEEFULFILLMENTPROVIDER":
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
