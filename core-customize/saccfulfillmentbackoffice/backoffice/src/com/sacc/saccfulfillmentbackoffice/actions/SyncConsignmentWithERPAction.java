/**
 *
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.event.EventService;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.sacc.core.event.SyncERPConsignmentEvent;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;


/**
 * @author monzer
 *
 */
public class SyncConsignmentWithERPAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		if (ctx == null || ctx.getData() == null)
		{
			return false;
		}
		final ConsignmentModel consignment = ctx.getData();
		if (CollectionUtils.isEmpty(consignment.getErpConsignmentProcesses()))
		{
			return false;
		}
		final ERPConsignmentProcessModel erpConsignmentProcessModel = consignment.getErpConsignmentProcesses().get(0);
		return erpConsignmentProcessModel != null && !ProcessState.RUNNING.equals(erpConsignmentProcessModel.getState());
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		final ERPConsignmentProcessModel erpConsignmentProcessModel = consignment.getErpConsignmentProcesses().get(0);

		if (ProcessState.RUNNING.equals(erpConsignmentProcessModel.getState()))
		{
			Messagebox.show("Cannot restart a running process, please wait util the process is finished", "Information",
					Messagebox.OK, Messagebox.INFORMATION);
			return new ActionResult(ActionResult.ERROR);
		}
		eventService.publishEvent(new SyncERPConsignmentEvent(consignment, consignment.getStatus()));
		Messagebox.show("Sync ERP Consignment Process is in progress, please wait..", "Information", Messagebox.OK,
				Messagebox.INFORMATION);
		return new ActionResult(ActionResult.SUCCESS);
	}

}
