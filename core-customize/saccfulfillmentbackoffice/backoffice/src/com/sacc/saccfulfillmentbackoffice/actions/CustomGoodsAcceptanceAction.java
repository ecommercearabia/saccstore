/**
 *
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.returns.OrderReturnException;
import de.hybris.platform.returns.ReturnActionResponse;
import de.hybris.platform.returns.ReturnCallbackService;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.sacc.saccerpclientservices.service.SaccERPService;


/**
 * @author mohammad-abumuhasien
 *
 */
public class CustomGoodsAcceptanceAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ReturnRequestModel, ReturnRequestModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomGoodsAcceptanceAction.class);

	protected static final String ACCEPT_GOODS_SUCCESS = "warehousingbackoffice.returnrequest.accept.goods.success";
	protected static final String ACCEPT_GOODS_FAILURE = "warehousingbackoffice.returnrequest.accept.goods.failure";
	protected static final String ACCEPT_GOODS_MODIFIED_FAILURE = "warehousingbackoffice.returnrequest.accept.goods.modified.failure";
	@Resource
	private ModelService modelService;
	@Resource
	private ReturnCallbackService returnCallbackService;
	@Resource
	private NotificationService notificationService;
	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	public boolean canPerform(final ActionContext<ReturnRequestModel> actionContext)
	{
		final Object data = actionContext.getData();
		ReturnRequestModel returnRequest = null;
		boolean decision = false;
		if (data instanceof ReturnRequestModel)
		{
			returnRequest = (ReturnRequestModel) data;
			if ((returnRequest.getReturnEntries() != null || !CollectionUtils.isEmpty(returnRequest.getReturnEntries()))
					&& returnRequest.getStatus().equals(ReturnStatus.WAIT) && !isReturnProcessRunning(returnRequest))
			{
				decision = true;
			}
		}
		return decision;
	}

	/**
	 * @param returnRequest
	 * @return
	 */
	private boolean isReturnProcessRunning(final ReturnRequestModel returnRequest)
	{
		for (final ReturnProcessModel returnProcess : returnRequest.getReturnProcess())
		{
			if (ProcessState.RUNNING.equals(returnProcess.getProcessState()))
			{
				return true;
			}
		}
		return false;
	}

	public String getConfirmationMessage(final ActionContext<ReturnRequestModel> actionContext)
	{
		return null;
	}

	public boolean needsConfirmation(final ActionContext<ReturnRequestModel> actionContext)
	{
		return false;
	}

	public ActionResult<ReturnRequestModel> perform(final ActionContext<ReturnRequestModel> actionContext)
	{
		final ReturnRequestModel returnRequest = actionContext.getData();
		this.getModelService().refresh(returnRequest);
		ActionResult<ReturnRequestModel> actionResult;
		if (this.canPerform(actionContext))
		{
			final ReturnActionResponse returnActionResponse = new ReturnActionResponse(returnRequest);
			try
			{
				this.getReturnCallbackService().onReturnReceptionResponse(returnActionResponse);

				this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
				{ actionContext.getLabel("warehousingbackoffice.returnrequest.accept.goods.success") });
				actionResult = new ActionResult("success");

			}
			catch (final OrderReturnException ex)
			{
				this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
				{ actionContext.getLabel("warehousingbackoffice.returnrequest.accept.goods.failure") });
				actionResult = new ActionResult("error");
				Messagebox.show(ex.getCause().getMessage(),
						actionContext.getLabel("warehousingbackoffice.returnrequest.accept.goods.failure"), Messagebox.OK,
						Messagebox.ERROR, 0, null);
			}
		}
		else
		{
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
			{ actionContext.getLabel("warehousingbackoffice.returnrequest.accept.goods.modified.failure") });
			actionResult = new ActionResult("error");
			Messagebox.show("Error", actionContext.getLabel("warehousingbackoffice.returnrequest.accept.goods.failure"),
					Messagebox.OK, Messagebox.ERROR, 0, null);
		}
		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		return actionResult;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	protected ReturnCallbackService getReturnCallbackService()
	{
		return this.returnCallbackService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}

}
