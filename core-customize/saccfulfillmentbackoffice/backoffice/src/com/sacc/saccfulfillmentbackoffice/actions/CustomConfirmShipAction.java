/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.shipping.service.WarehousingShippingService;

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zul.Messagebox;
import org.springframework.util.CollectionUtils;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;
import com.sacc.saccfulfillmentbackoffice.util.AbstractConsignmentWorkflow;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;


public class CustomConfirmShipAction extends AbstractConsignmentWorkflow
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	@Resource
	private WarehousingShippingService warehousingShippingService;

	private static final String CONFIRMATION_MESSAGE = "hmc.action.confirmship.confirmation.message";
	private static final String CONFIRM_SHIP_EVENT = "saccbackoffice.confirmship.event";

	public static final String SUCCESS_MESSAGE = "ship.action.message.success";
	public static final String FAILED_MESSAGE = "ship.action.message.failed";

	private static final Logger LOG = Logger.getLogger(CustomConfirmShipAction.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "notificationService")
	private NotificationService notificationService;

	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService itemWebService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;


	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final Object data = actionContext.getData();

		ConsignmentModel consignment = null;
		if (data instanceof ConsignmentModel)
		{
			consignment = (ConsignmentModel) data;
		}

		return !Objects.isNull(consignment) && !Objects.isNull(consignment.getOrder())
				&& !(consignment.getDeliveryMode() instanceof PickUpDeliveryModeModel) && !this.isFulfillmentExternal(consignment)
				&& this.getWarehousingShippingService().isConsignmentConfirmable(consignment)
				&& !((OrderModel) consignment.getOrder()).isSyncPartialCancelFailed() && consignment.getCarrierDetails() != null
				&& StringUtils.isNotBlank(consignment.getTrackingID()) && consignment.isPacked()
				&& checkERPConsignmentProcess(consignment);
	}

	private boolean checkERPConsignmentProcess(final ConsignmentModel consignment)
	{
		if (CollectionUtils.isEmpty(consignment.getErpConsignmentProcesses()))
		{
			return true;
		}
		final ERPConsignmentProcessModel erpConsignmentProcessModel = consignment.getErpConsignmentProcesses().get(0);
		if (ProcessState.RUNNING.equals(consignment.getErpConsignmentProcesses().get(0).getState()))
		{
			return false;
		}
		return true;
	}

	protected boolean checkRequiredStatusesFoorConfirmShipment(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			return false;
		}
		if (consignment.getStatus() == null)
		{
			return false;
		}
		final ConsignmentStatus status = consignment.getStatus();
		return ConsignmentStatus.READY_FOR_PICKUP.equals(status) || ConsignmentStatus.READY_FOR_SHIPPING.equals(status);
	}

	public String getConfirmationMessage(final ActionContext<ConsignmentModel> actionContext)
	{
		return null;
	}

	public boolean needsConfirmation(final ActionContext<ConsignmentModel> actionContext)
	{
		return false;
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignmentModel = actionContext.getData();
		for (final AbstractOrderEntryModel entry : consignmentModel.getOrder().getEntries())
		{
			List<ItemWebService> read = null;
			try
			{
				read = itemWebService.readMultiple(1, entry.getProduct().getCode());
				if (read == null || read.isEmpty())
				{
					throw new SaccERPWebServiceException("Could not read a product with code " + entry.getProduct().getCode(),
							SaccERPExeptionType.INVALID_PRODUCT);
				}
				if (Long.valueOf(read.get(0).getActualInventory()) < entry.getQuantity())
				{
					throw new SaccERPWebServiceException(
							"Insufficient stock level on ERP system for product " + entry.getProduct().getCode(),
							SaccERPExeptionType.INSUFFICIENT_INVENTORY);
				}
				LOG.info("Found item with No : " + read.get(0).getNo() + " and with stock levelof : " + read.get(0).getInventory());
			}
			catch (final SaccERPWebServiceException e)
			{
				String errorMsg = e.getMessage();
				if (e.getCause() != null)
				{
					errorMsg = e.getMessage() + " caused by: " + e.getCause().getMessage();
				}
				LOG.error(errorMsg, e);
				Messagebox.show(errorMsg, "Could not confirm the shipment", Messagebox.OK, Messagebox.ERROR, 0, null);
				return new ActionResult<ConsignmentModel>(ActionResult.ERROR, consignmentModel);
			}
		}

		getWarehousingShippingService().confirmShipConsignment(consignmentModel);
		return getConsignmentActionNotificationWithResult(new ActionResult(ActionResult.SUCCESS), actionContext, SUCCESS_MESSAGE,
				FAILED_MESSAGE, ConsignmentStatus.SHIPPED, null);

	}

	protected WarehousingShippingService getWarehousingShippingService()
	{
		return this.warehousingShippingService;
	}

	@Override
	protected boolean isFulfillmentExternal(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getFulfillmentSystemConfig() != null;
	}


}
