package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.warehousingbackoffice.labels.strategy.ConsignmentPrintDocumentStrategy;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.util.CollectionUtils;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;


public class CustomPrintPickListAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG;
	protected static final String PICKING_TEMPLATE_CODE = "NPR_Picking";
	@Resource
	private ConsignmentPrintDocumentStrategy consignmentPrintPickSlipStrategy;
	@Resource
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;

	@Resource(name = "eventService")
	private EventService eventService;

	static
	{
		LOG = LoggerFactory.getLogger(CustomPrintPickListAction.class);
	}

	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ActionResult<ConsignmentModel> result = new ActionResult("success");
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		final WorkflowActionModel pickWorkflowAction = getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(PICKING_TEMPLATE_CODE, consignment);
		if (pickWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals(pickWorkflowAction.getStatus()))
		{
			getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, PICKING_TEMPLATE_CODE, "pickConsignment");
		}
		CustomPrintPickListAction.LOG.info("Generating Pick Label for consignment {}", consignment.getCode());
		getConsignmentPrintPickSlipStrategy().printDocument(consignment);
		return result;
	}

	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		return !Objects.isNull(consignment) && !Objects.isNull(consignment.getOrder())
				&& consignment.getFulfillmentSystemConfig() == null
				&& !((OrderModel) consignment.getOrder()).isSyncPartialCancelFailed() && checkERPConsignmentProcess(consignment);
	}

	private boolean checkERPConsignmentProcess(final ConsignmentModel consignment)
	{
		if (CollectionUtils.isEmpty(consignment.getErpConsignmentProcesses()))
		{
			return true;
		}
		final ERPConsignmentProcessModel erpConsignmentProcessModel = consignment.getErpConsignmentProcesses().get(0);
		if (ProcessState.RUNNING.equals(consignment.getErpConsignmentProcesses().get(0).getState()))
		{
			return false;
		}
		return true;
	}

	public boolean needsConfirmation(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return false;
	}

	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return null;
	}

	protected ConsignmentPrintDocumentStrategy getConsignmentPrintPickSlipStrategy()
	{
		return consignmentPrintPickSlipStrategy;
	}

	protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}
}
