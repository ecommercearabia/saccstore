/**
 *
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import org.zkoss.zul.Messagebox;

import com.google.common.base.Preconditions;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;


/**
 * @author monzer
 *
 */
public class CheckShippingStatusAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	protected static final Logger LOG = Logger.getLogger(CheckShippingStatusAction.class);
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";
	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		if (consignment != null)
		{
			final ConsignmentStatus status = checkAndChangeConsignmentStatus(consignment);
			if (status == null)
			{
				Messagebox.show("Could not be updated", "Error", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				Messagebox.show("Updated with status : " + status.getCode());
			}
		}

		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
		statusFlags.add(StatusFlag.OBJECT_MODIFIED);
		actionResult.setStatusFlags(statusFlags);
		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		if (ctx == null)
		{
			return false;
		}
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null)
		{
			return false;
		}
		if (consignment.getCarrierDetails() == null)
		{
			return false;
		}
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore(), consignment.getCarrierDetails().getFulfillmentProviderType());

		return provider.isPresent() && provider.get().getActive()
				&& org.apache.commons.lang3.StringUtils.isNotBlank(consignment.getTrackingID());
	}

	private ConsignmentStatus checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
	{
		try
		{
			final FulfillmentProviderType type = consignment.getCarrierDetails().getFulfillmentProviderType();
			if (type == null)
			{
				LOG.error("Empty type for consignment: " + consignment.getCode());
				return null;
			}
			final Optional<ConsignmentStatus> optionalStatus = fulfillmentContext.updateStatus(consignment, type);
			if (optionalStatus.isEmpty())
			{
				LOG.error("Empty status for consignment: " + consignment.getCode());
				return null;
			}
			if (ConsignmentStatus.DELIVERY_COMPLETED.equals(optionalStatus.get()))
			{
				saccERPService.updateSaleOrderDeliveredStatus(consignment);
			}
		}
		catch (final FulfillmentException e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(consignment.getOrder().getStore()),
					consignment.getOrder(), e.getCause() != null ? e.getCause().getMessage() : e.getMessage()));
			Messagebox.show("Could not be updated, check the shipment details on consignment (Tacking #, Carrier)", "Error",
					Messagebox.OK, Messagebox.ERROR);
			return consignment.getStatus();
		}
		return consignment.getStatus();
	}

	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SMSAFULFILLMENTPROVIDER":
				return IntegrationProvider.SMSA;
			case "SAEEFULFILLMENTPROVIDER":
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}
}
