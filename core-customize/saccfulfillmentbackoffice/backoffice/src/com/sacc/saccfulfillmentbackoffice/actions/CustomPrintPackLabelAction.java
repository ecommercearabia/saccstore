/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.hybris.cockpitng.actions.ActionContext
 *  com.hybris.cockpitng.actions.ActionResult
 *  com.hybris.cockpitng.actions.CockpitAction
 *  de.hybris.platform.basecommerce.enums.ConsignmentStatus
 *  de.hybris.platform.ordersplitting.model.ConsignmentModel
 *  de.hybris.platform.servicelayer.config.ConfigurationService
 *  de.hybris.platform.warehousing.process.BusinessProcessException
 *  de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService
 *  de.hybris.platform.workflow.enums.WorkflowActionStatus
 *  de.hybris.platform.workflow.model.WorkflowActionModel
 *  javax.annotation.Resource
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.process.BusinessProcessException;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.warehousingbackoffice.labels.strategy.ConsignmentPrintDocumentStrategy;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.sacc.core.context.SerialNumberConfigurationContext;
import com.sacc.core.enums.SerialNumberSource;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;


public class CustomPrintPackLabelAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomPrintPackLabelAction.class);
	protected static final String PACK_CONSIGNMENT_CHOICE = "packConsignment";
	protected static final String PACKING_TEMPLATE_CODE = "NPR_Packing";
	protected static final String CAPTURE_PAYMENT_ON_CONSIGNMENT = "warehousing.capturepaymentonconsignment";
	@Resource
	private ConsignmentPrintDocumentStrategy consignmentPrintPackSlipStrategy;
	@Resource
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;
	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;

	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		LOG.info("Generating Pack Label for consignment {}", consignment.getCode());
		final WorkflowActionModel packWorkflowAction = this.getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(PACKING_TEMPLATE_CODE, consignment);
		if (packWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals(packWorkflowAction.getStatus()))
		{
			try
			{
				this.getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, PACKING_TEMPLATE_CODE,
						PACK_CONSIGNMENT_CHOICE);
			}
			catch (final BusinessProcessException businessProcessException)
			{
				LOG.info("Unable to trigger pack consignment process for consignment: {}", consignment.getCode());
			}
		}
		checkAndGenerateOrderNumber(consignment.getOrder());
		updateConsignmentInvoiceDate(consignment);
		this.getConsignmentPrintPackSlipStrategy().printDocument(consignment);
		return new ActionResult("success");
	}

	/**
	 * @param consignment
	 */
	private void updateConsignmentInvoiceDate(final ConsignmentModel consignment)
	{
		getModelService().refresh(consignment);
		if (consignment.getInvoiceDate() == null)
		{
			consignment.setInvoiceDate(new Date());
			getModelService().save(consignment);
			getModelService().refresh(consignment);
		}

	}

	private void checkAndGenerateOrderNumber(final AbstractOrderModel orderModel)
	{
		if (orderModel != null && StringUtils.isBlank(orderModel.getOrderNumber()))
		{
			final Optional<String> orderNumber = generateOrderNumber(orderModel);
			if (orderNumber.isPresent())
			{
				orderModel.setOrderNumber(orderNumber.get());
				getModelService().save(orderModel);
				getModelService().refresh(orderModel);
			}
		}

	}

	/**
	 * @param orderModel
	 * @return
	 */
	private Optional<String> generateOrderNumber(final AbstractOrderModel orderModel)
	{
		return getSerialNumberConfigurationContext().generateSerialNumberForBaseStore(orderModel.getStore(),
				SerialNumberSource.ABSTRACT_ORDER);
	}

	protected SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}


	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		return !Objects.isNull(consignment) && !Objects.isNull(consignment.getOrder())
				&& consignment.getFulfillmentSystemConfig() == null
				&& !((OrderModel) consignment.getOrder()).isSyncPartialCancelFailed() && checkRequiredStatusesForPack(consignment)
				&& checkERPConsignmentProcess(consignment);
	}

	private boolean checkERPConsignmentProcess(final ConsignmentModel consignment)
	{
		if (CollectionUtils.isEmpty(consignment.getErpConsignmentProcesses()))
		{
			return true;
		}
		final ERPConsignmentProcessModel erpConsignmentProcessModel = consignment.getErpConsignmentProcesses().get(0);
		if (ProcessState.RUNNING.equals(consignment.getErpConsignmentProcesses().get(0).getState()))
		{
			return false;
		}
		return true;
	}

	protected boolean checkRequiredStatusesForPack(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			return false;
		}
		if (consignment.getStatus() == null)
		{
			return false;
		}
		final ConsignmentStatus status = consignment.getStatus();
		return !ConsignmentStatus.READY.equals(status) && !ConsignmentStatus.CANCELLED.equals(status);
	}

	public boolean needsConfirmation(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return false;
	}

	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return null;
	}

	protected ConsignmentPrintDocumentStrategy getConsignmentPrintPackSlipStrategy()
	{
		return this.consignmentPrintPackSlipStrategy;
	}

	protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return this.warehousingConsignmentWorkflowService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return this.configurationService;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

}

