/**
 *
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.reallocate.ReallocateAction;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author monzer
 *
 */
public class CustomReallocateAction extends ReallocateAction
{

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		if (actionContext == null || actionContext.getData() == null)
		{
			return false;
		}
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment.getOrder() == null || consignment.getOrder().getStore() == null)
		{
			return false;
		}
		return super.canPerform(actionContext) && consignment.getOrder().getStore().isEnableWHReallocation();
	}

}
