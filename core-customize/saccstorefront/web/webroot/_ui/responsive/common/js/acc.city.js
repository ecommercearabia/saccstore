ACC.city = {
	get : function() {
		$("#areaId").change(
				function() {
					var isoCode = $(this).val();
					$.ajax({
						type : "GET",
						url : ACC.config.encodedContextPath + "/misc/country/" + isoCode + "/cites",
						dataType : "json",
						success : function(response) {
							
							$(".citytwo input").val('');
							$('#city-changed').val('')
 	                     
	                        if(response.data != null){
	                        	var emptyListLBL = "";
			                    var options = '';
			                    $('#city-changed').html('');
		                        var emptyOption = '<option disabled="disabled" selected="selected" class="__web-inspector-hide-shortcut__">Please select</option>';
		                        $('#city-changed').append(emptyOption);
		                        for (i = 0; i < response.data.length; i++) {
		                            var code = response.data[i].code;
		                            var name = response.data[i].name;
		                            options += '<option value="' + code + '">' + name + '</option>';
		                        }
		                        
		                        $('#city-changed').append(options);
	 	                        $('#city-changed').selectpicker('refresh');
	 	                      
	 	                     


		 	                      $(".citytwo").addClass('hidden');
		 	                       $(".cityone").removeClass('hidden');
	                        }
	                        else{
	                        	$(".citytwo").removeClass('hidden');
		 	                       $(".cityone").addClass('hidden');
	                        	
	                        	
	                        }
	                       
						}
				});
		});
	}






}

$(document).ready(function() {
	with (ACC.city) {
		get();
	}
});
