<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set value="${fn:escapeXml(component.styleClass)}" var="navigationClassHtml" />

<c:if test="${component.visible}">
<a href="javascript:;" class="personal_info"><i></i>
<b><sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								
									<ycommerce:testId code="header_LoggedUser">
										<spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" />
									</ycommerce:testId>
							
							</sec:authorize></b>
<span class="boder_personal_info hidden-sm hidden-xs">
<c:out value="${component.navigationNode.title}"/></span><span class="hidden-lg hidden-md far fa-chevron-down arrow-account"></span></a>
    <div class="${navigationClassHtml} js-${navigationClassHtml} NAVcompONENT" data-title="${fn:escapeXml(component.navigationNode.title)}">
        <nav class="${navigationClassHtml}__child-wrap ">
            <c:if test="${not empty component.navigationNode.title }">
                
                
                    
                
            </c:if>
            <c:forEach items="${component.navigationNode.children}" var="topLevelChild">
                <c:forEach items="${topLevelChild.entries}" var="entry">
                    <li>
                        <cms:component component="${entry.item}" evaluateRestriction="true" />
                    </li>
                </c:forEach>
            </c:forEach>
            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_signOut">
										<c:url value="/logout" var="logoutUrl"/>
										<a href="${fn:escapeXml(logoutUrl)}" >
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>
        </nav>
    </div>
    	<div class="darkback hidden"></div>
</c:if>