<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<div class="carousel__component--carousel js-owl-carousel owl-carousel owl-theme js-owl-${fn:toLowerCase(component.theme)}">
						<c:forEach items="${productData}" var="product">

							<c:url value="${productReference.target.url}" var="productUrl"/>

							<div class="carousel__item itemtheme">
								<c:url value="javascript:;" var="link"></c:url>
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:url value="/login" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${productReference.target.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!productReference.target.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
        <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}" data-productcode="${productReference.target.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${productReference.target.code}" data-pk="8796093055677"><i class="far fa-heart"></i></a>
				</span>
									<div class="thumbimage">
									<c:if test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock'}"><i class="outstock"><spring:theme code='productReference.target.variants.out.of.stock'/></i></c:if>
									<a href="${productUrl}">
									<c:if test="${not empty productReference.target.productLabel}"><div class="carousel__item--label">${productReference.target.productLabel}</div></c:if>
										<product:productPrimaryImage product="${productReference.target}" format="product"/>
										
									</a>
									</div>
									<div class="content_box">
									<div class="carousel__item--price-top">
									
										

									
									<div class="price">
									<format:fromPrice priceData="${productReference.target.price}"/></div>
									<div class="scratch"></div>
									</div>
									
									
									
									<div class="carousel__item--name name">
									<a href="${productUrl}">
									<c:choose>
						<c:when test="${fn:length(productReference.target.name) > 40}">
							<c:out value="${fn:substring(productReference.target.name, 0, 40)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(productReference.target.name)}
						</c:otherwise>
						</c:choose>
									</a>
									</div>
									
									<div class="carousel__item--reviews reviews">
										<product:productReviewSummary product="${productReference.target}" showLinks="false"/>
									</div>
									
							
									<div class="carousel__item--price-bottom">
									
									<div class="price">
									<format:fromPrice priceData="${productReference.target.price}"/></div>
									<div class="scratch"></div>
									</div>
									<c:if test="${not empty productReference.target.stock.stockLevel}">
									<div class="stock hidden">
										<spring:theme code="product.stocklevel" arguments="${productReference.target.stock.stockLevel}"/>
									</div>
									</c:if>
									<div class="addcart">
										<product:addtocartcarousel showQuantityBox="true" product="${productReference.target}" />
										</div>
									
									</div>
							
						
							</div>
							
						</c:forEach>
					</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>