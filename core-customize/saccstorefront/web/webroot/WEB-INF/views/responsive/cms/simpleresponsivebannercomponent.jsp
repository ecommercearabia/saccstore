<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>



<c:forEach items="${medias}" var="media">
	<c:choose>
		<c:when test="${empty imagerData}">
			<c:set var="imagerData">"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="imagerData">${imagerData},"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:otherwise>
	</c:choose>
	<c:if test="${empty altText}">
		<c:set var="altTextHtml" value="${fn:escapeXml(media.altText)}"/>
	</c:if>
</c:forEach>

<c:url value="${urlLink}" var="simpleResponsiveBannerUrl" />

<div class="simple-banner banner__component--responsive">
	<input type="hidden" value="${component.endDate}"/>
				<input type="hidden" value="${component.startDate}"/>
				<input type="hidden" value="${component.message}"/>
				<input type="hidden" value="${component.enableCounter}"/>
				<fmt:setLocale value="en_US" scope="session"/>
				<jsp:useBean id="curretntDate" class="java.util.Date" scope="request" />

	<c:set var="imagerDataJson" value="{${imagerData}}"/>
	<c:choose>
		<c:when test="${empty simpleResponsiveBannerUrl || simpleResponsiveBannerUrl eq '#'}">
			<c:if test="${component.enableCounter}">

			

<c:set var = "end" value = "${component.endDate}" />
<c:set var = "start" value = "${component.startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
<div class="topbanner-counter"> 
	<div class="topbanner-counter-text">
	<span> ${component.message}</span>
	</div>
	<fmt:setLocale value="en_US" scope="session"/>
				<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
</div>
			</c:if> 

</c:if> 	
		<img class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' alt='' title='' style="">
		</c:when>
		<c:otherwise>
			<a href="${fn:escapeXml(simpleResponsiveBannerUrl)}">
				<c:if test="${component.enableCounter}">

	
<c:set var = "end" value = "${component.endDate}" />
<c:set var = "start" value = "${component.startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
	<div class="topbanner-counter"> 
		<div class="topbanner-counter-text">
		<span> ${component.message}</span>
		</div>
		<fmt:setLocale value="en_US" scope="session"/>
					<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
	</div>
				</c:if> 

</c:if> 
				<img class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' title='' alt='' style="">
			</a>
		</c:otherwise>
	</c:choose>
</div>