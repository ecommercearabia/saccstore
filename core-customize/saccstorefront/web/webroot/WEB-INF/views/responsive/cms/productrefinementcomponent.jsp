<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
<div class="hidden-md hidden-lg top-mobile-facets"><spring:theme code="search.nav.refine.button"/> </h4><a href="javascript:;" class="hide-facets pull-right"><i class="far fa-times"></i></a><h4></div>
    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
    <nav:facetNavRefinements pageData="${searchPageData}"/>
</div>