<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


			<c:forEach items="${banners}" var="banner" varStatus="status">
			<div class=" box-${fn:toLowerCase(component.theme)} zaza ${fn:toLowerCase(component.theme) eq 'theme2' && cmsPage.uid eq 'homepage' ? 'no-space col-md-2 col-sm-4 col-xs-4' : ''}">
			
				<input type="hidden" value="${component.enableCounter}"/>
				<input type="hidden" value="${component.endDate}"/>
				<input type="hidden" value="${component.startDate}"/>
				<input type="hidden" value="${component.message}"/>
				<fmt:setLocale value="en_US" scope="session"/>
				<jsp:useBean id="curretntDate" class="java.util.Date" scope="request" />
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
				
						<a class="${fn:toLowerCase(banner.theme)}" tabindex="-1" href="${fn:escapeXml(encodedUrl)}"<c:if test="${banner.external}"> target="_blank"</c:if>>
							<c:if test="${component.enableCounter}">

							
			<c:set var = "end" value = "${component.endDate}" />
			<c:set var = "start" value = "${component.startDate}" />
			
			<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
			<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
			<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>
			
			<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
				<div class="topbanner-counter"> 
					<div class="topbanner-counter-text">
					<span> ${component.message}</span>
					</div>
					<fmt:setLocale value="en_US" scope="session"/>
								<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
				</div>
							</c:if> 
			
			</c:if>
							<img class="btn-block" src="${fn:escapeXml(banner.media.url)}" 
								alt="" 
								title=""/>
						<c:if test="${not empty banner.title}">
						<label class="devcontent">
						<c:if test="${not empty banner.title}"> <h2>${banner.title}</h2></c:if>
						<c:if test="${not empty banner.content}"> <span>${banner.content}</span></c:if>
						</label>
						</c:if>
					
			  
			  
						</a>
					
				</c:if>
			</div>
			</c:forEach>

