<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="grayback" style="background:${component.color}">
<c:if test="${not empty component.icon}">
	<div class="icon">
		<img class="icon" title="${fn:escapeXml(component.icon.altText)}" alt="${fn:escapeXml(component.icon.altText)}" src="${fn:escapeXml(component.icon.url)}">
	</div>
</c:if>
${ycommerce:sanitizeHTML(component.content)}</div>
