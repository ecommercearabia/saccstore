<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<div class="row">
<c:if test="${not empty wideImage}">
	<div class="col-md-12 col-sm-12 hidden-xs">
		<img class="btn-block margin-bottom-10" title="${fn:escapeXml(wideImage.altText)}" alt="${fn:escapeXml(wideImage.altText)}" src="${fn:escapeXml(wideImage.url)}">
	</div>
</c:if>
<c:if test="${not empty responsiveImage}">
	<div class="col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-md hidden-lg">
		<img class="btn-block margin-bottom-10" title="${fn:escapeXml(responsiveImage.altText)}" alt="${fn:escapeXml(responsiveImage.altText)}" src="${fn:escapeXml(responsiveImage.url)}">
	</div>
</c:if>
<c:if test="${not empty categoryContent}">
	<div class="headline hidden">
		<span>${categoryContent}</span>
	</div>
</c:if>
</div>

