<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:url value="${urlLink}" var="simpleBannerUrl" />


<div class="banner__component simple-banner">
	<input type="hidden" value="${component.enableCounter}"/>
				<input type="hidden" value="${component.endDate}"/>
				<input type="hidden" value="${component.startDate}"/>
				<input type="hidden" value="${component.message}"/>
				<fmt:setLocale value="en_US" scope="session"/>
<jsp:useBean id="curretntDate" class="java.util.Date" scope="request" />
	<c:choose>
		<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#'}">		
			<c:if test="${component.enableCounter}">

				
<c:set var = "end" value = "${component.endDate}" />
<c:set var = "start" value = "${component.startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
<div class="topbanner-counter"> 
	<div class="topbanner-counter-text">
	<span> ${component.message}</span>
	</div>
	<fmt:setLocale value="en_US" scope="session"/>
				<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
</div>
			</c:if> 

</c:if> 
		<img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(media.altText)}"
				src="${fn:escapeXml(media.url)}">
		</c:when>
		<c:otherwise>
			<a href="${fn:escapeXml(simpleBannerUrl)}">
				<c:if test="${component.enableCounter}">

<c:set var= "end" value = "${component.endDate}" />
<c:set var= "start" value = "${component.startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
	<div class="topbanner-counter"> 
		<div class="topbanner-counter-text">
		<span> ${component.message}</span>
		</div>
		<fmt:setLocale value="en_US" scope="session"/>
					<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
	</div>
				</c:if> 

</c:if> 
				<img title="${fn:escapeXml(media.altText)}"
				alt="${fn:escapeXml(media.altText)}" src="${fn:escapeXml(media.url)}"></a>
		</c:otherwise>
	</c:choose>
</div>