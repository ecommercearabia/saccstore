<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">



	<div class="cart-top-bar hidden">
        <div class="text-right">
            <spring:theme var="textHelpHtml" code="text.help" />
            <a href="" class="help js-cart-help" data-help="${fn:escapeXml(textHelpHtml)}">${textHelpHtml}
                <span class="far fa-info-circle"></span>
            </a>
            <div class="help-popup-content-holder js-help-popup-content">
                <div class="help-popup-content">
                    <strong>${fn:escapeXml(cartData.code)}</strong>
                    <spring:theme var="cartHelpContentVar" code="basket.page.cartHelpContent" htmlEscape="false" />
                    <c:set var="cartHelpContentVarSanitized" value="${ycommerce:sanitizeHTML(cartHelpContentVar)}" />
                    <div>${cartHelpContentVarSanitized}</div>
                </div>
            </div>
		</div>
	</div>

		<div class="row">
        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
        	<cart:cartValidation/>
	<cart:cartPickupValidation/>
            <cms:pageSlot position="TopContent" var="feature" >
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
             <cart:cartItems cartData="${cartData}"/>
        </div>
		<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
         <div class="row">

         <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterRightContentSlot" var="feature" element="div" class="col-md-12">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
    <c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
    <c:url value="/quote/create" var="createQuoteUrl" scope="session"/>
    <c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>
    <c:set var="showTax" value="false"/>


        <div class="col-xs-12 pull-right cart-actions--print">
            <div class="border">
                <div class="row">
                    <div class="col-md-12">
                        <ycommerce:testId code="checkoutButton">
                            <button class="btn btn-primary btn-block js-continue-checkout-button"  <c:if test="${not showCheckout}"> disabled </c:if> data-checkout-url="${fn:escapeXml(checkoutUrl)}">
                                <spring:theme code="checkout.checkout"/>
                            </button>
                        </ycommerce:testId>
                    </div>

                    <c:if test="${not empty siteQuoteEnabled and siteQuoteEnabled eq 'true'}">
                        <div class="col-md-12">
                            <button class="btn btn-default btn-block js-create-quote-button" data-create-quote-url="${fn:escapeXml(createQuoteUrl)}">
                                <spring:theme code="quote.create"/>
                            </button>
                        </div>
                    </c:if>

                    <div class="col-md-12">
                        <button class="btn btn-default btn-block js-continue-shopping-button" data-continue-shopping-url="${fn:escapeXml(continueShoppingUrl)}">
                            <spring:theme code="cart.page.continue"/>
                        </button>
                    </div>
                    <div class="col-md-12">
                    	<cms:pageSlot position="DisclaimerSlot" var="feature">
                			<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            			</cms:pageSlot>
                    </div>
                    <div class="col-md-12">
                    	<cms:pageSlot position="ImageSlot" var="feature">
                			<cms:component component="${feature}" element="div" class="yComponentWrapper image-cart"/>
            			</cms:pageSlot>
                    </div>
                </div>
            </div>
        </div>


   


            <cms:pageSlot position="BottomContentSlot" var="feature" element="div" class="col-md-12">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
		</c:if>

        </div>
		</div>
        </div>

	   <c:if test="${not empty cartData.rootGroups}">
           <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
           </cms:pageSlot>
        </c:if>
		
				
		<c:if test="${empty cartData.rootGroups}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper content__empty"/>
            </cms:pageSlot>
		</c:if>

</template:page>