<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="row">
    <div class="col-sm-6">
        <div class="checkout-headline">
            <span class="fal fa-lock-alt"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<ycommerce:testId code="checkoutStepThree">
					<div class="checkout-shipping">
<%-- 						<multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" /> --%>
						<div class="checkout-indent">
<%-- 							<div class="headline"><spring:theme code="checkout.summary.timeSlot.selectTimeSlotForOrder" /></div> --%>
							<spring:url var="selectTimeSlotUrl" value="{contextPath}/checkout/multi/time-slot/select" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
							<form:form id="selectTimeSlotForm" action="${fn:escapeXml(selectTimeSlotUrl)}" method="post" modelAttribute="timeSlotForm">
								<div class="form-group">
									
									<multi-checkout:timeSlotSelector timeSlot="${timeSlot}" selectedSlot="${cartData.timeSlotInfoData}"/>
									<spring:theme code="checkout.multi.orderNote.msg"/><br>
									<div class="mar_top">
									<formElement:formSelectBox idKey="address.notes"
										labelKey="address.notes" path="noteCode" mandatory="false" itemLabel="note"
										skipBlank="false" items="${supportedOrderNotes}" itemValue="code"
										selectCSSClass="form-control " />
										<formElement:formInputBox labelKey="address.note" idKey="address.note" path="note" placeholder="address.note"/>
								</div>
								</div>

								<input type="hidden" id="periodCode" name="periodCode" value="${cartData.timeSlotInfoData.periodCode }"/>
								<input type="hidden" id="start" name="start" value="${cartData.timeSlotInfoData.start }"/>
								<input type="hidden" id="end" name="end" value="${cartData.timeSlotInfoData.end }"/>
								<input type="hidden" id="day" name="day" value="${cartData.timeSlotInfoData.day }"/>
								<input type="hidden" id="date" name="date" value="${cartData.timeSlotInfoData.date }"/>
								
							</form:form>
						</div>
						<button id="timeSlotSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.timeSlot.continue"/></button>
					</div>
					
				</ycommerce:testId>
			</jsp:body>
		</multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>

    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>

</template:page>
