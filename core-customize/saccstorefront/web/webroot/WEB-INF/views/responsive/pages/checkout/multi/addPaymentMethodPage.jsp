<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="/checkout/multi/create-checkoutid/applepay" var="createCheckoutIdURL" />

<script type="text/javascript">
	window.wpwlOptions = {
			applePay: {
				merchantCapabilities: ["supports3DS"],
				supportedNetworks: ["masterCard", "visa", "mada"],
				supportedCountries: ['SA'],
				displayName: "Sky Sales Online",
				total: {
					label: "Sky Sales Online.",
					amount: ${cartData.totalPriceWithTax.value}
				},
				currencyCode: "SAR",
				countryCode: "SA"
			}
		}



	window.wpwlOptions.createCheckout = function(json) {
		var domain = window.location.host + ACC.config.contextPath;
		var URL = "https://" + domain + "/" + ACC.config.language + "/checkout/multi/payment-method/create-checkoutid/applepay";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", URL, false);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(null);
		var data = JSON.parse(JSON.parse(xhr.response));
		checkoutId = data.checkoutId;
		return checkoutId;
	}
</script>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

	<div class="row">
		<div class="col-sm-6">
			<div class="checkout-headline">
				<span class="glyphicon glyphicon-lock"></span>
				<spring:theme code="checkout.multi.secure.checkout" />
			</div>
			<multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}"
				progressBarId="${progressBarId}">
				<jsp:body>
                    <ycommerce:testId code="checkoutStepFour">
                        <div class="checkout-paymentmethod">
                            <div class="checkout-indent">

                                <div class="headline">
									<spring:theme code="checkout.multi.paymentMethod" />
								</div>
							<spring:url var="selectPaymentMethodUrl"
									value="{contextPath}/checkout/multi/payment-method/add"
									htmlEscape="false">
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
							
								<form:form method="post" modelAttribute="paymentDetailsForm"
									action="${selectPaymentMethodUrl}" id="selectPaymentMethodForm">
								
								<formElement:formSelectBoxDefaultEnabled idKey="payment.method."
										labelKey="payment.method.title"
										selectCSSClass="form-control hidden" path="paymentModeCode"
										mandatory="true" skipBlank="false"
										skipBlankMessageKey="form.select.none"
										items="${supportedPaymentModes}" />
								</form:form>
                                <c:forEach items="${supportedPaymentModes}" var="pm">
									<label class="container-radio ${fn:toLowerCase(pm.code) eq 'applepay' ? 'applePayaction hidden' : 'applePayaction'}" for="${pm.code}">
										<input id="${pm.code}" type="radio" name="paymentmothed" value="${pm.code}" />
										
										<c:if test="${pm.paymentModeType.code eq 'NOCARD'}">${pm.name} 
											<span class="textpay">
												<em class="fal fa-sack-dollar"></em>
											</span>
										</c:if>
										
										<c:if test="${pm.paymentModeType.code eq 'CARD'}">
											<c:choose>
												<c:when test="${pm.paymentModeType.code eq 'CONTINUE'}" >
													${pm.name}		
												</c:when>
												<c:when test="${pm.code eq 'card'}">
													${pm.name}
													<span class="textpay"><em class="mada_visa_master"></em></span>
												</c:when>
												<c:when test="${pm.code eq 'mada'}">
													${pm.name}
													<span class="textpay"><em class="mada"></em></span>
												</c:when>
												<c:when test="${fn:toLowerCase(pm.code) eq 'applepay'}">
												
													<table border="0" style="width:auto" class="applestyletext">
														<tr>
															<td><label><spring:theme code="payment.method.pay.using" />&nbsp;</label></td>
															<td>
															<c:url value="/checkout/multi/summary/callback" var="redirectURL" />
													<form action="${redirectURL}" class="paymentWidgets" data-brands="APPLEPAY"></form>
													
													<script src="https://oppwa.com/v1/paymentWidgets.js"></script>
															</td>
														</tr>
													</table>
													
												</c:when>						
											</c:choose>
										</c:if>
										
										<span class="${fn:toLowerCase(pm.code) eq 'applepay' ? 'hidden checkmark' : 'checkmark'}"></span>								
									</label>
								</c:forEach>

                            </div>
                        </div>
                        
                        
					<button id="paymentMethodSubmit" type="button"
							class="btn btn-primary btn-block checkout-next">
							<spring:theme code="checkout.multi.deliveryMethod.continue" />
						</button>

                    </ycommerce:testId>
                    
               </jsp:body>

			</multiCheckout:checkoutSteps>
		</div>

		<div class="col-sm-6 hidden-xs">
			<multiCheckout:checkoutOrderDetails cartData="${cartData}"
				showDeliveryAddress="true" showPaymentInfo="false"
				showTaxEstimate="false" showTax="true" />
		</div>

		<div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div"
				class="checkout-help">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
	</div>

</template:page>
