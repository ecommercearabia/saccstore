<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
<div class="se-pre-con"></div>
<div class="row no-margin">
<div class="col-md-12 col-sm-12 no-space">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12 no-space">  
     <cms:pageSlot position="Section2" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12">
    <cms:pageSlot position="Section3" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
    </div>
</div>
<div class="row padd30  no-margin lightback">
<div class="col-md-12 col-sm-12"> 
<cms:pageSlot position="Section4" var="feature" >
        <cms:component component="${feature}" element="h2" class="carousel__component--headline" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin lightback"> 
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section5" var="feature" element="div" class="row" >
        <cms:component limit="0" component="${feature}" element="div" class=" col-md-12 owl-carousel carousel js-owl-carousel owl-carousel js-owl-default-3 owl-theme no-space" />
    </cms:pageSlot>
</div>
</div>  
<div class="row padd30  no-margin lightback"> 
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section6" var="feature" element="div" class="row" >
        <cms:component limit="0" component="${feature}" element="div" class=" col-md-12 owl-carousel carousel js-owl-carousel owl-carousel js-owl-default-3 owl-theme no-space" />
    </cms:pageSlot>
</div>
</div>
</template:page>
