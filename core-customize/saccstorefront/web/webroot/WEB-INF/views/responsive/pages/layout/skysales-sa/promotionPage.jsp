<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
			<cms:pageSlot position="Section1" var="feature" element="div" class="row" >
               <cms:component component="${feature}" element="div"   class="col-md-12 m-b-20"/>
           </cms:pageSlot>
           <cms:pageSlot position="Section2" var="feature" element="div " class="row carousel__component m-b-20" >
               <cms:component component="${feature}" element="div"   class="col-md-12 carousel__component--carousel js-owl-carousel owl-carousel owl-theme js-owl-theme2" />
           </cms:pageSlot>
           <cms:pageSlot position="Section3" var="feature" element="div" class="row" >
               <cms:component component="${feature}" element="div"   class="col-md-4 m-b-20" />
           </cms:pageSlot>
           <cms:pageSlot position="Section4" var="feature" element="div"  class="row" >
               <cms:component component="${feature}" element="div"   class="col-md-12 m-b-20" />
           </cms:pageSlot>
            <cms:pageSlot position="Section5" var="feature" element="div" class="row" >
               <cms:component component="${feature}" element="div"    class="col-md-4 m-b-20" />
           </cms:pageSlot>
            <cms:pageSlot position="Section6" var="feature" element="div" class="row" >
               <cms:component component="${feature}" element="div"    class="col-md-12 m-b-20" />
           </cms:pageSlot>
</template:page>
