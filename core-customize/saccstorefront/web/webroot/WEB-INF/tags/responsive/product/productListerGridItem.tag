<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<c:set value="product-item" var="productTagClasses"/>
<c:forEach var="tag" items="${product.tags}">
	<c:set value="${productTagClasses} tag-${tag}" var="productTagClasses"/>
</c:forEach>

<div class="${product.code} detail ${fn:escapeXml(productTagClasses)}">
	<ycommerce:testId code="product_wholeProduct">
	
		<a class="thumb" href="${fn:escapeXml(productUrl)}" title="${fn:escapeXml(product.name)}">
			<product:productPrimaryImage product="${product}" format="zoom"/>
		
		
		
		</a>
		<c:url value="javascript:;" var="link"></c:url>
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:url value="/login" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${product.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!product.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
        <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}" data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${product.code}" data-pk="8796093055677"><i class="far fa-heart"></i></a>
				</span>
		<div class="details">
		
<ycommerce:testId code="product_productPrice">
	<div class="price-content">
					<c:choose>
						<c:when test="${not empty product.discount}">
							
							<p class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></p>
							<span class="scratched"><format:fromPrice priceData="${product.discount.price}"/></span>
						</c:when>
						<c:otherwise>
								
								<p class="price"><product:productListerItemPrice product="${product}"/></p>
								<span class="scratched"></span>
						</c:otherwise>
					</c:choose>
				</div>
					</ycommerce:testId>
			<ycommerce:testId code="product_productName">
				<a class="name" href="${fn:escapeXml(productUrl)}">
					<c:out escapeXml="false" value="${ycommerce:sanitizeHTML(product.name)}" />
				</a>
			</ycommerce:testId>
			<div class="carousel__item--reviews reviews">
										<product:productReviewSummary product="${product}" showLinks="false"/>
									</div>
			
									
									
<%-- 			<c:if test="${not empty product.productLabel}"> --%>
<%-- 				<div>${fn:escapeXml(product.productLabel)}</div> --%>
<%-- 			</c:if> --%>
<%-- 			<c:if test="${not empty product.countryOfOriginIsocode}"> --%>
<%-- 				<div>${fn:escapeXml(product.countryOfOriginIsocode)}</div> --%>
<%-- 			</c:if> --%>
			
			<c:if test="${not empty product.potentialPromotions}">
				<div class="promo">
					<c:forEach items="${product.potentialPromotions}" var="promotion">
						${ycommerce:sanitizeHTML(promotion.description)}
					</c:forEach>
				</div>
			</c:if>
				<c:set var="product" value="${product}" scope="request"/>
		<c:set var="addToCartText" value="${addToCartText}" scope="request"/>
		<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>
		<c:set var="isGrid" value="true" scope="request"/>
		<div class="addtocart">
	
			<div class="actions-container-for-${fn:escapeXml(component.uid)} <c:if test="${ycommerce:checkIfPickupEnabledForStore() and product.availableForPickup}"> pickup-in-store-available</c:if>">
<%-- 				<action:actions element="div" parentComponent="${component}"/> --%>
					<product:addtocartcarousel showQuantityBox="true" product="${product}" />
			</div>
		</div>
			
				
		
			<c:forEach var="variantOption" items="${product.variantOptions}">
				<c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
					<c:if test="${variantOptionQualifier.qualifier eq 'rollupProperty'}">
	                    <c:set var="rollupProperty" value="${variantOptionQualifier.value}"/>
	                </c:if>
					<c:if test="${variantOptionQualifier.qualifier eq 'thumbnail'}">
	                    <c:set var="imageUrlHtml" value="${fn:escapeXml(variantOptionQualifier.value)}"/>
	                </c:if>
	                <c:if test="${variantOptionQualifier.qualifier eq rollupProperty}">
	                    <c:set var="variantNameHtml" value="${fn:escapeXml(variantOptionQualifier.value)}"/>
	                </c:if>
				</c:forEach>
				<img style="width: 32px; height: 32px;" src="${imageUrlHtml}" title="${variantNameHtml}" alt="${variantNameHtml}"/>
			</c:forEach>
	
		
		</div>


		
	</ycommerce:testId>
</div>