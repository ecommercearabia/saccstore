<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<spring:htmlEscape defaultHtmlEscape="true" />


    

    <c:choose>
        <c:when test="${galleryImages == null || galleryImages.size() == 0}">
            <div id="sync1" class="owl-carousel owl-theme js-owl-carousel">
                <div class="item">
                    <div>
                        <spring:theme code="img.missingProductImage.responsive.product" var="imagePath" htmlEscape="false"/>
                        <c:choose>
                            <c:when test="${originalContextPath ne null}">
								<c:choose>
									<c:when test='${fn:startsWith(imagePath, originalContextPath)}'>	
										<c:url value="${imagePath}" var="imageUrl" context="/"/>
									</c:when>
									<c:otherwise>
										<c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
									</c:otherwise>
								</c:choose>
                            </c:when>
                            <c:otherwise>
                                <c:url value="${imagePath}" var="imageUrl" />
                            </c:otherwise>
                        </c:choose>
                        <img src="${fn:escapeXml(imageUrl)}"/>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
      <div class="app-figure" id="zoom-fig">

    
                <c:forEach items="${galleryImages}" var="container" varStatus="varStatus"  end="0" begin="0" step="1">
      
                     <a id="Zoom-1" class="MagicZoom col-md-9 col-lg-10 col-sm-12 col-xs-12 active pull-right" data-options="zoomWidth: 50%; zoomMode: magnifier; zoomDistance: 10;" 
            href="${fn:escapeXml(container.superZoom.url)}"
            data-zoom-image-2x="${fn:escapeXml(container.superZoom.url)}"
            data-image-2x="${fn:escapeXml(container.zoom.url)}"
        >
            <c:set value="none" var="display"/>
<c:if test="${not empty product.productLabel}"><c:set value="inline" var="display"/></c:if>
<div class="carousel__item--label label_image" style="display:${display}">${product.productLabel}</div>
  
            <img src="${fn:escapeXml(container.superZoom.url)}" srcset="${fn:escapeXml(container.superZoom.url)}"
                alt="" style="max-width:600px;"/><c:set value="hidden" var="isin"></c:set>
	
	
<%--                 <c:if test="${not empty product.productLabel}"> --%>
<%-- 				<div class="ribbon2 top ${not empty product.productLabelPosition ? product.productLabelPosition : 'top right'}" style="background-color:${not empty product.productLabelColor ? product.productLabelColor : '#000'}">${fn:escapeXml(product.productLabel)}</div> --%>
<%-- 				</c:if> --%>
        </a>

                   
                </c:forEach>
<%--                 <c:if test="${not empty product.youtubeIds}"> --%>
<!--                   <div data-slide-id="video-1" class="zoom-gallery-slide video-slide"> -->
<%--         <iframe width="100%" height="315" src="https://www.youtube.com/embed/${product.youtubeIds}" frameborder="0" allowfullscreen></iframe> --%>
<!--     </div> -->
<%--                 </c:if> --%>
      <product:productGalleryThumbnail galleryImages="${galleryImages}" />
       
    
    
    
        
       
     
            </div>
        </c:otherwise>
    </c:choose>
