<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
	<div class="col-xs-10 col-xs-push-1 col-sm-5 col-sm-push-0 col-md-5">
		<product:productImagePanel galleryImages="${galleryImages}" />
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-sm-7 col-md-7 col-xs-12">
		<div class="product-main-info">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 ${product.code}">
					<div class="product-details">
					 <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
						<div class="name">${fn:escapeXml(product.name)}</div>
<!-- 						<div class="id_code"> -->
<!-- 						<span class="sku">SKU</span> -->
<%-- 						<span class="code">${fn:escapeXml(product.code)}</span> --%>
<!-- 						</div> -->
					</ycommerce:testId>  
					<product:productReviewSummary product="${product}" showLinks="true"/>
				
								


			
						<product:productPromotionSection product="${product}"/>
						<div class="pricesection">
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<c:choose>
									<c:when test="${not empty product.discount}">
										<p class="price">
											<format:fromPrice
												priceData="${product.discount.discountPrice}" />
										</p>
										
										<span class="scratched"><format:fromPrice
												priceData="${product.discount.price}" /></span>
<%-- 										<span class="discount_style">(${product.discount.percentage})</span> --%>
										
									</c:when>
									<c:otherwise>
									
										<product:productPricePanel product="${product}" />
<span class="scratched"></span>
									</c:otherwise>

								</c:choose>
						</ycommerce:testId>
						</div>
						
				<div class="description">${ycommerce:sanitizeHTML(product.summary)}</div> 
				<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
					</cms:pageSlot>
					<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
					</cms:pageSlot>
					</div>
				<c:url value="javascript:;" var="link"></c:url>
				<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:url value="/login" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${product.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!product.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
        <span class="wishlist_icon">

	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}" data-productcode="${product.code}" data-pk=""><i class="fas fa-trash"></i><spring:theme code="Remove.wishlist" text="Remove to wishlist"/></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${product.code}" data-pk=""><i class="far fa-plus"></i><spring:theme code="Add.wishlist" text="Add to wishlist"/></a>
				</span>
				</div>
				
				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="boxborder">
				<div class="one_item"><i class="fal fa-shipping-fast"></i><h2><spring:theme code="boxorder.shipping"/>
					<span><spring:theme code="boxorder.shipping.desc"/></span></h2>
				</div>
				<c:if test="${not empty product.warranty}">
				<div class="one_item"><i class="fal fa-medal"></i><h2><spring:theme code="boxorder.warranty"/>
					<span>${product.warranty}</span></h2>
				</div>
				</c:if>
				<div class="one_item"><i class="fal fa-shield"></i><h2><spring:theme code="boxorder.secure"/>
					<span><spring:theme code="boxorder.secure.desc"/></span></h2></div>
				</div>
				</div>

				
					
				
			</div>
		</div>

	</div>
</div>