<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="productCarouselComponentData" required="true" type="com.sacc.sacccomponents.data.ProductCarouselComponentData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty productCarouselComponentData.products}">
		<div class="carousel__component">
			<div class="carousel__component--headline hidden">${fn:escapeXml(productCarouselComponentData.title)}</div>

			<c:choose>
				<c:when test="${productCarouselComponentData.popup}">
					<div class="carousel__component--carousel owl-carousel js-owl-carousel js-owl-lazy-reference js-owl-carousel-reference">
						<div id="quickViewTitle" class="quickView-header display-none">
							<div class="headline">
								<span class="headline-text"><spring:theme code="popup.quick.view.select"/></span>
							</div>
						</div>
						<c:forEach items="${productCarouselComponentData.products}" var="product">

							<c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
							<div class="carousel__item">
								<a href="${productQuickViewUrl}" class="js-reference-item">
									<div class="carousel__item--thumb">
										<product:productPrimaryReferenceImage product="${product}" format="product"/>
									</div>
									<div class="carousel__item--name">
									<c:choose>
						<c:when test="${fn:length(product.name) > 50}">
							<c:out value="${fn:substring(product.name, 0, 50)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									
									</div>
									<div class="carousel__item--price"><format:fromPrice priceData="${product.price}"/></div>
								</a>
							</div>
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
					<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default">
						<c:forEach items="${productCarouselComponentData.products}" var="product">

							<c:url value="${product.url}" var="productUrl"/>

							<div class="carousel__item">
								<a href="${productUrl}">
									<div class="carousel__item--thumb">
									<c:if test="${not empty product.productLabel}"><div class="carousel__item--label">${product.productLabel}</div></c:if>
										<product:productPrimaryImage product="${product}" format="product"/>
										
									
									</div>
									<div class="cont_detail_carousel">
									<div class="carousel__item--name">
									<c:choose>
						<c:when test="${fn:length(product.name) > 40}">
							<c:out value="${fn:substring(product.name, 0, 40)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									
									</div>
									
									
									
									<div class="price_counter">
									<div class="carousel__item--price"><format:fromPrice priceData="${product.price}"/></div>
										<product:addProductToCart showQuantityBox="true" product="${product}" />
										</div>
										</div>
								</a>
						
							</div>
							
						</c:forEach>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>

