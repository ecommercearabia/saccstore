<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<c:choose>
	<c:when test="${country == 'US'}">
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.state" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CA'}">
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CN'}">
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.street" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.building" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'JP'}">
		<formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.furtherSubarea" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.subarea" path="line2" inputCSS="form-control" mandatory="true"/>
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townJP" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:otherwise>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.addressName" labelKey="address.addressName" path="addressName" inputCSS="form-control" mandatory="true" /></div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12"><formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/></div>
			<div class="col-md-4 col-sm-4 col-xs-12"><formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" /></div>
			<div class="col-md-4 col-sm-4 col-xs-12"><formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" /></div>
		</div>
		
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formSelectBoxDefaultEnabled idKey="cityId" labelKey="address.city" path="cityCode" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${cities}" selectCSSClass="form-control"/></div>
			<c:if test="${isAreaVisible}">
				<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formSelectBoxDefaultEnabled idKey="areaId" labelKey="address.areaCode" path="areaCode" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${areas}" selectCSSClass="form-control"/></div>
			</c:if>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" /></div>
			<div class="col-md-12 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/></div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.districtName" labelKey="address.districtName" path="districtName" inputCSS="form-control" mandatory="true" /></div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control countrypicker f16" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" /></div>
			<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.mobileNumber" labelKey="address.mobileNumber" path="mobile" inputCSS="form-control" mandatory="true" /></div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.nearestLandmark" labelKey="address.nearestLandmark" path="nearestLandmark" inputCSS="form-control" mandatory="true" /></div>
			<div class="col-md-6 col-sm-12 col-xs-12"><formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" /></div>
		</div>
		
		
		
		
		
		
		
        
        
        
	</c:otherwise>
</c:choose>

