<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sefamElement"
	tagdir="/WEB-INF/tags/responsive/sefamElement"%>
<%@ taglib prefix="otp" tagdir="/WEB-INF/tags/responsive/otp"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row ">
	<div class="col-md-6">
		<img class="btn-block otpimg"
			src="${fn:escapeXml(themeResourcePath)}/images/OTP.jpg">
	</div>
	<div class="col-md-6">
		<div class="boxotp">




			<c:url var="otpVerifyActionURL" value="/verify" />
			<c:url var="changeNumberActionURL" value="/verify/change-number" />
			<c:url var="resendVerifyActionURL" value="/verify/resend" />

			<div class="row">
				<div class="col-md-12 text-center">
					<p class="otp_headline">
						<spring:theme code="otp.verify.code.title" />
					</p>
					<form:form method="post" id="changeNumber" class="otpForm"
						action="${changeNumberActionURL}">
						<p>
							<spring:theme code="otp.verify.code.verify.description" />&nbsp;${mobileNumber}

							<button type="submit" class="btn btnotpchange" value="Change Number">
								<i class="far fa-edit"></i>
							</button>

						</p>
					</form:form>
				</div>
			</div>
			<form:form method="post" id="otpForm" class="otpForm"
				modelAttribute="otpForm" action="${otpVerifyActionURL}">
				<div class="row">
					<div class="col-md-12">
						<formElement:formInputBox idKey="otpCode" labelKey="otp.code"
							path="otpCode" inputCSS="form-control" mandatory="true" />
					</div>
					<div class="col-md-12 pull-right">
						<input type="submit" class="btn btn-primary btn-block"
							name="verify" value=<spring:theme code="otp.mobile.verify"/> />
					</div>
				</div>
			</form:form>

			<form:form method="post" id="resend" class="resend" 
				action="${resendVerifyActionURL}">
				<div class="row"><br/>
					<div class="col-md-12 text-center">
						<p><spring:theme code="otp.mobile.message.resend"/><button type="submit" class="btn btnotpchange" value="Resend" ><spring:theme code="otp.mobile.resend"/></button></p>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>
