<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="day" required="true" type="com.sacc.sacctimeslotfacades.TimeSlotDayData" %>
<%@ attribute name="selectedSlot" required="false" type="com.sacc.sacctimeslotfacades.TimeSlotInfoData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div>
	<span>${day.day}</span>
	<span>${day.date}</span>
	<c:forEach items="${day.periods}" var="period">
		<span>${period.intervalFormattedValue}</span>
		<c:if test="${!period.enabled}">
			<span><spring:theme code="timeslot.period.unavailable"/></span>
		</c:if>
	</c:forEach>
</div>