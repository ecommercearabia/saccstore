<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="supportedPaymentData" required="true"
	type="com.sacc.saccpayment.entry.PaymentRequestData"%>
<%@ tag import="com.sacc.saccpayment.model.HyperpayPaymentProviderModel"%>



<c:url value="/checkout/multi/summary/callback" var="hyperpayFormURL" />
<script>

	var wpwlOptions = {
		locale : "${currentLanguage.isocode}",
		onError : function(error) {
			data = "[hyperpayForm.tag:ApplePay]:onCancel: " + JSON.stringify(error)
		    console.log(data);
		},
		onReady : function() {
			$(".wpwl-control-cardHolder").attr("placeholder",ACC.cardHolderName);
			$(".wpwl-label-cardHolder").text(ACC.cardHolderName);
		},
		applePay : {
			merchantCapabilities : [ "supports3DS", "supportsCredit", "supportsDebit"],
			supportedNetworks: ["masterCard", "visa"],
			displayName : "Sky Sales Online.",
			total : {
				label : "Sky Sales Online."
			},
			currencyCode : "SAR",
			countryCode  : "SA",
			onPaymentAuthorized : function(payment) {
				return {
					status : "SUCCESS"
				}
			}
		}
	}
	

</script>
<br />

<c:if test="${not empty cmsSite.paymentWarningMessage }">
	<p class="alert alert-warning">${cmsSite.paymentWarningMessage }</p>
</c:if>

<script src="${supportedPaymentData.scriptSrc}">
	
</script>
<form action="${hyperpayFormURL}" class="paymentWidgets"
	data-brands="${supportedPaymentData.paymentProviderData.brands}"></form>

