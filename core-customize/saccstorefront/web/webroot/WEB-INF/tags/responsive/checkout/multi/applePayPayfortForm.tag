<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="supportedPaymentData" required="true" type="com.sacc.saccpayment.entry.PaymentRequestData" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<ycommerce:testId code="checkoutSteps">

xxxxxxxxxxxxxxxxxxxx
</ycommerce:testId>