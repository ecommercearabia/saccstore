<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="supportedPaymentData" required="true" type="com.sacc.saccpayment.entry.PaymentRequestData" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<ycommerce:testId code="checkoutSteps">
    <div >

<form action='${supportedPaymentData.paymentProviderData.url}' method='post' name='frm'>

	<c:forEach var="entry" items="${supportedPaymentData.paymentProviderData}">
		<c:if test="${entry.key ne 'url' }">

	<input type="hidden" name="${entry.key}" value="${entry.value}">
	</c:if>
</c:forEach>

	 <button  type="submit" class="btn btn-primary btn-place-order btn-block">
                         <span>  <spring:theme code="checkout.summary.pay.placeOrder" text="pay & Place Order"/></span> 
                        </button>
	</form>
    </div>
</ycommerce:testId>