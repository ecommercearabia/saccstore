<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cart" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-detail-overview">
    <div class="row">
        <div class="col-sm-3">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderID_label">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                    <span class="item-value">${fn:escapeXml(cartData.code)}</span>
                </ycommerce:testId>
            </div>
        </div>
            <div class="col-sm-3">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderStatus_label">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <span class="item-value"><spring:theme code="text.account.order.status.display.pending"/></span>
                    </ycommerce:testId>
                </div>
            </div>
        <div class="col-sm-3">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewStatusDate_label">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.datePlaced"/></span>
<%--                     <span class="item-value"><fmt:formatDate value="${cart.saveTime}" dateStyle="medium" timeStyle="short" type="both"/></span> --%>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                    <span class="item-label"><spring:theme code="text.account.order.total"/></span>
                    <span class="item-value"><format:price priceData="${cart.totalPriceWithTax}"/></span>
                </ycommerce:testId>
            </div>
        </div>

    </div>
 </div> 