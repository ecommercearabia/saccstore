<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="label-order">
    <spring:theme code="text.account.paymentType"/>
</div>
<div class="value-order">
	<c:if test='${order.paymentMode.code.equalsIgnoreCase("applepay")}'>
		${order.paymentMode.name} - &nbsp;	
	</c:if>
    ${fn:escapeXml(order.paymentInfo.cardTypeData.name)} &nbsp;
    ${fn:escapeXml(order.paymentInfo.cardNumber)}
</div>

