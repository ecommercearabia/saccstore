<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<footer>

    <div class="row no-margin margin_top padd30 maxwidth-800">
    	
    	<div class="col-md-6 col-lg-6 col-xs-12">
        <div class="newslatterheader row">
<div class="col-md-3 col-sm-12 col-xs-12">
<i class="fal fa-envelope"></i>
</div>
<div class="col-md-9 col-sm-12 col-xs-12">
<h2><spring:theme code="sign.up.for.newsletter" text="sign up for newsletter" /></h2>
<p><spring:theme code="sign.up.for.newsletter.2" text="we'll never share your email address with a third party" /></p>
</div>
    </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xs-12">
        		<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
    <div id="mc_embed_signup" class="box">
            <form action="https://skysalesonline.us19.list-manage.com/subscribe/post-json?u=4e1eb1741127ca26e9e0e9141&amp;id=6ec9dee2c9&c=?"
                method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<spring:theme code='sign.up.for.newsletter.email' text='email address' />" required>
                <div style="position: absolute; left: -5000px; display:none;" aria-hidden="true">
                    <input type="text" name="b_e44c1f194bec93e238615469e_f6f826e769" tabindex="-1" value="">
                </div>
                <div class="clear"><input type="submit" value="<spring:theme code='sign.up.for.newsletter.btn' text='Subscribe' />" name="subscribe" id="mc-embedded-subscribe" class="mc-button button"></div>
                <div id="subscribe-result">
                </div>
            </form>
        </div>
<!--End mc_embed_signup-->
    	</div>
    </div>
    <div class="row no-margin margin_top padd30 borderbottom">
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 contactstyle margint40 hidden-sm hidden-xs">
    	  <cms:pageSlot position="FooterContactUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
        
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 appstyle margint40 hidden">
    	 <cms:pageSlot position="FooterAppStoreLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 margint40 socialicons text-center hidden-lg hidden-md">
        <cms:pageSlot position="FooterFollowUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
     </div>
    
    	<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 margint40">
    		<cms:pageSlot position="FooterNavLinks2" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
    </div>
    <div class="row no-margin padd30" >
   
    <div class="col-sm-6 col-md-3 pull-right hidden-sm hidden-xs logo text-center margint20">
    		 <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
       <div class="col-sm-6 col-md-3 socialicons pull-right text-center margint20 hidden-sm hidden-xs">
        <cms:pageSlot position="FooterFollowUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
     </div>
      <div class="col-sm-12 col-md-3 pull-right paymenttext margint20">
    
    <cms:pageSlot position="FooterPaymentLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <cms:pageSlot position="Footer" var="feature" element="div" class="logo hidden-md hidden-lg">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
  
    </div>
    
    <div class="col-sm-12 col-md-3 pull-left copyright margint20">
    
   
    <c:url var="link" value="/misc/vat-pdf"/>
    <a href="${link}" target="_blank" class="pull-right">
	    <cms:pageSlot position="ZakatFooter" var="feature" element="div" class="logovat btn-block">
	        <cms:component component="${feature}" class="btn-block"/>
	    </cms:pageSlot>
	    
    </a>
    <div class="pull-right">
     <cms:pageSlot position="QRFooter" var="feature" element="div" class="logovat btn-block ">
	        <cms:component component="${feature}" class="btn-block"/>
	</cms:pageSlot>
    </div>
     
   
    
    
     <cms:pageSlot position="CopyrightFooter" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    </div>
    
    
    
   
</footer>

