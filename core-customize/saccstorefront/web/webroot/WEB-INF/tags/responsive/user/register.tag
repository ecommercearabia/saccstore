<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="thirdpartyAuthenticationProvider" tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider"%>


<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl"/>
<spring:url value="/login/register/privacypolicy" var="getPrivacyPolicyUrl"/>

<div class="user-register__headline">
	<spring:theme code="register.new.customer" />
</div>
<p class="user-register_info">
	<spring:theme code="register.description" />
</p>

<thirdpartyAuthenticationProvider:providers providers="${supportedThirdPartyAuthenticationProvider}"/>

${requestScope.recaptchaChallangeAnswered }
<form:form method="post" modelAttribute="registerForm" action="${action}">
<div class="row">
	<div class="col-md-2 col-sm-12">
	
	<formElement:formSelectBoxDefaultEnabled idKey="register.title"
		labelKey="register.title" selectCSSClass="form-control"
		path="titleCode" mandatory="true" skipBlank="false"
		skipBlankMessageKey="form.select.none" items="${titles}" />
		</div>
		<div class="col-md-5 col-sm-12">
	<formElement:formInputBox idKey="register.firstName"
		labelKey="register.firstName" path="firstName" inputCSS="form-control"
		mandatory="true" placeholder="register.firstName" />
		</div>
		<div class="col-md-5 col-sm-12">
	<formElement:formInputBox idKey="register.lastName"
		labelKey="register.lastName" path="lastName" inputCSS="form-control"
		mandatory="true"  placeholder="register.lastName"/>
		</div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-12">
	<formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry" labelKey="register.mobileCountry" selectCSSClass="form-control countrypicker f16"
		path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
	</div>
	
	<div class="col-md-6 col-sm-12">	
	<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobileNumber" path="mobileNumber" inputCSS="form-control"
		mandatory="true" placeholder="register.mobileNumber.example" />
	</div>
	</div>
	<div class="row">
	<div class="col-md-12 col-sm-12">
	<formElement:formInputBox idKey="register.email"
		labelKey="register.email" path="email" inputCSS="form-control"
		mandatory="true" placeholder="register.email"/>
	</div>
	</div>
	<div class="row">
	<div class="col-md-6 col-sm-12">	
		<div class="pass">
	<formElement:formPasswordBox idKey="password" labelKey="register.pwd"
		path="pwd" inputCSS="form-control password-strength" mandatory="true" />
		</div>
		</div>
	<div class="col-md-6 col-sm-12">
		<div class="pass_con">
	<formElement:formPasswordBox idKey="register.checkPwd"
		labelKey="register.checkPwd" path="checkPwd" inputCSS="form-control"
		mandatory="true" /></div>
</div>
</div>
<div class="row">
	
    <c:if test="${ not empty consentTemplateData }">
       <div class="col-md-12 col-sm-12">
        <form:hidden path="consentForm.consentTemplateId" value="${consentTemplateData.id}" />
        <form:hidden path="consentForm.consentTemplateVersion" value="${consentTemplateData.version}" />
        <div class="checkbox">
            <label class="control-label uncased">
                <form:checkbox path="consentForm.consentGiven" disabled="true"/>
                <c:out value="${consentTemplateData.description}" />

            </label>
        </div>
		<div class="help-block">
			<spring:theme code="registration.consent.link" />
		</div>
</div>
    </c:if>

<div class="col-md-12 col-sm-12">
	<spring:theme code="register.termsConditions" arguments="${getTermsAndConditionsUrl}###${getPrivacyPolicyUrl}" var="termsConditionsHtml" argumentSeparator="###"  htmlEscape="false" />
	
							
							
	<template:errorSpanField path="termsCheck">
		<div class="checkbox">
			<label class="control-label uncased">
				<form:checkbox id="registerChkTermsConditions" path="termsCheck" disabled="true"/>
				${ycommerce:sanitizeHTML(termsConditionsHtml)}
			</label>
		</div>
	</template:errorSpanField>
</div>

<div class="col-md-12 col-sm-12">
	<spring:theme code="register.otpConsentCheck" htmlEscape="false" var="otpConsentHtml"/>
	<template:errorSpanField path="otpConsentCheck">
		<div class="checkbox">
			<label class="control-label uncased">
				<form:checkbox id="registerOTPConsentCheck" path="otpConsentCheck" />
				${ycommerce:sanitizeHTML(otpConsentHtml)}
			</label>
		</div>
	</template:errorSpanField>
</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12">
	<div class="form-actions clearfix btn_register">
		<ycommerce:testId code="register_Register_button">
			<button type="submit" class="btn btn-default btn-block">
				<spring:theme code='${actionNameKey}' />
			</button>
		</ycommerce:testId>
	</div>
	</div>
	</div>
</form:form>
