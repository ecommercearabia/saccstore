<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="urlLink" required="false" type="java.lang.String"%>
<%@ attribute name="medias" required="false" type="java.util.List" %>
<%@ attribute name="header" required="false" type="java.lang.String"%>
<%@ attribute name="content" required="false" type="java.lang.String"%>
<%@ attribute name="linkTarget" required="false" type="java.lang.String"%>
<%@ attribute name="youtubeId" required="false" type="java.lang.String"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="enableCounter" required="false" type="java.lang.Boolean"%>
<%@ attribute name="message" required="false" type="java.lang.String"%>
<%@ attribute name="endDate" required="false" type="java.util.Date"%>
<%@ attribute name="startDate" required="false" type="java.util.Date"%>

			<input type="hidden" value="${enableCounter}"/>
				<input type="hidden" value="${endDate}"/>
				<input type="hidden" value="${startDate}"/>
				<input type="hidden" value="${message}"/>
				<fmt:setLocale value="en_US" scope="session"/>
				<jsp:useBean id="curretntDate" class="java.util.Date" scope="request" />

<c:set var="defult" value=""/>
<c:forEach items="${medias}" var="media">
	<c:choose>
		<c:when test="${empty imagerData}">
			<c:set var="imagerData">"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="defult" value="${media.url}"/>
			<c:set var="imagerData">${imagerData},"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
		</c:otherwise>
	</c:choose>
		<c:set var="img" value="${ycommerce:encodeJSON(media.url)}"/>
	<c:if test="${empty altText}">
		<c:set var="altTextHtml" value="${fn:escapeXml(media.altText)}"/>
	</c:if>
</c:forEach>

<c:url value="${urlLink}" var="simpleResponsiveBannerUrl" />
<c:choose >
	<c:when test="${not empty youtubeId}">
	<c:set var="rand" value="${Math.random() *  Math.random() * 1000000000 + loop.index}"></c:set>
		<fmt:parseNumber var = "i" integerOnly = "true" type = "number" value = "${rand}" />
		<div class="fack">
			<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' title='' alt='' style="">
		<div class="player class" style="    position: absolute; z-index:100;
    top: 0;
    overflow: hidden;
    bottom: 0;
    left: 0;
    right: 0;"></div>
		</div>
	</c:when>
		
		
		<c:otherwise>
	<div class="simple-banner banner__component--responsive">
	<c:set var="imagerDataJson" value="{${imagerData}}"/>
	<c:choose>
		<c:when test="${empty simpleResponsiveBannerUrl || simpleResponsiveBannerUrl eq '#'}">
			<c:if test="${enableCounter}">

<c:set var = "end" value = "${endDate}" />
<c:set var = "start" value = "${startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
	<div class="topbanner-counter"> 
		<div class="topbanner-counter-text">
		<span> ${message}</span>
		</div>
		<fmt:setLocale value="en_US" scope="session"/>
					<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
	</div>
				</c:if> 

</c:if> 
			
			<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' alt='' title='' style="">
		</c:when>
		<c:otherwise>
			<a href="${fn:escapeXml(simpleResponsiveBannerUrl)}">
				<c:if test="${enableCounter}">

<c:set var = "end" value = "${endDate}" />
<c:set var = "start" value = "${startDate}" />

<c:set var="endFormattedDate" ><fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" /></c:set> 
<c:set var="startFormattedDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS"  value = "${start}" /></c:set>
<c:set var="newcurretntDate" ><fmt:formatDate pattern ="yyyy-MM-dd hh:mm:ss.SSS" value = "${curretntDate}" /></c:set>

<c:if test="${(newcurretntDate gt startFormattedDate)  && ( newcurretntDate lt endFormattedDate)}">
	<div class="topbanner-counter"> 
		<div class="topbanner-counter-text">
		<span> ${message}</span>
		</div>
		<fmt:setLocale value="en_US" scope="session"/>
					<div class="countdown" data-countdown='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss.SSS" value = "${end}" />'></div>
	</div>
				</c:if> 

</c:if>
				<img src="${defult}" class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' title='' alt='' style="">
			</a>
		</c:otherwise>
	</c:choose>
</div>
</c:otherwise>


</c:choose>

