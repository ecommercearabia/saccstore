/**
 *
 */
package com.sacc.saccstorefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("customUpdatePasswordFormValidator")
public class UpdatePasswordFormValidator implements Validator
{
	private static final String PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";

	private static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePasswordForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdatePwdForm updatePasswordForm = (UpdatePwdForm) object;
		final String newPassword = updatePasswordForm.getPwd();
		final String checkPassword = updatePasswordForm.getCheckPwd();

		if (StringUtils.isNotEmpty(newPassword) && StringUtils.isNotEmpty(checkPassword)
				&& !StringUtils.equals(newPassword, checkPassword))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else if (StringUtils.isEmpty(checkPassword))
		{
			errors.rejectValue("checkPwd", "updatePwd.checkPwd.invalid");
		}
		else if (!isValidPassword(newPassword))
		{
			errors.rejectValue("pwd", "updatePwd.pwd.invalid");
		}
	}

	private boolean isValidPassword(final String password)
	{
		final Matcher matcher = PASSWORD_PATTERN.matcher(password);
		return matcher.matches();
	}
}
