/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validator for password forms.
 */
@Component("customPasswordValidator")
public class PasswordValidator implements Validator
{
	private static final String UPDATE_PWD_INVALID = "updatePwd.pwd.invalid";

	private static final String PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";

	private static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePasswordForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdatePasswordForm passwordForm = (UpdatePasswordForm) object;
		final String currPasswd = passwordForm.getCurrentPassword();
		final String newPasswd = passwordForm.getNewPassword();
		final String checkPasswd = passwordForm.getCheckNewPassword();

		if (StringUtils.isEmpty(currPasswd))
		{
			errors.rejectValue("currentPassword", "profile.currentPassword.invalid");
		}
		if (StringUtils.isEmpty(newPasswd))
		{
			errors.rejectValue("newPassword", UPDATE_PWD_INVALID);
		}
		else if (!isValidPassword(newPasswd))
		{
			errors.rejectValue("newPassword", UPDATE_PWD_INVALID);
		}
		if (StringUtils.isEmpty(checkPasswd))
		{
			errors.rejectValue("checkNewPassword", UPDATE_PWD_INVALID);
		}

	}

	private boolean isValidPassword(final String password)
	{
		final Matcher matcher = PASSWORD_PATTERN.matcher(password);
		return matcher.matches();
	}
}
