/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.controllers.pages;

import static de.hybris.platform.commercefacades.constants.CommerceFacadesConstants.CONSENT_GIVEN;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ConsentForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.commercefacades.consent.data.AnonymousConsentData;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sacc.core.model.MerchantTransactionModel;
import com.sacc.core.service.CustomCartService;
import com.sacc.core.service.CustomOrderService;
import com.sacc.core.service.MerchantTransactionService;
import com.sacc.saccstorefront.controllers.ControllerConstants;
import com.sacc.saccwishlistfacade.data.MetaData;
import com.sacc.saccwishlistfacade.data.ResponseData;



/**
 * CheckoutController
 */
@Controller
@RequestMapping(value = "/checkout")
public class CheckoutController extends AbstractCheckoutController
{
	private static final Logger LOG = LoggerFactory.getLogger(CheckoutController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String MERCHANT_ID_PATH_VARIABLE_PATTERN = "{merchantId:.*}";

	private static final String THE_RESPONSE_RETURNED_SUCCESSFULLY = "The response returned successfully";

	protected static final String REDIRECT_URL_ERROR = REDIRECT_PREFIX + "/checkout/multi/hop/error";
	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";
	private static final String CHECKOUT_ORDER_PENDING_CMS_PAGE_LABEL = "orderPending";

	private static final String CONTINUE_URL_KEY = "continueUrl";
	private static final String CONSENT_FORM_GLOBAL_ERROR = "consent.form.global.error";

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "guestRegisterValidator")
	private GuestRegisterValidator guestRegisterValidator;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "consentFacade")
	protected ConsentFacade consentFacade;

	@Resource(name = "customerConsentDataStrategy")
	protected CustomerConsentDataStrategy customerConsentDataStrategy;

	@Resource(name = "defaultCartServiceForAccelerator")
	protected CustomCartService customCartService;

	@Resource(name = "cartConverter")
	private Converter<CartModel, CartData> cartConverter;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;


	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String checkout(final RedirectAttributes redirectModel)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			if (validateCart(redirectModel))
			{
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				checkoutFacade.prepareCartForCheckout();
				return getCheckoutRedirectUrl();
			}
		}

		LOG.info("Missing, empty or unsupported cart");

		// No session cart or empty session cart. Bounce back to the cart page.
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderConfirmation(@PathVariable("orderCode")
	final String orderCode, final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		return processOrderCode(orderCode, model, request, redirectModel);
	}

	@RequestMapping(value = "/orderPending/" + MERCHANT_ID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderPending(@PathVariable("merchantId")
	final String merchantId, final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(merchantId))
		{
			LOG.warn("merchantId is empty or null");
			//  you can add message
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.checkout.merchant.id",
			//					null);
			return FORWARD_PREFIX + "/404";

		}
		// we need to check the below method after finished
		//		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		CartData cartData = null;

		// First try to get the cart by the merchant id,

		final List<CartModel> cartByMerchantTransactionId = customCartService.getCartByMerchantTransactionId(merchantId);
		if (cartByMerchantTransactionId.isEmpty())
		{
			LOG.info("Attempted to load an cart Pending that does not exist or is not visible.");
			LOG.info("Checking if the cart is already changed into order");

			final String orderCode = merchantId.split("_")[0];

			final OrderModel order = getCustomOrderService().getOrderForCode(orderCode);//getOrderDataByOrderCode(orderCode);
			if (order != null)
			{
				return REDIRECT_PREFIX + "/checkout/orderConfirmation/" + orderCode;
			}


			LOG.info("No Order found for merchantId[{}] Redirect to home page", merchantId);
			LOG.info("Trying to get MerchantTransactionModel for merchantTransactionId[{}]", merchantId);
			final MerchantTransactionModel merchantTransactionById = getMerchantTransactionService()
					.getMerchantTransactionById(merchantId);

			if (merchantTransactionById != null && Strings.isNotBlank(merchantTransactionById.getDecision())
					&& Strings.isNotBlank(merchantTransactionById.getResultCode()))
			{
				getMerchantTransactionService().removeMerchantTransaction(merchantTransactionById);

				return REDIRECT_URL_ERROR + "/?decision=" + merchantTransactionById.getDecision() + "&reasonCode="
						+ merchantTransactionById.getResultCode();
			}

			LOG.info("Order is still being proccessed by the webhook");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.checkout.merchant.id",
					null);
			return REDIRECT_PREFIX + ROOT;
		}


		final MerchantTransactionModel merchantTransactionById = getMerchantTransactionService()
				.getMerchantTransactionById(merchantId);

		if (merchantTransactionById != null && Strings.isNotBlank(merchantTransactionById.getDecision())
				&& Strings.isNotBlank(merchantTransactionById.getResultCode()))
		{

			LOG.info("Found an error with merchantId[{}], redirecting to error page", merchantId);
			getMerchantTransactionService().removeMerchantTransaction(merchantTransactionById);

			return REDIRECT_URL_ERROR + "/?decision=" + merchantTransactionById.getDecision() + "&reasonCode="
					+ merchantTransactionById.getResultCode();
		}


		cartData = cartConverter.convert(cartByMerchantTransactionId.get(0));
		//		cartData = getCheckoutFacade().getCheckoutCart();

		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
				entry.setProduct(product);
			}
		}
		model.addAttribute("merchantId", merchantId.split("_")[0]);
		model.addAttribute("orderCode", cartData.getCode());
		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

		final List<CouponData> giftCoupons = cartData.getAppliedOrderPromotions().stream()
				.filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).flatMap(p -> p.getGiveAwayCouponCodes().stream())
				.collect(Collectors.toList());
		model.addAttribute("giftCoupons", giftCoupons);

		//processEmailAddress(model, cartData);

		final String continueUrl = (String) getSessionService().getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		final ContentPageModel checkoutOrderConfirmationPage = getContentPageForLabelOrId(CHECKOUT_ORDER_PENDING_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, checkoutOrderConfirmationPage);
		setUpMetaDataForContentPage(model, checkoutOrderConfirmationPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}


	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String orderConfirmation(final GuestRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getGuestRegisterValidator().validate(form, bindingResult);
		return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel);
	}

	protected String processRegisterGuestUserRequest(final GuestRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			form.setTermsCheck(false);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return processOrderCode(form.getOrderCode(), model, request, redirectModel);
		}
		try
		{
			getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode());
			getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
			getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
		}
		catch (final DuplicateUidException e)
		{
			// User already exists
			LOG.debug("guest registration failed.");
			form.setTermsCheck(false);
			model.addAttribute(new GuestRegisterForm());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"guest.checkout.existingaccount.register.error", new Object[]
					{ form.getUid() });
			return REDIRECT_PREFIX + request.getHeader("Referer");
		}

		// Consent form data
		try
		{
			final ConsentForm consentForm = form.getConsentForm();
			if (consentForm != null && consentForm.getConsentGiven())
			{
				getConsentFacade().giveConsent(consentForm.getConsentTemplateId(), consentForm.getConsentTemplateVersion());
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error occurred while creating consents during registration", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CONSENT_FORM_GLOBAL_ERROR);
		}

		// save anonymous-consent cookies as ConsentData
		final Cookie cookie = WebUtils.getCookie(request, WebConstants.ANONYMOUS_CONSENT_COOKIE);
		if (cookie != null)
		{
			try
			{
				final ObjectMapper mapper = new ObjectMapper();
				final List<AnonymousConsentData> anonymousConsentDataList = Arrays.asList(mapper.readValue(
						URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8.displayName()), AnonymousConsentData[].class));
				anonymousConsentDataList.stream().filter(consentData -> CONSENT_GIVEN.equals(consentData.getConsentState()))
						.forEach(consentData -> consentFacade.giveConsent(consentData.getTemplateCode(),
								Integer.valueOf(consentData.getTemplateVersion())));
			}
			catch (final UnsupportedEncodingException e)
			{
				LOG.error(String.format("Cookie Data could not be decoded : %s", cookie.getValue()), e);
			}
			catch (final IOException e)
			{
				LOG.error("Cookie Data could not be mapped into the Object", e);
			}
			catch (final Exception e)
			{
				LOG.error("Error occurred while creating Anonymous cookie consents", e);
			}
		}

		customerConsentDataStrategy.populateCustomerConsentDataInSession();

		return REDIRECT_PREFIX + "/";
	}

	protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final OrderData orderDetails;

		try
		{
			orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load an order confirmation that does not exist or is not visible. Redirect to home page.");
			return REDIRECT_PREFIX + ROOT;
		}

		addRegistrationConsentDataToModel(model);

		if (orderDetails.isGuestCustomer() && !StringUtils.substringBefore(orderDetails.getUser().getUid(), "|")
				.equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID)))
		{
			return getCheckoutRedirectUrl();
		}

		if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : orderDetails.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
				entry.setProduct(product);
			}
		}

		model.addAttribute("orderCode", orderCode);
		model.addAttribute("orderData", orderDetails);
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
		model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
		model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

		final List<CouponData> giftCoupons = orderDetails.getAppliedOrderPromotions().stream()
				.filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).flatMap(p -> p.getGiveAwayCouponCodes().stream())
				.collect(Collectors.toList());
		model.addAttribute("giftCoupons", giftCoupons);

		processEmailAddress(model, orderDetails);

		final String continueUrl = (String) getSessionService().getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		final ContentPageModel checkoutOrderConfirmationPage = getContentPageForLabelOrId(
				CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, checkoutOrderConfirmationPage);
		setUpMetaDataForContentPage(model, checkoutOrderConfirmationPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		if (ResponsiveUtils.isResponsive())
		{
			return getViewForPage(model);
		}

		return ControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
	}

	protected void processEmailAddress(final Model model, final CartData cartData)
	{
		String uid;

		if (cartData.isThirdPartyCustomer())
		{
			uid = cartData.getUser().getUid();
			LOG.info("uid IS: {}", uid);
			uid = uid.substring(uid.lastIndexOf('|') + 1);
		}
		else
		{
			uid = cartData.getUser().getUid();
		}
		model.addAttribute("email", uid);
	}

	protected void processEmailAddress(final Model model, final OrderData orderDetails)
	{
		String uid;

		if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm"))
		{
			final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
			guestRegisterForm.setOrderCode(orderDetails.getGuid());
			uid = orderDetails.getUser().getUid();
			LOG.info("uid IS: {}", uid);
			uid = uid.substring(uid.lastIndexOf('|') + 1);
			guestRegisterForm.setUid(uid);
			model.addAttribute("guestRegisterForm", guestRegisterForm);
		}
		else if (orderDetails.isThirdPartyCustomer())
		{
			uid = orderDetails.getUser().getUid();
			LOG.info("uid IS: {}", uid);
			uid = uid.substring(uid.lastIndexOf('|') + 1);
		}
		else
		{
			uid = orderDetails.getUser().getUid();
		}
		model.addAttribute("email", uid);
	}

	protected GuestRegisterValidator getGuestRegisterValidator()
	{
		return guestRegisterValidator;
	}

	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}

	/**
	 * @return the customOrderService
	 */
	public CustomOrderService getCustomOrderService()
	{
		return customOrderService;
	}

	@RequestMapping(value = "/orderPending/isconvert/" + MERCHANT_ID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@ResponseBody()
	public ResponseData isConvertOrderPendingToOrder(@PathVariable("merchantId")
	final String merchantId, final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		LOG.info("isConvertOrderPendingToOrder={}", merchantId);
		final ResponseData data = new ResponseData();

		final MetaData metaData = new MetaData();
		try
		{
			final OrderModel order = getCustomOrderService().getOrderForCode(merchantId);//getOrderDataByOrderCode(orderCode);
			if (order != null)
			{
				LOG.info("Order with merchantTransactionid[{}] placed and Found redirecting to order confirmation page", merchantId);
				data.setData(true);
				metaData.setDisplayMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
				metaData.setMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
				metaData.setStatusCode(HttpStatus.SC_OK);
			}
			else
			{

				LOG.info("Couldn't find order with merchanttransactionId[{}]", merchantId);
				final MerchantTransactionModel merchantTransactionById = getMerchantTransactionService()
						.getMerchantTransactionById(merchantId);

				if (merchantTransactionById != null && Strings.isNotBlank(merchantTransactionById.getDecision())
						&& Strings.isNotBlank(merchantTransactionById.getResultCode()))
				{
					data.setData(true);
					metaData.setDisplayMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
					metaData.setMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
					metaData.setStatusCode(HttpStatus.SC_OK);
				}
				else
				{
					data.setData(false);
					metaData.setDisplayMessage("order note found");
					metaData.setMessage("order note found");
					metaData.setStatusCode(HttpStatus.SC_NOT_FOUND);
				}

			}

		}
		catch (final Exception e)
		{
			final MerchantTransactionModel merchantTransactionById = getMerchantTransactionService()
					.getMerchantTransactionById(merchantId);

			if (merchantTransactionById != null && Strings.isNotBlank(merchantTransactionById.getDecision())
					&& Strings.isNotBlank(merchantTransactionById.getResultCode()))
			{
				LOG.info("Found an error with merchantId[{}], redirecting to error page", merchantId);
				data.setData(true);
				metaData.setDisplayMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
				metaData.setMessage(THE_RESPONSE_RETURNED_SUCCESSFULLY);
				metaData.setStatusCode(HttpStatus.SC_OK);
			}
			else
			{
				data.setData(false);
				metaData.setDisplayMessage("order note found");
				metaData.setMessage("order note found");
				metaData.setStatusCode(HttpStatus.SC_NOT_FOUND);
			}

		}
		data.setMeta(metaData);

		return data;
	}

}
