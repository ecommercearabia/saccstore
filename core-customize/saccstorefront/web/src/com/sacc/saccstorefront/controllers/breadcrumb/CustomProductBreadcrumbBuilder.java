/**
 *
 */
package com.sacc.saccstorefront.controllers.breadcrumb;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.category.model.CategoryModel;


/**
 * @author mnasro
 *
 */
public class CustomProductBreadcrumbBuilder extends ProductBreadcrumbBuilder
{

	@Override
	protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category)
	{
		final String categoryUrl = getCategoryModelUrlResolver().resolve(category);
		return new Breadcrumb(categoryUrl, category.getDisplayName(), null, category.getCode());
	}
}
