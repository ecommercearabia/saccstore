/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.PaymentModeService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccstorefront.controllers.ControllerConstants;
import com.sacc.saccstorefront.form.PaymentDetailsForm;
import com.sacc.saccuserfacades.user.facade.CustomUserFacade;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final Map<String, String> CYBERSOURCE_SOP_CARD_TYPES = new HashMap<>();
	private static final Gson GSON = new GsonBuilder().create();
	private static final String PAYMENT_METHOD = "payment-method";
	private static final String CART_DATA_ATTR = "cartData";
	private static final String SUPPORTED_PAYMENT_MODES_ATTR = "supportedPaymentModes";

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodCheckoutStepController.class);

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	@Resource(name = "userFacade")
	private CustomUserFacade customUserFacade;

	protected CustomUserFacade getCustomUserFacade()
	{
		return customUserFacade;
	}

	@RequestMapping(value = "/create-checkoutid/applepay", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Object getApplePayCheckoutId()
	{
		// TODO: TO be discussed with nasro
		final String paymentModeCode = "applepay";
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final AddressData addressData = cartData.getDeliveryAddress();
		addressData.setShippingAddress(Boolean.TRUE);
		addressData.setBillingAddress(Boolean.TRUE);
		getAddressVerificationFacade().verifyAddressData(addressData);
		getCheckoutFacade().saveBillingAddress(addressData);
		getCheckoutFacade().setPaymentMode(paymentModeCode);

		final Pair<Optional<PaymentRequestData>, Boolean> supportedPaymentData = getCheckoutFacade().getSupportedPaymentData();
		if (Boolean.FALSE.equals(supportedPaymentData.getRight()))
		{
			return GSON.toJson(supportedPaymentData.getLeft().get());
		}
		LOGGER.error("Could not create checkout session");
		return "";
	}


	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getCheckoutFacade().setDeliveryModeIfAvailable();
		setupAddPaymentPage(model);

		setCheckoutStepLinksForModel(model, getCheckoutStep());
		setupSilentOrderPostPage(model);

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	protected void setupSilentOrderPostPage(final Model model)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModes();

		if (supportedPaymentModes.isPresent())
		{
			model.addAttribute(SUPPORTED_PAYMENT_MODES_ATTR, supportedPaymentModes.get());
		}

		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		paymentDetailsForm.setPaymentModeCode(cartData.getPaymentMode() != null ? cartData.getPaymentMode().getCode() : null);
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);
	}

	protected boolean checkPaymentSubscription(final Model model, final PaymentDetailsForm paymentDetailsForm,
			final NoCardPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getId()))
		{
			if (paymentDetailsForm.getSaveInAccount() && getCustomUserFacade().getNoCardPaymentInfos(true).isPresent()
					&& getCustomUserFacade().getNoCardPaymentInfos(true).get().size() <= 1)
			{
				getCustomUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setGeneralPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return false;
		}
		return true;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid
	final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			setupSilentOrderPostPage(model);
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final String paymentModeCode = paymentDetailsForm.getPaymentModeCode();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final AddressData addressData = cartData.getDeliveryAddress();

		addressData.setShippingAddress(Boolean.TRUE);
		addressData.setBillingAddress(Boolean.TRUE);

		getAddressVerificationFacade().verifyAddressData(addressData);

		final PaymentModeModel paymentModeModel = paymentModeService.getPaymentModeForCode(paymentModeCode);

		switch (paymentModeModel.getPaymentModeType())
		{
			case PIS:

			case NOCARD:

			case CONTINUE:
				final NoCardPaymentInfoData noCardPaymentInfoData = new NoCardPaymentInfoData();
				final NoCardTypeData noCardTypeData = new NoCardTypeData();
				noCardTypeData.setCode(paymentDetailsForm.getPaymentModeCode().toUpperCase());
				noCardPaymentInfoData.setNoCardTypeData(noCardTypeData);
				noCardPaymentInfoData.setDefaultPaymentInfo(true);
				noCardPaymentInfoData.setBillingAddress(addressData);
				noCardPaymentInfoData.setSaved(true);

				final Optional<NoCardPaymentInfoData> createPaymentSubscription = getCheckoutFacade()
						.createPaymentSubscription(noCardPaymentInfoData);

				if (createPaymentSubscription.isPresent())
				{

				}

				if (!checkPaymentSubscription(model, paymentDetailsForm, createPaymentSubscription.get()))
				{
					setupSilentOrderPostPage(model);
					return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
				}

				getCheckoutFacade().saveBillingAddress(addressData);
				model.addAttribute("paymentId", noCardPaymentInfoData.getId());

				break;

			case CARD:

				getCheckoutFacade().saveBillingAddress(addressData);

				//				final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
				//				fillInPaymentData(paymentDetailsForm, ccPaymentInfoData);
				//				ccPaymentInfoData.setBillingAddress(addressData);
				//
				//				final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(ccPaymentInfoData);
				//				if (!checkPaymentSubscription(model, paymentDetailsForm, newPaymentSubscription))
				//				{
				//					return ControllerConstants..MultiStepCheckout.AddPaymentMethodPage;
				//				}
				//				model.addAttribute("paymentId", ccPaymentInfoData.getId());

				//				cartService.getSessionCart();


				break;

			default:
				break;
		}
		getCheckoutFacade().setPaymentMode(paymentDetailsForm.getPaymentModeCode());
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}



	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}


}
