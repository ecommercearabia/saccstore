/**
 *
 */
package com.sacc.saccstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sacc.saccstorefront.form.ThirdPartyProfileForm;
import com.sacc.saccstorefront.form.validation.ThirdPartyProfileValidator;
import com.sacc.saccstorefront.security.ThirdPartyAutoLoginStrategy;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.sacc.saccthirdpartyauthentication.service.ThirdPartyUserService;
import com.sacc.saccuserfacades.country.facade.CountryFacade;
import com.sacc.saccuserfacades.customer.facade.CustomCustomerFacade;



/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/third-party")
public class ThirdPartyProfileController extends AbstractSearchPageController
{

	private static final String THIRD_PARTY_USER = "ThirdPartyUser";
	private static final String TOKEN = "token";
	private static final String TYPE = "type";
	private static final String EXISTS = "exists";


	private static final Logger LOG = Logger.getLogger(AccountPageController.class);

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	@Resource(name = "thirdPartyProfileValidator")
	private ThirdPartyProfileValidator profileValidator;

	@Resource(name = "thirdPartyUserService")
	private ThirdPartyUserService thirdPartyUserService;

	@Resource(name = "thirdPartyAutoLoginStrategy")
	private ThirdPartyAutoLoginStrategy autoLoginStrategy;

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
	private static final String UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE = "third-party-profile";
	private static final String TITLE_DATA_ATTR = "titleData";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";


	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	public String getUpdateProfileView(final Model model, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final String redirectUrl = "";

		final Map<String, Object> sessionAttributes = extractSessionAttributes();
		if (sessionAttributes.isEmpty())
		{
			return REDIRECT_PREFIX + "/login";
		}
		final ThirdPartyAuthenticationUserData profile = (ThirdPartyAuthenticationUserData) sessionAttributes.get(THIRD_PARTY_USER);
		final String token = String.valueOf(sessionAttributes.get(TOKEN));
		final String type = String.valueOf(sessionAttributes.get(TYPE));
		final Boolean exists = (Boolean) sessionAttributes.get(EXISTS);

		final CustomerData customer = null;
		if (Boolean.TRUE.equals(exists))
		{
			final String username = profile.getEmail() + "|" + profile.getId();

			autoLoginStrategy.login(username, token, type, request, response);

			LOG.info(profile.getEmail() + " is authenticated successfully!");
		}

		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());

		final ThirdPartyProfileForm profileForm = new ThirdPartyProfileForm();
		profileForm.setFirstName(profile.getFirstName());
		profileForm.setLastName(profile.getLastName());
		profileForm.setEmail(profile.getEmail());
		profileForm.setMobileNumber(profile.getMobileNumber());

		model.addAttribute("thirdPartyProfileForm", profileForm);
		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		if (profileForm.getEmail() == null || profileForm.getMobileCountry() == null || profileForm.getMobileCountry() == null
				|| profileForm.getFirstName() == null || profileForm.getLastName() == null)
		{
			return getViewForPage(model);
		}
		else
		{
			return REDIRECT_PREFIX + "/";
		}
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	public String updateProfileView(final ThirdPartyProfileForm profile, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		profile.setTitleCode("mr");
		final Map<String, Object> sessionAttributes = extractSessionAttributes();
		if (Collections.EMPTY_MAP.equals(sessionAttributes))
		{
			return "";
		}

		final ThirdPartyAuthenticationUserData sessionedProfile = (ThirdPartyAuthenticationUserData) sessionAttributes
				.get(THIRD_PARTY_USER);

		if (sessionedProfile != null)
		{
			if (StringUtils.isBlank(profile.getFirstName()))
			{
				profile.setFirstName(sessionedProfile.getFirstName());
			}
			if (StringUtils.isBlank(profile.getLastName()))
			{
				profile.setLastName(sessionedProfile.getLastName());
			}
			if (StringUtils.isBlank(profile.getEmail()))
			{
				profile.setEmail(sessionedProfile.getEmail());
			}
		}
		profileValidator.validate(profile, bindingResult);
		String returnedAction = "";
		if (bindingResult.hasErrors())
		{
			returnedAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
			return returnedAction;
		}

		returnedAction = REDIRECT_PREFIX + "/";

		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(profile.getTitleCode());
		customerData.setFirstName(profile.getFirstName());
		customerData.setLastName(profile.getLastName());
		final CountryData mobileCountry = new CountryData();
		mobileCountry.setIsocode(profile.getMobileCountry());
		customerData.setMobileCountry(mobileCountry);
		customerData.setMobileNumber(profile.getMobileNumber());
		customerData.setCustomerId(sessionedProfile.getId());
		customerData.setUid(sessionedProfile.getId() + "|" + sessionedProfile.getEmail());
		customerData.setName(sessionedProfile.getName());
		final ThirdPartyAuthenticationType thirdPartyType = ThirdPartyAuthenticationType
				.valueOf(String.valueOf(sessionAttributes.get(TYPE)));
		customerData.setThirdPartyType(thirdPartyType);
		final Boolean exists = (Boolean) sessionAttributes.get(EXISTS);
		try
		{
			if (Boolean.TRUE.equals(exists))
			{
				customerFacade.updateProfile(customerData);
			}
			else
			{
				thirdPartyUserService.saveUser(customerData);
			}
			final String token = (String) sessionAttributes.get(TOKEN);
			final String type = (String) sessionAttributes.get(TYPE);

			autoLoginStrategy.login(customerData.getUid(), token, type, request, response);

			LOG.info(profile.getEmail() + " is authenticated successfully!");
			LOG.info(profile.getEmail() + " is authenticated successfully!");
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.profile.confirmationUpdated", null);
		}
		catch (final DuplicateUidException e)
		{
			LOG.error(e.getMessage());
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			returnedAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
			return returnedAction;
		}

		getSessionService().removeAttribute(EXISTS);
		getSessionService().removeAttribute(TOKEN);
		getSessionService().removeAttribute(TYPE);
		getSessionService().removeAttribute(THIRD_PARTY_USER);

		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);
		model.addAttribute("thirdPartyProfileForm", profile);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return returnedAction;
	}

	@ModelAttribute("mobileCountries")
	public Collection<CountryData> getMobileCountries()
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		return mobileCountries.isPresent() ? mobileCountries.get() : null;
	}

	protected String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
		final ContentPageModel cmsPage = getContentPageForLabelOrId(cmsPageLabelOrId);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, cmsPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}

	private boolean checkCustomerAttributes(final CustomerData customer)
	{
		if (StringUtils.isNotBlank(customer.getUid()))
		{
			final String email = customer.getUid().split("\\|")[1];
			if (StringUtils.isBlank(email))
			{
				return false;
			}
		}
		if (StringUtils.isBlank(customer.getFirstName()))
		{
			return false;
		}
		if (StringUtils.isBlank(customer.getLastName()))
		{
			return false;
		}
		if (customer.getMobileCountry() == null)
		{
			return false;
		}
		if (StringUtils.isBlank(customer.getMobileNumber()))
		{
			return false;
		}

		return true;
	}

	private Map<String, Object> extractSessionAttributes()
	{
		final Map<String, Object> sessionAttributes = getSessionService().getAllAttributes();
		if (!sessionAttributes.containsKey(THIRD_PARTY_USER))
		{
			return Collections.EMPTY_MAP;
		}
		if (!sessionAttributes.containsKey(TOKEN))
		{
			return Collections.EMPTY_MAP;
		}
		if (!sessionAttributes.containsKey(TYPE))
		{
			return Collections.EMPTY_MAP;
		}
		if (!sessionAttributes.containsKey(EXISTS))
		{
			return Collections.EMPTY_MAP;
		}
		return sessionAttributes;
	}

}
