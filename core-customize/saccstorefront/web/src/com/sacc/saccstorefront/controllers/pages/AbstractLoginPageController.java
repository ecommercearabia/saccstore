package com.sacc.saccstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.GenericSearchConstants.LOG;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.DisabledException;
import org.springframework.ui.Model;

import com.sacc.saccstorefront.filters.exception.ReCaptchaFailedException;


/**
 * Abstract base class for login page controllers
 */
public abstract class AbstractLoginPageController extends AbstractRegisterPageController
{
	private static final Logger LOG = Logger.getLogger(AbstractLoginPageController.class);

	protected static final String SPRING_SECURITY_LAST_USERNAME = "SPRING_SECURITY_LAST_USERNAME";

	protected static final String SPRING_SECURITY_LAST_EXCEPTION = "SPRING_SECURITY_LAST_EXCEPTION";

	@Resource(name = "userService")
	private UserService userService;

	protected String getDefaultLoginPage(final boolean loginError, final HttpSession session, final Model model)
			throws CMSItemNotFoundException
	{
		final LoginForm loginForm = new LoginForm();
		model.addAttribute(loginForm);
		model.addAttribute(getNewRegisterForm());
		model.addAttribute(new GuestForm());

		final String username = (String) session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
		if (username != null)
		{
			session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);
		}

		loginForm.setJ_username(username);
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);
		model.addAttribute("attempts", session.getAttribute("attempts"));
		addRegistrationConsentDataToModel(model);

		final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage("header.link.login", null, "header.link.login", getI18nService().getCurrentLocale()),
				null);
		model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));

		if (loginError)
		{
			model.addAttribute("loginError", Boolean.valueOf(loginError));
			final Object exception = session.getAttribute(SPRING_SECURITY_LAST_EXCEPTION);
			LOG.info("AbstractLoginPageController : Login Exception is " + exception);
			if (isUserBlocked(username, exception))
			{
				LOG.info("AbstractLoginPageController : DisabledException: User " + username + " got blocked from logging in");
				GlobalMessages.addErrorMessage(model, "login.error.account.is.disabled.title");
			}
			if (exception instanceof ReCaptchaFailedException)
			{
				LOG.info("AbstractLoginPageController : ReCaptchaFailedException: "
						+ ((ReCaptchaFailedException) exception).getMessage());
				GlobalMessages.addErrorMessage(model, "login.error.recaptcha");
			}
			else
			{
				LOG.info("AbstractLoginPageController : BadCredentialsException");
				GlobalMessages.addErrorMessage(model, "login.error.account.not.found.title");
			}
		}

		return getView();
	}

	private boolean isUserBlocked(final String username, final Object exception)
	{
		LOG.info("AbstractLoginPageController : Login Exception is " + exception);
		if (exception != null && exception instanceof DisabledException)
		{
			LOG.info("AbstractLoginPageController : DisabledException: User " + username + " got blocked from logging in");
			return true;
		}
		try
		{
			final UserModel user = userService.getUserForUID(username);
			if (user == null)
			{
				return false;
			}
			return user.isLoginDisabled();
		}
		catch (final Exception e)
		{
			return false;
		}
	}


}
