/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sacc.core.model.MerchantTransactionModel;
import com.sacc.core.service.CustomOrderService;
import com.sacc.core.service.MerchantTransactionService;
import com.sacc.facades.facade.CustomCheckoutFlowFacade;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.hyperpay.service.HyperpayCheckoutIdService;
import com.sacc.saccpayment.model.HyperpayCheckoutIdModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccstorefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{

	private static final Gson GSON = new GsonBuilder().create();

	private static final Logger LOG = LoggerFactory.getLogger(SummaryCheckoutStepController.class);

	private static final String SUMMARY = "summary";
	private static final String CART = "/cart";
	private static final String RESPONSE_CODE = "responseCode";
	private static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";
	private static final String REDIRECIT_TO_CHECKOUT_ORDER_PENDING = REDIRECT_PREFIX + "/checkout/orderPending/";


	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	@Resource(name = "hyperpayCheckoutIdService")
	private HyperpayCheckoutIdService hyperpayCheckoutIdService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, // NOSONAR
			CommerceCartModificationException
	{
		if (validateCart(redirectAttributes))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART;
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode, Arrays.asList(
						ProductOption.BASIC, ProductOption.PRICE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.PRICE_RANGE));
				entry.setProduct(product);
			}
		}

		final String paymentModeCode = getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode();
		if (StringUtils.equalsAnyIgnoreCase(paymentModeCode, "card", "mada", "applepay"))
		{
			final Pair<Optional<PaymentRequestData>, Boolean> paymentData = getCheckoutFacade().getSupportedPaymentData();
			if (paymentData != null && paymentData.getRight() && cartService.getSessionCart() != null)
			{
				try
				{
					final CartModel sessionCart = cartService.getSessionCart();
					return completeOrderForOrderCode(getCheckoutMap(cartService.getSessionCart()), sessionCart.getOrderCode());
				}
				catch (final InvalidCartException e)
				{
					LOG.error("invalid cart with exception: " + e.getMessage());
				}
			}

			if (paymentData != null && paymentData.getLeft().isPresent())
			{
				model.addAttribute("supportedPaymentData", paymentData.getLeft().get());
			}
		}
		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());

		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption());
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());

		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);

		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}


	/**
	 * @param sessionCart
	 */
	private Map<String, Object> getCheckoutMap(final CartModel sessionCart)
	{
		final Optional<PaymentProviderModel> supportedPaymentProvider = getCheckoutFacade().getSupportedPaymentProvider();

		if (supportedPaymentProvider.isEmpty() || Objects.isNull(sessionCart))
		{
			return MapUtils.EMPTY_MAP;
		}

		final PaymentProviderModel paymentProviderModel = supportedPaymentProvider.get();

		if (paymentProviderModel instanceof HyperpayPaymentProviderModel)
		{
			return sessionCart.getHyperpayCheckoutMap();
		}
		else if (paymentProviderModel instanceof PaymentProviderModel)
		{
			return sessionCart.getPaymentResponseMap();
		}
		return MapUtils.EMPTY_MAP;

	}


	@RequestMapping(value = "/placeOrder")
	@PreValidateQuoteCheckoutStep
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm")
	final PlaceOrderForm placeOrderForm, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException, // NOSONAR
			InvalidCartException, CommerceCartModificationException
	{
		if (validateOrderForm(placeOrderForm, model))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART;
		}

		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return enterStep(model, redirectModel);
		}

		return redirectToOrderConfirmationPage(orderData);
	}

	@RequestMapping(value = "/callback", method = RequestMethod.POST)
	@RequireHardLogIn
	public String callback(final HttpServletRequest httpServletRequest, @RequestParam(value = "encResp", required = false)
	final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCallback(httpServletRequest);
	}

	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCancelCallback(final Model model, final HttpServletRequest httpServletRequest,
			@RequestParam(value = "encResp", required = false)
			final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCallback(httpServletRequest);
	}

	@RequestMapping(value = "/callback/cancel", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCancelCallback(final Model model, final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/callback/cancel", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelCallback(final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/callback/ccavenue", method = RequestMethod.POST)
	@RequireHardLogIn
	public String postCallbackCCAvenue(final HttpServletRequest httpServletRequest,
			@RequestParam(value = "encResp", required = false)
			final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCCAvenueCallback(httpServletRequest);
	}


	@RequestMapping(value = "/callback/ccavenue", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getbackCCAvenue(final Model model, final HttpServletRequest httpServletRequest,
			@RequestParam(value = "encResp", required = false)
			final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCCAvenueCallback(httpServletRequest);
	}


	private String getDecisionAndReasonCodeString(String decision, String reason)
	{
		final String formatString = "/?decision=%s&reasonCode=%s";
		decision = StringUtils.isBlank(decision) ? "0" : decision;
		reason = StringUtils.isBlank(decision) ? "0000" : reason;
		return String.format(formatString, decision, reason);
	}

	@RequestMapping(value = "/callback/payfort/create", method = RequestMethod.POST)
	@RequireHardLogIn
	public String getPayfort3dsecureCallback(final Model model, final HttpServletRequest httpServletRequest)
			throws InvalidCartException
	{
		final Optional<PaymentResponseData> orderPaymentResponseData = getCheckoutFacade()
				.getOrderPaymentResponseData(httpServletRequest);

		if (orderPaymentResponseData.isEmpty())
		{
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString("0", "0000");
		}

		final PaymentResponseData paymentResponseData = orderPaymentResponseData.get();
		final Map<String, Object> responseData = paymentResponseData.getResponseData();

		if (PaymentResponseStatus.FAILURE.equals(paymentResponseData.getStatus()))
		{
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString("0", (String) responseData.get(RESPONSE_CODE));
		}

		if (responseData.containsKey("3dsecureUrl"))
		{

			return REDIRECT_PREFIX + responseData.get("3dsecureUrl");
		}
		else if (PaymentResponseStatus.SUCCESS.equals(paymentResponseData.getStatus()))
		{
			return completeOrder(getPayfortReturnMap(httpServletRequest));
		}

		return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString("0", (String) responseData.get(RESPONSE_CODE));
	}


	@RequestMapping(value = "/callback/payfort/return", method = RequestMethod.POST)
	@RequireHardLogIn
	public String getPayfortReturnCallback(final HttpServletRequest httpServletRequest) throws InvalidCartException
	{
		final Optional<PaymentResponseData> orderPaymentResponseData = getCheckoutFacade()
				.getOrderPaymentResponseData(httpServletRequest);

		if (orderPaymentResponseData.isEmpty())
		{
			return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=0000";
		}

		final PaymentResponseData paymentResponseData = orderPaymentResponseData.get();
		final Map<String, Object> responseData = paymentResponseData.getResponseData();

		if (PaymentResponseStatus.FAILURE.equals(paymentResponseData.getStatus()))
		{
			return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=" + responseData.get(RESPONSE_CODE);
		}


		return completeOrder(getPayfortReturnMap(httpServletRequest));
	}

	/**
	 * @param httpServletRequest
	 * @return
	 */
	private Map<String, Object> getPayfortReturnMap(final HttpServletRequest httpServletRequest)
	{
		final Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
		if (MapUtils.isEmpty(parameterMap))
		{
			return MapUtils.EMPTY_MAP;
		}
		final Map<String, Object> newMap = new LinkedHashMap<>();
		for (final Entry<String, String[]> entry : parameterMap.entrySet())
		{
			newMap.put(entry.getKey(), entry.getValue()[0]);

		}
		return newMap;
	}

	private Pair<MerchantTransactionModel, Boolean> trySaveMerchantTransaction(final String merchantTransactionId)
	{
		try
		{
			final MerchantTransactionModel merchantTrasnaction = getModelService().create(MerchantTransactionModel.class);
			final String orderCode = merchantTransactionId.split("_")[0];
			merchantTrasnaction.setId(orderCode);
			merchantTrasnaction.setSource("STOREFRONT");
			getModelService().save(merchantTrasnaction);
			return Pair.of(merchantTrasnaction, true);
		}
		catch (final ModelSavingException e)
		{
			LOG.info(e.toString());
			LOG.warn("MerchantTransactionModel already exist for {}", merchantTransactionId);
			return Pair.of(getMerchantTransactionService().getMerchantTransactionById(merchantTransactionId), false);
		}
	}


	private Pair<String, Boolean> isSuccessfulPaidOrder(final Map<String, Object> orderInfoMap)
	{
		if (!getCheckoutFacade().isSuccessfulPaidOrderByOrder())
		{
			LOG.warn("NOT_SUCCESSFUL_PAYMENT: {}", orderInfoMap);

			final Object resultObj = orderInfoMap.get("result");
			String decision = "0";
			String reason = "0000";
			if (resultObj instanceof Map && ((Map<String, Object>) resultObj).get("code") != null)
			{
				decision = "1";
				reason = (String) ((Map<String, Object>) resultObj).get("code");
			}
			return Pair.of(REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(decision, reason), true);
		}

		return Pair.of(null, false);
	}

	private String resolveCallback(final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		String merchantTransactionId = "";
		MerchantTransactionModel transaction = null;
		try
		{
			final Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
			LOG.info("[resolveCallback] parameterMap: {}", GSON.toJson(parameterMap));
			final String id = Arrays.stream(parameterMap.get("id")).collect(Collectors.joining());
			final HyperpayCheckoutIdModel checkoutIdById = getHyperpayCheckoutIdService().getCheckoutIdById(id);
			final Pair<MerchantTransactionModel, Boolean> merchantTransaction = trySaveMerchantTransaction(
					checkoutIdById.getOrderCode());
			transaction = merchantTransaction.getLeft();

			if (!merchantTransaction.getRight())
			{
				// This code should run only if the WEBHOOK is handling the payment
				final OrderModel orderForCode = getCustomOrderService().getOrderForCode(checkoutIdById.getOrderCode());
				if (orderForCode != null)
				{
					return REDIRECT_URL_ORDER_CONFIRMATION + checkoutIdById.getOrderCode();
				}

				if (transaction != null && Strings.isNotBlank(transaction.getDecision())
						&& Strings.isNotBlank(transaction.getResultCode()))
				{
					getMerchantTransactionService().removeMerchantTransaction(transaction);
					// Redirect to error page
					return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(transaction.getDecision(), transaction.getResultCode());
				}

				return REDIRECIT_TO_CHECKOUT_ORDER_PENDING + checkoutIdById.getOrderCode();
			}


			final Optional<PaymentResponseData> paymentResponseData = getCheckoutFacade().getOrderPaymentResponseData(id);


			if (paymentResponseData.isEmpty() || paymentResponseData.get().getResponseData() == null
					|| !(paymentResponseData.get().getResponseData() instanceof Map))
			{
				LOG.error("Could not get PaymentResponseData for mechant transaction id [{}]", id);
				return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString("0", "0000");
			}

			final Map<String, Object> orderInfoMap = paymentResponseData.get().getResponseData();
			final String orderInfoMapjson = GSON.toJson(orderInfoMap);
			LOG.info(orderInfoMapjson);
			merchantTransactionId = (String) orderInfoMap.get("merchantTransactionId");
			if (StringUtils.isBlank(merchantTransactionId))
			{
				if (transaction != null)
				{
					getMerchantTransactionService().removeMerchantTransaction(transaction);
				}
				LOG.error("MerchantTransactionId is blank");
				return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString("0", "0000");
			}


			final Pair<String, Boolean> successfulPaidOrder = isSuccessfulPaidOrder(orderInfoMap);
			if (Boolean.TRUE.equals(successfulPaidOrder.getRight()))
			{
				if (transaction != null)
				{
					getMerchantTransactionService().removeMerchantTransaction(transaction);
				}
				return successfulPaidOrder.getLeft();
			}

			return completeOrder(orderInfoMap);
		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			//			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return getCheckoutStep().currentStep();
		}



	}

	/**
	 * @param httpServletRequest
	 * @return
	 */
	private String resolveCCAvenueCallback(final HttpServletRequest httpServletRequest)
	{
		final OrderData orderData = null;
		try
		{
			final Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();

			final String[] encResp = parameterMap.get("encResp");

			final String data = Arrays.stream(encResp).collect(Collectors.joining());

			final Optional<PaymentResponseData> paymentResponseData = getCheckoutFacade().getOrderPaymentResponseData(data);

			if (!paymentResponseData.isPresent() || paymentResponseData.get().getResponseData() == null
					|| !(paymentResponseData.get().getResponseData() instanceof Map))
			{
				return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=0000";
			}

			final Map<String, Object> orderInfoMap = paymentResponseData.get().getResponseData();

			if (!getCheckoutFacade().isSuccessfulPaidOrderByOrder())
			{
				LOG.warn("NOT_SUCCESSFUL_PAYMENT: {}", orderInfoMap);
				if (orderInfoMap.containsKey("result") && orderInfoMap.get("result") instanceof Map
						&& ((Map<String, Object>) orderInfoMap.get("result")).get("code") != null)
				{
					return REDIRECT_URL_ERROR + "/?decision=1&reasonCode="
							+ ((Map<String, Object>) orderInfoMap.get("result")).get("code");
				}
				return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=0000";
			}

			return completeOrder(orderInfoMap);

		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			//			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return getCheckoutStep().currentStep();
		}
	}


	protected String completeOrderForOrderCode(final Map<String, Object> orderInfoMap, final String orderCode)
			throws InvalidCartException
	{
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true);


		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.get().getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			LOG.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(paymentSubscriptionResultData.get().getDecision(),
					paymentSubscriptionResultData.get().getResultCode());
		}

		final OrderData orderData = getCheckoutFacade().placeOrder();

		return orderData == null ? REDIRECIT_TO_CHECKOUT_ORDER_PENDING + orderCode : redirectToOrderConfirmationPage(orderData);


	}

	protected String completeOrder(final Map<String, Object> orderInfoMap) throws InvalidCartException
	{
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true);


		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.get().getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			LOG.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(paymentSubscriptionResultData.get().getDecision(),
					paymentSubscriptionResultData.get().getResultCode());
		}

		final OrderData orderData = getCheckoutFacade().placeOrder();
		return redirectToOrderConfirmationPage(orderData);


	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}

		if (getCustomCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOG.error(String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			invalid = true;
		}

		if (!cartData.isCalculated())
		{
			LOG.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}


	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}


	/**
	 * @return the hyperpayCheckoutIdService
	 */
	public HyperpayCheckoutIdService getHyperpayCheckoutIdService()
	{
		return hyperpayCheckoutIdService;
	}


	/**
	 * @return the customOrderService
	 */
	public CustomOrderService getCustomOrderService()
	{
		return customOrderService;
	}

}
