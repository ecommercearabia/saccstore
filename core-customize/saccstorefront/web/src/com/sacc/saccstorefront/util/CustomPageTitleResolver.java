package com.sacc.saccstorefront.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author mnasro
 *
 */
public class CustomPageTitleResolver extends PageTitleResolver
{
	@Override
	public String resolveProductPageTitle(final ProductModel product)
	{
		// Lookup categories
		final List<CategoryModel> path = getCategoryPath(getProductAndCategoryHelper().getBaseProduct(product));
		// Lookup site (or store)
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		// Construct page title
		final String identifier = product.getName();
		final String articleNumber = product.getCode();
		final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
		final StringBuilder builder = new StringBuilder(productName);

		for (final CategoryModel pathElement : path)
		{
			builder.append(TITLE_WORD_SEPARATOR).append(pathElement.getDisplayName());
		}

		if (currentSite != null)
		{
			builder.append(TITLE_WORD_SEPARATOR).append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(builder.toString());
	}

	@Override
	public String resolveCategoryPageTitle(final CategoryModel category)
	{
		final StringBuilder stringBuilder = new StringBuilder();
		final List<CategoryModel> categories = this.getCategoryPath(category);
		for (final CategoryModel c : categories)
		{
			stringBuilder.append(c.getDisplayName()).append(TITLE_WORD_SEPARATOR);
		}

		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite != null)
		{
			stringBuilder.append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}

	/**
	 * creates page title for given code and facets
	 */
	@Override
	public <STATE> String resolveCategoryPageTitle(final CategoryModel category, final List<BreadcrumbData<STATE>> appliedFacets)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		final String name = category.getDisplayName();
		final StringBuilder builder = new StringBuilder();
		if (CollectionUtils.isEmpty(appliedFacets))
		{
			if (!StringUtils.isEmpty(name))
			{
				builder.append(name).append(TITLE_WORD_SEPARATOR);
			}
			builder.append(currentSite.getName());
		}
		else
		{
			for (final BreadcrumbData pathElement : appliedFacets)
			{
				builder.append(pathElement.getFacetValueName()).append(TITLE_WORD_SEPARATOR);
			}
			builder.append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(builder.toString());
	}

}

