/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccuserfacades.customer.facade.impl;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.saccuserfacades.customer.facade.CustomCustomerFacade;


/**
 * @author mnasro
 *
 *         Default implementation for the {@link CustomerFacade}.
 */
public class DefaultCustomCustomerFacade extends DefaultCustomerFacade implements CustomCustomerFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerFacade.class);


	@Resource(name = "customerService")
	private CustomerService customerService;

	/** The customer converter. */
	@Resource(name = "customerConverter")
	private Converter<UserModel, CustomerData> customerConverter;

	/** The customer reverse converter. */
	@Resource(name = "customerReverseConverter")
	private Converter<CustomerData, CustomerModel> customerReverseConverter;


	@Override
	public CustomerData getCustomerByCustomerId(final String id)
	{
		if (id != null)
		{
			final CustomerModel customerModel = customerService.getCustomerByCustomerId(id);
			if (customerModel != null)
			{
				return customerConverter.convert(customerModel);
			}
		}
		return null;
	}

	@Override
	public boolean isCustomerExists(final String customerId)
	{
		if (customerId != null)
		{
			return getCustomerByCustomerId(customerId) != null;
		}
		return false;
	}

	/**
	 * Sets the common properties for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	@Override
	protected void setCommonPropertiesForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		customerModel.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		setTitleForRegister(registerData, customerModel);
		setUidForRegister(registerData, customerModel);
		setMobileNumberForRegister(registerData, customerModel);
		customerModel.setMobileNumber(registerData.getMobileNumber());
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setUid(customerModel.getUid());
	}

	/**
	 * Update profile.
	 *
	 * @param customerData
	 *           the customer data
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());
		customerReverseConverter.convert(customerData, customer);
		getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());
	}

	/**
	 * Sets the mobile number for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	protected void setMobileNumberForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		if (StringUtils.isNotBlank(registerData.getMobileCountry()) && StringUtils.isNotBlank(registerData.getMobileNumber()))
		{
			customerModel.setMobileNumber(registerData.getMobileNumber());
			customerModel.setMobileCountry(getCommonI18NService().getCountry(registerData.getMobileCountry()));
		}
	}


	/**
	 * Gets the current customer.
	 *
	 * @return the current customer
	 */
	@Override
	public CustomerData getCurrentCustomer()
	{
		return customerConverter.convert(getCurrentUser());
	}
}
