/**
 *
 */
package com.sacc.saccuserfacades.area.facade.impl;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccuser.area.service.AreaService;
import com.sacc.saccuser.model.AreaModel;
import com.sacc.saccuserfacades.area.facade.AreaFacade;



/**
 * The Class DefaultAreaFacade.
 *
 * @author mnasro
 */
public class DefaultAreaFacade implements AreaFacade
{

	/** The area service. */
	@Resource(name = "areaService")
	private AreaService areaService;


	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;



	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	@Override
	public Optional<List<AreaData>> getByCityCode(final String cityCode)
	{
		final Optional<List<AreaModel>> areas = getAreaService().getByCityCode(cityCode);
		if (areas.isPresent())
		{
			return Optional.ofNullable(getAreaConverter().convertAll(areas.get()));
		}
		return Optional.empty();
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<AreaData>> getAll()
	{
		final Optional<List<AreaModel>> areas = getAreaService().getAll();
		if (areas.isPresent())
		{
			return Optional.ofNullable(getAreaConverter().convertAll(areas.get()));
		}
		return Optional.empty();
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaData> get(final String code)
	{

		final Optional<AreaModel> area = getAreaService().get(code);
		if (area.isPresent())
		{
			return Optional.of(getAreaConverter().convert(area.get()));
		}
		return Optional.empty();
	}

	protected AreaService getAreaService()
	{
		return areaService;
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}
}
