/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.facade;

import java.util.List;

import com.sacc.saccorderfacades.data.OrderNoteData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderNoteFacade
{
	public List<OrderNoteData> getOrderNotesByCurrentSite();
}
