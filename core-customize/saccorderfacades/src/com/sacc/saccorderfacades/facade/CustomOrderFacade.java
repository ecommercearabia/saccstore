/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.facade;

import de.hybris.platform.core.enums.OrderStatus;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomOrderFacade
{
	public int getNumberOfOrders(final OrderStatus... statuses);
}
