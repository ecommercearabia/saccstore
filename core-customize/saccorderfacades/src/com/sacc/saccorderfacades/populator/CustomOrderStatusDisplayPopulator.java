/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.populator;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomOrderStatusDisplayPopulator implements Populator<OrderModel, OrderHistoryData>
{
	private static final String SHIPPED_STATUS_DISPLAY = "shipped";

	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)
	{
		if (CollectionUtils.isNotEmpty(source.getConsignments()))
		{
			changeStatusDisplay(target, source.getConsignments().stream().findFirst().get());
		}
		target.setOrderRefId(source.getOrderNumber());
	}

	private void changeStatusDisplay(final OrderHistoryData target, final ConsignmentModel consignment)
	{
		if (consignment != null && ConsignmentStatus.SHIPPED.equals(consignment.getStatus()))
		{
			target.setStatusDisplay(SHIPPED_STATUS_DISPLAY);
		}
	}
}
