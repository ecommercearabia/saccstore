/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import javax.annotation.Resource;


/**
 * @author mnasro
 * @author monzer
 *
 */
public class CustomAbstractOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;


	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populateNetTotalPrice(source, target);
			target.setOrderRefId(source.getOrderNumber());
		}
	}

	/**
	 *
	 */
	private void populateNetTotalPrice(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final PriceData totalPriceWithTax = (createPrice(source, calcTotalWithTax(source)));
		final PriceData totalTax = createPrice(source, source.getTotalTax());
		final BigDecimal net = totalPriceWithTax.getValue().subtract(totalTax.getValue());

		target.setNetTotalPrice(createPrice(source, net.doubleValue()));
	}

	protected PriceData createPrice(final AbstractOrderModel source, final Double val)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}

		final CurrencyModel currency = source.getCurrency();
		if (currency == null)
		{
			throw new IllegalArgumentException("source order currency must not be null");
		}

		// Get double value, handle null as zero
		final double priceValue = val != null ? val.doubleValue() : 0d;

		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);
	}

	protected Double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}

		return totalPrice.doubleValue();
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

}
