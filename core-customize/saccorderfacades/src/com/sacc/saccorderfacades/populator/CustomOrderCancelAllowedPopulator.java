/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.populator;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomOrderCancelAllowedPopulator implements Populator<OrderModel, OrderData>
{
	private static final Logger LOG = Logger.getLogger(CustomOrderCancelAllowedPopulator.class);
	private static final String SHIPPED_STATUS_DISPLAY = "shipped";
	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Resource(name = "customNotCancellableOrderStatus")
	private List<OrderStatus> customNotCancellableOrderStatus;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		target.setCustomerCancelAllowed(source != null && !CollectionUtils.isEmpty(source.getEntries())
				&& orderCancelService.isCancelPossible(source, userService.getCurrentUser(), true, true).isAllowed()
				&& source.getStatus() != null && !customNotCancellableOrderStatus.contains(source.getStatus())
				&& !source.isSyncPartialCancelFailed() && source.isConsignmentCancellable());
		if (CollectionUtils.isNotEmpty(source.getConsignments()))
		{
			target.setCustomerReturnAllowed(
					source.getConsignments().stream().filter(Objects::nonNull).allMatch(c -> isReturnStillAllowed(c)));
			changeStatusDisplay(target, source.getConsignments().stream().findFirst().get());
		}

	}

	private boolean isReturnStillAllowed(final ConsignmentModel consignment)
	{
		if (StringUtils.isEmpty(consignment.getActualDeliveryDate()))
		{
			return true;
		}
		LocalDate deliveryDate = null;
		try
		{
			deliveryDate = LocalDate.parse(consignment.getActualDeliveryDate(), DateTimeFormatter.ofPattern("d/M/yyyy"));
			return deliveryDate.plusDays(15).isAfter(LocalDate.now());
		}
		catch (final Exception e)
		{
			LOG.error(
					"Exception occurred while parsing the actualDeliveryDate of the consignment, please make sure to set the date in the format dd/MM/yyyy",
					e);
		}
		return false;
	}

	private void changeStatusDisplay(final OrderData target, final ConsignmentModel consignment)
	{
		if (consignment != null && ConsignmentStatus.SHIPPED.equals(consignment.getStatus()))
		{
			target.setStatusDisplay(SHIPPED_STATUS_DISPLAY);
		}
	}
}
