/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class OrderThirdPartyCustomerPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null)
		{
			checkForThirdPartyCustomer(source, target);
		}
	}

	protected void checkForThirdPartyCustomer(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (CustomerType.THIRD_PARTY.equals(((CustomerModel) source.getUser()).getType()))
		{
			target.setThirdPartyCustomer(true);
		}
	}

}
