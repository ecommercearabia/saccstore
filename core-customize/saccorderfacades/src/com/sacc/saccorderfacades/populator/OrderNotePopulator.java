/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.populator;

import de.hybris.platform.converters.Populator;

import com.sacc.saccorder.model.OrderNoteModel;
import com.sacc.saccorderfacades.data.OrderNoteData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class OrderNotePopulator implements Populator<OrderNoteModel, OrderNoteData>
{

	@Override
	public void populate(final OrderNoteModel source, final OrderNoteData target)
	{
		if (source != null)
		{
			target.setCode(source.getCode());
			target.setNote(source.getNote());
		}
	}

}
