package com.sacc.saccorderfacades.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class SaccorderfacadesConstants extends GeneratedSaccorderfacadesConstants
{
	public static final String EXTENSIONNAME = "saccorderfacades";
	
	private SaccorderfacadesConstants()
	{
		//empty
	}
	
	
}
