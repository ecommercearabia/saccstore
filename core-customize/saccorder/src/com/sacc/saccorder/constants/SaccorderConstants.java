/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorder.constants;

/**
 * Global class for all Saccorder constants. You can add global constants for your extension into this class.
 */
public final class SaccorderConstants extends GeneratedSaccorderConstants
{
	public static final String EXTENSIONNAME = "saccorder";

	private SaccorderConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccorderPlatformLogo";
}
