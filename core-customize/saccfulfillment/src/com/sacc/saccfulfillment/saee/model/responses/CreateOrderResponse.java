package com.sacc.saccfulfillment.saee.model.responses;

/**
 * @author Husam Dababneh
 */
public class CreateOrderResponse extends Response
{

	private String message;
	private String waybill;
	private String epayment_url;

	public CreateOrderResponse()
	{
		// TODO Auto-generated constructor stub
	}

	public CreateOrderResponse(final boolean success, final int error_code, final String error, final String message,
			final String waybill, final String epayment_url)
	{
		super(success, error_code, error);
		this.message = message;
		this.waybill = waybill;
		this.epayment_url = epayment_url;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	public String getWaybill()
	{
		return waybill;
	}

	public void setWaybill(final String waybill)
	{
		this.waybill = waybill;
	}

	public String getEpayment_url()
	{
		return epayment_url;
	}

	public void setEpayment_url(final String epayment_url)
	{
		this.epayment_url = epayment_url;
	}

}
