package com.sacc.saccfulfillment.saee.model.responses;

/**
 * @author Husam Dababneh
 */
public class Details
{

	private int id;
	private String city;
	// TODO: enum
	private int status;
	private String notes;
	private String updated_at;


	/**
	 *
	 */
	public Details()
	{
		super();
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final int id)
	{
		this.id = id;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final int status)
	{
		this.status = status;
	}

	/**
	 * @return the notes
	 */
	public String getNotes()
	{
		return notes;
	}

	/**
	 * @param notes
	 *           the notes to set
	 */
	public void setNotes(final String notes)
	{
		this.notes = notes;
	}

	/**
	 * @return the updated_at
	 */
	public String getUpdated_at()
	{
		return updated_at;
	}

	/**
	 * @param updated_at
	 *           the updated_at to set
	 */
	public void setUpdated_at(final String updated_at)
	{
		this.updated_at = updated_at;
	}


}
