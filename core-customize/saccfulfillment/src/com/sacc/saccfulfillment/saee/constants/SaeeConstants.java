package com.sacc.saccfulfillment.saee.constants;

public class SaeeConstants
{
	private SaeeConstants()
	{
	}

	public static final String URL = "https://www.k-w-h.com/";
	public static final String secret = "$2y$10$m9dolGpdQ5eCtiA0E/xuyus.wNhj75WRH3gib0pp3wBAfVzO/.AR.";
}
