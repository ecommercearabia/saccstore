package com.sacc.saccfulfillment.saee.service;

import java.util.Optional;

import com.sacc.saccfulfillment.saee.exceptions.SaeeException;
import com.sacc.saccfulfillment.saee.model.SaeeOrderData;
import com.sacc.saccfulfillment.saee.model.responses.Details;


public interface SaeeService
{

	public Optional<String> createShipment(SaeeOrderData param, String baseURL) throws SaeeException;

	public Optional<byte[]> getPDF(String waybill, String baseURL) throws SaeeException;

	public Optional<Details> getStatus(String waybill, String baseURL) throws SaeeException;
}
