/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.interceptor;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.warehousing.interceptor.ConsignmentPackagingInfoPrepareInterceptor;
import de.hybris.platform.warehousing.model.PackagingInfoModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;


/**
 * @author amjad.shati@erabia.com
 * @author Tuqa
 *
 */

public class CustomConsignmentPackagingInfoPrepareInterceptor extends ConsignmentPackagingInfoPrepareInterceptor
{
	protected static final Logger LOG = Logger.getLogger(CustomConsignmentPackagingInfoPrepareInterceptor.class);



	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Override
	public void onPrepare(final ConsignmentModel consignment, final InterceptorContext context) throws InterceptorException
	{
		LOG.info("Start Prepare.....");
		final Optional<FulfillmentProviderModel> provider = fulfillmentProviderContext
				.getProvider(consignment.getOrder().getStore());

		if (context.isNew(consignment))
		{
			LOG.info("Calculate Packaging Info........");

			final PackagingInfoModel packagingInfo = getModelService().create(PackagingInfoModel.class);
			packagingInfo.setConsignment(consignment);

			final double grossWeight = getGrossWeight(consignment, provider);

			packagingInfo.setGrossWeight(String.valueOf(grossWeight));
			packagingInfo.setHeight(DEFAULT_VALUE);
			packagingInfo.setLength(DEFAULT_VALUE);
			packagingInfo.setWidth(DEFAULT_VALUE);
			packagingInfo.setDimensionUnit(DEFAULT_DIMENSION_UNIT);
			packagingInfo.setWeightUnit(DEFAULT_WEIGHT_UNIT);
			packagingInfo.setCreationtime(getTimeService().getCurrentTime());
			packagingInfo.setModifiedtime(getTimeService().getCurrentTime());
			packagingInfo.setInsuredValue(DEFAULT_VALUE);
			context.registerElementFor(packagingInfo, PersistenceOperation.SAVE);
			consignment.setPackagingInfo(packagingInfo);
		}
	}

	/**
	 *
	 */
	private double getGrossWeight(final ConsignmentModel consignment, final Optional<FulfillmentProviderModel> provider)
	{
		LOG.info("Calculate Gross Weigth for Consigment" + consignment.getCode());


		double grossWeight = consignment.getConsignmentEntries().stream().filter(e -> e != null && e.getQuantity() != null)
				.mapToDouble(entry -> entry.getWeight() * entry.getQuantity()).summaryStatistics().getSum();

		final CMSSiteModel site = (CMSSiteModel) consignment.getOrder().getSite();

		if (provider.isEmpty() || !(provider.get() instanceof SmsaFulfillmentProviderModel))
		{
			LOG.info("Default Gross Weigth Calculation");
			return grossWeight;
		}

		final SmsaFulfillmentProviderModel smsaProvider = (SmsaFulfillmentProviderModel) provider.get();

		if (site.isFixedCalcWeight())
		{
			LOG.info("Fixed Gross Weigth Calculation");

			grossWeight = consignment.getConsignmentEntries().stream().filter(e -> e != null && e.getQuantity() != null)
					.mapToDouble(entry -> smsaProvider.getFixedWeight() * entry.getQuantity()).summaryStatistics().getSum();
			return grossWeight;
		}


		if (site.isFixedCalcWeightWithoutQuantity())
		{
			LOG.info("Fixed Gross Weigth Calculation without Quantity");
			return smsaProvider.getFixedWeight();

		}

		return grossWeight;
	}


}
