package com.sacc.saccfulfillment.shipa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order
{
	private double amount;
	private String description;
	private String goodsValue;
	private String id;
	private String paymentMethod;
	private Recipient recipient;

	private Sender sender;
	private String typeDelivery;

	public Order(final double amount, final String description, final String goodsValue, final String id,
			final String paymentMethod, final Recipient recipient, final Sender sender, final String typeDelivery)
	{
		super();
		this.amount = amount;
		this.description = description;
		this.goodsValue = goodsValue;
		this.id = id;
		this.paymentMethod = paymentMethod;
		this.recipient = recipient;

		this.sender = sender;
		this.typeDelivery = typeDelivery;
	}

	public Order()
	{

	}

	public Order(final double amount, final String description, final String id, final String paymentMethod,
			final Recipient recipient, final Sender sender, final String typeDelivery)
	{
		super();
		this.amount = amount;
		this.description = description;
		this.id = id;
		this.paymentMethod = paymentMethod;
		this.recipient = recipient;
		this.sender = sender;
		this.typeDelivery = typeDelivery;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getGoodsValue()
	{
		return goodsValue;
	}

	public void setGoodsValue(final String goodsValue)
	{
		this.goodsValue = goodsValue;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(final String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public Recipient getRecipient()
	{
		return recipient;
	}

	public void setRecipient(final Recipient recipient)
	{
		this.recipient = recipient;
	}

	public Sender getSender()
	{
		return sender;
	}

	public void setSender(final Sender sender)
	{
		this.sender = sender;
	}

	public String getTypeDelivery()
	{
		return typeDelivery;
	}

	public void setTypeDelivery(final String typeDelivery)
	{
		this.typeDelivery = typeDelivery;
	}

	@Override
	public String toString()
	{
		return "Order [amount=" + amount + ", description=" + description + ", goodsValue=" + goodsValue + ", id=" + id
				+ ", paymentMethod=" + paymentMethod + ", recipient=" + recipient + ", sender=" + sender + ", typeDelivery="
				+ typeDelivery + "]";
	}

}
