package com.sacc.saccfulfillment.shipa.model.cancel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelReason
{
	private int reason;

	public CancelReason()
	{

	}

	public CancelReason(final int reason)
	{
		super();
		this.reason = reason;
	}

	public int getReason()
	{
		return reason;
	}

	public void setReason(final int reason)
	{
		this.reason = reason;
	}

}
