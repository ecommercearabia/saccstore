package com.sacc.saccfulfillment.shipa.model.success;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryInfo
{
	private String reference;
	private String codeStatus;
	private String startTime;
	private String endTime;
	private String expectedTime;
	private String infoStatus;

	public DeliveryInfo(final String reference, final String codeStatus, final String startTime, final String endTime,
			final String expectedTime, final String infoStatus)
	{
		super();
		this.reference = reference;
		this.codeStatus = codeStatus;
		this.startTime = startTime;
		this.endTime = endTime;
		this.expectedTime = expectedTime;
		this.infoStatus = infoStatus;
	}

	public DeliveryInfo()
	{

	}

	public String getReference()
	{
		return reference;
	}

	public String getInfoStatus()
	{
		return infoStatus;
	}

	public void setInfoStatus(final String infoStatus)
	{
		this.infoStatus = infoStatus;
	}

	public void setReference(final String reference)
	{
		this.reference = reference;
	}

	public String getCodeStatus()
	{
		return codeStatus;
	}

	public void setCodeStatus(final String codeStatus)
	{
		this.codeStatus = codeStatus;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(final String startTime)
	{
		this.startTime = startTime;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(final String endTime)
	{
		this.endTime = endTime;
	}

	public String getExpectedTime()
	{
		return expectedTime;
	}

	public void setExpectedTime(final String expectedTime)
	{
		this.expectedTime = expectedTime;
	}

	@Override
	public String toString()
	{
		return "DeliveryInfo [reference=" + reference + ", codeStatus=" + codeStatus + ", startTime=" + startTime + ", endTime="
				+ endTime + ", expectedTime=" + expectedTime + ", infoStatus=" + infoStatus + "]";
	}


}
