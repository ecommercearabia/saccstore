package com.sacc.saccfulfillment.shipa.exception;

import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.exception.enums.FulfillmentExceptionType;


public class ShipaException extends FulfillmentException
{


	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type)
	{
		super(type);
	}

	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type, final Throwable throwable)
	{
		super(type, throwable);
	}

	private static final long serialVersionUID = 1L;



}