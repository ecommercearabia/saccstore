package com.sacc.saccfulfillment.context;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.exception.FulfillmentException;


/**
 *
 */
public interface FulfillmentContext
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel) throws FulfillmentException;

	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderType type)
			throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderType type) throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderType type) throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel, FulfillmentProviderType type)
			throws FulfillmentException;
}