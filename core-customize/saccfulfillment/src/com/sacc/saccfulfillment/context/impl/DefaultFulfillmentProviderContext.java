package com.sacc.saccfulfillment.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SaeeFulfillmentProviderModel;
import com.sacc.saccfulfillment.model.ShipaFulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;
import com.sacc.saccfulfillment.strategy.FulfillmentProviderStrategy;


/**
 *
 */
public class DefaultFulfillmentProviderContext implements FulfillmentProviderContext
{
	@Resource(name = "fulfillmentProviderMap")
	private Map<Class<?>, FulfillmentProviderStrategy> fulfillmentProviderMap;

	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";
	private static final String FULFILLMENT_PROVIDOR_MUSTN_T_BE_NULL = "type mustn't be null";

	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentBaseStore();
	}

	protected Optional<FulfillmentProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final FulfillmentProviderStrategy strategy = getFulfillmentProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}

	protected Map<Class<?>, FulfillmentProviderStrategy> getFulfillmentProviderMap()
	{
		return fulfillmentProviderMap;
	}

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreUid != null, BASESTORE_UID_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreUid);
	}

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		final Optional<FulfillmentProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreModel);
	}

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isBlank(baseStoreModel.getFulfillmentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SHIPAFULFILLMENTPROVIDER":
				return getProvider(baseStoreModel, ShipaFulfillmentProviderModel.class);
			case "SMSAFULFILLMENTPROVIDER":
				return getProvider(baseStoreModel, SmsaFulfillmentProviderModel.class);
			case "SAEEFULFILLMENTPROVIDER":
				return getProvider(baseStoreModel, SaeeFulfillmentProviderModel.class);
			default:
				return Optional.empty();
		}
	}

	@Override
	public Optional<FulfillmentProviderModel> getProvider(final BaseStoreModel baseStoreModel, final FulfillmentProviderType type)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);

		Preconditions.checkArgument(type != null, FULFILLMENT_PROVIDOR_MUSTN_T_BE_NULL);

		if (!checkFulfillmentProvidorType(baseStoreModel.getFulfillmentProvidorTypes(), type))
		{
			return Optional.empty();
		}

		switch (type)
		{
			case SHIPA:
				return getProvider(baseStoreModel, ShipaFulfillmentProviderModel.class);
			case SMSA:
				return getProvider(baseStoreModel, SmsaFulfillmentProviderModel.class);
			case SAEE:
				return getProvider(baseStoreModel, SaeeFulfillmentProviderModel.class);
			default:
				return Optional.empty();
		}
	}

	/**
	 *
	 */
	private boolean checkFulfillmentProvidorType(final List<FulfillmentProviderType> fulfillmentProvidorTypes,
			final FulfillmentProviderType type)
	{
		if (fulfillmentProvidorTypes == null)
		{
			return false;
		}
		if (fulfillmentProvidorTypes.isEmpty())
		{
			return false;
		}
		if (type == null)
		{
			return false;
		}
		if (!fulfillmentProvidorTypes.contains(type))
		{
			return false;
		}

		return true;
	}
}