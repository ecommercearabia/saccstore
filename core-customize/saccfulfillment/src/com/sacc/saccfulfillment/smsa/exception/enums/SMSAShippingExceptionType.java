package com.sacc.saccfulfillment.smsa.exception.enums;

/**
 * @author Husam Dababneh
 */
public enum SMSAShippingExceptionType
{
	MISSING_ARGUMENT, REMOTE_ERROR;
}
