
package com.sacc.saccfulfillment.smsa.service;

import java.util.Optional;

import com.sacc.saccfulfillment.smsa.model.SMSAShipmentData;


/**
 * @author Husam Dababneh
 *
 */
public interface SmsaService
{
	/**
	 * @param awbNumber
	 *           , of the shipment
	 * @param smsa
	 *           , to get the passKey
	 * @return String, status of the shipment.
	 */
	public Optional<String> getStatus(String awbNumber, String passkey);

	/**
	 * @param order
	 *           , order you want to create it's shipment.
	 * @return String, result as success or response is returned if failed.
	 */
	public Optional<String> createShipment(final SMSAShipmentData smsaShipmentData);

	/**
	 *
	 * @param awbNumber
	 *           , of the shipment.
	 * @param smsa
	 *           , to get the passKey
	 * @return array of bytes.
	 */
	public Optional<byte[]> getPDF(String awbNumber, String passkey);

}
