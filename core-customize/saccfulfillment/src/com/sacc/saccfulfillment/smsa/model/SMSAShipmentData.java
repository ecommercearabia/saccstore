package com.sacc.saccfulfillment.smsa.model;


/**
 * @author Husam Dababneh
 */

public class SMSAShipmentData
{
	private String passKey;
	private String refNo;
	private String sentDate;
	private String idNo;
	private String customerName;
	private String customerCountryIsoCode;
	private String customerCityName;
	private String customerZipCode;
	private String customerPOBox;
	private String customerMobileNumber;
	private String customerTelephoneNumber1;
	private String customerTelephoneNumber2;
	private String customerAddressLine1;
	private String customerAddressLine2;
	private String shipmentType;
	private Integer numberOfItems;
	private String emailAddress;
	private String carriageValue;
	private String carriageCurrency;
	private String codAmount;
	private String weight;
	private String customsValue;
	private String customsCurrency;
	private String insuranceAmount;
	private String insuranceCurrency;
	private String itemDescription;
	private String shipperName;
	private String shipperContactName;
	private String shipperAddress1;
	private String shipperAddress2;
	private String shipperCity;
	private String shipperPhone;
	private String shipperCountry;
	private String preferredDeliveryDate;
	private String gpsPoints;


	public SMSAShipmentData()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the passKey
	 */
	public String getPassKey()
	{
		return passKey;
	}


	/**
	 * @param passKey
	 *           the passKey to set
	 */
	public void setPassKey(final String passKey)
	{
		this.passKey = passKey;
	}


	/**
	 * @return the refNo
	 */
	public String getRefNo()
	{
		return refNo == null ? "" : refNo;
	}


	/**
	 * @param refNo
	 *           the refNo to set
	 */
	public void setRefNo(final String refNo)
	{
		this.refNo = refNo;
	}


	/**
	 * @return the sentDate
	 */
	public String getSentDate()
	{
		return sentDate == null ? "" : sentDate;
	}


	/**
	 * @param sentDate
	 *           the sentDate to set
	 */
	public void setSentDate(final String sentDate)
	{
		this.sentDate = sentDate;
	}


	/**
	 * @return the idNo
	 */
	public String getIdNo()
	{
		return idNo == null ? "" : idNo;
	}


	/**
	 * @param idNo
	 *           the idNo to set
	 */
	public void setIdNo(final String idNo)
	{
		this.idNo = idNo;
	}


	/**
	 * @return the customerName
	 */
	public String getCustomerName()
	{
		return customerName == null ? "" : customerName;
	}


	/**
	 * @param customerName
	 *           the customerName to set
	 */
	public void setCustomerName(final String customerName)
	{
		this.customerName = customerName;
	}


	/**
	 * @return the customerCountryIsoCode
	 */
	public String getCustomerCountryIsoCode()
	{
		return customerCountryIsoCode == null ? "" : customerCountryIsoCode;
	}


	/**
	 * @param customerCountryIsoCode
	 *           the customerCountryIsoCode to set
	 */
	public void setCustomerCountryIsoCode(final String customerCountryIsoCode)
	{
		this.customerCountryIsoCode = customerCountryIsoCode;
	}


	/**
	 * @return the customerCityName
	 */
	public String getCustomerCityName()
	{
		return customerCityName == null ? "" : customerCityName;
	}


	/**
	 * @param customerCityName
	 *           the customerCityName to set
	 */
	public void setCustomerCityName(final String customerCityName)
	{
		this.customerCityName = customerCityName;
	}


	/**
	 * @return the customerZipCode
	 */
	public String getCustomerZipCode()
	{
		return customerZipCode == null ? "" : customerZipCode;
	}


	/**
	 * @param customerZipCode
	 *           the customerZipCode to set
	 */
	public void setCustomerZipCode(final String customerZipCode)
	{
		this.customerZipCode = customerZipCode;
	}


	/**
	 * @return the customerPOBox
	 */
	public String getCustomerPOBox()
	{
		return customerPOBox == null ? "" : customerPOBox;
	}


	/**
	 * @param customerPOBox
	 *           the customerPOBox to set
	 */
	public void setCustomerPOBox(final String customerPOBox)
	{
		this.customerPOBox = customerPOBox;
	}


	/**
	 * @return the customerMobileNumber
	 */
	public String getCustomerMobileNumber()
	{
		return customerMobileNumber == null ? "" : customerMobileNumber;
	}


	/**
	 * @param customerMobileNumber
	 *           the customerMobileNumber to set
	 */
	public void setCustomerMobileNumber(final String customerMobileNumber)
	{
		this.customerMobileNumber = customerMobileNumber;
	}


	/**
	 * @return the customerTelephoneNumber1
	 */
	public String getCustomerTelephoneNumber1()
	{
		return customerTelephoneNumber1 == null ? "" : customerTelephoneNumber1;
	}


	/**
	 * @param customerTelephoneNumber1
	 *           the customerTelephoneNumber1 to set
	 */
	public void setCustomerTelephoneNumber1(final String customerTelephoneNumber1)
	{
		this.customerTelephoneNumber1 = customerTelephoneNumber1;
	}


	/**
	 * @return the customerTelephoneNumber2
	 */
	public String getCustomerTelephoneNumber2()
	{
		return customerTelephoneNumber2 == null ? "" : customerTelephoneNumber2;
	}


	/**
	 * @param customerTelephoneNumber2
	 *           the customerTelephoneNumber2 to set
	 */
	public void setCustomerTelephoneNumber2(final String customerTelephoneNumber2)
	{
		this.customerTelephoneNumber2 = customerTelephoneNumber2;
	}


	/**
	 * @return the customerAddressLine1
	 */
	public String getCustomerAddressLine1()
	{
		return customerAddressLine1 == null ? "" : customerAddressLine1;
	}


	/**
	 * @param customerAddressLine1
	 *           the customerAddressLine1 to set
	 */
	public void setCustomerAddressLine1(final String customerAddressLine1)
	{
		this.customerAddressLine1 = customerAddressLine1;
	}


	/**
	 * @return the customerAddressLine2
	 */
	public String getCustomerAddressLine2()
	{
		return customerAddressLine2 == null ? "" : customerAddressLine2;
	}


	/**
	 * @param customerAddressLine2
	 *           the customerAddressLine2 to set
	 */
	public void setCustomerAddressLine2(final String customerAddressLine2)
	{
		this.customerAddressLine2 = customerAddressLine2;
	}


	/**
	 * @return the shipmentType
	 */
	public String getShipmentType()
	{
		return shipmentType == null ? "" : shipmentType;
	}


	/**
	 * @param shipmentType
	 *           the shipmentType to set
	 */
	public void setShipmentType(final String shipmentType)
	{
		this.shipmentType = shipmentType;
	}


	/**
	 * @return the numberOfItems
	 */
	public Integer getNumberOfItems()
	{
		return numberOfItems == null ? 0 : numberOfItems;
	}


	/**
	 * @param numberOfItems
	 *           the numberOfItems to set
	 */
	public void setNumberOfItems(final Integer numberOfItems)
	{
		this.numberOfItems = numberOfItems;
	}


	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress == null ? "" : emailAddress;
	}


	/**
	 * @param emailAddress
	 *           the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}


	/**
	 * @return the carriageValue
	 */
	public String getCarriageValue()
	{
		return carriageValue == null ? "" : carriageValue;
	}


	/**
	 * @param carriageValue
	 *           the carriageValue to set
	 */
	public void setCarriageValue(final String carriageValue)
	{
		this.carriageValue = carriageValue;
	}


	/**
	 * @return the carriageCurrency
	 */
	public String getCarriageCurrency()
	{
		return carriageCurrency == null ? "" : carriageCurrency;
	}


	/**
	 * @param carriageCurrency
	 *           the carriageCurrency to set
	 */
	public void setCarriageCurrency(final String carriageCurrency)
	{
		this.carriageCurrency = carriageCurrency;
	}


	/**
	 * @return the codAmount
	 */
	public String getCodAmount()
	{
		return codAmount == null ? "" : codAmount;
	}


	/**
	 * @param codAmount
	 *           the codAmount to set
	 */
	public void setCodAmount(final String codAmount)
	{
		this.codAmount = codAmount;
	}


	/**
	 * @return the weight
	 */
	public String getWeight()
	{
		return weight == null ? "" : weight;
	}


	/**
	 * @param weight
	 *           the weight to set
	 */
	public void setWeight(final String weight)
	{
		this.weight = weight;
	}


	/**
	 * @return the customsValue
	 */
	public String getCustomsValue()
	{
		return customsValue == null ? "" : customsValue;
	}


	/**
	 * @param customsValue
	 *           the customsValue to set
	 */
	public void setCustomsValue(final String customsValue)
	{
		this.customsValue = customsValue;
	}


	/**
	 * @return the customsCurrency
	 */
	public String getCustomsCurrency()
	{
		return customsCurrency == null ? "" : customsCurrency;
	}


	/**
	 * @param customsCurrency
	 *           the customsCurrency to set
	 */
	public void setCustomsCurrency(final String customsCurrency)
	{
		this.customsCurrency = customsCurrency;
	}


	/**
	 * @return the insuranceAmount
	 */
	public String getInsuranceAmount()
	{
		return insuranceAmount == null ? "" : insuranceAmount;
	}


	/**
	 * @param insuranceAmount
	 *           the insuranceAmount to set
	 */
	public void setInsuranceAmount(final String insuranceAmount)
	{
		this.insuranceAmount = insuranceAmount;
	}


	/**
	 * @return the insuranceCurrency
	 */
	public String getInsuranceCurrency()
	{
		return insuranceCurrency == null ? "" : shipperName;
	}


	/**
	 * @param insuranceCurrency
	 *           the insuranceCurrency to set
	 */
	public void setInsuranceCurrency(final String insuranceCurrency)
	{
		this.insuranceCurrency = insuranceCurrency;
	}


	/**
	 * @return the itemDescription
	 */
	public String getItemDescription()
	{
		return itemDescription == null ? "" : shipperName;
	}


	/**
	 * @param itemDescription
	 *           the itemDescription to set
	 */
	public void setItemDescription(final String itemDescription)
	{
		this.itemDescription = itemDescription;
	}


	/**
	 * @return the shipperName
	 */
	public String getShipperName()
	{
		return shipperName == null ? "" : shipperName;
	}


	/**
	 * @param shipperName
	 *           the shipperName to set
	 */
	public void setShipperName(final String shipperName)
	{
		this.shipperName = shipperName;
	}


	/**
	 * @return the shipperContactName
	 */
	public String getShipperContactName()
	{
		return shipperContactName == null ? "" : shipperContactName;
	}


	/**
	 * @param shipperContactName
	 *           the shipperContactName to set
	 */
	public void setShipperContactName(final String shipperContactName)
	{
		this.shipperContactName = shipperContactName;
	}


	/**
	 * @return the shipperAddress1
	 */
	public String getShipperAddress1()
	{
		return shipperAddress1 == null ? "" : shipperAddress1;
	}


	/**
	 * @param shipperAddress1
	 *           the shipperAddress1 to set
	 */
	public void setShipperAddress1(final String shipperAddress1)
	{
		this.shipperAddress1 = shipperAddress1;
	}


	/**
	 * @return the shipperAddress2
	 */
	public String getShipperAddress2()
	{
		return shipperAddress2 == null ? "" : shipperAddress2;
	}


	/**
	 * @param shipperAddress2
	 *           the shipperAddress2 to set
	 */
	public void setShipperAddress2(final String shipperAddress2)
	{
		this.shipperAddress2 = shipperAddress2;
	}


	/**
	 * @return the shipperCity
	 */
	public String getShipperCity()
	{
		return shipperCity == null ? "" : shipperCity;
	}


	/**
	 * @param shipperCity
	 *           the shipperCity to set
	 */
	public void setShipperCity(final String shipperCity)
	{
		this.shipperCity = shipperCity;
	}


	/**
	 * @return the shipperPhone
	 */
	public String getShipperPhone()
	{
		return shipperPhone == null ? "" : shipperPhone;
	}


	/**
	 * @param shipperPhone
	 *           the shipperPhone to set
	 */
	public void setShipperPhone(final String shipperPhone)
	{
		this.shipperPhone = shipperPhone;
	}


	/**
	 * @return the shipperCountry
	 */
	public String getShipperCountry()
	{
		return shipperCountry == null ? "" : shipperCountry;
	}


	/**
	 * @param shipperCountry
	 *           the shipperCountry to set
	 */
	public void setShipperCountry(final String shipperCountry)
	{
		this.shipperCountry = shipperCountry;
	}


	/**
	 * @return the preferredDeliveryDate
	 */
	public String getPreferredDeliveryDate()
	{
		return preferredDeliveryDate == null ? "" : preferredDeliveryDate;
	}


	/**
	 * @param preferredDeliveryDate
	 *           the preferredDeliveryDate to set
	 */
	public void setPreferredDeliveryDate(final String preferredDeliveryDate)
	{
		this.preferredDeliveryDate = preferredDeliveryDate;
	}


	/**
	 * @return the gpsPoints
	 */
	public String getGpsPoints()
	{
		return gpsPoints == null ? "" : gpsPoints;
	}


	/**
	 * @param gpsPoints
	 *           the gpsPoints to set
	 */
	public void setGpsPoints(final String gpsPoints)
	{
		this.gpsPoints = gpsPoints;
	}


	@Override
	public String toString()
	{
		return "SMSAShipmentData [passKey=" + passKey + ", refNo=" + refNo + ", sentDate=" + sentDate + ", idNo=" + idNo
				+ ", customerName=" + customerName + ", customerCountryIsoCode=" + customerCountryIsoCode + ", customerCityName="
				+ customerCityName + ", customerZipCode=" + customerZipCode + ", customerPOBox=" + customerPOBox
				+ ", customerMobileNumber=" + customerMobileNumber + ", customerTelephoneNumber1=" + customerTelephoneNumber1
				+ ", customerTelephoneNumber2=" + customerTelephoneNumber2 + ", customerAddressLine1=" + customerAddressLine1
				+ ", customerAddressLine2=" + customerAddressLine2 + ", shipmentType=" + shipmentType + ", numberOfItems="
				+ numberOfItems + ", emailAddress=" + emailAddress + ", carriageValue=" + carriageValue + ", carriageCurrency="
				+ carriageCurrency + ", codAmount=" + codAmount + ", weight=" + weight + ", customsValue=" + customsValue
				+ ", customsCurrency=" + customsCurrency + ", insuranceAmount=" + insuranceAmount + ", insuranceCurrency="
				+ insuranceCurrency + ", itemDescription=" + itemDescription + ", shipperName=" + shipperName
				+ ", shipperContactName=" + shipperContactName + ", shipperAddress1=" + shipperAddress1 + ", shipperAddress2="
				+ shipperAddress2 + ", shipperCity=" + shipperCity + ", shipperPhone=" + shipperPhone + ", shipperCountry="
				+ shipperCountry + ", preferredDeliveryDate=" + preferredDeliveryDate + ", gpsPoints=" + gpsPoints + "]";
	}




}
