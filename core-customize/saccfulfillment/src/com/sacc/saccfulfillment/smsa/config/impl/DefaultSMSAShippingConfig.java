package com.sacc.saccfulfillment.smsa.config.impl;

import org.apache.axis.AxisFault;

import com.sacc.saccfulfillment.smsa.config.SMSAShippingConfig;
import com.smsaexpress.track.secom.SMSAWebServiceSoapProxy;
import com.smsaexpress.track.secom.SMSAWebServiceSoapStub;


/**
 * @author Husam Dababneh
 *
 */
public class DefaultSMSAShippingConfig implements SMSAShippingConfig
{

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.smsashipping.config.SMSAShippingConfig#getSDKService()
	 */
	@Override
	public SMSAWebServiceSoapStub getStubService() throws AxisFault
	{
		return new SMSAWebServiceSoapStub();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.smsashipping.config.SMSAShippingConfig#getProxyService()
	 */
	@Override
	public SMSAWebServiceSoapProxy getProxyService()
	{
		return new SMSAWebServiceSoapProxy();
	}

}
