package com.sacc.saccfulfillment.smsa.config;

import org.apache.axis.AxisFault;

import com.smsaexpress.track.secom.SMSAWebServiceSoapProxy;
import com.smsaexpress.track.secom.SMSAWebServiceSoapStub;


/**
 * @author Husam Dababneh
 *
 */
public interface SMSAShippingConfig
{

	/**
	 * Get Stub SDK Service
	 *
	 * @return SMSAWebServiceSoapStub
	 * @throws AxisFault
	 */
	public SMSAWebServiceSoapStub getStubService() throws AxisFault;

	/**
	 * Get Proxy SDK Service
	 *
	 * @return SMSAWebServiceSoapProxy
	 */
	public SMSAWebServiceSoapProxy getProxyService();
}
