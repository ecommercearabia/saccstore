/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.enums.FulfillmentActionHistoryType;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.exception.enums.FulfillmentExceptionType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.FullfillmentHistoryEntryModel;
import com.sacc.saccfulfillment.model.SaeeFulfillmentProviderModel;
import com.sacc.saccfulfillment.saee.exceptions.SaeeException;
import com.sacc.saccfulfillment.saee.model.SaeeOrderData;
import com.sacc.saccfulfillment.saee.model.responses.Details;
import com.sacc.saccfulfillment.saee.service.SaeeService;
import com.sacc.saccfulfillment.service.CarrierService;
import com.sacc.saccfulfillment.service.FulfillmentService;
import com.sacc.saccpayment.enums.PaymentModeType;


/**
 * @author Husam Dababneh
 */
public class DefaultSaeeFulfillmentService implements FulfillmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultSaeeFulfillmentService.class);

	private static final String ORDER_MUST_NOT_BE_NULL = "Order Must not be null";
	private static final String DELIVERY_ADDRESS_MUST_NOT_BE_NULL = "Deliviry Address must not be null";
	private static final String CITY_MUST_NOT_BE_NULL = "City must not be null";

	@Resource(name = "saeeService")
	private SaeeService saeeService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "saeeFulfillmentStatusMap")
	private Map<Integer, ConsignmentStatus> saeeFulfillmentStatusMap;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("creating SaeeShipment");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;
		final SaeeOrderData data = prepareSAEEShipmentData(consignmentModel, saeefulfillmentProviderModel);

		saveShipmentRequest(consignmentModel, data.toString(), Operation.CREATE);

		Optional<String> trackingID = Optional.empty();
		try
		{
			trackingID = saeeService.createShipment(data, saeefulfillmentProviderModel.getBaseUrl());
		}
		catch (final SaeeException ex)
		{
			saveActionInHistory(consignmentModel, data.toString(), "Fail", FulfillmentActionHistoryType.CREATESHIPMENT);
			switch (ex.getType())
			{
				case MISSING_ATTRIBUTE:
					throw new FulfillmentException(FulfillmentExceptionType.MISSING_ARGUMENT);
				case SERVER_DOWN:
					throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
			}
		}
		if (!trackingID.isPresent())
		{
			throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
		}
		modelService.refresh(consignmentModel);
		saveTrackingIdAndCarrier(saeefulfillmentProviderModel, consignmentModel, trackingID.get());
		saveShipmentResponse(consignmentModel, trackingID.get(), Operation.CREATE);

		saveActionInHistory(consignmentModel, data.toString(), trackingID.get(), FulfillmentActionHistoryType.CREATESHIPMENT);

		return trackingID;
	}

	/**
	 *
	 */
	private void saveActionInHistory(final ConsignmentModel consignmentModel, final String request, final String response,
			final FulfillmentActionHistoryType actionType)
	{
		final List<FullfillmentHistoryEntryModel> history = new ArrayList<FullfillmentHistoryEntryModel>();
		if (consignmentModel.getFulfillmentActionHistory() != null)
		{
			history.addAll(consignmentModel.getFulfillmentActionHistory());
		}

		final FullfillmentHistoryEntryModel historyEntry = modelService.create(FullfillmentHistoryEntryModel.class);
		historyEntry.setCarrier(consignmentModel.getCarrierDetails().getName());
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		historyEntry.setFulfillmentActionType(actionType);
		history.add(historyEntry);

		consignmentModel.setFulfillmentActionHistory(history);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
	}

	private SaeeOrderData prepareSAEEShipmentData(final ConsignmentModel consignmentModel,
			final SaeeFulfillmentProviderModel fulfillmentProviderModel) throws SaeeException
	{

		final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();


		LOG.info("Preparing Saee ShipmentData");

		final SaeeOrderData data = new SaeeOrderData();
		data.setSecret(fulfillmentProviderModel.getSecret());
		data.setSendername(fulfillmentProviderModel.getSendername());
		data.setSenderaddress(fulfillmentProviderModel.getSenderaddress());
		data.setSendercity(fulfillmentProviderModel.getSendercity());

		data.setSendercountry(fulfillmentProviderModel.getSendercountry());
		data.setSendermail(fulfillmentProviderModel.getSendermail());
		data.setSenderphone(fulfillmentProviderModel.getSenderphone());
		data.setQuantity(getQuantity(fulfillmentProviderModel));

		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MUST_NOT_BE_NULL);
		final OrderModel orderModel = (OrderModel) consignmentModel.getOrder();
		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(deliveryAddress.getCity() != null, CITY_MUST_NOT_BE_NULL);

		data.setOrdernumber(orderModel.getCode());
		data.setName(deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname());
		data.setMobile(deliveryAddress.getMobile());
		data.setStreetaddress(deliveryAddress.getAddressName());
		data.setDistrict(StringUtils.isEmpty(deliveryAddress.getDistrictName()) ? "" : deliveryAddress.getDistrictName());

		if (deliveryAddress.getCity().getSaeeCity() == null || StringUtils.isEmpty(deliveryAddress.getCity().getSaeeCity()))
		{
			throw new SaeeException(FulfillmentExceptionType.MISSING_ATTRIBUTE, "Saee Cities Should not be Empty or null", "");
		}

		data.setCity(deliveryAddress.getCity().getSaeeCity());
		data.setEmail(deliveryAddress.getEmail());
		data.setCashondelivery(getCodAmount(consignmentModel));
		data.setWeight(Float.valueOf(getWeightOfItems(consignmentModel)));
		final Optional<String> descs = getProductsSKUs(consignmentModel);
		data.setDescription(descs.isPresent() ? descs.get() : "");


		LOG.info("Finished preparing Saee ShipmentData");
		return data;
	}

	/**
	 *
	 */
	private String getAddress(final AddressModel deliveryAddress)
	{
		return "Street: " + deliveryAddress.getStreetname() + "\r" + "Address:" + deliveryAddress.getAddressName() + "\r"
				+ "District:" + deliveryAddress.getDistrict() + "\r" + "Mobile: " + deliveryAddress.getDistrict() + "\r";
	}

	/**
	 *
	 */
	private Optional<String> getProductsSKUs(final ConsignmentModel consignmentModel)
	{
		final StringBuilder builder = new StringBuilder();

		for (final Iterator<ConsignmentEntryModel> it = consignmentModel.getConsignmentEntries().iterator(); it.hasNext();)
		{
			final ConsignmentEntryModel consignmentEntryModel = it.next();
			if (consignmentEntryModel != null)
			{
				builder.append(consignmentEntryModel.getOrderEntry().getProduct().getCode() + (it.hasNext() ? ", " : ""));
			}
		}
		return Optional.ofNullable(builder.toString());
	}

	/**
	 *
	 */
	private int getQuantity(final SaeeFulfillmentProviderModel fulfillmentProviderModel)
	{
		if (fulfillmentProviderModel.getQuantity() != null)
		{
			return fulfillmentProviderModel.getQuantity();
		}
		return 1;
	}

	/**
	 *
	 */
	private float getCodAmount(final ConsignmentModel consignmentModel)
	{
		if (PaymentModeType.CARD.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType())
				|| PaymentModeType.CONTINUE.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType()))
		{
			return 0.0f;
		}
		else if (PaymentModeType.NOCARD.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType()))
		{
			return consignmentModel.getOrder().getTotalPrice().floatValue();
		}
		return 0.0f;
	}

	/**
	 *
	 */
	private String getWeightOfItems(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getPackagingInfo() == null ? "" : consignmentModel.getPackagingInfo().getGrossWeight();
	}

	/**
	 *
	 */
	private int getNumberOfItems(final ConsignmentModel consignmentModel)
	{
		if (consignmentModel == null || consignmentModel.getConsignmentEntries() == null
				|| consignmentModel.getConsignmentEntries().isEmpty())
		{
			return 0;
		}
		int count = 0;
		for (final ConsignmentEntryModel consignmentEntryModel : consignmentModel.getConsignmentEntries())
		{
			count += consignmentEntryModel.getQuantity() == null ? 0 : consignmentEntryModel.getQuantity().intValue();
		}

		return count;
	}

	private enum Operation
	{
		CREATE, UPDATE, STATUS
	}

	private void saveTrackingIdAndCarrier(final SaeeFulfillmentProviderModel saeeFulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId)
	{
		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		consignmentModel.setCarrierDetails(carrierService.create(saeeFulfillmentProviderModel.getCode(),
				saeeFulfillmentProviderModel.getName(), FulfillmentProviderType.SAEE));
		saveConsignment(consignmentModel);
	}

	private void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentRequestBody(request);
				break;
			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveConsignment(final ConsignmentModel consignment)
	{
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting PDF for Saee Shipment order");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;

		try
		{
			final Optional<byte[]> pdf = saeeService.getPDF(consignmentModel.getTrackingID(),
					saeefulfillmentProviderModel.getBaseUrl());

			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Success",
					FulfillmentActionHistoryType.PRINTAWB);
			return pdf;
		}
		catch (final Exception e)
		{
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail",
					FulfillmentActionHistoryType.CREATESHIPMENT);
			throw new FulfillmentException(FulfillmentExceptionType.CLIENT_ERROR);
		}
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting status for Saee Shipment order");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;
		final Optional<Details> status = saeeService.getStatus(consignmentModel.getTrackingID(),
				saeefulfillmentProviderModel.getBaseUrl());

		final ConsignmentStatus consignmentStatus = saeeFulfillmentStatusMap.get(status.get().getStatus());
		consignmentModel.setFulfillmentStatus(consignmentStatus);
		consignmentModel.setFulfillmentStatusText(status.get().getNotes());

		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);

		saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(),
				consignmentStatus == null ? "No mapping found for status: " + status.get().getStatus() : consignmentStatus.toString(),
				FulfillmentActionHistoryType.GETSTATUS);

		return Optional.ofNullable(consignmentStatus == null ? null : consignmentStatus.toString());
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		final Optional<String> currentStatus = getStatus(consignmentModel, fulfillmentProviderModel);
		if (currentStatus.isEmpty())
		{
			LOG.error("Couldn't fetch consignment status");
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail",
					FulfillmentActionHistoryType.UPDATESTATUS);
			throw new FulfillmentException(FulfillmentExceptionType.COULDNT_FETCH_STATUS);
		}
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(ConsignmentStatus.valueOf(currentStatus.get())))
		{
			updateConsignmentStatus(consignmentModel, ConsignmentStatus.valueOf(currentStatus.get()));
		}

		saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), currentStatus.get(),
				FulfillmentActionHistoryType.UPDATESTATUS);

		modelService.refresh(consignmentModel);
		return Optional.ofNullable(consignmentModel.getStatus());

	}

	private void updateConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus newStatus)
	{
		modelService.refresh(consignment);
		consignment.setStatus(newStatus);
		consignment.setActualDeliveryDate(LocalDate.now().format(DateTimeFormatter.ofPattern("d/M/yyyy")));
		modelService.save(consignment);
		LOG.info("Consignment status updated for consignment: " + consignment.getCode());
	}
}
