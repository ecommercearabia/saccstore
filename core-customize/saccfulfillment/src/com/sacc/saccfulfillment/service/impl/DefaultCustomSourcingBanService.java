/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.impl;

import de.hybris.platform.warehousing.sourcing.ban.service.impl.DefaultSourcingBanService;

import javax.annotation.Resource;

import com.sacc.saccfulfillment.dao.SourcingBanConfigurationDao;
import com.sacc.saccfulfillment.model.SourcingBanConfigModel;
import com.sacc.saccfulfillment.service.CustomSourcingBanService;


/**
 * @author monzer
 */
public class DefaultCustomSourcingBanService extends DefaultSourcingBanService implements CustomSourcingBanService
{

	@Resource(name = "sourcingBanConfigurationDao")
	private SourcingBanConfigurationDao sourcingBanConfigDao;

	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		return sourcingBanConfigDao.getSourcingBanConfiguration();
	}

}
