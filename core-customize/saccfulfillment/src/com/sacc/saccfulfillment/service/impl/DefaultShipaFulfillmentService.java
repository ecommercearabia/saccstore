/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.model.PackagingInfoModel;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.exception.enums.FulfillmentExceptionType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.ShipaFulfillmentProviderModel;
import com.sacc.saccfulfillment.service.CarrierService;
import com.sacc.saccfulfillment.service.FulfillmentService;
import com.sacc.saccfulfillment.shipa.exception.ShipaException;
import com.sacc.saccfulfillment.shipa.model.Order;
import com.sacc.saccfulfillment.shipa.model.Recipient;
import com.sacc.saccfulfillment.shipa.model.Sender;
import com.sacc.saccfulfillment.shipa.model.cancel.ResponseStatus;
import com.sacc.saccfulfillment.shipa.model.getorder.GetOrderResponse;
import com.sacc.saccfulfillment.shipa.model.orderdata.OrderData;
import com.sacc.saccfulfillment.shipa.model.success.ResponseSucceed;
import com.sacc.saccfulfillment.shipa.service.ShipaService;
import com.sacc.sacctimeslot.model.TimeSlotInfoModel;


/**
 * @author mohammad-abu-muhasien
 */
public class DefaultShipaFulfillmentService implements FulfillmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultShipaFulfillmentService.class);
	/**
	 *
	 */
	private static final String PROVIDER_CAN_NOT_BE_NULL = "Provider Can not be null";

	private static final String CONSIGNMENT_MODEL_MUSTN_T_BE_NULL = "ConsignmentModel mustn't be null or empty";
	private static final String CONSIGNMENT_ORDER_MUSTN_T_BE_NULL = "ConsignmentModel Order mustn't be null or empty";
	private static final String FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL = "FulfillmentProviderModel Order mustn't be null or empty";
	private static final String DELIVERY_ADDRESS_MUSTN_T_BE_NULL = "deliveryAddress  mustn't be null or empty";
	private static final String PACKAGING_INFO_MUSTN_T_BE_NULL = "packagingInfo  mustn't be null or empty";
	private static final String REFERENCE_MUSTN_T_BE_NULL = "Reference Id  mustn't be null or empty";
	private static final String SENT_ORDERS_CAN_NOT_BE_ZERO_OR_NOLL = "Sent Orders  list can not be zero or null";
	private static final String UPDATED_ORDER_RESPONSE_CAN_NOT_BE_EMPTY_OR_NOLL = "Update orders Response   can not be empty or null";
	private static final String CASH_ON_DELIVERY = "CashOnDelivery";
	private static final String PREPAID = "Prepaid";
	private static final String CCOD = "CCOD";
	@Resource(name = "shipaService")
	private ShipaService shipaService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;


	@Resource(name = "defaultFulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "shipaFulfillmentStatusMap")
	private Map<String, ConsignmentStatus> shipaFulfillmentStatusMap;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder() != null, CONSIGNMENT_ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;
		final Order selectedOrder = getSelectedOrder(consignmentModel, shipaFulfillmentProviderModel);
		Preconditions.checkArgument(selectedOrder != null, SENT_ORDERS_CAN_NOT_BE_ZERO_OR_NOLL);
		LOG.info(selectedOrder);
		saveShipmentRequest(consignmentModel, selectedOrder.toString(), Operation.CREATE);

		final List<ResponseSucceed> sendOrders = shipaService.sendOrders(shipaFulfillmentProviderModel.getBaseUrl(),
				shipaFulfillmentProviderModel.getApiKey(), new Order[]
				{ selectedOrder });

		saveShipmentResponse(consignmentModel, sendOrders.get(0).toString(), Operation.CREATE);

		saveTrackingIdAndCarrier(shipaFulfillmentProviderModel, consignmentModel,
				sendOrders.get(0).getDeliveryInfo().getReference());

		final OrderData orderDataInfo = getOrderDataInfo(consignmentModel, shipaFulfillmentProviderModel);
		saveShipmentRequest(consignmentModel, orderDataInfo.toString(), Operation.UPDATE);
		LOG.info(orderDataInfo.toString());
		modelService.refresh(consignmentModel);
		try
		{
			Thread.sleep(10000);
		}
		catch (final InterruptedException e)
		{
			LOG.error("Error sleeping thread for 10 seconds", e);
		}
		final ResponseStatus updateOrderDataByReferenceId = shipaService.updateOrderDataByReferenceId(
				shipaFulfillmentProviderModel.getBaseUrl(), shipaFulfillmentProviderModel.getApiKey(),
				consignmentModel.getTrackingID(), orderDataInfo);
		LOG.info(updateOrderDataByReferenceId.toString());
		saveShipmentResponse(consignmentModel, updateOrderDataByReferenceId.toString(), Operation.UPDATE);
		Preconditions.checkArgument(updateOrderDataByReferenceId != null, UPDATED_ORDER_RESPONSE_CAN_NOT_BE_EMPTY_OR_NOLL);

		return Optional.ofNullable(consignmentModel.getTrackingID());
	}

	private void saveTrackingIdAndCarrier(final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId)
	{
		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		consignmentModel.setCarrierDetails(carrierService.create(shipaFulfillmentProviderModel.getCode(),
				shipaFulfillmentProviderModel.getName(), FulfillmentProviderType.SHIPA));
		saveConsignment(consignmentModel);
	}

	private void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentRequestBody(request);
				break;
			case UPDATE:
				consignment.setUpdateShipmentRequestBody(request);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case UPDATE:
				consignment.setUpdateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveConsignment(final ConsignmentModel consignment)
	{
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	protected Order getSelectedOrder(final ConsignmentModel consignmentModel,
			final ShipaFulfillmentProviderModel fulfillmentProviderModel)
	{
		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUSTN_T_BE_NULL);

		final Sender sender = new Sender(fulfillmentProviderModel.getSenderAddress(), fulfillmentProviderModel.getSenderEmail(),
				fulfillmentProviderModel.getSenderName(), fulfillmentProviderModel.getSenderPhone());

		final Order order = new Order();
		final Recipient recipient = new Recipient(deliveryAddress.getCountry().getName(), deliveryAddress.getCity().getName(),
				deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname(), deliveryAddress.getMobile());
		order.setId(consignmentModel.getCode() + "_" + System.currentTimeMillis());
		order.setPaymentMethod(getPaymentMethod(consignmentModel.getOrder().getPaymentMode()));
		order.setGoodsValue(String.valueOf(consignmentModel.getOrder().getTotalPrice()));
		order.setAmount(consignmentModel.getOrder().getTotalPrice());
		if (!CASH_ON_DELIVERY.equalsIgnoreCase(order.getPaymentMethod()) && !CCOD.equalsIgnoreCase(order.getPaymentMethod()))
		{
			order.setAmount(0);
		}
		order.setSender(sender);
		order.setDescription(fulfillmentProviderModel.getDescription());

		if (fulfillmentProviderModel.getShipaDeliveryType() != null)
		{
			order.setTypeDelivery(fulfillmentProviderModel.getShipaDeliveryType().getCode().toLowerCase());
		}

		order.setRecipient(recipient);

		return order;
	}

	/**
	 *
	 */
	protected String getPaymentMethod(final PaymentModeModel paymentMode)
	{
		if (paymentMode != null)
		{
			switch (paymentMode.getPaymentModeType())
			{
				case CARD:
				case CONTINUE:
					return PREPAID;
				case NOCARD:
					if ("ccod".equalsIgnoreCase(paymentMode.getCode()))
					{
						return CCOD;
					}
					return CASH_ON_DELIVERY;
			}
		}
		return null;
	}

	protected String getAddress(final AddressModel deliveryAddress)
	{
		final StringBuilder address = new StringBuilder();
		if (deliveryAddress.getCountry() != null && !StringUtils.isEmpty(deliveryAddress.getCountry().getName()))
		{
			address.append(deliveryAddress.getCountry().getName()).append("_");
		}
		if (!StringUtils.isEmpty(deliveryAddress.getLine1()))
		{
			address.append("Address Line 1: ").append(deliveryAddress.getLine1()).append(", ");
		}
		if (deliveryAddress.getCity() != null && !StringUtils.isEmpty(deliveryAddress.getCity().getName()))
		{
			address.append("City: ").append(deliveryAddress.getCity().getName()).append(", ");
		}
		if (deliveryAddress.getArea() != null && !StringUtils.isEmpty(deliveryAddress.getArea().getName()))
		{
			address.append("Area: ").append(deliveryAddress.getArea().getName()).append(", ");
		}
		if (!StringUtils.isEmpty(deliveryAddress.getNearestLandmark()))
		{
			address.append("Nearest Landmark: ").append(deliveryAddress.getNearestLandmark());
		}
		return address.toString();
	}

	protected String getEstimatedDeliveryDate(final TimeSlotInfoModel timeSlotInfo)
	{
		if (timeSlotInfo != null)
		{
			final LocalTime localTime = LocalTime.parse(timeSlotInfo.getEnd(), DateTimeFormatter.ofPattern("H:mm"));
			return timeSlotInfo.getDate() + " " + localTime.format(DateTimeFormatter.ofPattern("hh:mm:ss a"));
		}
		else
		{
			LOG.warn("TimeSlotInfo not found on Order, setting default estimated delivery date on update shipment request.");
			return ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Dubai")).plusDays(1)
					.format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss a"));
		}
	}

	protected OrderData getOrderDataInfo(final ConsignmentModel consignmentModel, final ShipaFulfillmentProviderModel provider)
			throws FulfillmentException
	{

		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		final PackagingInfoModel packagingInfo = consignmentModel.getPackagingInfo();
		final TimeSlotInfoModel timeSlotInfo = consignmentModel.getOrder().getTimeSlotInfo();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUSTN_T_BE_NULL);

		final OrderData orderData = new OrderData();
		if (packagingInfo != null)
		{
			orderData.setEstimatedDeliveryDate(getEstimatedDeliveryDate(timeSlotInfo));
			orderData.setHeight(getDouble(packagingInfo.getHeight()));
			orderData.setLength(getDouble(packagingInfo.getLength()));
			orderData.setWeight(getDouble(packagingInfo.getGrossWeight()));
			orderData.setWidth(getDouble(packagingInfo.getWidth()));
			orderData.setQuantity(consignmentModel.getConsignmentEntries().stream().filter(Objects::nonNull)
					.map(ConsignmentEntryModel::getQuantity).collect(Collectors.summingLong(Long::longValue)));
			setCoordinates(deliveryAddress, orderData);
			orderData.setRecipientAddress(getAddress(deliveryAddress));
			orderData.setReady(true);
		}

		return orderData;

	}

	private void setCoordinates(final AddressModel deliveryAddress, final OrderData orderData) throws FulfillmentException
	{
		if (deliveryAddress.getLongitude() != null && deliveryAddress.getLatitude() != null
				&& deliveryAddress.getLongitude().doubleValue() + deliveryAddress.getLatitude().doubleValue() > 0)
		{
			orderData.setRecipientCoordinates(deliveryAddress.getLatitude() + "," + deliveryAddress.getLongitude());
		}
		else
		{
			setDefaultCoordinates(deliveryAddress, orderData);
		}
	}

	private void setDefaultCoordinates(final AddressModel deliveryAddress, final OrderData orderData) throws FulfillmentException
	{
		if (deliveryAddress.getArea() != null && deliveryAddress.getArea().getLatitude() != null
				&& deliveryAddress.getArea().getLongitude() != null
				&& deliveryAddress.getArea().getLongitude().doubleValue() + deliveryAddress.getArea().getLatitude().doubleValue() > 0)
		{
			LOG.warn("Coordinates not found on deliveryAddress, setting coordinates from deliveryAddress.area");
			orderData
					.setRecipientCoordinates(deliveryAddress.getArea().getLatitude() + "," + deliveryAddress.getArea().getLongitude());
		}
		else if (deliveryAddress.getCity() != null && deliveryAddress.getCity().getLatitude() != null
				&& deliveryAddress.getCity().getLongitude() != null
				&& deliveryAddress.getCity().getLongitude().doubleValue() + deliveryAddress.getCity().getLatitude().doubleValue() > 0)
		{
			LOG.warn("Coordinates not found on deliveryAddress, setting coordinates from deliveryAddress.city");
			orderData
					.setRecipientCoordinates(deliveryAddress.getCity().getLatitude() + "," + deliveryAddress.getCity().getLongitude());
		}
		else
		{
			LOG.error("No latitude and longitude found on either deliveryAddress, city and area.");
			throw new ShipaException(FulfillmentExceptionType.MISSING_COORDINATES);
		}
	}

	private double getDouble(final String value)
	{
		if (StringUtils.isEmpty(value))
		{
			return 0;
		}
		return Double.parseDouble(value);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws ShipaException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final String referenceId = consignmentModel.getTrackingID();
		Preconditions.checkArgument(referenceId != null, REFERENCE_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;

		final String shipaPdfTemplate = shipaFulfillmentProviderModel.getShipaPdfTemplate() != null
				? shipaFulfillmentProviderModel.getShipaPdfTemplate().getCode().toLowerCase()
				: null;

		return Optional.ofNullable(shipaService.downloadOrderByReferenceID(shipaFulfillmentProviderModel.getBaseUrl(),
				shipaFulfillmentProviderModel.getApiKey(), referenceId, shipaFulfillmentProviderModel.getCopies(), shipaPdfTemplate)
				.getBody());

	}

	private enum Operation
	{
		CREATE, UPDATE, STATUS
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(fulfillmentProviderModel != null, FULFILLMENT_PROVIDER_MUSTN_T_BE_NULL);

		final String trackingID = consignmentModel.getTrackingID();
		Preconditions.checkArgument(trackingID != null, REFERENCE_MUSTN_T_BE_NULL);

		final ShipaFulfillmentProviderModel shipaFulfillmentProviderModel = (ShipaFulfillmentProviderModel) fulfillmentProviderModel;

		LOG.info("Getting consignment status for tracking ID: " + trackingID);
		final GetOrderResponse getOrderResponse = shipaService.getOrderByReferenceID(shipaFulfillmentProviderModel.getBaseUrl(),
				shipaFulfillmentProviderModel.getApiKey(), trackingID);
		LOG.info(getOrderResponse);
		saveShipmentResponse(consignmentModel, getOrderResponse.toString(), Operation.STATUS);

		return Optional.ofNullable(getOrderResponse.getDeliveryInfo().getCodeStatus());
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		final Optional<String> currentStatus = getStatus(consignmentModel, fulfillmentProviderModel);
		if (!currentStatus.isPresent())
		{
			LOG.error("Couldn't fetch consignment status");
			throw new FulfillmentException(FulfillmentExceptionType.COULDNT_FETCH_STATUS);
		}
		updateConsignmentStatus(consignmentModel, shipaFulfillmentStatusMap.get(currentStatus.get().trim().toUpperCase()));
		modelService.refresh(consignmentModel);
		return Optional.ofNullable(consignmentModel.getStatus());
	}

	private void updateConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus newStatus)
	{
		modelService.refresh(consignment);
		consignment.setStatus(newStatus);
		modelService.save(consignment);
		LOG.info("Consignment status updated for consignment: " + consignment.getCode());
	}
}