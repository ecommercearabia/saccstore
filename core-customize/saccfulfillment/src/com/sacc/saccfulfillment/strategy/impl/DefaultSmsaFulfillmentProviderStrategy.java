/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;
import com.sacc.saccfulfillment.service.FulfillmentProviderService;
import com.sacc.saccfulfillment.strategy.FulfillmentProviderStrategy;


/**
 * @author Husam Dababneh
 */
public class DefaultSmsaFulfillmentProviderStrategy implements FulfillmentProviderStrategy
{
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	protected FulfillmentProviderService getFulfillmentProviderService()
	{
		return fulfillmentProviderService;
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getFulfillmentProviderService().getActive(baseStoreUid, SmsaFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getFulfillmentProviderService().getActive(baseStoreModel, SmsaFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getFulfillmentProviderService().getActiveProviderByCurrentBaseStore(SmsaFulfillmentProviderModel.class);
	}

}
