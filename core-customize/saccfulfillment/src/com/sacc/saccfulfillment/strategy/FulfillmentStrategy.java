/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.strategy;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentStrategy
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel,
			FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException;
}
