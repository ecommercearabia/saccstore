/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.dao.impl;

import com.sacc.saccfulfillment.model.SaeeFulfillmentProviderModel;


/**
 * @author Husam Dababneh
 */
public class DefaultSaeeFulfillmentProviderDao extends DefaultFulfillmentProviderDao
{

	public DefaultSaeeFulfillmentProviderDao()
	{
		super(SaeeFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return SaeeFulfillmentProviderModel._TYPECODE;
	}

}
