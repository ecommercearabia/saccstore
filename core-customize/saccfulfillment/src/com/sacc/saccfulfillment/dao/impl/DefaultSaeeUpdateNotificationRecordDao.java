/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.dao.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.dao.SaeeUpdateNotificationRecordDao;
import com.sacc.saccfulfillment.model.SaeeUpdateNotificationRecordModel;


/**
 * The Class DefaultSaeeUpdateNotificationRecordDao.
 *
 * @author monzer
 */
public class DefaultSaeeUpdateNotificationRecordDao implements SaeeUpdateNotificationRecordDao
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSaeeUpdateNotificationRecordDao.class);

	/** The Constant QUERY. */
	private static final String QUERY = "SELECT {PK} FROM {" + SaeeUpdateNotificationRecordModel._TYPECODE + "}";

	/** The Constant WHERE_DONE. */
	private static final String WHERE_DONE = "WHERE {" + SaeeUpdateNotificationRecordModel.DONE + "}=?"
			+ SaeeUpdateNotificationRecordModel.DONE;

	/** The Constant WHERE_SECRET. */
	private static final String WHERE_SECRET = " AND {" + SaeeUpdateNotificationRecordModel.SECRETKEY + "}=?"
			+ SaeeUpdateNotificationRecordModel.SECRETKEY;

	/** The Constant WHERE_TRACKING_ID. */
	private static final String WHERE_TRACKING_ID = " AND {" + SaeeUpdateNotificationRecordModel.TRACKINGID + "}=?"
			+ SaeeUpdateNotificationRecordModel.TRACKINGID;

	/** The Constant WHERE_REQUEST_STATUS. */
	private static final String WHERE_REQUEST_STATUS = " AND {" + SaeeUpdateNotificationRecordModel.REQUESTSTATUS + "}=?"
			+ SaeeUpdateNotificationRecordModel.REQUESTSTATUS;

	private static final String WHERE_AUTHORIZED = " AND {" + SaeeUpdateNotificationRecordModel.AUTHORIZED + "}=?"
			+ SaeeUpdateNotificationRecordModel.AUTHORIZED;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The flexible search service. */
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * Creates the notification record.
	 *
	 * @required @param secretKey the secret key
	 * @required @param response the response
	 * @required @param trackingId the tracking id
	 * @required @param requestStatus the request status
	 * @required @param done the done
	 */
	@Override
	public void createNotificationRecord(final String requestBody, final String secretKey, final String response,
			final String trackingId,
			final String requestStatus, final boolean done, final Boolean authorized)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(requestBody), "requestBody is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(response), "response Status is null");
		final SaeeUpdateNotificationRecordModel model = modelService.create(SaeeUpdateNotificationRecordModel.class);
		model.setRequestBody(requestBody);
		model.setSecretKey(secretKey);
		model.setNotificationResponse(response);
		model.setTrackingId(trackingId);
		model.setRequestStatus(requestStatus);
		model.setDone(done);
		model.setAuthorized(authorized);
		modelService.save(model);
		LOG.info("Notification record saved successfully!");
	}

	/**
	 * Find all notification records.
	 *
	 * @return the optional
	 */
	@Override
	public Optional<List<SaeeUpdateNotificationRecordModel>> findAllNotificationRecords()
	{
		final String query = QUERY;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final List<SaeeUpdateNotificationRecordModel> result = flexibleSearchService
				.<SaeeUpdateNotificationRecordModel> search(searchQuery).getResult();
		if (CollectionUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		return Optional.of(result);
	}

	/**
	 * Find all notification records by query.
	 *
	 * @param secretKey
	 *           the secret key
	 * @param trackingId
	 *           the tracking id
	 * @param requestStatus
	 *           the request status
	 * @required @param done the done
	 * @return the optional
	 */
	@Override
	public Optional<List<SaeeUpdateNotificationRecordModel>> findAllNotificationRecordsByQuery(final String secretKey,
			final String trackingId, final String requestStatus, final boolean done, final Boolean authorized)
	{
		final Map<String, Object> queryParamsMap = new HashedMap();
		final String query = buildQuery(secretKey, trackingId, requestStatus, done, queryParamsMap);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final List<SaeeUpdateNotificationRecordModel> result = flexibleSearchService
				.<SaeeUpdateNotificationRecordModel> search(searchQuery).getResult();
		if (CollectionUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		return Optional.of(result);
	}

	/**
	 * Builds the query.
	 *
	 * @param secretKey
	 *           the secret key
	 * @param trackingId
	 *           the tracking id
	 * @param requestStatus
	 *           the request status
	 * @param done
	 *           the done
	 * @param queryParams
	 *           the query params
	 * @return the string
	 */
	private String buildQuery(final String secretKey, final String trackingId, final String requestStatus,
			final boolean done, final Map<String, Object> queryParams)
	{
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(WHERE_DONE);
		queryParams.put(SaeeUpdateNotificationRecordModel.DONE, done);
		if (StringUtils.isNotBlank(secretKey))
		{
			queryBuilder.append(WHERE_SECRET);
			queryParams.put(SaeeUpdateNotificationRecordModel.SECRETKEY, secretKey);
		}
		if (StringUtils.isNotBlank(trackingId))
		{
			queryBuilder.append(WHERE_TRACKING_ID);
			queryParams.put(SaeeUpdateNotificationRecordModel.TRACKINGID, trackingId);
		}
		if (StringUtils.isNotBlank(requestStatus))
		{
			queryBuilder.append(WHERE_REQUEST_STATUS);
			queryParams.put(SaeeUpdateNotificationRecordModel.REQUESTSTATUS, requestStatus);
		}
		return queryBuilder.toString();
	}

}
