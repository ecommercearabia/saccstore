/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.dao.impl;

import com.sacc.saccfulfillment.dao.FulfillmentProviderDao;
import com.sacc.saccfulfillment.model.ShipaFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipaFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipaFulfillmentProviderDao()
	{
		super(ShipaFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipaFulfillmentProviderModel._TYPECODE;
	}

}
