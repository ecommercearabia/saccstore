<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
-->
<items xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="items.xsd">

    <collectiontypes>
        <collectiontype code="GenderList" elementtype="Gender" autocreate="true" generate="true" type="list"/>
        <collectiontype code="SwatchColorSet" elementtype="SwatchColorEnum" autocreate="true" generate="true"
                        type="set"/>
		<collectiontype code="EmailAddressList" elementtype="EmailAddress" autocreate="true" generate="true" type="list" />
    </collectiontypes>

    <enumtypes>
        <enumtype generate="true" code="SwatchColorEnum" autocreate="true" dynamic="true">
            <value code="BLACK"/>
            <value code="BLUE"/>
            <value code="BROWN"/>
            <value code="GREEN"/>
            <value code="GREY"/>
            <value code="ORANGE"/>
            <value code="PINK"/>
            <value code="PURPLE"/>
            <value code="RED"/>
            <value code="SILVER"/>
            <value code="WHITE"/>
            <value code="YELLOW"/>
        </enumtype>
        
        <enumtype code="IntegrationProvider" generate="true" autocreate="true" dynamic="true">
                  <description>This ENUM type will be used for specifying integration type</description>
                  <value code="ERP" />
                  <value code="HYPERPAY" />
                  <value code="SAEE" />
                  <value code="SMSA" />
            </enumtype>
            
            <enumtype generate="true" code="SerialNumberSource" autocreate="true">
            <value code="ORDER"/>
            <value code="CONSIGNMENT"/>
            <value code="ABSTRACT_ORDER"/>
            <value code="RETURN"/>
        </enumtype>
    </enumtypes>

    <itemtypes>
		<itemtype code="BaseStore" autocreate="false" generate="false">
			<description>Extends existing store type with additional attributes.</description>
			<attributes>
				<attribute type="boolean" qualifier="stockLevelControlEnable">
					<persistence type="property" />
					<modifiers optional="true" />
					<description>Stock level storefront control enable</description>
				</attribute>
				<attribute qualifier="captchaPublicKey" autocreate="true"
		                              type="java.lang.String">
					<description>Recaptcha public key</description>
					<persistence type="property" />
					<modifiers read="true" write="true" search="false"
		                                    initial="false" optional="true" partof="true" />
				</attribute>
				<attribute qualifier="captchaPrivateKey" autocreate="true"
		                              type="java.lang.String">
					<description>Recaptcha private key</description>
					<persistence type="property" />
					<modifiers read="true" write="true" search="false"
		                                    initial="false" optional="true" partof="true" />
				</attribute>
				<attribute type="java.lang.String" qualifier="vatNumber">
					<description>Sacc VatNumber </description>
					<persistence type="property" />
					<modifiers read="true" write="true" search="false"
		                                    initial="true" optional="false"/>
				</attribute>
				<attribute type="SerialNumberConfig" qualifier="orderSerialNumberConfig">
					<description>the serial number for abstract Order number</description>
					<persistence type="property"/>
					<modifiers read="true" write="true" search="true"/>
				</attribute>
				<attribute type="SerialNumberConfig" qualifier="returnSerialNumberConfig">
					<description>the serial number for abstract Order number</description>
					<persistence type="property"/>
					<modifiers read="true" write="true" search="true"/>
				</attribute>
				<attribute type="Media" qualifier="saccIcon">
					<persistence type="property" />
					<modifiers read="true" write="true" optional="true" />
					<description>SACC icon</description>
				</attribute>
				<attribute type="boolean" qualifier="enableWHReallocation">
					<persistence type="property" />
					<modifiers read="true" write="true" optional="true" />
					<description>Enable Reallocation on warehousing</description>
				</attribute>
				<attribute type="boolean" qualifier="enableForceDefaultLanguage">
					<description>Enable/Disable Default Language</description>
					<persistence type="property"/>
					<modifiers read="true" write="true" search="true" optional="false"/>
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
			</attributes>
		</itemtype>
            
            <itemtype code="SerialNumberConfig" autocreate="true" generate="true">
            <deployment table="SerialNumberConfig" typecode="10920"/>
            <attributes>
                <attribute type="java.lang.String" qualifier="code">
                    <description>invoice Number Character Format</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="int" qualifier="number">
                    <description>Serial number</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="char" qualifier="repeatedChar">
                    <description>Serial number repeated char</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="int" qualifier="numberOfDigits">
                    <description>Number Of Digits</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="int" qualifier="incrementValue">
                    <description>Value of number increment</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="java.lang.String" qualifier="prefix">
                    <description>prefix</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="java.lang.String" qualifier="suffix">
                    <description>suffix</description>
                    <modifiers optional="true" read="true" write="true"/>
                    <persistence type="property"/>
                </attribute>
                <attribute type="boolean" qualifier="enabled">
                        <description>enabled</description>
                        <persistence type="property"/>
                        <modifiers read="true" write="true" search="true"/>
                    </attribute>
            </attributes>
        </itemtype>
        
        <itemtype code="AbstractOrder" autocreate="false" generate="false">
            <attributes>
                <attribute qualifier="orderNumber" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true"
                        optional="true" />
                    <description>Order Number</description>
                </attribute>
            </attributes>
        </itemtype>
        
        <itemtype code="ReturnRequest" autocreate="false" generate="false">
                <description>Extending ReturnRequest type with additional attributes.</description>
                <attributes>
                    <attribute qualifier="salesOrderRefId" type="java.lang.String">
                        <description>Returned Order Reference Number</description>
                        <persistence type="property" />
                    </attribute>
                    <attribute qualifier="returnRefId" type="java.lang.String">
                        <description>Returned Reference Number</description>
                        <persistence type="property" />
                    </attribute>
                    <attribute qualifier="goodsAcceptanceDate" type="java.util.Date">
                        <description>Returned Reference Number</description>
                        <persistence type="property" />
                    </attribute>
                </attributes>
            </itemtype>
        
		<itemtype code="merchantTransaction" autocreate="true" generate="true">
			<deployment table="merchantTransaction" typecode="10921"/>
            <attributes>
                <attribute qualifier="id" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true" unique="true" optional="false" />
                    <description>Merchant Transaction Id </description>
                </attribute>
                <attribute qualifier="source" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true" optional="false" />
                    <description>Source</description>
                </attribute>
                <attribute qualifier="decision" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true"  optional="true" />
                    <description>Decision</description>
                </attribute>
                <attribute qualifier="resultCode" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true"  optional="true" />
                    <description>Result Code</description>
                </attribute>
                <attribute qualifier="version" type="java.lang.String">
                    <persistence type="property" />
                    <modifiers read="true" write="true" search="true"  optional="true" />
                    <description>Version</description>
                </attribute>
            </attributes>
        </itemtype>
        
        <!-- Add your item definitions here -->


        <!-- TypeGroups are for structure of this file only -->

        <typegroup name="Apparel">
            <itemtype code="ApparelProduct" extends="Product"
                      autocreate="true" generate="true"
                      jaloclass="com.sacc.core.jalo.ApparelProduct">
                <description>Base apparel product extension that contains additional attributes.</description>
                <attributes>
                    <attribute qualifier="genders" type="GenderList">
                        <description>List of genders that the ApparelProduct is designed for</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>

            <itemtype code="ApparelStyleVariantProduct" extends="VariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="com.sacc.core.jalo.ApparelStyleVariantProduct">
                <description>Apparel style variant type that contains additional attribute describing variant style.
                </description>
                <attributes>
                    <attribute qualifier="style" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Color/Pattern of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>

                    <attribute qualifier="swatchColors" type="SwatchColorSet">
                        <description>A normalized color mapping to a standardized front-end navigable name.
                        </description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>

            </itemtype>

            <itemtype code="ApparelSizeVariantProduct" extends="ApparelStyleVariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="com.sacc.core.jalo.ApparelSizeVariantProduct">
                <description>Apparel size variant type that contains additional attribute describing variant size.
                </description>
                <attributes>
                    <attribute qualifier="size" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Size of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>
        </typegroup>

        <typegroup name="Electronics">
            <itemtype code="ElectronicsColorVariantProduct" extends="VariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="com.sacc.core.jalo.ElectronicsColorVariantProduct">
                <description>Electronics color variant type that contains additional attribute describing variant color.
                </description>
                <attributes>
                    <attribute qualifier="color" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Color of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>
        </typegroup>
            
            <typegroup name="Emails">
                  <itemtype code="SendingErrorEmailProcess" autocreate="true" generate="true" extends="BusinessProcess"
                        jaloclass="com.sacc.core.jalo.SendingErrorEmailProcess">
                        <attributes>
                              <attribute type="java.lang.String" qualifier="errorMsg">
                                    <modifiers read="true" write="true" search="true"/>
                                    <persistence type="property">
                                          <columntype>
                                                <value>HYBRIS.JSON</value>
                                          </columntype>
                                    </persistence>
                              </attribute>
                              <attribute type="AbstractOrder" qualifier="order">
                                    <modifiers read="true" write="true" search="true"/>
                                    <persistence type="property"/>
                              </attribute>
                              <attribute type="IntegrationProvider" qualifier="integrationProvider">
                                    <modifiers read="true" write="true" search="true"/>
                                    <persistence type="property"/>
                              </attribute>
                        </attributes>
                  </itemtype>
                  <itemtype code="Customer" autocreate="false" generate="false">
						<attributes>
							<attribute qualifier="numberOfForgetPasswordRequests" type="int">
								<modifiers read="true" write="true" search="true" optional="true" />
								<persistence type="property" /> 
								<defaultvalue>0</defaultvalue>
								<description>how many time the customer request a reset for password</description>
							</attribute>
							<attribute qualifier="lastDateOfForgetPasswordRequest" type="java.util.Date">
								<modifiers read="true" write="true" search="true" optional="true" />
								<persistence type="property" />
								<description>Last date when customer request a password reset</description>
							</attribute>
						</attributes>
					</itemtype>
                  <itemtype code="CMSSite" autocreate="false" generate="false">
                        <attributes>
                        	  <attribute qualifier="maximumNumberOfForgetPasswordRequests" type="int">
                                    <modifiers read="true" write="true" search="true" optional="true" />
                                    <persistence type="property" />
                                    <description>Maximum number of allowed password rest requests</description>
                                    	<defaultvalue>0</defaultvalue>
                              </attribute>
                               <attribute type="boolean" qualifier="passwordResetRequestLimitationFeatureEnabled">
                                  <modifiers read="true" write="true" search="true" optional="true" />
                                  <persistence type="property" />
                                 <defaultvalue>Boolean.FALSE</defaultvalue>
                                  <description>Boolean to enable or disable password reset request limitation feature</description>
                              </attribute>
                              <attribute qualifier="integrationErrorEmailsReceiver" type="java.lang.String">
                                    <modifiers read="true" write="true" search="true" optional="true" />
                                    <persistence type="property" />
                                    <description>Integration Error Emails Receiver</description>
                              </attribute>
                             
                              <attribute qualifier="headerMetaTagScriptTop" type="java.lang.String">
                                    <modifiers read="true" write="true" search="true" />
                                    <persistence type="property">
                                          <columntype>
                                                <value>HYBRIS.JSON</value>
                                          </columntype>
                                    </persistence>
                                    <description>headerMetaTagScriptTop</description>
                              </attribute>
                              <attribute qualifier="headerMetaTagScriptBottom" type="java.lang.String">
                                    <modifiers read="true" write="true" search="true" />
                                    <persistence type="property">
                                          <columntype>
                                                <value>HYBRIS.JSON</value>
                                          </columntype>
                                    </persistence>
                                    <description>headerMetaTagScriptBottom</description>
                              </attribute>
                              <attribute qualifier="bodyMetaTagScriptTop" type="java.lang.String">
                                    <modifiers read="true" write="true" search="true" />
                                    <persistence type="property">
                                          <columntype>
                                                <value>HYBRIS.JSON</value>
                                          </columntype>
                                    </persistence>
                                    <description>bodyMetaTagScriptTop</description>
                              </attribute>
                              <attribute qualifier="bodyMetaTagScriptBottom" type="java.lang.String">
                                    <modifiers read="true" write="true" search="true" />
                                    <persistence type="property">
                                          <columntype>
                                                <value>HYBRIS.JSON</value>
                                          </columntype>
                                    </persistence>
                                    <description>bodyMetaTagScriptBottom</description>
                              </attribute>
                              <attribute qualifier="zakatCertificate" type="Media">
                                    <description>The zakat certificate file.</description>
                                    <persistence type="property" />
                              </attribute>
                              <attribute qualifier="paymentWarningMessage" type="localized:java.lang.String">
                                    <modifiers read="true" write="true" search="true" optional="true" />
                                    <persistence type="property" />
                                    <description>Payment Warning message</description>
                              </attribute>
                              <attribute qualifier="addressWarningMessage" type="localized:java.lang.String">
                                    <modifiers read="true" write="true" search="true" optional="true" />
                                    <persistence type="property" />
                                    <description>Address Warning message</description>
                              </attribute>
                              
                              <attribute qualifier="enableStarsReview" type="boolean">
                                  <modifiers read="true" write="true" search="true" optional="true" />
                                  <persistence type="property" />
                                 <defaultvalue>Boolean.FALSE</defaultvalue>
                                  <description>Boolean to enable or disable  Stars Rreview feature</description>
                              </attribute>
                              
                               <attribute type="boolean" qualifier="fixedCalcWeight">
                                  <modifiers read="true" write="true" search="true" optional="true" />
                                  <persistence type="property" />
                                 <defaultvalue>Boolean.FALSE</defaultvalue>
                                  <description>Boolean to enable or disable fixed Weight Calculation  </description>
                              </attribute>
                               <attribute type="boolean" qualifier="fixedCalcWeightWithoutQuantity">
                                  <modifiers read="true" write="true" search="true" optional="true" />
                                  <persistence type="property" />
                                 <defaultvalue>Boolean.FALSE</defaultvalue>
                                  <description>Boolean to enable or disable fixed Calculation Weight without quantity</description>
                              </attribute>
                              
                             
                        </attributes>
                  </itemtype>
            </typegroup>
            <typegroup name="StockControl">
                  <itemtype code="Product" autocreate="false" generate="false">
                        <attributes>
                              <attribute qualifier="stockDisplayAmount" type="int">
                                    <modifiers read="true" write="true" search="true"
                                          optional="true" />
                                    <persistence type="property" />
                                    <description>Stock Display Amount</description>
                              </attribute>
                        </attributes>
                  </itemtype>
            </typegroup>
            <typegroup name="CategoryCoreChanges">
			<itemtype code="Category" generate="false" autocreate="false">
			<attributes>
				<attribute qualifier="storeFrontName" type="localized:java.lang.String">
					<description>Store front tName</description>
					<modifiers read="true" write="true" search="true" optional="true"/>
					<persistence type="property"/>	
				</attribute>
				<attribute qualifier="displayName" type="java.lang.String">
					<modifiers write="false"/>
					<persistence type="dynamic" attributeHandler="categoryDisplayNameAttributeHandler" />
					<description>Display Name</description>
				</attribute>
			</attributes>
		</itemtype>
		</typegroup>
		
		    <typegroup name="Cronjob">
            <itemtype code="SaccShareFolderCronJob" autocreate="true" generate="true" extends="CronJob"
                      jaloclass="com.sacc.core.jalo.SaccShareFolderCronJob">
                <deployment table="SaccShareFolderCronJob" typecode="10923"/>
                <attributes>
                    <attribute qualifier="emailList" type="EmailAddressList">
                        <modifiers read="true" write="true" search="true" optional="true"/>
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="fromEmail" type="EmailAddress">
                        <modifiers read="true" write="true" search="true" optional="true"/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>
        </typegroup>

    </itemtypes>
</items>
