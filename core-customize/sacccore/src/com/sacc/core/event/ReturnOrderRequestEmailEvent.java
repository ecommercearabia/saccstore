package com.sacc.core.event;

import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;



/**
 * @author monzer
 */
public class ReturnOrderRequestEmailEvent extends AbstractEvent
{

	private ReturnRequestModel returnRequest;

	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process
	 *           the process
	 */
	public ReturnOrderRequestEmailEvent(final ReturnRequestModel returnRequest)
	{
		super();
		this.returnRequest = returnRequest;
	}

	/**
	 * @return the returnRequest
	 */
	public ReturnRequestModel getReturnRequest()
	{
		return returnRequest;
	}

	/**
	 * @param returnRequest
	 *           the returnRequest to set
	 */
	public void setReturnRequest(final ReturnRequestModel returnRequest)
	{
		this.returnRequest = returnRequest;
	}

}
