package com.sacc.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.google.common.base.Preconditions;
import com.sacc.Ordermanagement.model.ReturnOrderRequestProcessModel;


/**
 *
 * @author monzer
 *
 */
public class ReturnOrderRequestEmailListener extends AbstractAcceleratorSiteEventListener<ReturnOrderRequestEmailEvent>
{

	/** The model service. */
	private ModelService modelService;

	/** The business process service. */
	private BusinessProcessService businessProcessService;

	/**
	 * Gets the business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets the business process service.
	 *
	 * @param businessProcessService
	 *           the new business process service
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets the model service.
	 *
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * On site event.
	 *
	 * @param event
	 *           the event
	 */
	@Override
	protected void onSiteEvent(final ReturnOrderRequestEmailEvent event)
	{
		Preconditions.checkArgument(event.getReturnRequest() != null, "AbstractOrder must not be null.");
		final ReturnOrderRequestProcessModel returnOrderEmail = getBusinessProcessService().createProcess(
				"sendReturnOrderRequestEmail-process-" + event.getReturnRequest().getCode() + "-" + System.currentTimeMillis(),
				"returnOrderRequestEmail-process");
		returnOrderEmail.setReturnRequest(event.getReturnRequest());
		getModelService().save(returnOrderEmail);
		getBusinessProcessService().startProcess(returnOrderEmail);
	}

	/**
	 * Should handle event.
	 *
	 * @param event
	 *           the event
	 * @return true, if successful
	 */
	@Override
	protected boolean shouldHandleEvent(final ReturnOrderRequestEmailEvent event)
	{
		final OrderModel order = event.getReturnRequest().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel() != null;
	}

	/**
	 * Gets the site channel for event.
	 *
	 * @param event
	 *           the event
	 * @return the site channel for event
	 */
	@Override
	protected SiteChannel getSiteChannelForEvent(final ReturnOrderRequestEmailEvent event)
	{
		final OrderModel order = event.getReturnRequest().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}
}
