/**
 *
 */
package com.sacc.core.event;

import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.model.ERPConsignmentProcessModel;


/**
 * @author monzer
 *
 */
public class SyncERPConsignmentEventListener extends AbstractSiteEventListener<SyncERPConsignmentEvent>
{
	/**
	 *
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SyncERPConsignmentEventListener.class);
	/** The model service. */
	private ModelService modelService;

	/** The business process service. */
	private BusinessProcessService businessProcessService;

	/**
	 * Gets the business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets the business process service.
	 *
	 * @param businessProcessService
	 *           the new business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets the model service.
	 *
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final SyncERPConsignmentEvent event)
	{
		Preconditions.checkArgument(event != null, "Event must not be null or empty.");
		Preconditions.checkArgument(event.getConsignment() != null, "Consignment must not be null.");
		final ConsignmentModel consignment = event.getConsignment();

		final ERPConsignmentProcessModel eRPConsignmentProcessModel = CollectionUtils.isEmpty(
				consignment.getErpConsignmentProcesses()) ? createNewProcess(event) : consignment.getErpConsignmentProcesses().get(0);

		if (eRPConsignmentProcessModel == null)
		{
			LOG.error("No ERP Consignment process found or created!");
			return;
		}

		// Single erp consignment process for each consignment.
		if (ProcessState.CREATED.equals(eRPConsignmentProcessModel.getState()) || event.getProcessTarget() == null)
		{
			eRPConsignmentProcessModel.setErpConsignment(event.getConsignment());
			getBusinessProcessService().startProcess(eRPConsignmentProcessModel);
		}
		else if (ProcessState.RUNNING.equals(eRPConsignmentProcessModel.getState()))
		{
			LOG.error("Cannot restart the {} Process now since it is currently running", eRPConsignmentProcessModel.getCode());
		}
		else
		{
			LOG.info("Restarting process ...");
			getBusinessProcessService().restartProcess(eRPConsignmentProcessModel, event.getProcessTarget().getProcessCode());
		}

		getModelService().save(eRPConsignmentProcessModel);
	}

	private ERPConsignmentProcessModel createNewProcess(final SyncERPConsignmentEvent event)
	{
		return getBusinessProcessService().createProcess(
				"SyncERPConsignment-Process-" + event.getConsignment().getCode() + "-" + System.currentTimeMillis(),
				"erp-consignment-process");
	}

	@Override
	protected boolean shouldHandleEvent(final SyncERPConsignmentEvent event)
	{
		return event != null && event.getConsignment() != null;
	}

}
