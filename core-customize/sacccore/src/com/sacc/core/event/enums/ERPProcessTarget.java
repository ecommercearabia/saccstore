package com.sacc.core.event.enums;

public enum ERPProcessTarget
{
	PICK("syncPickWithERP"), PACK("syncPackWithERP"), CONFIRM_SHIP("syncConfirmShipWithERP");

	private String processCode;

	/**
	 * @param string
	 */
	ERPProcessTarget(final String processCode)
	{
		this.processCode = processCode;
	}

	/**
	 * @return the processCode
	 */
	public String getProcessCode()
	{
		return processCode;
	}

}
