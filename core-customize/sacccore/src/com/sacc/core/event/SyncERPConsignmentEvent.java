/**
 *
 */
package com.sacc.core.event;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import com.sacc.core.event.enums.ERPProcessTarget;



/**
 * @author monzer
 *
 */
public class SyncERPConsignmentEvent extends AbstractEvent
{

	private final ConsignmentModel consignment;
	private final ERPProcessTarget processTarget;

	/**
	 * @param consignment
	 */
	public SyncERPConsignmentEvent(final ConsignmentModel consignment, final ERPProcessTarget processTarget)
	{
		super();
		this.consignment = consignment;
		this.processTarget = processTarget;
	}

	/**
	 * @param consignment2
	 * @param status
	 */
	public SyncERPConsignmentEvent(final ConsignmentModel consignment, final ConsignmentStatus status)
	{
		super();
		this.consignment = consignment;
		switch (status)
		{
			case READY:
				processTarget = null;
				break;
			case PICKPACK:
				this.processTarget = ERPProcessTarget.PICK;
				break;
			case READY_FOR_PICKUP:
			case READY_FOR_SHIPPING:
				this.processTarget = ERPProcessTarget.PACK;
				break;
			case SHIPPED:
			case DELIVERY_COMPLETED:
				this.processTarget = ERPProcessTarget.CONFIRM_SHIP;
				break;
			default:
				this.processTarget = null;
		}
	}

	/**
	 * @return the consignment
	 */
	public ConsignmentModel getConsignment()
	{
		return consignment;
	}

	/**
	 * @return the processTarget
	 */
	public ERPProcessTarget getProcessTarget()
	{
		return processTarget;
	}
}
