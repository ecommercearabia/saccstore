/**
 *
 */
package com.sacc.core.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.core.context.SerialNumberConfigurationContext;
import com.sacc.core.enums.SerialNumberSource;
import com.sacc.core.strategies.SerialNumberConfigurationStrategy;

/**
 * @author monzer
 *
 */
public class DefualtSerialNumberConfigurationContext implements SerialNumberConfigurationContext
{

	@Resource(name = "serialNumberConfigurationStrategyMap")
	private Map<SerialNumberSource, SerialNumberConfigurationStrategy> serialNumberStrategyMap;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore(final SerialNumberSource source)
	{
		final Optional<SerialNumberConfigurationStrategy> strategy = getStrategyBySource(source);
		if(strategy.isPresent()) {
			return strategy.get().generateSerialNumberByCurrentStore();
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore, final SerialNumberSource source)
	{
		final Optional<SerialNumberConfigurationStrategy> strategy = getStrategyBySource(source);
		if (strategy.isPresent())
		{
			return strategy.get().generateSerialNumberForBaseStore(baseStore);
		}
		return Optional.empty();
	}

	/**
	 *
	 */
	private Optional<SerialNumberConfigurationStrategy> getStrategyBySource(final SerialNumberSource source)
	{
		return Optional.ofNullable(serialNumberStrategyMap.get(source));
	}

}
