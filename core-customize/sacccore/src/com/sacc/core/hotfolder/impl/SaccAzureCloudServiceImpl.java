package com.sacc.core.hotfolder.impl;


import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import com.sacc.core.hotfolder.SaccAzureCloudService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

/**
 * Implementation of SaccAzureCloudService interface for Azure Cloud Service
 */
public class SaccAzureCloudServiceImpl implements SaccAzureCloudService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(SaccAzureCloudServiceImpl.class.getName());

    private static final String AZURE_BLOB_STORAGE_CONNECTION_STRING = "media.globalSettings.windowsAzureBlobStorageStrategy.connection";

    private static final String AZURE_BLOB_STORAGE_CONTAINER_ADDRESS = "media.globalSettings.windowsAzureBlobStorageStrategy.containerAddress";

    @Resource
    private ConfigurationService configurationService;

     /**
      * To upload Media to azure blob storage
      * @param folderName
      * @param fileName
      * @param sourceFile
      * @return
      */
     @Override
     public String uploadMedia(String folderName, String fileName, final File sourceFile)
     {
         final String storageConnectionString = (String) configurationService.getConfiguration()
                 .getProperty(AZURE_BLOB_STORAGE_CONNECTION_STRING);
         LOGGER.debug(" Azurecloud blob storage configured connehction string {}", storageConnectionString);
 
         final String containerAddressQualifier = (String) configurationService.getConfiguration()
                 .getProperty(AZURE_BLOB_STORAGE_CONTAINER_ADDRESS);
         LOGGER.debug(" Azurecloud blob storage configured container address {}", containerAddressQualifier);
 
         try
         {
             if(StringUtils.isNotEmpty(folderName) && StringUtils.isNotEmpty(fileName)) {
                   //Retrieve storage account from connection-string.
                 final CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
 
                   //Create the blob client.
                 final CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
 
                   //Get a reference to a container.
                 final CloudBlobContainer container = blobClient.getContainerReference(containerAddressQualifier);
                 container.createIfNotExists();
 
                   //Create a permissions object and Include public access in the permissions object.
                 final BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                 containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                 container.uploadPermissions(containerPermissions);
 
                 LOGGER.debug(" File uploaded to Azurecloud blob storage in the name of {}", folderName);
                 final CloudBlockBlob blob = container.getBlockBlobReference(  folderName + "/" + fileName);
                 try (InputStream inputStream = new FileInputStream(sourceFile)) {
                     blob.upload(inputStream, sourceFile.length());
                 }
                 return blob.getUri().toString();
             }
         }
         catch (final IOException | URISyntaxException | InvalidKeyException | StorageException azureEx)
         {
             LOGGER.error(" Exception encountered : Azurecloud blob storage upload ", azureEx.getMessage());
         }
         return null;
     }

}
