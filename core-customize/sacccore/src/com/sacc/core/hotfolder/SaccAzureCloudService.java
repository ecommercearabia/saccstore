package com.sacc.core.hotfolder;

import java.io.File;

/**
 * SaccAzureCloudService interface for Azure Cloud Service
 */
public interface SaccAzureCloudService {

    /**
     * To upload Media to azure blob storage
     * @param folderName
     * @param fileName
     * @param sourceFile
     * @return
     */
    String uploadMedia(String folderName, String fileName, final File sourceFile);


}
