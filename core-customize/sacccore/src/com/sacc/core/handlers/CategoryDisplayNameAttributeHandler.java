package com.sacc.core.handlers;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.logging.log4j.util.Strings;


/**
 * @author Tuqa
 *
 */
public class CategoryDisplayNameAttributeHandler implements DynamicAttributeHandler<String, CategoryModel>
{


	@Override
	public String get(final CategoryModel categoryModel)
	{
		if (categoryModel == null)
		{
			return Strings.EMPTY;
		}

		return !Strings.isBlank(categoryModel.getStoreFrontName()) ? categoryModel.getStoreFrontName() : categoryModel.getName();

	}

	@Override
	public void set(final CategoryModel categoryModel, final String name)
	{
		throw new UnsupportedOperationException();
	}

}
