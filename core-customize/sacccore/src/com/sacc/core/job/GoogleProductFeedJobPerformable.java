package com.sacc.core.job;

import com.sacc.core.dao.SaccProductFeedDao;
import com.sacc.core.exception.stocklevel.StockLevelException;
import com.sacc.core.exception.stocklevel.type.StockLevelExceptionType;
import com.sacc.core.hotfolder.SaccAzureCloudService;
import com.sacc.core.model.SaccShareFolderCronJobModel;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.util.CSVConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class GoogleProductFeedJobPerformable extends AbstractJobPerformable<SaccShareFolderCronJobModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(GoogleProductFeedJobPerformable.class);
    private  static final String PRODUCT_CODE = "Product_Feed";
    private static final String BASE_URL_PROPERTY = "website.skysales-sa.https";

    @Resource(name = "defaultSaccProductFeedDao")
    private SaccProductFeedDao defaultSaccProductFeedDao;
    private List<String> productFeedHeaderList;
    @Resource(name = "baseSiteService")
    private BaseSiteService baseSiteService;
    @Resource(name = "defaultProductModelUrlResolver")
    private DefaultProductModelUrlResolver defaultProductModelUrlResolver;
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    @Resource(name = "productDiscountPopulator")
    private Populator<ProductModel, ProductData> productDiscountPopulator;
    @Resource(name = "stockService")
    private StockService stockService;
    @Resource(name = "warehouseService")
    private WarehouseService warehouseService;
    @Resource(name = "saccAzureCloudService")
    private SaccAzureCloudService saccAzureCloudService;

    public List<String> getProductFeedHeaderList() {
        return productFeedHeaderList;
    }

    public void setProductFeedHeaderList(List<String> productFeedHeaderList) {
        this.productFeedHeaderList = productFeedHeaderList;
    }

    @Override
    public PerformResult perform(SaccShareFolderCronJobModel saccShareFolderCronJobModel)
    {
        LOG.info("Performing saccShareFolderCronJob");
        try
        {
            Locale newLocale = new Locale("ar");

            final ArrayList<Locale> localeList = new ArrayList<>();

            localeList.add(newLocale);
            localeList.add(Locale.ENGLISH);

            for (final Locale locale: localeList)
            {
                LOG.info("Creating attachmentFile for " + locale);
                getAttachmentFile(locale);
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private void getAttachmentFile(final Locale locale) throws IOException
    {
        final Calendar calendar = Calendar.getInstance();
        final Date currentDate = calendar.getTime();
        final String fileName = PRODUCT_CODE + "_" +locale+ "_" + currentDate.getTime() + ".txt";
        File file = new File(fileName);
        if (!file.exists())
        {
            file.createNewFile();
        }
        writeDataLineByLine(file, locale);
        saccAzureCloudService.uploadMedia("googleFeed", fileName, file);
    }

    private void writeDataLineByLine(File file, final Locale locale)
    {
        // creating a file object for file
        try {
            LOG.info("Started adding Product data for the Locale " + locale);
            // create FileWriter object with file as parameter

            PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
                    CSVConstants.DEFAULT_ENCODING)));

            final ArrayList<List<String>> productHeader = new ArrayList<>();
            productHeader.add(getProductFeedHeaderList());
            // adding header to csv
            for (final List<String> headerList : productHeader)
            {
                final String header = StringUtils.join(headerList.toArray(), ";");
                writer.println(header);
            }
            final BaseSiteModel baseSiteModel = getBaseSiteModel();
            final List<ProductModel> productModel = defaultSaccProductFeedDao.fetchProductModel(baseSiteModel);
            LOG.info("Products count from DAO " + productModel.size());
            if (CollectionUtils.isNotEmpty(productModel))
            {
                for (final ProductModel product : productModel)
                {
                    LOG.info("Adding productData for product " + product.getCode());
                    String imageLink = null;
                    String availability = null;
                    String price = null;
                    String finalTax = null;
                    String specifications;
                    String brand = null;

                    final List<String> productDataList = new ArrayList<>();
                    productDataList.add(StringUtils.isNotEmpty(product.getEan()) ? product.getEan() : "");
                    final String productName = StringUtils.isNotEmpty(product.getName(locale)) ?
                            product.getName(locale).replaceAll("[\r\n,.]", "") : "";
                    productDataList.add(StringUtils.isNotEmpty(productName) ? productName : "");
                    final List<String> categoryCodesList = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(product.getSupercategories()))
                    {
                        for (final CategoryModel categoryModel : product.getSupercategories())
                        {
                            final String categoryCode = categoryModel.getName(locale);
                            if (StringUtils.isNotEmpty(categoryCode))
                            {
                                if (categoryCode.startsWith("B"))
                                {
                                    brand = categoryCode;
                                    break;
                                }
                                categoryCodesList.add(categoryCode);
                            }
                        }
                        if (StringUtils.isEmpty(brand))
                        {
                            brand = product.getSupercategories().stream().findFirst().get().getName(locale);
                        }
                    }
                    productDataList.add(CollectionUtils.isNotEmpty(categoryCodesList) ? StringUtils.join(categoryCodesList.toArray(), ",") : StringUtils.EMPTY);
                    final String summary = StringUtils.isNotEmpty(product.getSummary(locale)) ?
                        product.getSummary(locale).replaceAll("[\r\n,.]", "") : "";
                    productDataList.add(StringUtils.isNotEmpty(summary) ? summary : "");
                    final String urlResolver = defaultProductModelUrlResolver.resolve(product);
                    productDataList.add(StringUtils.isNotEmpty(product.getCode()) && StringUtils.isNotEmpty(urlResolver) ? configurationService.getConfiguration().getString(BASE_URL_PROPERTY) + "/" + locale + urlResolver :
                            "");
                    if (CollectionUtils.isNotEmpty(product.getGalleryImages()))
                    {
                        Collection<MediaModel> mediaModel = product.getGalleryImages().stream().findFirst().get().getMedias();
                        if (CollectionUtils.isNotEmpty(mediaModel))
                        {
                            imageLink = mediaModel.stream().findFirst().get().getURL();
                        }
                    }
                    productDataList.add(StringUtils.isNotEmpty(imageLink) ? configurationService.getConfiguration().getString(BASE_URL_PROPERTY) + imageLink
                            : "");
                    final WarehouseModel warehouse = warehouseService.getWarehouseForCode("skysales-sa");
                    if (warehouse == null)
                    {
                        LOG.error("Warehouse not found for id : {}", warehouse);
                        throw new StockLevelException(StockLevelExceptionType.WAREHOUSE_NOT_FOUND,
                                "Warehouse not found for id :" + warehouse);
                    }
                    final StockLevelModel stockLevel = stockService.getStockLevel(product, warehouse);
                    if (null != stockLevel)
                    {
                        if (stockLevel.getAvailable() > 0)
                        {
                            availability = StockLevelStatus.INSTOCK.getCode();
                        }
                        else
                        {
                            availability = StockLevelStatus.OUTOFSTOCK.getCode();
                        }
                    }
                    productDataList.add(StringUtils.isNotEmpty(availability) ? availability : "");
                    if (StringUtils.isNotEmpty(availability) && availability.equalsIgnoreCase(StockLevelStatus.OUTOFSTOCK.getCode()) )
                    {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");

                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, -7);
                        Date todate1 = cal.getTime();
                        String fromdate = dateFormat.format(todate1);
                        productDataList.add(fromdate);
                    }
                    else
                    {
                        productDataList.add("");
                    }
                    productDataList.add("SAR");
                    if (CollectionUtils.isNotEmpty(product.getEurope1Prices()))
                    {
                        price = String.valueOf(product.getEurope1Prices().stream().filter(prices -> prices.getCurrency() != null &&
                                prices.getCurrency().getIsocode().equals("SAR")).findFirst().get().getPrice());
                    }
                    productDataList.add(StringUtils.isNotEmpty(price) ? price : "");
                    final BigDecimal discountPrice = populateDiscountPrice(product);
                    LOG.info("Product " + product.getCode() + "with applicable DiscountPrice " + discountPrice);
                    productDataList.add(StringUtils.isNotEmpty(discountPrice.toString()) ? discountPrice.toString() : "");
                    final String description = product.getDescription(locale);
                    specifications = StringUtils.isNotEmpty(description) ?
                            Jsoup.parse(description).text().replaceAll("[\r\n,.]+", "") : "";

                    productDataList.add(StringUtils.isNotEmpty(specifications) ? specifications : "");
                    productDataList.add(StringUtils.isNotEmpty(product.getCode()) ? product.getCode() : "");
                    if (CollectionUtils.isNotEmpty(product.getEurope1Taxes()))
                    {
                        Double tax = product.getEurope1Taxes().stream().findFirst().get().getTax().getValue();
                        finalTax = (BigDecimal.valueOf(tax / 100)).multiply(discountPrice).toString();
                    }
                    LOG.info("Product " + product.getCode() + "with applicable Tax " + finalTax);
                    productDataList.add(StringUtils.isNotEmpty(finalTax) ? finalTax : "");
                    productDataList.add(StringUtils.isNotEmpty(brand) ? brand : "");
                    writer.println(StringUtils.join(productDataList.toArray(), ";"));
                }
            }
            // closing writer connection
            writer.close();
            LOG.info("added Products for the Locale " + locale);
        }
        catch (IOException | StockLevelException e)
        {
            LOG.error("Failed to ProductsProducts for the Locale " + locale , e.getMessage());
        }
    }

    private BigDecimal populateDiscountPrice(final ProductModel productModel)
    {

        final ProductData productData = new ProductData();
        productDiscountPopulator.populate(productModel, productData);
        LOG.info("DiscountPrice for the product " + productModel.getCode());
        return productData.getDiscount() != null && productData.getDiscount().getDiscountPrice() !=null &&
                productData.getDiscount().getDiscountPrice().getValue() !=null
                ? productData.getDiscount().getDiscountPrice().getValue() : BigDecimal.valueOf(0);
    }

    private BaseSiteModel getBaseSiteModel()
    {
        final BaseSiteModel baseSiteModel = baseSiteService.getBaseSiteForUID("skysales-sa");
        return baseSiteModel;
    }
}
