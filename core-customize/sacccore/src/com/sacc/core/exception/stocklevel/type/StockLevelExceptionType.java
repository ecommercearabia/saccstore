/**
 *
 */
package com.sacc.core.exception.stocklevel.type;

/**
 * @author monzer
 *
 */
public enum StockLevelExceptionType
{

	BASE_STORE_NOT_FOUND("BaseStore not found"), WAREHOUSE_NOT_FOUND("Warehouse not found"), WAREHOUSE_NOT_SUPPORTED(
			"Warehouse Not Supported"), CATALOG_NOT_FOUND("Catalog not found"), PRODUCT_NOT_FOUND(
					"Product not found"), STOCK_STATUS_NOT_SUPPORTED(
							"Stock status not supported"), INVALID_STOCK_LEVEL_CREATION("cannot create new StockLevel");


	private String message;

	/**
	 * @param message
	 */
	private StockLevelExceptionType(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}
}
