/**
 *
 */
package com.sacc.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductInStockFlagValueProvider;
import de.hybris.platform.commerceservices.stock.strategies.CommerceAvailabilityCalculationStrategy;
import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomProductInStockFlagValueProvider extends ProductInStockFlagValueProvider
{
	private CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy;
	private WarehouseSelectionStrategy warehouseSelectionStrategy;
	private StockService stockService;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			final ProductModel product = (ProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

			final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();

			if (baseSiteModel != null && baseSiteModel.getStores() != null && !baseSiteModel.getStores().isEmpty()
					&& getCommerceStockService().isStockSystemEnabled(baseSiteModel.getStores().get(0)))
			{
				fieldValues.addAll(createFieldValue(product, indexConfig.getBaseSite().getStores().get(0), indexedProperty));
			}
			else
			{
				fieldValues.addAll(createFieldValue(product, null, indexedProperty));
			}
			return fieldValues;
		}
		else
		{
			throw new FieldValueProviderException("Cannot get stock of non-product item");
		}
	}

	@Override
	protected List<FieldValue> createFieldValue(final ProductModel product, final BaseStoreModel baseStore,
			final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		if (baseStore != null)
		{
			addFieldValues(fieldValues, indexedProperty, Boolean.valueOf(isInStock(product, baseStore)));
		}
		else
		{
			addFieldValues(fieldValues, indexedProperty, Boolean.TRUE);
		}

		return fieldValues;
	}

	@Override
	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty, final Object value)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}

	@Override
	protected StockLevelStatus getProductStockLevelStatus(final ProductModel product, final BaseStoreModel baseStore)
	{
		Long calculateAvailability = getCommerceStockLevelCalculationStrategy().calculateAvailability(
				getStockService().getStockLevels(product, getWarehouseSelectionStrategy().getWarehousesForBaseStore(baseStore)));

		if (baseStore.isStockLevelControlEnable())
		{
			if (calculateAvailability == null)
			{
				calculateAvailability = 0L;
			}
			if (calculateAvailability < product.getStockDisplayAmount())
			{
				return calculateAvailability >= 1 ? StockLevelStatus.INSTOCK : StockLevelStatus.OUTOFSTOCK;
			}
			else
			{
				return product.getStockDisplayAmount() >= 1 ? StockLevelStatus.INSTOCK : StockLevelStatus.OUTOFSTOCK;
			}
		}

		return getCommerceStockService().getStockLevelStatusForProductAndBaseStore(product, baseStore);
	}

	@Override
	protected boolean isInStock(final ProductModel product, final BaseStoreModel baseStore)
	{
		return isInStock(getProductStockLevelStatus(product, baseStore));
	}

	@Override
	protected boolean isInStock(final StockLevelStatus stockLevelStatus)
	{
		return stockLevelStatus != null && !StockLevelStatus.OUTOFSTOCK.equals(stockLevelStatus);
	}

	public CommerceAvailabilityCalculationStrategy getCommerceStockLevelCalculationStrategy()
	{
		return commerceStockLevelCalculationStrategy;
	}

	public void setCommerceStockLevelCalculationStrategy(
			final CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy)
	{
		this.commerceStockLevelCalculationStrategy = commerceStockLevelCalculationStrategy;
	}

	public WarehouseSelectionStrategy getWarehouseSelectionStrategy()
	{
		return warehouseSelectionStrategy;
	}

	public void setWarehouseSelectionStrategy(final WarehouseSelectionStrategy warehouseSelectionStrategy)
	{
		this.warehouseSelectionStrategy = warehouseSelectionStrategy;
	}

	public StockService getStockService()
	{
		return stockService;
	}

	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}


}
