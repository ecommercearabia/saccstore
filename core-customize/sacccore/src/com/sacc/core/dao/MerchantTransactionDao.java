/**
 *
 */
package com.sacc.core.dao;

import com.sacc.core.model.MerchantTransactionModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface MerchantTransactionDao
{
	MerchantTransactionModel getMerchantTransactionById(final String id);
}
