/**
 *
 */
package com.sacc.core.dao;

import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomCartDao
{
	List<CartModel> getCartByOrderCode(final String orderCode);
}
