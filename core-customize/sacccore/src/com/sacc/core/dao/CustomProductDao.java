/**
 *
 */
package com.sacc.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author core
 *
 */
public interface CustomProductDao
{
	public List<ProductModel> getGoingFast(int size, final CatalogVersionModel catalogVersionModel);

	public List<ProductModel> getLimitedStock(final CatalogVersionModel catalogVersionModel, final int limitedStock);
}
