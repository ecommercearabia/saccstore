package com.sacc.core.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface SaccProductFeedDao {

    List<ProductModel> fetchProductModel(final BaseSiteModel baseSiteModel);
}
