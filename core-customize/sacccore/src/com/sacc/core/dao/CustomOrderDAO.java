/**
 *
 */
package com.sacc.core.dao;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author core
 *
 */
public interface CustomOrderDAO
{
	OrderModel findOrderByCode(final String code);
}
