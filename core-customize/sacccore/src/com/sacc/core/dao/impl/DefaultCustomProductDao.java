/**
 *
 */
package com.sacc.core.dao.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.sacc.core.dao.CustomProductDao;


/**
 * @author core
 *
 */
public class DefaultCustomProductDao implements CustomProductDao
{

	protected static final Logger LOG = Logger.getLogger(DefaultCustomProductDao.class);
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Override
	public List<ProductModel> getGoingFast(final int size, final CatalogVersionModel catalogVersionModel)
	{
		Preconditions.checkNotNull((catalogVersionModel), "catalogVersion is null");
		if (size == 0)
		{
			LOG.error("Size can't be zero or under!");
			throw new IllegalArgumentException();
		}

		final StringBuilder query = new StringBuilder();
		query.append(" SELECT  ");
		query.append(" {p.pk}, ");
		query.append(" SUM({e.quantity}) as prodcount ");
		query.append(" FROM {Product AS p join OrderEntry AS e on {e.product} = {p.pk}} ");
		query.append(" WHERE {p.approvalStatus} = ?approvalStatus");
		query.append(" GROUP BY {p.pk} ");
		query.append(" order by prodcount desc ");
		//query.append(" limit 10");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());

		//		final Map<String, Object> params = new HashMap<>();
		//		params.put("catalogVersion", catalogVersionModel);

		//		searchQuery.getQueryParameters().putAll(params);
		searchQuery.setCount(size);
		final Map<String, Object> params = new HashMap<>();
		params.put("approvalStatus", ArticleApprovalStatus.APPROVED);

		searchQuery.getQueryParameters().putAll(params);
		final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	/**
	 * @param resList
	 * @return
	 */
	private List<ProductModel> reLister(final List<List<Object>> resList, final int size)
	{
		final Iterator<List<Object>> ite = resList.iterator();
		List<Object> insideList = null;
		final List<ProductModel> resultList = new ArrayList<>();
		int i = 0;
		while (ite.hasNext())
		{
			insideList = ite.next();
			final Iterator<Object> ite2 = insideList.iterator();
			Object obj = null;
			while (ite2.hasNext())
			{
				obj = ite2.next();
				if (i >= size)
				{
					break;
				}

				if (obj instanceof ProductModel)
				{
					resultList.add((ProductModel) obj);
					i++;
				}

			}
		}
		return resultList;

	}

	@Override
	public List<ProductModel> getLimitedStock(final CatalogVersionModel catalogVersionModel, final int limitedStock)
	{

		Preconditions.checkNotNull((catalogVersionModel), "catalogVersion is null");
		if (limitedStock <= 0)
		{
			LOG.error("limitedStock can't be zero or under!");
			throw new IllegalArgumentException();
		}

		final StringBuilder query = new StringBuilder();
		query.append(" SELECT  ");
		query.append(" {p.pk}, ");
		query.append(" {sl.productCode}, ");
		query.append(" ( Sum({sl.available})  - Sum({sl.reserved})) ");
		query.append(" FROM {StockLevel as sl join Product as p on {p.code} = {sl.productCode} ");
		query.append(" JOIN CatalogVersion as cv on {cv.pk} = {p.catalogVersion}} ");
		query.append(" WHERE {cv.pk} = ?catalogVersion and");
		query.append(" {p.approvalStatus} = ?approvalStatus");
		query.append(" GROUP BY {sl.productCode}, {p.pk} ");
		query.append(
				" Having (SUM({sl.available})  - SUM({sl.reserved})) > 0 and (SUM({sl.available})  - SUM({sl.reserved})) <= ?limitedStock");
		query.append(" order BY (SUM({sl.available})  - SUM({sl.reserved})) DESC ");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());

		final Map<String, Object> params = new HashMap<>();
		params.put("catalogVersion", catalogVersionModel);
		params.put("limitedStock", limitedStock);
		params.put("approvalStatus", ArticleApprovalStatus.APPROVED);

		searchQuery.getQueryParameters().putAll(params);

		final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();

	}

}
