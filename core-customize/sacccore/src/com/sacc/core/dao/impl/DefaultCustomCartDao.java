/**
 *
 */
package com.sacc.core.dao.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.dao.CustomCartDao;


/**
 * @author core
 *
 */
public class DefaultCustomCartDao implements CustomCartDao
{

	private static final String ORDER_CODE_CANNOT_BE_EMPTY_OR_NULL = "OrderCode Cannot Be Empty or null";
	private static final String ORDER_CODE_PARAM = "orderCodeParam";
	private static final String GET_CART_BY_ORDERCODE_QUERY = "SELECT {c:" + CartModel.PK + "} FROM {" + CartModel._TYPECODE
			+ " as c} where {c:" + CartModel.ORDERCODE + "} = ?" + ORDER_CODE_PARAM;

	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomCartDao.class);


	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<CartModel> getCartByOrderCode(final String orderCode)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(orderCode), ORDER_CODE_CANNOT_BE_EMPTY_OR_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_CART_BY_ORDERCODE_QUERY);
		query.addQueryParameter(ORDER_CODE_PARAM, orderCode);

		final List<CartModel> result = getFlexibleSearchService().<CartModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			return Collections.emptyList();
		}

		if (result.size() > 1)
		{
			LOG.error("Found Multiple Carts with the same orderCode");
			LOG.error("Carts : [{}]", String.join(", ", result.stream().map(e -> e.getCode()).collect(Collectors.toList())));

		}

		return result;
	}

	/**
	 * @return the flexibleSearchService
	 */
	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

}
