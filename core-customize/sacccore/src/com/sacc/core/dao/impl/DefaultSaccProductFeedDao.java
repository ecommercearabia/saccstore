package com.sacc.core.dao.impl;

import com.sacc.core.dao.SaccProductFeedDao;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultSaccProductFeedDao implements SaccProductFeedDao
{
    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<ProductModel> fetchProductModel(final BaseSiteModel baseSiteModel)
    {
        final String query = "SELECT {p.pk} FROM {Product AS p JOIN CatalogVersion AS cv ON {p.catalogVersion}={cv.pk} "
                + " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
                + " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}} "
                + " WHERE {site.pk} = ?site AND {p.approvalStatus} = ?approvalStatus";

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", baseSiteModel);
        params.put(ProductModel.APPROVALSTATUS, ArticleApprovalStatus.APPROVED);
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
        fQuery.addQueryParameters(params);
        fQuery.setResultClassList(Collections.singletonList(ProductModel.class));

        final SearchResult<ProductModel> searchResult = flexibleSearchService.search(fQuery);
        return searchResult.getResult();
    }
}
