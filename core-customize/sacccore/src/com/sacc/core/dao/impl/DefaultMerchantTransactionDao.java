/**
 *
 */
package com.sacc.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.dao.MerchantTransactionDao;
import com.sacc.core.model.MerchantTransactionModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public class DefaultMerchantTransactionDao implements MerchantTransactionDao
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultMerchantTransactionDao.class);
	private static final String ID_CANNOT_BE_EMPTY_OR_NULL = "ID Cannot Be Empty or null";
	private static final String ID_PARAM = "id";
	private static final String GET_MERCHANTTRANSACTION_BY_ID_QUERY = "SELECT {c:" + MerchantTransactionModel.PK + "} FROM {"
			+ MerchantTransactionModel._TYPECODE + " as c} where {c:" + MerchantTransactionModel.ID + "} = ?" + ID_PARAM;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public MerchantTransactionModel getMerchantTransactionById(final String id)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(id), ID_CANNOT_BE_EMPTY_OR_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_MERCHANTTRANSACTION_BY_ID_QUERY);
		query.addQueryParameter(ID_PARAM, id);

		final List<MerchantTransactionModel> result = getFlexibleSearchService().<MerchantTransactionModel> search(query)
				.getResult();

		if (CollectionUtils.isEmpty(result))
		{
			return null;
		}

		return result.get(0);
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}


}
