/**
 *
 */
package com.sacc.core.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.List;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomCartService extends CartService
{

	List<CartModel> getCartByMerchantTransactionId(final String merchantTransactionId);

}
