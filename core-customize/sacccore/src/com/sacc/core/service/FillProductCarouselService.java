/**
 *
 */
package com.sacc.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.sacc.sacccustomcronjobs.enums.FillCarouselComponentType;


/**
 * @author core
 *
 */
public interface FillProductCarouselService
{
	public List<ProductModel> getGoingFast(int size, final CatalogVersionModel catalogVersionModel);

	public List<ProductModel> getLimitedStock(final BaseStoreModel store, final CatalogVersionModel catalogVersionModel,
			final int limitedStock, int size);

	public List<ProductModel> getProducts(FillCarouselComponentType type, final BaseStoreModel store,
			final CatalogVersionModel catalogVersionModel, final int limitedStock, int size);

	public void updateProduct(FillCarouselComponentType type, ProductCarouselComponentModel carousel, final BaseStoreModel store,
			final CatalogVersionModel catalogVersionModel, final int limitedStock, int size, boolean isSyncNeeded);
}
