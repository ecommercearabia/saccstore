/**
 *
 */
package com.sacc.core.service;

/**
 * @author core
 *
 */
public interface SynchronizeService
{
	public void synchronize(final String catalogId);
}
