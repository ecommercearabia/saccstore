/**
 *
 */
package com.sacc.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.impl.DefaultOrderService;

import javax.annotation.Resource;

import com.sacc.core.dao.CustomOrderDAO;
import com.sacc.core.service.CustomOrderService;


/**
 * @author core
 *
 */
public class DefaultCustomOrderService extends DefaultOrderService implements CustomOrderService
{
	@Resource(name = "customOrderDAO")
	private CustomOrderDAO customOrderDAO;

	@Override
	public OrderModel getOrderForCode(final String orderCode)
	{
		return getCustomOrderDAO().findOrderByCode(orderCode);
	}

	public CustomOrderDAO getCustomOrderDAO()
	{
		return customOrderDAO;
	}


}
