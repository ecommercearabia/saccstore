/**
 *
 */
package com.sacc.core.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.assertj.core.util.Preconditions;
import org.springframework.util.CollectionUtils;

import com.sacc.core.dao.CustomProductDao;
import com.sacc.core.service.FillProductCarouselService;
import com.sacc.core.service.SynchronizeService;
import com.sacc.sacccustomcronjobs.enums.FillCarouselComponentType;


/**
 * @author core
 *
 */
public class DefaultFillProductCarouselService implements FillProductCarouselService
{


	@Resource(name = "productService")
	private ProductService productService;


	@Resource(name = "customProductDao")
	private CustomProductDao customProductDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commerceStockService")
	private CommerceStockService commerceStockService;


	@Resource(name = "synchronizeService")
	private SynchronizeService synchronizeService;



	@Override
	public List<ProductModel> getGoingFast(final int size, final CatalogVersionModel catalogVersionModel)
	{
		Preconditions.checkNotNull(catalogVersionModel, "catalogVersionModel is null");

		if (size <= 0)
		{
			return Collections.emptyList();
		}

		final List<ProductModel> productlist = getCustomProductDao().getGoingFast(size, catalogVersionModel);
		return productlist;
	}

	@Override
	public List<ProductModel> getLimitedStock(final BaseStoreModel store, final CatalogVersionModel catalogVersionModel,
			final int limitedStock, final int size)

	{
		Preconditions.checkNotNull(store, "store is null");
		Preconditions.checkNotNull(catalogVersionModel, "catalogVersionModel is null");

		if (size <= 0 || limitedStock <= 0)
		{
			return Collections.emptyList();
		}

		final List<ProductModel> productlist = getCustomProductDao().getLimitedStock(catalogVersionModel, limitedStock);
		final int listIndex = productlist.size() - 1;
		if (listIndex <= size)
		{
			return productlist;
		}

		return productlist.subList(listIndex - size, listIndex);


	}

	/**
	 * @param allProducts
	 * @param store
	 * @param limitedStock
	 * @return
	 */
	private Map<ProductModel, Long> getProductsWithStock(final List<ProductModel> allProducts, final BaseStoreModel store,
			final int limitedStock)
	{
		if (CollectionUtils.isEmpty(allProducts))
		{
			return Collections.emptyMap();
		}


		final Map<ProductModel, Long> productsWithStock = new HashMap<>();


		for (final ProductModel productModel : allProducts)
		{
			Long stockLevel = null;
			try
			{
				stockLevel = getCommerceStockService().getStockLevelForProductAndBaseStore(productModel, store);

				if (stockLevel == null || stockLevel.longValue() <= 0 || stockLevel.longValue() > limitedStock)
				{
					continue;

				}
				productsWithStock.put(productModel, stockLevel);

			}
			catch (final Exception e)
			{

			}
		}
		return productsWithStock;
	}

	/**
	 * @param size
	 * @param size2
	 * @return
	 */
	private int getLimitedStockSize(final int returndListSize, final int size)
	{
		//TODO calc available size

		return returndListSize <= size ? returndListSize : size;
	}

	private <K, V extends Comparable<? super V>> Map<K, V> sortByValue(final Map<K, V> unsortMap)
	{
		final List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<K, V>>()
		{
			public int compare(final Map.Entry<K, V> o1, final Map.Entry<K, V> o2)
			{
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		final Map<K, V> result = new LinkedHashMap<K, V>();
		for (final Map.Entry<K, V> entry : list)
		{
			result.put(entry.getKey(), entry.getValue());
		}

		return result;

	}

	@Override
	public List<ProductModel> getProducts(final FillCarouselComponentType type, final BaseStoreModel store,
			final CatalogVersionModel catalogVersionModel, final int limitedStock, final int size)
	{

		Preconditions.checkNotNull(type, "FillCarouselComponentType is null");
		List<ProductModel> productList = null;

		switch (type)
		{
			case GOING_FAST:
				productList = getGoingFast(size, catalogVersionModel);
				break;
			case LIMITED_STOCK:
				productList = getLimitedStock(store, catalogVersionModel, limitedStock, size);
				break;

		}

		return productList;
	}

	@Override
	public void updateProduct(final FillCarouselComponentType type, final ProductCarouselComponentModel carousel,
			final BaseStoreModel store, final CatalogVersionModel catalogVersionModel, final int limitedStock, final int size,
			final boolean isSyncNeeded)
	{
		Preconditions.checkNotNull(carousel, "ProductCarouselComponentModel is null");
		Preconditions.checkNotNull(type, "FillCarouselComponentType is null");

		final List<ProductModel> productList = getProducts(type, store, catalogVersionModel, limitedStock, size);
		if (CollectionUtils.isEmpty(productList))
		{
			return;
		}
		carousel.setProducts(productList);

		getModelService().save(carousel);

		if (isSyncNeeded && carousel.getCatalogVersion() != null && carousel.getCatalogVersion().getCatalog() != null
				&& carousel.getCatalogVersion().getCatalog().getId() != null)
		{
			getSynchronizeService().synchronize(carousel.getCatalogVersion().getCatalog().getId());
		}

	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @return the commerceStockService
	 */
	protected CommerceStockService getCommerceStockService()
	{
		return commerceStockService;
	}


	/**
	 * @return the customProductDao
	 */
	protected CustomProductDao getCustomProductDao()
	{
		return customProductDao;
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the synchronizeService
	 */
	protected SynchronizeService getSynchronizeService()
	{
		return synchronizeService;
	}

}
