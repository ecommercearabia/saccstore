/**
 *
 */
package com.sacc.core.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.core.model.SerialNumberConfigModel;
import com.sacc.core.service.AbstractSerialNumberConfigurationService;


/**
 * Custom Invoice Number Generator based on @SerialNumberConfigModel.
 *
 * @author monzer
 */
public class DefaultReturnRequestSerialNumberService extends AbstractSerialNumberConfigurationService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultReturnRequestSerialNumberService.class);

	/**
	 * Generate invoice number for base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the optional
	 */
	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		if (baseStore == null)
		{
			LOG.error("DefaultOrderInvoiceSerialNumberService: Null Base Store");
			return Optional.empty();
		}
		final SerialNumberConfigModel invoiceSerialNumberModel = baseStore.getReturnSerialNumberConfig();
		if (invoiceSerialNumberModel == null)
		{
			LOG.error("DefaultOrderInvoiceSerialNumberService: Null Order Invoice Serial Number Configuration Model");
			return Optional.empty();
		}

		synchronized (DefaultReturnRequestSerialNumberService.class)
		{
			return getNextSerialNumber(invoiceSerialNumberModel);
		}
	}

}
