/**
 *
 */
package com.sacc.core.service.impl;

import de.hybris.platform.acceleratorservices.order.impl.DefaultCartServiceForAccelerator;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.dao.CustomCartDao;
import com.sacc.core.service.CustomCartService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCartServiceImpl extends DefaultCartServiceForAccelerator implements CustomCartService
{

	@Resource(name = "customCartDao")
	private CustomCartDao customCartDao;

	@Override
	public List<CartModel> getCartByMerchantTransactionId(final String merchantTransactionId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(merchantTransactionId), "merchantTransactionId must not be null");

		final String[] split = merchantTransactionId.split("_");
		final String orderCode = split[0];
		return getCustomCartDao().getCartByOrderCode(orderCode);
	}

	/**
	 * @return the customCartDao
	 */
	protected CustomCartDao getCustomCartDao()
	{
		return customCartDao;
	}

}
