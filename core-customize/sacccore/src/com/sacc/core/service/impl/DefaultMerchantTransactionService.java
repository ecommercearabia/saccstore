/**
 *
 */
package com.sacc.core.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.sacc.core.dao.MerchantTransactionDao;
import com.sacc.core.model.MerchantTransactionModel;
import com.sacc.core.service.MerchantTransactionService;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public class DefaultMerchantTransactionService implements MerchantTransactionService
{
	@Resource(name = "merchantTransactionDao")
	private MerchantTransactionDao merchantTransactionDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public MerchantTransactionModel getMerchantTransactionById(final String id)
	{
		return getMerchantTransactionDao().getMerchantTransactionById(id);
	}

	@Override
	public void removeMerchantTransaction(final MerchantTransactionModel model)
	{
		if (model == null)
		{
			return;
		}

		try
		{
			getModelService().remove(model);
		}
		catch (final Exception e)
		{
			return;
		}
	}

	/**
	 * @return the merchantTransactionDao
	 */
	public MerchantTransactionDao getMerchantTransactionDao()
	{
		return merchantTransactionDao;
	}



	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

}
