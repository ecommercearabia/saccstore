/**
 *
 */
package com.sacc.core.service.stock.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.warehousing.atp.services.impl.WarehousingCommerceStockService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomWarehousingCommerceStockService extends WarehousingCommerceStockService
{
	@Override
	public Long getStockLevelForProductAndBaseStore(final ProductModel product, final BaseStoreModel baseStore)
	{
		validateParameterNotNull(product, "product cannot be null");
		validateParameterNotNull(baseStore, "baseStore cannot be null");
		final Long calculateAvailability = getCommerceStockLevelCalculationStrategy().calculateAvailability(
				getStockService().getStockLevels(product, getWarehouseSelectionStrategy().getWarehousesForBaseStore(baseStore)));

		if (baseStore.isStockLevelControlEnable())
		{
			if (calculateAvailability < product.getStockDisplayAmount())
			{
				return calculateAvailability;
			}
			else
			{
				return Long.valueOf(product.getStockDisplayAmount());
			}
		}
		return calculateAvailability;
	}
}
