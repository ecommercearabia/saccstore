/**
 *
 */
package com.sacc.core.service.stock;

import com.sacc.core.exception.stocklevel.StockLevelException;
import com.sacc.core.service.stock.enums.StockLevelResponse;


/**
 * @author monzer
 *
 */
public interface StockLevelService
{

	StockLevelResponse createProductInStockLevel(final String baseStoreUid, final String warehouseCode, final String productCode,
			final String inStockStatus, final String catalogId, final String catalogVersion, final int available,
			final int reserved, final boolean autocreate, final boolean removeInventoryEvents) throws StockLevelException;

}
