/**
 *
 */
package com.sacc.core.service.stock.enums;

/**
 * @author monzer
 *
 */
public enum StockLevelResponse
{

	CREATED, UPDATED

}
