/**
 *
 */
package com.sacc.core.service.stock.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.sacc.core.exception.stocklevel.StockLevelException;
import com.sacc.core.exception.stocklevel.type.StockLevelExceptionType;
import com.sacc.core.service.stock.StockLevelService;
import com.sacc.core.service.stock.enums.StockLevelResponse;


/**
 * @author monzer
 *
 */
public class DefaultStockLevelService implements StockLevelService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultStockLevelService.class);


	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "stockService")
	private StockService stockService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "warehouseService")
	private WarehouseService warehouseService;

	@Override
	public StockLevelResponse createProductInStockLevel(final String baseStoreUid, final String warehouseCode,
			final String productCode,
			final String inStockStatus, final String catalogId, final String catalogVersion, final int available, final int reserved,
			final boolean autocreate, final boolean removeInventoryEvents) throws StockLevelException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(baseStoreUid), "baseStoreUid is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(warehouseCode), "warehouseCode is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(productCode), "productCode is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(inStockStatus), "inStockStatus is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(catalogId), "catalogId is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(catalogVersion), "catalogVersion is null");

		final BaseStoreModel baseStoreForUid = baseStoreService.getBaseStoreForUid(baseStoreUid);
		if (baseStoreForUid == null)
		{
			LOG.error("Base Store not found for id : {}", baseStoreForUid);
			throw new StockLevelException(StockLevelExceptionType.BASE_STORE_NOT_FOUND, "Base Store not found for id :" + baseStoreForUid);
		}

		final WarehouseModel warehouseForCode = warehouseService.getWarehouseForCode(warehouseCode);
		if (warehouseForCode == null)
		{
			LOG.error("Warehouse not found for id : {}", warehouseCode);
			throw new StockLevelException(StockLevelExceptionType.WAREHOUSE_NOT_FOUND,
					"Warehouse not found for id :" + warehouseCode);
		}

		final Optional<WarehouseModel> warehouse = baseStoreForUid.getWarehouses().stream()
				.filter(whouse -> whouse.getCode().equals(warehouseForCode.getCode())).findAny();
		if (warehouse.isEmpty())
		{
			LOG.error("Warehouse {} not supported for id : {}", warehouseCode, baseStoreForUid.getUid());
			throw new StockLevelException(StockLevelExceptionType.WAREHOUSE_NOT_SUPPORTED,
					"Warehouse " + warehouseCode + " not supported for id :" + baseStoreForUid.getUid());
		}

		final CatalogVersionModel activeCatalogVersion = getActiveCatalogVersion(catalogId, catalogVersion);
		if (activeCatalogVersion == null)
		{
			LOG.error("No Active Catalog Version found for id : {} and version of : {}", catalogId, catalogVersion);
			throw new StockLevelException(StockLevelExceptionType.CATALOG_NOT_FOUND,
					"No Active Catalog Version found for id : " + catalogId + " and version of :" + catalogVersion);
		}
		ProductModel productForCode = null;
		try
		{
			productForCode = productService.getProductForCode(activeCatalogVersion, productCode);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error("No product found for code : {} and catalog of : {}", productCode, catalogId);
			throw new StockLevelException(StockLevelExceptionType.PRODUCT_NOT_FOUND,
					"No product found for code : " + productCode + " and catalog of :" + catalogId);
		}
		InStockStatus stockStatus = null;
		try
		{
			stockStatus = InStockStatus.valueOf(inStockStatus);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error("{} is not an In Stock Status", inStockStatus);
			throw new StockLevelException(StockLevelExceptionType.STOCK_STATUS_NOT_SUPPORTED,
					inStockStatus + " is not an In Stock Status");
		}

		final StockLevelModel stockLevel = stockService.getStockLevel(productForCode, warehouseForCode);
		if (stockLevel != null)
		{
			updateStockLevel(productForCode, stockLevel, available, reserved, stockStatus);
			return StockLevelResponse.UPDATED;
		}
		else if (stockLevel == null && autocreate)
		{
			createStockLevel(productForCode, available, warehouseForCode, stockStatus, reserved, removeInventoryEvents);
			return StockLevelResponse.CREATED;
		}
		else
		{
			LOG.error("StockLevel is not found and autocreate must be true in order to create new StockLevel");
			throw new StockLevelException(StockLevelExceptionType.INVALID_STOCK_LEVEL_CREATION,
					"StockLevel is not found and autocreate must be true in order to create new StockLevel");
		}

	}

	private CatalogVersionModel getActiveCatalogVersion(final String catalogId, final String catalogVersion)
	{
		try
		{
			final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(catalogId, catalogVersion);
			return catalogVersionModel;
		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
	}

	private void createStockLevel(final ProductModel product, final Integer quantity, final WarehouseModel warehouse,
			final InStockStatus inStockStatus, final int reserve, final boolean removeInventoryEvents)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(reserve);
		if (removeInventoryEvents)
		{
			newStock.setInventoryEvents(Collections.EMPTY_LIST);
		}
		newStock.setProductCode(product.getCode());
		newStock.setCreateResponse("Created successfully by StockLevelService");
		newStock.setInStockStatus(inStockStatus);
		modelService.save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE: " + product.getCode());
	}

	private void updateStockLevel(final ProductModel product, final StockLevelModel stockLevel, final Integer quantity,
			final Integer reserved, final InStockStatus inStockStatus)
	{
		stockLevel.setReserved(reserved);
		stockLevel.setInventoryEvents(Collections.EMPTY_LIST);
		stockLevel.setAvailable(quantity);
		stockLevel.setInStockStatus(inStockStatus);
		saveUpdateStockLevelResponse(stockLevel, "Update Stock By StockLevelServce");
		modelService.save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: " + product.getCode());
	}

	private void saveUpdateStockLevelResponse(final StockLevelModel stockLevel, final String updateRecord)
	{

		final List<String> history = new ArrayList<>();
		if (stockLevel.getUpdateRecords() != null)
		{
			history.addAll(stockLevel.getUpdateRecords());
		}

		history.add(updateRecord);
		stockLevel.setUpdateRecords(history);
	}

}
