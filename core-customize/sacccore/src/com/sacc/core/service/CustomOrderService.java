/**
 *
 */
package com.sacc.core.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomOrderService extends OrderService
{
	OrderModel getOrderForCode(final String orderCode);
}
