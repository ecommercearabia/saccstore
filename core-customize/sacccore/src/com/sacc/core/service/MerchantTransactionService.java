/**
 *
 */
package com.sacc.core.service;

import com.sacc.core.model.MerchantTransactionModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public interface MerchantTransactionService
{
	MerchantTransactionModel getMerchantTransactionById(final String id);

	void removeMerchantTransaction(final MerchantTransactionModel model);
}
