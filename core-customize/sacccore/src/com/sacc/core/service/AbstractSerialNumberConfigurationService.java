/**
 *
 */
package com.sacc.core.service;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.tx.Transaction;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.core.model.SerialNumberConfigModel;


/**
 * The Interface OrderInvoiceSerialNumberService.
 *
 * @author monzer
 */
public abstract class AbstractSerialNumberConfigurationService
{

	private static final Logger LOG = LoggerFactory.getLogger(AbstractSerialNumberConfigurationService.class);

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Generate invoice number by current store.
	 *
	 * @return the optional
	 */
	public final Optional<String> generateSerialNumberByCurrentStore()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		return this.generateSerialNumberForBaseStore(currentBaseStore);
	}

	/**
	 * Generate invoice number for base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the optional
	 */
	public abstract Optional<String> generateSerialNumberForBaseStore(BaseStoreModel baseStore);

	/**
	 * Gets the next invoice number.
	 *
	 * @param configModel
	 *           the config model
	 * @return the next invoice number
	 */
	protected final synchronized Optional<String> getNextSerialNumber(final SerialNumberConfigModel configModel)
	{
		final Transaction tx = Transaction.current();
		tx.begin();

		boolean success = true;
		try
		{
			if (configModel == null)
			{
				LOG.error("DefaultOrderInvoiceSerialNumberService: Null Order Invoice Serial Number Configuration Model");
				success = false;
				return Optional.empty();
			}
			if (!configModel.isEnabled())
			{
				LOG.info("DefaultOrderInvoiceSerialNumberService: Invoice Serial Number generator config is disabled");
				success = false;
				return Optional.empty();
			}
			getModelService().refresh(configModel);
			int serialNumber = configModel.getNumber();
			serialNumber += configModel.getIncrementValue();

			configModel.setNumber(serialNumber);
			getModelService().save(configModel);
			getModelService().refresh(configModel);

			final String invoiceFormat = StringUtils.repeat(
					Character.isWhitespace(configModel.getRepeatedChar()) ? '0' : configModel.getRepeatedChar(),
					configModel.getNumberOfDigits());
			final String invoiceSerialNumber = invoiceFormat + serialNumber;

			final String invoiceNumber = invoiceSerialNumber.substring(String.valueOf(serialNumber).length());
			final String prefix = StringUtils.isBlank(configModel.getPrefix()) ? "" : configModel.getPrefix();
			final String suffix = StringUtils.isBlank(configModel.getSuffix()) ? "" : configModel.getSuffix();

			return Optional.of(prefix + invoiceNumber + suffix);
		}
		catch (final Exception e)
		{
			LOG.error("Exception During Generating Serial Number: {}", e.getMessage());
			success = false;
			return Optional.empty();
		}
		finally
		{
			if (success)
			{
				tx.commit();
			}
			else
			{
				tx.rollback();
			}
		}
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

}
