/**
 *
 */
package com.sacc.core.strategies.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.core.service.AbstractSerialNumberConfigurationService;
import com.sacc.core.strategies.SerialNumberConfigurationStrategy;


/**
 * @author monzer
 *
 */
public class AbstractOrderSerialNumberConfigurationStrategy implements SerialNumberConfigurationStrategy
{

	@Resource(name = "abstractOrderSerialNumberService")
	private AbstractSerialNumberConfigurationService abstractOrderSerialNumberService;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore()
	{
		return abstractOrderSerialNumberService.generateSerialNumberByCurrentStore();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		return abstractOrderSerialNumberService.generateSerialNumberForBaseStore(baseStore);
	}

}
