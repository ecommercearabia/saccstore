/**
 *
 */
package com.sacc.saccorderconfirmation.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.sacc.saccorderconfirmation.dao.OrderEmailDao;
import com.sacc.saccorderconfirmation.model.OrderConfirmationEmailModel;
import com.sacc.saccorderconfirmation.service.OrderEmailService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderEmailService implements OrderEmailService
{
	@Resource(name = "orderEmailDao")
	private OrderEmailDao orderEmailDao;

	public OrderEmailDao getOrderEmailDao()
	{
		return orderEmailDao;
	}

	@Override
	public List<OrderConfirmationEmailModel> findByStoreUid(final String storeUid)
	{
		ServicesUtil.validateParameterNotNull(storeUid, "storeUid must not be null");

		return getOrderEmailDao().findByStoreUid(storeUid);
	}
}
