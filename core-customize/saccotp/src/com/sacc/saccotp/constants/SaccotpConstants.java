/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccotp.constants;

/**
 * Global class for all Saccotp constants. You can add global constants for your extension into this class.
 */
public final class SaccotpConstants extends GeneratedSaccotpConstants
{
	public static final String EXTENSIONNAME = "saccotp";

	private SaccotpConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccotpPlatformLogo";
}
