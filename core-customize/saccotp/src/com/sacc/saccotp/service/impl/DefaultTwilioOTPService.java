package com.sacc.saccotp.service.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccotp.context.OTPProviderContext;
import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccotp.exception.enums.OTPExceptionType;
import com.sacc.saccotp.model.OTPProviderModel;
import com.sacc.saccotp.model.TwilioOTPProviderModel;
import com.sacc.saccotp.service.OTPService;
import com.sacc.saccotp.twilio.service.TwilioService;


/**
 * @author mnasro
 *
 *         The Class DefaultTwilioOTPService.
 */
public class DefaultTwilioOTPService implements OTPService
{

	/** The twilio service. */
	@Resource(name = "twilioService")
	private TwilioService twilioService;

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	/**
	 * Send OTP code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean sendOTPCode(final String countryCode, final String mobileNumber) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if(!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final TwilioOTPProviderModel twilioOTPProviderModel =(TwilioOTPProviderModel)provider.get();

		return getTwilioService().sendOTPCode(twilioOTPProviderModel.getAuthToken(), twilioOTPProviderModel.getApiKey(), twilioOTPProviderModel.getAccountSid(), countryCode, mobileNumber);
	}

	/**
	 * Verify code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryCode, final String mobileNumber, final String code) throws OTPException
	{
		final Optional<OTPProviderModel> provider = getOtpProviderContext().getProvider(TwilioOTPProviderModel.class);
		if (!provider.isPresent())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, "provider not found");
		}
		final TwilioOTPProviderModel twilioOTPProviderModel = (TwilioOTPProviderModel) provider.get();

		return getTwilioService().verifyCode(twilioOTPProviderModel.getAuthToken(), twilioOTPProviderModel.getApiKey(),
				twilioOTPProviderModel.getAccountSid(), countryCode, mobileNumber, code);
	}


	/**
	 * Gets the twilio service.
	 *
	 * @return the twilio service
	 */
	protected TwilioService getTwilioService()
	{
		return twilioService;
	}

	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

}
