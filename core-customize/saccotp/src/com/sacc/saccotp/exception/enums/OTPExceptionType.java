package com.sacc.saccotp.exception.enums;

public enum OTPExceptionType
{
	DISABLED, SERVICE_UNAVAILABLE, CMS_SITE_NOT_FOUND, OTP_CONFIG_UNAVAILABLE, OTP_TYPE_NOTE_FOUND;
}
