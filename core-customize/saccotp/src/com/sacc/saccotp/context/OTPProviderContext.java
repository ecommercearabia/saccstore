package com.sacc.saccotp.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.sacc.saccotp.model.OTPProviderModel;


/**
 * The Interface OTPProviderContext.
 *
 * @author mnasro
 *
 */
public interface OTPProviderContext
{

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	public Optional<OTPProviderModel> getProvider(Class<?> providerClass);


	/**
	 * Gets the provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the provider
	 */
	public Optional<OTPProviderModel> getProvider(CMSSiteModel cmsSiteModel);


	/**
	 * Gets the provider by current site.
	 *
	 * @return the provider by current site
	 */
	public Optional<OTPProviderModel> getProviderByCurrentSite();

}
