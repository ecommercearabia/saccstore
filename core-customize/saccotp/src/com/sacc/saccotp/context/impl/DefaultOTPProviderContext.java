package com.sacc.saccotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.sacc.saccotp.context.OTPProviderContext;
import com.sacc.saccotp.model.OTPProviderModel;
import com.sacc.saccotp.model.TwilioOTPProviderModel;
import com.sacc.saccotp.strategy.OTPProviderStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPProviderContext.
 *
 * @author mnasro
 *
 */
public class DefaultOTPProviderContext implements OTPProviderContext
{

	/** The otp provider map. */
	@Resource(name = "otpProviderMap")
	private Map<Class<?>, OTPProviderStrategy> otpProviderMap;

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant CMSSITE_MUSTN_T_BE_NULL. */
	private static final String CMSSITE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant OTP_PROVIDER__MUSTN_T_BE_NULL. */
	private static final String OTP_PROVIDER__MUSTN_T_BE_NULL = "otpProvider mustn't be null";

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Gets the cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	@Override
	public Optional<OTPProviderModel> getProvider(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<OTPProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentSite();
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<OTPProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPProviderStrategy strategy = getOTPProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the OTP provider map.
	 *
	 * @return the OTP provider map
	 */
	protected Map<Class<?>, OTPProviderStrategy> getOTPProviderMap()
	{
		return otpProviderMap;
	}

	/**
	 * Gets the provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the provider
	 */
	@Override
	public Optional<OTPProviderModel> getProvider(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(cmsSiteModel.getRegistrationOTPProvider()),
				OTP_PROVIDER__MUSTN_T_BE_NULL);

		if (StringUtils.isEmpty(cmsSiteModel.getRegistrationOTPProvider())
				|| !"TwilioOTPProvider".equals(cmsSiteModel.getRegistrationOTPProvider()))
		{
			return Optional.empty();
		}

		return getProvider(TwilioOTPProviderModel.class);
	}

	/**
	 * Gets the provider by current site.
	 *
	 * @return the provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getProviderByCurrentSite()
	{
		return getProvider(getCmsSiteService().getCurrentSite());

	}
}
