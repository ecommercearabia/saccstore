package com.sacc.saccotp.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.sacc.saccotp.entity.SessionData;
import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccotp.model.OTPProviderModel;


/**
 * The Interface OTPContext.
 *
 * @author mnasro
 */
public interface OTPContext
{
	public boolean isEnabledByCurrentSite();

	public boolean isEnabled(final CMSSiteModel cmsSiteModel);

	public Optional<SessionData> getSessionData(String sessionKey);

	public void removeSessionData(String sessionKey);

	public void sendOTPCodeByCurrentSiteAndSessionData(final String countryisoCode, final String mobileNumber, SessionData data)
			throws OTPException;

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param otpProviderModel
	 *           the otp provider model
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException;

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param cmsSiteModel
	 *           the cms site model
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final CMSSiteModel cmsSiteModel)
			throws OTPException;

	/**
	 * Send OTP code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @throws OTPException
	 *            the OTP exception
	 */
	public void sendOTPCodeByCurrentSite(final String countryisoCode, final String mobileNumber) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param otpProviderModel
	 *           the otp provider model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final CMSSiteModel cmsSiteModel) throws OTPException;

	/**
	 * Verify code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCodeByCurrentSite(final String countryisoCode, final String mobileNumber, final String code)
			throws OTPException;

}
