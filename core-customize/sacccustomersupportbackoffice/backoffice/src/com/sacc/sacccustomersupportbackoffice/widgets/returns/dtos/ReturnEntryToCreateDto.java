package com.sacc.sacccustomersupportbackoffice.widgets.returns.dtos;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.returns.model.RefundEntryModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.ListModelArray;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ReturnEntryToCreateDto
{
	private RefundEntryModel refundEntry = null;
	private int returnableQuantity;
	private int quantityToReturn;
	private String refundEntryComment;
	private boolean discountApplied;
	private BigDecimal tax;
	private ListModelArray reasonsModel = new ListModelArray(new ArrayList());

	public ReturnEntryToCreateDto(final AbstractOrderEntryModel orderEntry, final int returnableQuantity,
			final List<String> reasons)
	{
		final RefundEntryModel defaultRefundEntry = new RefundEntryModel();
		defaultRefundEntry.setOrderEntry(orderEntry);
		defaultRefundEntry.setAmount(BigDecimal.ZERO);
		this.setRefundEntry(defaultRefundEntry);
		this.setReturnableQuantity(returnableQuantity);
		this.setQuantityToReturn(0);
		this.setReasonsModel(new ListModelArray(reasons));
		this.setDiscountApplied(orderEntry.getDiscountValues() != null && orderEntry.getDiscountValues().size() > 0);
	}

	public RefundEntryModel getRefundEntry()
	{
		return this.refundEntry;
	}

	public void setRefundEntry(final RefundEntryModel refundEntry)
	{
		this.refundEntry = refundEntry;
	}

	public String getRefundEntryComment()
	{
		return this.refundEntryComment;
	}

	public void setRefundEntryComment(final String refundEntryComment)
	{
		this.refundEntryComment = refundEntryComment;
	}

	public ListModelArray getReasonsModel()
	{
		return this.reasonsModel;
	}

	public void setReasonsModel(final ListModelArray reasonsModel)
	{
		this.reasonsModel = reasonsModel;
	}

	public int getReturnableQuantity()
	{
		return this.returnableQuantity;
	}

	public void setReturnableQuantity(final int returnableQuantity)
	{
		this.returnableQuantity = returnableQuantity;
	}

	public int getQuantityToReturn()
	{
		return this.quantityToReturn;
	}

	public void setQuantityToReturn(final int quantityToReturn)
	{
		this.quantityToReturn = quantityToReturn;
	}

	public boolean isDiscountApplied()
	{
		return this.discountApplied;
	}

	public void setDiscountApplied(final boolean discountApplied)
	{
		this.discountApplied = discountApplied;
	}

	public BigDecimal getTax()
	{
		return this.tax;
	}

	public void setTax(final BigDecimal tax)
	{
		this.tax = tax;
	}
}
