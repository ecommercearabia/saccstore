/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.sacccustomersupportbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SacccustomersupportbackofficeConstants extends GeneratedSacccustomersupportbackofficeConstants
{
	public static final String EXTENSIONNAME = "sacccustomersupportbackoffice";

	private SacccustomersupportbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
