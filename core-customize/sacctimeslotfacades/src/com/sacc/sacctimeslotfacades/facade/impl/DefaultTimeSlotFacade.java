/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacctimeslotfacades.facade.impl;

import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.store.services.BaseStoreService;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.sacc.sacctimeslot.enums.TimeFormat;
import com.sacc.sacctimeslot.model.PeriodModel;
import com.sacc.sacctimeslot.model.TimeSlotModel;
import com.sacc.sacctimeslot.model.TimeSlotWeekDayModel;
import com.sacc.sacctimeslot.service.TimeSlotService;
import com.sacc.sacctimeslotfacades.PeriodData;
import com.sacc.sacctimeslotfacades.TimeSlotData;
import com.sacc.sacctimeslotfacades.TimeSlotDayData;
import com.sacc.sacctimeslotfacades.exception.TimeSlotException;
import com.sacc.sacctimeslotfacades.exception.type.TimeSlotExceptionType;
import com.sacc.sacctimeslotfacades.facade.TimeSlotFacade;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotFacade implements TimeSlotFacade
{
	private static final String NO_TIMESLOT_CONFIGURATIONS_MSG = "No timeslot found for the zoneDeliveryModeCode";
	private static final String NO_TIMESLOT_WEEKDAYS_MSG = "No active timeslot weedays found";
	private static final String NO_TIMEZONE_FOUND_MSG = "No timezone found on TimeSlotModel";
	private static final String INVALID_NUMBER_OF_DAYS_MSG = "Attribute numberOfDays mustn't be zero or negative";

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Override
	public Optional<TimeSlotData> getTimeSlotData(final String zoneDeliveryModeCode) throws TimeSlotException
	{
		final Optional<TimeSlotModel> timeSlot = timeSlotService.getActive(zoneDeliveryModeCode);
		validateTimeSlotConfigurations(timeSlot);

		final TimeSlotData timeSlotData = new TimeSlotData();
		final List<TimeSlotDayData> timeSlotDays = new ArrayList<>();

		final String timezone = timeSlot.get().getTimezone();
		final int numberOfDays = timeSlot.get().getNumberOfDays();



		final Map<WeekDay, TimeSlotWeekDayModel> timeSlotWeekDays = getDays(timeSlot.get().getTimeSlotWeekDays());
		ZonedDateTime today = getTimeNow(timezone);
		for (int i = 0; i < numberOfDays; i++)
		{
			final TimeSlotWeekDayModel timeSlotWeekDay = timeSlotWeekDays.get(WeekDay.valueOf(today.getDayOfWeek().toString()));
			if (timeSlotWeekDay != null && timeSlotWeekDay.isActive())
			{
				final TimeSlotDayData dayData = createTimeSlotDayData(timeSlotWeekDay, today);
				timeSlotDays.add(dayData);
			}
			today = today.plusDays(1);
		}
		if (timeSlotDays.isEmpty())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_WEEKDAYS, NO_TIMESLOT_WEEKDAYS_MSG);
		}

		timeSlotData.setTimeSlotDays(timeSlotDays);

		return Optional.ofNullable(timeSlotData);
	}

	private void validateTimeSlotConfigurations(final Optional<TimeSlotModel> timeSlot) throws TimeSlotException
	{
		if (!timeSlot.isPresent())
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_CONFIGURATIONS_AVAILABLE, NO_TIMESLOT_CONFIGURATIONS_MSG);
		}
		if (CollectionUtils.isEmpty(timeSlot.get().getTimeSlotWeekDays()))
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOT_WEEKDAYS, NO_TIMESLOT_WEEKDAYS_MSG);
		}
		if (StringUtils.isEmpty(timeSlot.get().getTimezone()))
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMEZONE_FOUND, NO_TIMEZONE_FOUND_MSG);
		}
		if (timeSlot.get().getNumberOfDays() <= 0)
		{
			throw new TimeSlotException(TimeSlotExceptionType.INVALID_NUMBER_OF_DAYS, INVALID_NUMBER_OF_DAYS_MSG);
		}
	}

	private ZonedDateTime getTimeNow(final String timezone)
	{
		final ZoneId zoneId = ZoneId.of(timezone);
		return ZonedDateTime.ofInstant(Instant.now(), zoneId);
	}

	private DateTimeFormatter getDateTimeFormatter(final TimeFormat format)
	{
		if (TimeFormat.HOUR_12.equals(format))
		{
			return DateTimeFormatter.ofPattern("hh:mm a");
		}
		else
		{
			return DateTimeFormatter.ofPattern("H:mm");
		}
	}

	@Override
	public DateTimeFormatter getDateTimeFormatter(final String zoneDeliveryModeCode)
	{
		final Optional<TimeSlotModel> optional = timeSlotService.getActive(zoneDeliveryModeCode);
		if (optional.isPresent())
		{
			return getDateTimeFormatter(optional.get().getTimeFormat());
		}
		return getDateTimeFormatter(TimeFormat.HOUR_24);
	}

	private Map<WeekDay, TimeSlotWeekDayModel> getDays(final List<TimeSlotWeekDayModel> timeSlotWeekDays)
	{
		return timeSlotWeekDays.stream()
				.collect(Collectors.toMap(TimeSlotWeekDayModel::getDay, timeSlotWeekDay -> timeSlotWeekDay));
	}

	private TimeSlotDayData createTimeSlotDayData(final TimeSlotWeekDayModel timeSlotWeekDay, final ZonedDateTime currentDay)
	{
		final TimeSlotDayData dayData = new TimeSlotDayData();
		final List<PeriodData> periods = new ArrayList<>();

		dayData.setDate(currentDay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		dayData.setDay(timeSlotWeekDay.getName());

		for (final PeriodModel period : timeSlotWeekDay.getPeriods())
		{
			if (period.isActive())
			{
				final PeriodData periodData = createPeriodData(period, dayData.getDate());
				periods.add(periodData);
			}
		}
		dayData.setPeriods(periods);

		return dayData;
	}

	private PeriodData createPeriodData(final PeriodModel period, final String date)
	{
		final DateTimeFormatter formatter = getDateTimeFormatter(period.getTimeSlotWeekDay().getTimeSlot().getTimeFormat());
		final PeriodData periodData = new PeriodData();

		periodData.setCode(period.getCode());

		final LocalTime intervalStart = LocalTime.parse(period.getStart(), getDateTimeFormatter(TimeFormat.HOUR_24));
		final LocalTime intervalEnd = LocalTime.parse(period.getEnd(), getDateTimeFormatter(TimeFormat.HOUR_24));

		periodData.setStart(intervalStart);
		periodData.setEnd(intervalEnd);
		periodData.setIntervalFormattedValue(intervalStart.format(formatter) + " - " + intervalEnd.format(formatter));

		setEnabled(period, periodData, date, period.getCapacity());

		return periodData;
	}

	private void setEnabled(final PeriodModel period, final PeriodData periodData, final String date, final int capacity)
	{
		if (!maxOrderLimitReached(periodData, date, period.getTimeSlotWeekDay().getTimeSlot().getTimezone(), capacity)
				&& isPeriodEnabledNow(periodData, period.getTimeSlotWeekDay().getTimeSlot().getTimezone(), date, period.getExpiry()))
		{
			periodData.setEnabled(true);
		}
	}

	/**
	 * Returns true if the capacity limit is reached per this timeslot.
	 *
	 * @param timezone
	 * @param date
	 */
	private boolean maxOrderLimitReached(final PeriodData periodData, final String date, final String timezone, final int capacity)
	{
		final int numberOfOrdersByTimeSlot = timeSlotService.getNumberOfOrdersByTimeSlot(
				baseStoreService.getCurrentBaseStore(),
				periodData.getStart().format(getDateTimeFormatter(TimeFormat.HOUR_24)),
				periodData.getEnd().format(getDateTimeFormatter(TimeFormat.HOUR_24)), date, timezone);
		return numberOfOrdersByTimeSlot >= capacity;
	}

	private boolean isPeriodEnabledNow(final PeriodData periodData, final String timezone, final String date, final long expiry)
	{
		final ZonedDateTime now = getTimeNow(timezone);

		final ZonedDateTime intervalStart = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				periodData.getStart(), now.getZone());

		return !(now.isAfter(intervalStart.minusSeconds(expiry)));
	}

	@Override
	public boolean isTimeSlotEnabled(final String zoneDeliveryModeCode)
	{
		return timeSlotService.isTimeSlotEnabled(zoneDeliveryModeCode);
	}
}
