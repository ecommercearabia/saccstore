/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacctimeslotfacades.facade;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

import com.sacc.sacctimeslotfacades.TimeSlotData;
import com.sacc.sacctimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface TimeSlotFacade
{
	/**
	 * Retrieves TimeSlotData by the related ZoneDeliveryMode.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlotData, Optional<TimeSlotData>
	 */
	public Optional<TimeSlotData> getTimeSlotData(String zoneDeliveryModeCode) throws TimeSlotException;

	/**
	 * Returns true if the TimeSlot related to the ZoneDeliveryMode code is active.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlotEnabled, boolean
	 */
	public boolean isTimeSlotEnabled(String zoneDeliveryModeCode);

	public DateTimeFormatter getDateTimeFormatter(String zoneDeliveryModeCode);
}
