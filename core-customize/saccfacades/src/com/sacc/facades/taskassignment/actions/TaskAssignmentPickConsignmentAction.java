/**
 *
 */
package com.sacc.facades.taskassignment.actions;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.taskassignment.actions.AbstractTaskAssignmentActions;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TaskAssignmentPickConsignmentAction extends AbstractTaskAssignmentActions
{
	private static final Logger LOGGER;
	protected static final String CONSIGNMENT_ACTION_EVENT_NAME = "ConsignmentActionEvent";
	protected static final String PICK_CONSIGNMENT_CHOICE = "pickConsignment";
	protected static final String PACKING_DECISION = "Packing";
	protected static final String PICKING_TEMPLATE_CODE = "NPR_Picking";

	static
	{
		LOGGER = LoggerFactory.getLogger(TaskAssignmentPickConsignmentAction.class);
	}

	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		final Optional<ItemModel> attachedConsignmentOptional = this.getAttachedConsignment(workflowAction);
		WorkflowDecisionModel result = null;
		if (attachedConsignmentOptional.isPresent())
		{
			final ConsignmentModel attachedConsignment = (ConsignmentModel) attachedConsignmentOptional.get();
			this.getWorkflowActionAndAssignPrincipal(workflowAction, attachedConsignment, "picked");
			this.getConsignmentBusinessProcessService().triggerChoiceEvent(attachedConsignment, CONSIGNMENT_ACTION_EVENT_NAME,
					PICK_CONSIGNMENT_CHOICE);
			final Optional<WorkflowDecisionModel> decisionModel = workflowAction.getDecisions().stream()
					.filter(decision -> PACKING_DECISION.equals(decision.getToActions().iterator().next().getName())).findFirst();
			result = (decisionModel.isPresent() ? decisionModel.get() : null);
		}
		return result;
	}

	protected void getWorkflowActionAndAssignPrincipal(final WorkflowActionModel workflowAction,
			final ConsignmentModel attachedConsignment, final String actionLabel)
	{
		final WorkflowActionModel packingWorkflowAction = this.getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(PICKING_TEMPLATE_CODE, attachedConsignment);
		this.assignNewPrincipalToAction(workflowAction, packingWorkflowAction);
		LOGGER.info("Consignment: {} has been {} by: {}", new Object[]
		{ attachedConsignment.getCode(), actionLabel, packingWorkflowAction.getPrincipalAssigned().getDisplayName() });
	}
}
