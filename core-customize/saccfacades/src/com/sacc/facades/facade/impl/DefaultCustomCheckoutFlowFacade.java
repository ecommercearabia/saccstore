package com.sacc.facades.facade.impl;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;

import com.sacc.facades.facade.CustomAcceleratorCheckoutFacade;
import com.sacc.facades.facade.CustomCheckoutFlowFacade;
import com.sacc.saccorder.cart.exception.CartValidationException;
import com.sacc.saccorder.cart.service.CartValidationService;
import com.sacc.sacctimeslotfacades.facade.TimeSlotFacade;


/**
 * @author mnasro
 *
 *         The Class DefaultCustomCheckoutFlowFacade.
 */
public class DefaultCustomCheckoutFlowFacade implements CustomCheckoutFlowFacade
{

	/** The checkout flow facade. */
	@Resource(name = "checkoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;


	@Override
	public boolean hasValidCart()
	{
		try
		{
			return checkoutFlowFacade.hasValidCart() && cartService.hasSessionCart()
					&& cartValidationService.validateCartMaxAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartMinAmount(cartService.getSessionCart());
		}
		catch (final CartValidationException e)
		{
			return false;
		}
	}

	/**
	 * Gets the checkout flow facade.
	 *
	 * @return the checkoutFlowFacade
	 */
	public de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade getCheckoutFlowFacade()
	{
		return checkoutFlowFacade;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	@Override
	public boolean hasNoPaymentMode()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || cartData.getPaymentMode() == null;
	}

	@Override
	public boolean hasPaymentProvider()
	{
		if (!getCheckoutFacade().getCheckoutCart().getPaymentMode().isPaymentProviderNeeded())
		{
			return false;
		}

		return getCheckoutFacade().getSupportedPaymentProvider().isPresent();
	}

	/**
	 * @return the checkoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Override
	public boolean hasNoTimeSlot()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return timeSlotFacade.isTimeSlotEnabled(cartData.getDeliveryMode().getCode()) && cartData.getTimeSlotInfoData() == null;
	}

}
