/**
 *
 */
package com.sacc.facades.facade.impl;

import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.storesession.impl.DefaultStoreSessionFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomStoreSessionFacade extends DefaultStoreSessionFacade
{

	@Resource(name = "userService")
	private UserService userService;

	@Override
	protected LanguageData findBestLanguage(final Collection<LanguageData> availableLanguages, final List<Locale> preferredLocales)
	{

		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		if (currentBaseStore == null || currentBaseStore.getDefaultLanguage() == null
				|| !currentBaseStore.isEnableForceDefaultLanguage())
		{
			return super.findBestLanguage(availableLanguages, preferredLocales);
		}

		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser == null || currentUser.getSessionLanguage() == null || getUserService().isAnonymousUser(currentUser))
		{
			return getLanguageConverter().convert(currentBaseStore.getDefaultLanguage());
		}

		return getLanguageConverter().convert(currentUser.getSessionLanguage());
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}
}
