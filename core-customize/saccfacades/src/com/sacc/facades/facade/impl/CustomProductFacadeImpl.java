/**
 *
 */
package com.sacc.facades.facade.impl;

import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customerreview.model.CustomerReviewModel;

import java.util.List;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomProductFacadeImpl<REF_TARGET> extends DefaultProductFacade<REF_TARGET>
{
	@Override
	public List<ReviewData> getReviews(final String productCode, final Integer numberOfReviews)
	{
		final ProductModel product = getProductService().getProductForCode(productCode);
		final List<CustomerReviewModel> reviews = getCustomerReviewService().getReviewsForProduct(product);

		if (numberOfReviews == null)
		{
			return getCustomerReviewConverter().convertAll(reviews);
		}
		else if (numberOfReviews.intValue() < 0)
		{
			throw new IllegalArgumentException();
		}
		else
		{
			return getCustomerReviewConverter().convertAll(reviews.subList(0, Math.min(numberOfReviews.intValue(), reviews.size())));
		}
	}
}
