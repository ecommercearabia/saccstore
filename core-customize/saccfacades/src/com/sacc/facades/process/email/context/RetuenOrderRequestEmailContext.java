/**
 *
 */
package com.sacc.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.Ordermanagement.model.ReturnOrderRequestProcessModel;


/**
 * @author monzer
 *
 */
public class RetuenOrderRequestEmailContext extends AbstractEmailContext<ReturnOrderRequestProcessModel>
{

	private ReturnRequestModel returnRequest;
	private MediaModel barcode;
	private MediaModel qrcode;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	private OrderData orderData;

	private ConsignmentModel consignment;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	private String vatId;

	private String displayName;

	private MediaModel icon;

	@Override
	protected BaseSiteModel getSite(final ReturnOrderRequestProcessModel returnProcessModel)
	{
		return returnProcessModel.getReturnRequest().getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final ReturnOrderRequestProcessModel returnProcessModel)
	{
		return (CustomerModel) returnProcessModel.getReturnRequest().getOrder().getUser();
	}

	@Override
	protected LanguageModel getEmailLanguage(final ReturnOrderRequestProcessModel returnProcessModel)
	{
		return returnProcessModel.getReturnRequest().getOrder().getLanguage();
	}

	@Override
	public void init(final ReturnOrderRequestProcessModel returnProcessModel, final EmailPageModel abstractPageModel)
	{
		super.init(returnProcessModel, abstractPageModel);
		this.returnRequest = returnProcessModel.getReturnRequest();
		final OrderModel order = returnRequest.getOrder();

		Optional<ConsignmentModel> consignment = Optional.empty();
		if (order != null)
		{
			consignment = order.getConsignments().stream().filter(ConsignmentModel::isShipped).findFirst();
			orderData = orderConverter.convert(order);
			vatId = order.getStore().getVatNumber();
			icon = order.getStore().getSaccIcon();

		}
		if (consignment.isPresent())
		{
			this.consignment = consignment.get();
			barcode = consignment.get().getBarcode();
			qrcode = consignment.get().getQrcode();
		}
		final CustomerModel customer = getCustomer(returnProcessModel);
		if (Objects.nonNull(customer))
		{
			displayName = customer.getTitle().getName() + " " + customer.getDisplayName();
		}
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *           the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}

	/**
	 * @return the returnRequest
	 */
	public ReturnRequestModel getReturnRequest()
	{
		return returnRequest;
	}

	/**
	 * @param returnRequest
	 *           the returnRequest to set
	 */
	public void setReturnRequest(final ReturnRequestModel returnRequest)
	{
		this.returnRequest = returnRequest;
	}



	/**
	 * @return the qrcode
	 */
	public MediaModel getQrcode()
	{
		return qrcode;
	}

	/**
	 * @param qrcode
	 *           the qrcode to set
	 */
	public void setQrcode(final MediaModel qrcode)
	{
		this.qrcode = qrcode;
	}

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}

	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}

	/**
	 * @return the consignment
	 */
	public ConsignmentModel getConsignment()
	{
		return consignment;
	}

	/**
	 * @param consignment
	 *           the consignment to set
	 */
	public void setConsignment(final ConsignmentModel consignment)
	{
		this.consignment = consignment;
	}

	/**
	 * @return the vatId
	 */
	public String getVatId()
	{
		return vatId;
	}

	/**
	 * @param vatId
	 *           the vatId to set
	 */
	public void setVatId(final String vatId)
	{
		this.vatId = vatId;
	}

	/**
	 * @return the displayName
	 */
	@Override
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the icon
	 */
	public MediaModel getIcon()
	{
		return icon;
	}

	/**
	 * @param icon
	 *           the icon to set
	 */
	public void setIcon(final MediaModel icon)
	{
		this.icon = icon;
	}

}
