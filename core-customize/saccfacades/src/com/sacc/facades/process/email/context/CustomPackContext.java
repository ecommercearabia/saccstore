/**
 *
 */
package com.sacc.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehousing.labels.context.PackContext;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;

import com.sacc.core.service.BarcodeGenaratorService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomPackContext extends PackContext
{
	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	private OrderData orderData;
	private static final Logger LOG = Logger.getLogger(CustomPackContext.class);

	private MediaModel barcode;
	private MediaModel qrcode;

	private String vatId;

	private MediaModel icon;

	private DateTool dateTool = new DateTool();

	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());

		if (getConsignment().getBarcode() == null)
		{
			try
			{
				barcode = barcodeGenaratorService.generateBarcodeAsMedia(businessProcessModel.getConsignment());

			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}

		if (getConsignment().getQrcode() == null)
		{
			try
			{
				barcodeGenaratorService.generateQRCodeAsMedia(businessProcessModel.getConsignment());
			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}

		barcode = getConsignment().getBarcode();
		qrcode = getConsignment().getQrcode();
		vatId = getConsignment().getOrder().getStore().getVatNumber();
		icon = getConsignment().getOrder().getStore().getSaccIcon();
	}

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}


	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *           the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}

	/**
	 * @return the qrcode
	 */
	public MediaModel getQrcode()
	{
		return qrcode;
	}

	/**
	 * @param qrcode
	 *           the qrcode to set
	 */
	public void setQrcode(final MediaModel qrcode)
	{
		this.qrcode = qrcode;
	}

	/**
	 * @return the vatId
	 */
	public String getVatId()
	{
		return vatId;
	}

	/**
	 * @param vatId
	 *           the vatId to set
	 */
	public void setVatId(final String vatId)
	{
		this.vatId = vatId;
	}

	/**
	 * @param icon
	 *           the icon to set
	 */
	public void setIcon(final MediaModel icon)
	{
		this.icon = icon;
	}

	/**
	 * @return the icon
	 */
	public MediaModel getIcon()
	{
		return icon;
	}

	/**
	 * @return the dateTool
	 */
	public DateTool getDateTool()
	{
		return dateTool;
	}

	/**
	 * @param dateTool
	 *           the dateTool to set
	 */
	public void setDateTool(final DateTool dateTool)
	{
		this.dateTool = dateTool;
	}
}
