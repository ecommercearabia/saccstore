/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sacc.saccwarehousingcustomwebservices;


/**
 * Simple test class to demonstrate how to include utility classes to your webmodule.
 */
public class SaccwarehousingcustomwebservicesWebHelper
{
	private SaccwarehousingcustomwebservicesWebHelper()
	{
	}

	public static final String getTestOutput()
	{
		return "testoutput";
	}
}
